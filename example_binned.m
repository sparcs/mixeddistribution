%In this example we show how to use interval censoring 
%in case you have binned data for a continuous distribution

%create a mixture of 2 normal distibutions with clearly different mean and
%standard deviation:
pd=MixedDistribution({'Normal','Normal'},'mu',{1,3},'sigma',{2,0.2});
%generate data set using this mixture of two distributions
data=pd.random(1000,1);
figure
plotdist(pd,'data' ,data)
pd

%fit pd1 using the original generated data based on pd:
pd1=fitmixdist(data,{'Normal','Normal'});
figure
plotdist(pd1);
pd1

%create binned data (20 bins) based on the same data as above
%freq are the counts of 20 bins:
[freq,edges]=histcounts(data,20);
%mindata is the lower edge
mindata=edges(1:end-1);
%maxdata is the higher edge;
maxdata=edges(2:end);

%fit pd2 based on the binned data, using interval censoring:
pd2=fitmixdist(mindata,{'Normal','Normal'},'censoring',maxdata,'frequency',freq);
pd2
figure
plotdist(pd2) %plotting the binned data 
%the parameters of pd2 and pd1 should be almost the same, but there is some
%bias due to binning