function [H, p, KSstat, critval] = my_kstest(this, data1, varargin)
    %Kolmogorov-Smirnov test, by default with the InputData
    %this can be a MixedDistribution, a SingleDistr or any other probability object
    %if one output H=KSstat (+faster computation)
    if nargin == 1
        [data1, cens1] = shared.unfoldfreq(this.InputData);
        if ~(isempty(cens1) || all(cens1 == 0))
            if islogical(cens1)
                %right censored written in generic way
                cens = data1;
                cens(cens1) = inf;
                cens1 = cens;
            end
            data1 = [data1, cens1];
        end
    end
    validoptions = {'alpha', 'tail', 'data' 'cens', 'freq', 'censoring', 'frequency'};
    opts = reshape(varargin, 2, length(varargin) / 2);
    if ~isempty(opts)
        opts(1, :) = lower(opts(1, :));
    end
    if any(~ismember(opts(1, :), validoptions))
        error('my_ecdf:optionsnotvalid', 'Options invalid.\nValid options are: %s\n', sprintf('''%s'' ', validoptions{:}));
    end
    ndx = strcmp(opts(1, :), 'data');
    if any(ndx)
        data1 = opts{2, ndx};
        opts = opts(:, ~ndx); %not allowed for ecdf
    end

    ndx = strncmp(opts(1, :), 'cens', 4);
    if any(ndx)
        cens = opts{2, ndx};
    else
        cens = [];
    end
    ndx = strncmp(opts(1, :), 'freq', 4);
    if any(ndx)
        freq = opts{2, ndx};
    else
        freq = [];
    end
    ndx = strcmp(opts(1, :), 'alpha');
    if any(ndx)
        alpha = opts{2, ndx};
    else
        alpha = 0.05;
    end
    ndx = strcmp(opts(1, :), 'tail');
    if any(ndx)
        tail = opts{2, ndx};
    else
        tail = 0;
    end
    if ~isempty(cens)
        if islogical(cens)
            data2 = data1;
            data2(cens) = Inf;
            cens = data2;
        end
        data1 = [data1, cens];
    end
    if all(freq == 1)
        freq = [];
    end
    CDF = [data1(:, 1), this.cdf(data1(:, 1))];
    if nargout > 3
        %(x, alpha,CDF,tail,freq)
        [H, p, KSstat, critval] = shared.fast_kstest(data1, alpha, CDF, tail, freq);
    elseif nargout == 1
        %H is now the KS statistic
        H = shared.fast_kstest(data1, alpha, CDF, tail, freq);
    elseif nargout > 1
        [H, p, KSstat] = shared.fast_kstest(data1, alpha, CDF, tail, freq);
    else
        [H1, p, KSstat, critval] = shared.fast_kstest(data1, alpha, CDF, tail, freq);
       % H1 = false;
       % p = 0;
        %        fprintf('H = %g\np=%g\nKS=%g\ncritval=%g\n', H1, p, KSstat, critval);
        [sampleCDF, x] = shared.my_ecdf(data1, varargin{:});
        if size(sampleCDF, 2) == 2
            sampleCDF = sampleCDF(:, 1);
        end
        stairs(x, sampleCDF)
        hold on;
        x = x(2:end);
        nullCDF = this.cdf(x);
        plot(x, nullCDF, 'b-');
        delta1 = sampleCDF(1:end - 1) - nullCDF; % Vertical difference at jumps approaching from the LEFT.
        %delta2 = sampleCDF(2:end) - nullCDF; % Vertical difference at jumps approaching from the RIGHT.
        deltaCDF = abs([ delta1]);
        [KS, ndx] = max(deltaCDF);
 
        if ~H1
            h = '***';
        else
            h = '';
        end
        text(0.05, 0.95, sprintf('KS=%g\np= %g%s', KS, p, h), 'units', 'normalized', 'FontSize', 8, 'tag', 'small-text', 'VerticalAlignment', 'top');
        % if ndx > length(delta2)
        %     ndx = ndx - length(delta2);
        plot([x(ndx), x(ndx)], [sampleCDF(ndx), nullCDF(ndx)], 'r*-')
        % else
        %f=find(ndx);
        %    plot([x(ndx), x(ndx)], [sampleCDF(ndx+1), nullCDF(ndx)], 'r*-')
        % end
    end
end

