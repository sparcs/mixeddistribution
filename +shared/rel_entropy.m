function KL = rel_entropy(P, Q)
    %Kullback–Leibler divergence between two distributions P and Q. 
    %A simple interpretation of the divergence of P from Q is the expected excess 
    %surprise from using Q as a model when the actual distribution is P.
    % KL=rel_entropy(P,Q) P and Q are distributions or mixed distributions
    % check if both have the same support range and are both continuous or not
    infoP = shared.basic_info(P);
    infoQ = shared.basic_info(Q);
    if ~isequal(infoP.support, infoQ.support)
        error('Support should be the same for relative entropy')
    end
    if infoP.iscontinuous ~= infoQ.iscontinuous
        error('Both should be continuous or not')
    end
    if infoP.iscontinuous
        fun = @(x)P.pdf(x).*log(P.pdf(x)./Q.pdf(x));
        KL = integral(fun, infoP.support(1), infoP.support(2));
    else
        %can be improved
        ran = infoP.support;
        ran2 = [-3000 3000];
        ran(isinf(ran)) = ran2(isinf(ran));
        x = [ran(1):ran(end)];
        ndx = P.pdf(x) > 1E-20;
        x = x(ndx);
        KL = sum(P.pdf(x) .* log(P.pdf(x) ./ Q.pdf(x)));
    end
end

