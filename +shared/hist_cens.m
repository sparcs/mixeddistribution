function h = hist_cens(data, cens, varargin)
    %'spreadcens',  min/maximum value to which the cens will be spread or logical [1 0]= no
    %spread
    %show censored data in a histogram (can be improved by dividing them to the right tail)
    %the plot is normalized (pdf) to the uncensored data
    options = reshape(varargin, 2, length(varargin) / 2);
%     ndx = strcmpi(options(1, :), 'BinEdges');
%     if any(ndx)
%         binedges=options{2,ndx};
%     else
%        binedges=[];    
%     end
    ndx = strcmp(options(1, :), 'spreadcens');
    if any(ndx)
        spreadcens = options{2, ndx};
        if islogical(spreadcens)
            if spreadcens
                data1 = data;
                if ~isempty(cens)
                    data1(data1 == -inf) = cens(data1 == -inf);
                    spreadcens = [min(data1), max(data)];
                end
            else
                spreadcens = [];
            end
        end

        options = options(:, ~ndx);
        ndx = strcmpi(options(1, :), 'binlimits');
        if ~isempty(spreadcens) && numel(spreadcens) == 1
            spreadcens = [0 spreadcens];
        end
        if ~any(ndx) && ~isempty(spreadcens) && max(data) < spreadcens(2)
            options = [options, {'BinLimits'; spreadcens}];
        end
    else
        spreadcens = [];
    end
 
    if islogical(cens) || all(cens == 1 | cens == 0)
        cens = logical(cens);
        rightcens = true;
    elseif ~isempty(cens)
        mindata = data;
        maxdata = cens;
        data(data == -inf) = maxdata(data == -inf);
        cens = maxdata > mindata;
        rightcens = false;
    end
    if ~isempty(cens) && any(cens)
        %cens = cens > 0;
        [c_pdf, x] = histcounts(data, 'Normalization', 'pdf', options{:});          
        c_1 = histcounts(data, 'Normalization', 'count', options{:});
        ratio = c_pdf ./ c_1;
        ratio = mean(ratio(~isnan(ratio)));
        c_pdf = histcounts(data(~cens), 'Normalization', 'count', 'BinEdges', x);
        area=sum(c_pdf(:) .* diff(x(:)));
        if area>0
        c_pdf = c_pdf ./area;
        end
        c_cens = histcounts(data(cens), 'Normalization', 'count', 'BinEdges', x);
        area=sum(c_cens(:) .* diff(x(:)));
        if area>0
        c_cens = c_cens ./area;
        end
        %c_cens = c_cens .* ratio;
        bincenters = (x(2:end)' +  x(1:end-1)')/2;
        if ~isempty(spreadcens)
            if rightcens
                maxbin = find(x(2:end) > spreadcens(2), 1);
                if isempty(maxbin)
                    maxbin = numel(bincenters);
                end
                for i = maxbin - 1: -1:1
                    if c_cens(i) > 0
                        %  totcens=sum(c_cens);
                        c_cens(i + 1:maxbin) = c_cens(i) / (maxbin - (i + 1) + 1);
                        c_cens(i) = 0;
                        % totcens-sum(c_cens) %used to check the mass balance
                    end
                end
            else
                bins1 = zeros(numel(data), numel(bincenters));
                maxbin = x(2:end);
                minbin = x(1:end - 1);
                for i = 1:numel(data)
                    if cens(i)
                        ndx = (maxbin > mindata(i) & minbin <= maxdata(i));
                        indx = find(ndx);
                        ndx = ndx + 0.0;
                        if ~isempty(indx)
                            ndx(indx(1)) = (maxbin(indx(1)) - mindata(i)) / (maxbin(indx(1)) - minbin(indx(1)));
                        end
                        if numel(indx) > 1
                            ndx(indx(end)) = (maxdata(i) - minbin(indx(end))) / (maxbin(indx(end)) - minbin(indx(end)));
                        end
                        ndx = ndx / sum(ndx);
                        bins1(i, :) = ndx;
                    end
                end
                c_cens = sum(bins1, 1) .* ratio;
            end
        end
        
        %h(2)=bars_edges(x,c_cens'+c_pdf',[1 0.8 0.8]);
        %hold on
        %h(1)=bars_edges(x,c_pdf',[0.8 0.8 1]);
        h = bar(bincenters, [c_pdf', c_cens'], 1, 'stacked');
    else
        [c_pdf, x] = histcounts(data, 'Normalization', 'pdf', options{:});
        bincenters=(x(2:end)' +  x(1:end-1)')/2;
        h = bar(bincenters, c_pdf', 1, 'stacked');
    end
end

function hbar = bars_edges(edges, height, color)
    if nargin < 3
        color = [0.8 0.8 1];
    end
    height(end + 1) = 0;
    x = repmat(edges(:)', 3, 1);
    h = zeros(3, numel(height));
    h(2:3, :) = repmat(height(:)', 2, 1);
    x = x(:);
    h = h(:);
    h = h(1:end - 1);
    x = [x(2:end - 2); edges(end); edges(1)];
    h1 = fill(x, h, color);
    if nargout == 1
        hbar = h1;
    end
end
