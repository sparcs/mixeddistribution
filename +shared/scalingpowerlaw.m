function [t, c, scaling] = scalingpowerlaw(this, opt)
    if nargin < 2
        opt.scaletodata = true;
        opt.binedges = [];
        if ~isempty(this.InputData)
            opt.data = this.InputData.data;
            opt.censoring = this.InputData.cens;
            opt.frequency = this.InputData.freq;
            if ~isempty(opt.censoring) && ~islogical(opt.censoring)
                opt.binedges = [opt.data; max(opt.censoring)];
            end
        else
            opt.data = [];
            opt.censoring = [];
            opt.frequency = [];
        end
    else
        f = {'data', 'censoring', 'frequency', 'binedges'};
        for i = 1:numel(f)
            if ~isfield(opt, f{i})
                opt.(f{i}) = [];
            end
        end
        if ~isfield(opt, 'scaletodata')
            opt.scaletodata = true;
        end
    end
    scaling.Pxmin = 1;
 
    if isempty(this)
        scaling.xmin = 0;
        [t, c] = get_ecdf(opt.data, opt.censoring, opt.frequency, opt);
        return;
    end
    allpar = this.AllParameters;
    fxmin = strcmp({allpar.name}, 'xmin');
    scaling.fixedxmin = false;
    if any(fxmin)
        scaling.xmin = this.get('xmin');
        isfixed = [allpar.isfixed];
        scaling.fixedxmin = isfixed(fxmin);
    else
        scaling.xmin = this.support(1);
    end
 
    if opt.scaletodata && (this.NumComponents == 1 && any(fxmin))
        scaling.xmin = this.get('xmin');
        [scaling.Pxmin, t, c] = get_Pfromdata(scaling.xmin, opt.data, opt.censoring, opt.frequency, opt);
        if ~isempty(this.BootstrapData)
            origdata = this.bootstrap(this.BootstrapData.bootdata);
            nboot = this.BootstrapData.bootdata.nboot;
            scaling.boot.Pxmin = zeros(nboot, 1);
            if ~scaling.fixedxmin
                scaling.boot.xmin = this.BootstrapData.parvalues(:, fxmin);
            else
                scaling.boot.xmin = scaling.xmin * ones(nboot, 1);
            end
            for i = 1:nboot
                scaling.boot.Pxmin(i) = get_Pfromdata(scaling.boot.xmin(i), origdata.bootdata(:, i), origdata.bootcens(:, i), origdata.bootfreq(:, i), opt);
            end
        end
    else
        scaling.Pxmin = 1;
        [t, c] = get_ecdf(opt.data, opt.censoring, opt.frequency, opt);
        if ~isempty(this.BootstrapData)
            nboot = this.BootstrapData.bootdata.nboot;
            scaling.boot.Pxmin = ones(nboot, 1);
            if any(fxmin) && ~scaling.fixedxmin
                scaling.boot.xmin = this.BootstrapData.parvalues(:, fxmin);
            else
                scaling.boot.xmin = scaling.xmin * ones(nboot, 1);
            end
        end
    end

end
function [Pxmin, t, c] = get_Pfromdata(xmin, data, cens, freq, opt)
    Pxmin = 1;
    [t, c, isbinned] = get_ecdf(data, cens, freq, opt);
    f1 = find(t >= xmin, 1);
    if ~isempty(f1)
        if isbinned
            f1 = f1 - 1;
        end
        Pxmin = c(f1);
    end
end
function [t, c, isbinned] = get_ecdf(data, cens, freq, opt)
    isbinned = ~isempty(opt.binedges) || (islogical(cens) && any(cens));
    if isbinned
        [c, t] = shared.my_ecdf(data, 'censoring', cens, 'frequency', freq);
        if size(c,2)==2
            c=c(:,1);
        end
        c = 1 - c;
    else
        n = length(data);
        t = sort(data);
        c = (n: -1:1)' ./ n;
    end
end
