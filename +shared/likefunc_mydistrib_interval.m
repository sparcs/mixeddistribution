function [nll, acov] = likefunc_mydistrib_interval(pdffun, cdffun,truncdata, params, mindata, maxdata, freq) % likelihood function
    %interval censoring
    % n = length(x);
    if nargin < 6
        maxdata = [];
    end
    if nargin < 7
        freq = [];
    end
    if isempty(freq)
        freq = ones(size(mindata));
    end
    if isempty(maxdata)
        maxdata = mindata;
    end
    %saves one inthazard 
    %             cdf=cdffun(data,cpar{:});
    %             pdf=(1-cdf).*hazfun(data,cpar{:});
    cens = maxdata > mindata;
    cpar = num2cell(params);
    nll = 0;
    if any(cens) % compute log survivor function for censoring points
        cdfdiff = cdffun(maxdata(cens), cpar{:}) - cdffun(mindata(cens), cpar{:});
        cdfdiff(cdfdiff < 1E-250) = 1E-250; % log(0)=Inf otherwise
        nll = nll - sum(freq(cens) .* log(cdfdiff));
    end

    if any(~cens) % compute log pdf for observed data
        nll = nll - sum(freq(~cens) .* log(pdffun(mindata(~cens), cpar{:}) + 1E-300));
    end
    if ~isempty(truncdata)
        maxtrunc = truncdata(:, 2);
        mintrunc = truncdata(:, 1);
        ndx = ~isnan(mintrunc) & maxtrunc > mintrunc;
        cdfdiff = cdffun(maxtrunc(ndx), cpar{:}) - cdffun(mintrunc(ndx), cpar{:});
        cdfdiff(cdfdiff < 1E-250) = 1E-250; % log(0)=Inf otherwise
        nll = nll + sum(freq(ndx) .* log(cdfdiff));
    end
 %  fprintf('%g    %g\n',params,    nll);
    %             if isnan(nll)
    %                 nll=1E40;
    %             end
    acov = nan * eye(numel(params));
end
