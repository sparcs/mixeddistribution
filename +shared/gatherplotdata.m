function [opt, res] = gatherplotdata(this, varargin)
    if ~isempty(this) && ~isa(this, 'MixedDistribution')
        %a SingleDistr, gmdistribution or any basic ParametricDistribution can be translated to MixedDistribution
        this = MixedDistribution(this);
    end
    
    defaultopts = struct('aic', false, 'bootstrap', [], 'censoring', [], 'data', [], 'frequency', [], 'binedges', [],  ...
        'kernel', [], 'movie', [], 'mp4file', [], 'negloglik', false, 'numbins', 30, 'pars', {{}}, 'rtext', '', 'title', '',  ...
        'title2', '', 'type', 'pdf','linecolor','r', 'xlim', [], 'ylim', [],'binscale','linear', 'spreadcens', [], 'fastupdate', false, 'scaletodata', false);
    if ~isempty(this) && (this.NumComponents == 1) && any(strcmp(this.Components{1}.d.DistributionName, {'pareto', 'taperedpareto','boundedpareto', 'generalizedpareto'}))
        defaultopts.type = 'powerlaw';
    end

    varargin = cellfun(@(x)shared.iif(iscell(x),{x},x), varargin, 'UniformOutput', false);
    opts = reshape(varargin, 2, length(varargin) / 2);
    opts(1, :) = lower(opts(1, :));
    validoptions = fieldnames(defaultopts);
    if any(~ismember(opts(1, :), validoptions))
        error('MixedDistribution:plotdist:optionsnotvalid', 'Options invalid.\nValid options are: %s\n', sprintf('''%s'' ', validoptions{:}));
    end
    opt = defaultopts;

    %override the previous results with the options supplied by the user
    for i = 1:numel(validoptions)
        ndx = strcmp(opts(1, :), validoptions{i});
        if any(ndx)
            opt.(validoptions{i}) = opts{2, ndx};
        end
    end

    res.xmin = [];
    if strcmp(opt.type, 'powerlaw') && ~isempty(this) && (this.NumComponents == 1)
        if isprop(this.Components{1}.d, 'xmin')
            res.xmin = this.Components{1}.d.xmin;
        end
    end
 
    switch opt.type
        case 'pdf'
            res.y_label = 'Density';
        case {'survivor', 'survival'}
            res.y_label = 'Survival';
        case 'hazard'
            res.y_label = 'Hazard function';
        case 'cumhazard'
            res.y_label = 'Cumulative hazard function';
        case 'cdf'
            res.y_label = 'Cumulative distribution';
        case 'powerlaw'
            res.y_label = 'Pr(X \geq x)';
        otherwise
            error('MixedDistribution:plot:unknown', 'Unknown type: %s', opt.type);
    end
   
    if isempty(opt.data)
        opt.data = this.InputData;
    end
    if isstruct(opt.data)
        data = opt.data.data;
        opt.censoring = opt.data.cens;
        opt.frequency = opt.data.freq;
        opt.data = data;
    end
    
    if size(opt.censoring, 2) > size(opt.censoring, 1)
        opt.censoring = opt.censoring.';
    end
    if size(opt.frequency, 2) > size(opt.frequency, 1)
        opt.frequency = opt.frequency.';
    end
    if size(opt.data, 2) > size(opt.data, 1)
        opt.data = opt.data.';
    end
 
    if size(opt.data, 2) == 2
        opt.censoring = opt.data(:, 2);
        opt.data = opt.data(:, 1);
    end
    if isempty(opt.censoring)
        opt.censoring = false(size(opt.data));
    end
    if ~islogical(opt.censoring) && all(ismember(opt.censoring, [1 0]))
        opt.censoring = logical(opt.censoring);
    end

    res.intervalcens = ~islogical(opt.censoring);
    if ~res.intervalcens || ~strcmp(opt.type, 'powerlaw')
        [opt.data, opt.censoring, opt.frequency] = shared.unfoldfreq(opt.data, opt.censoring, opt.frequency);
    end
    if res.intervalcens && isempty(opt.binedges)
        lowval = unique(opt.data);
        highval = unique(opt.censoring);
        if isscalar(setdiff(lowval, highval))
            opt.binedges = union(lowval, highval);
        else
            opt.binedges = [];
        end
   % else
      %  opt.binedges = [];
    end
    
    if isempty(opt.bootstrap)
        opt.bootstrap = ~isempty(this) && ~isempty(this.BootstrapData);
    end

    if ~isempty(opt.pars)
        if ischar(opt.pars)
            opt.pars = str2num(opt.pars); %#ok<ST2NM> 
        end
        if iscell(opt.pars)
            opt.pars = [opt.pars{:}];
        end
        if ~isempty(opt.pars)
            this.ParameterValues = opt.pars;
            disp(this);
        end
    end
    if isempty(opt.title)
        opt.title = '';
        if ~isempty(this)
            for i = 1:this.NumComponents
                if strcmp(this.Components{i}.DistributionName, 'FlexHazard')
                    opt.title = [opt.title this.Components{i}.d.distname '+'];
                else
                    opt.title = [opt.title this.Components{i}.DistributionName '+'];
                end
                opt.title(opt.title == '_') = '-';
            end
            opt.title = opt.title(1:end - 1);
        end
    end
    
    if ~isempty(opt.movie)
        parvalues = opt.movie;
        kmax = 0;
        if ~isstruct(parvalues)
            parvalues = struct('details', parvalues);
            %you can also enter the struct from fit.
        elseif isfield(parvalues, 'll')
            [~, kmax] = min([parvalues.ll]);
        end
        nframes = arrayfun(@(x)size(x.details,1), parvalues);
        ndx = strcmpi(opts(1, :), 'title');
        opts = opts(:, ~ndx);
        hax = gca;
        hax.NextPlot = 'replaceChildren';
        ndx = strcmpi(opts(1, :), 'movie');
        opts = opts(:, ~ndx);
        this1 = this;
        if ~isfield(opt, 'cens')
            opt.cens = [];
        end
        M(sum(nframes) + 1) = struct('cdata', [], 'colormap', []);
        k1 = 1;
        for k = 1:numel(nframes)
            if numel(nframes) > 1
                titl = sprintf('Iteration %d/%d', k, numel(nframes));
                if k == kmax
                    titl = [titl ' (best model)']; %#ok<AGROW> 
                end
            else
                titl = opt.title;
            end
            for j = 1:nframes(k)
                this1.ParameterValues = parvalues(k).details(j, :);
                this1.negloglik = this1.likefunc(opt.data, opt.cens);
                plotdist(this1, opts{:}, 'title', titl, 'fastupdate', j > 1)
                drawnow
                M(k1) = getframe(gcf);
                k1 = k1 + 1;
            end
        end
        plotdist(this, opts{:}, 'title', opt.title)
        drawnow
        M(k1) = getframe(gcf);
        res.handles.frames = M;
        if ~isempty(opt.mp4file)
            res.handles.mp4file = opt.mp4file;
            plotdist(res.handles); %save to mp4 file
        end
        return;
    end

    if ~isempty(opt.xlim)
        res.range = opt.xlim;
        if ~isempty(opt.data)
            ndx = opt.data > res.range(1) & opt.data < res.range(2);
            opt.data = opt.data(ndx);
            if ~isempty(opt.censoring)
                opt.censoring = opt.censoring(ndx);
            end
        end
    else
        if ~isempty(opt.data)
            res.range = [min(opt.data) max(opt.data)];
        else
            try
                res.range = this.getrange(1E-3);
            catch
                res.range = [0 1000];
            end
        end
    end
    %hold off
    if isempty(this)
        res.info.iscontinuous = true;
    else
        res.info = this.getInfo;
    end
    if ~res.info.iscontinuous
        res.t = (res.range(1):res.range(2) + 0.5);
    else
        res.t = linspace(res.range(1), res.range(2), 500);
    end
 
    if strcmp(opt.type, 'powerlaw')
        if ~isempty(res.xmin) && ~opt.scaletodata
            %truncated data >=xmin if you do not scale to the data
            ndx1 = opt.data >= res.xmin;
            if any(~ndx1)
                disp('truncated data to x>xmin');
                if ~isempty(opt.censoring)
                    opt.censoring = opt.censoring(ndx1);
                end
                if ~isempty(opt.frequency)
                    opt.frequency = opt.frequency(ndx1);
                end
                opt.data = opt.data(ndx1);
                if ~isempty(opt.binedges)
                    ndx = opt.binedges >= res.xmin;
                    opt.binedges = opt.binedges(ndx);
                end
            end
        end
        if ~isempty(opt.data)
            [res.t, res.c, res.scaling] = shared.scalingpowerlaw(this, opt);
        else
            res.scaling=struct('xmin',0,'Pxmin',1);
        end
    end
    if isempty(this)
        res.overallfun = [];
        res.compsfun = [];
    else
        switch opt.type
            case 'pdf'
                [res.overallfun, res.compsfun] = this.pdf(res.t);
            case {'survivor', 'survival'}
                [res.overallfun, res.compsfun] = this.survival(res.t);
            case 'hazard'
                [res.overallfun, res.compsfun] = this.hazard(res.t);
            case 'cumhazard'
                [res.overallfun, res.compsfun] = this.cumhazard(res.t);
            case 'cdf'
                [res.overallfun, res.compsfun] = this.cdf(res.t);
            case 'powerlaw'
                [res.overallfun, res.compsfun] = this.survival(res.t(:)');
                ndx = res.t < res.scaling.xmin;
                res.overallfun(ndx) = nan;
                res.overallfun = res.overallfun .* res.scaling.Pxmin;
                res.compsfun(ndx) = nan;
                res.compsfun = res.compsfun .* res.scaling.Pxmin;
            otherwise
                error('MixedDistribution:plot:unknown', 'Unknown type: %s', opt.type);
        end
        if opt.bootstrap
            res.bootdata = this.bootCL(opt.type, 'x', res.t, 'ignorenan', true);
            if strcmp(opt.type, 'powerlaw')
                boot_res = res.bootdata.allvalues;
                for i = 1:size(boot_res, 1)
                    boot_res(i, :) = boot_res(i, :) .* res.scaling.boot.Pxmin(i);
                    ndx = res.t < res.scaling.boot.xmin(i);
                    boot_res(i, ndx) = nan;
                end
                res.bootdata.allvalues = boot_res;
                res.bootdata.CL = quantile(boot_res, res.bootdata.quantiles);
            end
        else
            res.bootdata = [];
        end
    end
 
 


    if ~islogical(opt.aic)
        res.aic = opt.aic;
        opt.aic = true;
    elseif opt.aic && ~isempty(this)
        res.aic = this.AIC;
    else
        opt.aic = false;
        res.aic = [];
    end

    if ~islogical(opt.negloglik)
        res.negloglik = opt.negloglik;
        opt.negloglik = true;
    elseif opt.negloglik && ~isempty(this)
        res.negloglik = this.negloglik;
    else
        opt.negloglik = false;
        res.negloglik = [];
    end
    if isempty(this)
        res.NumComponents = 1;
    else
        res.NumComponents = this.NumComponents;
    end
end


function [overallfun] = scale2data(overallfun, t, c, xmin, opt)
    for m = 1:numel(xmin)
        Pxmin1 = [];
        f1 = find(t >= xmin(m), 1);
        if ~isempty(f1)
            if ~isempty(opt.binedges)
                f1 = f1 - 1;
            end
            Pxmin1 = c(f1)
        end
        if ~isempty(Pxmin1)
            overallfun(m, overallfun(m, :) == 1) = NaN;
            overallfun(m, :) = overallfun(m, :) .* Pxmin1;
        end
    end
end
