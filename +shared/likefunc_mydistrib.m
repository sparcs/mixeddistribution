function [nll, acov] = likefunc_mydistrib(pdffun, cdffun,truncdata, params, data, cens, freq) % likelihood function
    % n = length(x)
    if nargin < 6
        cens = [];
    end
    if nargin < 7
        freq = [];
    end

    if isempty(freq)
        freq = ones(size(data));
    end
    if isempty(cens)
        cens = false(size(data));
    elseif ~islogical(cens) && ~all(cens == 1 | cens == 0)
        %interval censoring
        [nll, acov] = shared.likefunc_mydistrib_interval(pdffun, cdffun, params, data, cens, freq);
        return;
    else
        cens = logical(cens);
    end
    %saves one inthazard 
    %             cdf=cdffun(data,cpar{:});
    %             pdf=(1-cdf).*hazfun(data,cpar{:});

    cpar = num2cell(params);
    nll = 0;
    if any(cens) % compute log survivor function for censoring points
        cdf = cdffun(data(cens), cpar{:});
        cdf(cdf == 1) = 1 - eps;
        nll = nll - sum(freq(cens) .* log(1 - cdf));
    end
    if any(~cens) % compute log pdf for observed data
        nll = nll - sum(freq(~cens) .* log(pdffun(data(~cens), cpar{:}) + 1E-300));
    end
    if ~isempty(truncdata)
        maxtrunc = truncdata(:, 2);
        mintrunc = truncdata(:, 1);
        ndx = ~isnan(mintrunc) & maxtrunc > mintrunc;
        cdfdiff = cdffun(maxtrunc(ndx), cpar{:}) - cdffun(mintrunc(ndx), cpar{:});
        cdfdiff(cdfdiff < 1E-250) = 1E-250; % log(0)=Inf otherwise
        nll = nll + sum(freq(ndx) .* log(cdfdiff));
    end
    if isinf(nll) || isnan(nll)
        error('MixedDistribution:likefun_nan', 'Likefunc returns nan/inf');
    end
    acov = nan * eye(numel(params));
end
