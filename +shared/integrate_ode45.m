
function res = integrate_ode45(funt, x, odeopt, varargin)
    %*** enter here the integral of the Hazard function (or use this code
    %for numerical approximation
    function dxdt = fun(t, ~, varargin)
        dxdt = funt(t, varargin{:});
        %prob.HumpDistribution.HazardFunc(t, varargin{:});
    end
    siz = size(x);
    x(x < 0) = 0;
    x(isnan(x)) = 0;
    if numel(x) == 1
        if x == 0
            res = 0;
            return;
        else
            x = [0; x; x + 0.1];
            ndx = 2;
        end
    elseif numel(x) == 2
        if any(x == 0)
            xtrx = max(x) + 0.1;
        else
            xtrx = 0;
        end
        [x, ~, ndx] = unique([x(:); xtrx]);
        ix = find(x == xtrx);
        ndx(ndx == ix) = [];
    else
        [x, ~, ndx] = unique(x(:));
        if length(x) == 1
            x = [x; x + 0.1; x + 0.2];
        end
    end
    % deval is simpler but a bit slower
    if isempty(odeopt)
        odeopt = odeset('RelTol', 1e-9, 'AbsTol', 1e-9);
    end
    if x(1) == 0
        [~, res] = ode45(@(t,x)fun(t,x,varargin{:}), x, 0, odeopt);
    else
        [~, res] = ode45(@(t,x)fun(t,x,varargin{:}), [0; x], 0, odeopt);
        res = res(2:end);
    end
    res = reshape(res(ndx), siz(1), siz(2));
end
