%my implemntation of the contains function
function res = strcontains(fullstr, pattern)
    %contains in Matlab 2017 is slower
    if iscell(pattern)&&ischar(fullstr)
        res=logical(size(pattern));
        for i=1:length(pattern)
            res(i)=shared.strcontains(fullstr,pattern{i});
        end
    elseif ischar(fullstr)
        res = ~isempty(strfind(fullstr, pattern)); %#ok<STREMP>
    elseif iscell(fullstr)
        res = ~cellfun('isempty', strfind(fullstr, pattern)); %#ok<STRCL1> is faster for chars/cells 
    elseif isempty(fullstr)
        res = false;
    else
        res = [];
    end
end
