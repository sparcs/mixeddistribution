function [mindata,maxdata,freq,edges]=makebins(data,nbins,logbins)
    if nargin<2
        nbins=20;
    end
    if nargin<3
        logbins=false;
    end
    if logbins
        [freq, edges] = histcounts(log10(data), nbins);
        edges = 10.^edges;
    else
        [freq, edges] = histcounts(data, nbins);
        edges(edges < min(data)) = min(data);
    end
    freq=freq';
    %mindata is the lower edge
    mindata = edges(1:end - 1).';
    %maxdata is the higher edge;
    maxdata = edges(2:end).';
end