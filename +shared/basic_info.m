function info1 = basic_info(distrcode)
    %important information about a distribution 
    %         name
    %         code
    %      support
    %         plim
    %     pinteger
    %
    %the distrcode can either be the code for histdist or the distributionName
    %the information by MATLAB is not accurate: here are corrected
    %data, for the plim and support

    %copied from histdist
    persistent info
    if nargin == 0
        if isempty(info)
            shared.basic_info('-reset');
        end
        info1 = info;
        return;
    end
    if nargin == 1 && ischar(distrcode) && strcmp(distrcode, '-full')
        shared.basic_info('Normal')
        d = makedist('normal');
        info1 = d.getInfo;
        f = fieldnames(info1);
        for i = length(info): -1:1
            try
                d = makedist(info(i).code);
            catch
                d = [];
            end
            if ~isempty(d)
                try
                    info2 = d.getInfo;
                    for j = 1:length(f)
                        if isfield(info2, f{j})
                            info1(i).(f{j}) = info2.(f{j});
                        end
                    end
                catch
                end
            end
        end
        return
    end
    if ~ischar(distrcode)
        if isa(distrcode, 'MixedDistribution')
            info1 = distrcode.getInfo;
            return;
        end
        distrcode = distrcode.DistributionName;
    end
    if strcmp(distrcode, '-reset')
        fitdist('-reset')
        info = [];
        %cleanup the info field
        shared.basic_info('Normal');
        return;
    end
    if any(shared.strcontains(distrcode, {'_fixed_', '['}))
        dist_opt = SingleDist.parse_distrib(distrcode);
 
        info1 = dist_opt.d_info;

        % info2 = shared.basic_info(info1.code);
  
        mask = dist_opt.mask;
        if ~isempty(info1.pnames)
            info1.pnames = info1.pnames(mask);
        end
        if isempty(info1.pinteger)
            info1.pinteger = [];
        else
            info1.pinteger = info1.pinteger(mask);
        end
        %        info1.pdescription = info1.pdescription(mask);
        %        info1.prequired = info1.prequired(mask);
        info1.pmask = mask;
        info1.plim = info1.plim(:, mask);
        return;
    end

    switch(lower(distrcode))
        case 'ev'
            distrcode = 'extremevalue';
        case 'gev'
            distrcode = 'generalizedextremevalue';
        case 'gp'
            distrcode = 'generalizedpareto';
        case 'hn'
            distrcode = 'halfnormal';
        case 'nbin'
            distrcode = 'negativebinomial';
        case 'wbl'
            distrcode = 'weibull';
    end
    %the code is extracted from the name of the class (lowercase):
    %  prob.Distribution<code>
    if isempty(info)
        
    
        %make sure that for user-defined distributions this
        %information is given in the getInfo method!
        %the own distributions that have correct getInfo are not needed to
        %add
        %fields: name, code ,support plim,pnames;
        info = [struct('name', 'Beta', 'code', 'beta', 'support', [0 1], 'plim', [realmin realmin; Inf Inf], 'pnames', 'a,b');  ... 
            struct('name','BoundedPareto','code','boundedpareto','support', [0 Inf], 'plim',[1,1E-30 , 1E-30;Inf,Inf, Inf],'pnames', 'alpha,xmin,xmax'); ...
            struct('name', 'Binomial', 'code', 'binomial', 'support', [0 Inf], 'plim', [1 0; Inf 1], 'pnames', 'N,p');  ...
            struct('name', 'Birnbaum-Saunders', 'code', 'birnbaumsaunders', 'support', [0 Inf], 'plim', [realmin 0; Inf Inf], 'pnames', 'beta,gamma');  ...
            struct('name', 'Burr', 'code', 'burr', 'support', [0 Inf], 'plim', [realmin realmin realmin; Inf Inf Inf], 'pnames', 'alpha,c,k');  ...
            struct('name', 'EpsilonSkewNormal', 'code', 'epsilonskewnormal', 'support', [-Inf Inf], 'plim', [-Inf, realmin, -1 + eps; Inf, Inf, 1 - eps], 'pnames', 'theta,sigma,epsilon');  ...                   
            struct('name', 'Exponential', 'code', 'exponential', 'support', [0 Inf], 'plim', [realmin; Inf], 'pnames', 'mu');  ...
            struct('name', 'Extreme Value', 'code', 'extremevalue', 'support', [-Inf Inf], 'plim', [-Inf 0; Inf Inf], 'pnames', 'mu,sigma');  ...
            struct('name', 'Gamma', 'code', 'gamma', 'support', [0 Inf], 'plim', [realmin 0; Inf Inf], 'pnames', 'a,b');  ...
            struct('name', 'Generalized Extreme Value', 'code', 'generalizedextremevalue', 'support', [-Inf Inf], 'plim', [-Inf 0 -Inf; Inf Inf Inf], 'pnames', 'k,sigma,mu');  ...
            struct('name', 'Generalized Pareto', 'code', 'generalizedpareto', 'support', [-Inf Inf], 'plim', [-Inf 0 -Inf; Inf Inf Inf], 'pnames', 'k,sigma,theta');  ...
            struct('name', 'Half Normal', 'code', 'halfnormal', 'support', [-Inf Inf], 'plim', [-Inf 0; Inf Inf], 'pnames', 'mu,sigma');  ...
            struct('name', 'Inverse Gaussian', 'code', 'inversegaussian', 'support', [0 Inf], 'plim', [realmin realmin; Inf Inf], 'pnames', 'mu,lambda');  ...
            struct('name', 'Laplace', 'code', 'laplace', 'support', [-Inf Inf], 'plim', [-Inf 0; Inf Inf], 'pnames', 'mu,sigma');  ...
            struct('name', 'Logistic', 'code', 'logistic', 'support', [-Inf Inf], 'plim', [-Inf 0; Inf Inf], 'pnames', 'mu,sigma');  ...
            struct('name', 'Log-Logistic', 'code', 'loglogistic', 'support', [0 Inf], 'plim', [-Inf 0; Inf Inf], 'pnames', 'mu,sigma');  ...
            struct('name', 'Lognormal', 'code', 'lognormal', 'support', [0 Inf], 'plim', [-Inf 0; Inf Inf], 'pnames', 'mu,sigma');  ...
            struct('name', 'Loguniform', 'code', 'loguniform', 'support', [0 Inf], 'plim', [realmin realmin; Inf Inf], 'pnames', 'Lower,Upper');  ...
            struct('name', 'Nakagami', 'code', 'nakagami', 'support', [0 Inf], 'plim', [realmin realmin; Inf Inf], 'pnames', 'mu,omega');  ...
            struct('name', 'Negative Binomial', 'code', 'negativebinomial', 'support', [0 Inf], 'plim', [realmin realmin; Inf 1], 'pnames', 'R,P');  ...
            struct('name', 'Normal', 'code', 'normal', 'support', [-Inf Inf], 'plim', [-Inf realmin; Inf Inf], 'pnames', 'mu,sigma');  ...
            struct('name', 'Poisson', 'code', 'poisson', 'support', [0 Inf], 'plim', [0; Inf], 'pnames', 'lambda');  ...
            struct('name', 'Pareto', 'code' , 'pareto', 'support', [0 Inf], 'plim', [1, 1E-30; Inf, Inf], 'pnames', 'alpha,xmin');  ...
            struct('name', 'Rayleigh', 'code', 'rayleigh', 'support', [0 Inf], 'plim', [realmin; Inf], 'pnames', 'B');  ...
            struct('name', 'Rician', 'code', 'rician', 'support', [0 Inf], 'plim', [0 realmin; Inf Inf], 'pnames', 's,sigma');  ...
            struct('name', 'Siler', 'code', 'siler', 'support', [0 Inf], 'plim', [0, 0, 0, 0, 0; Inf, Inf, Inf, Inf, Inf], 'pnames', 'a1,a2,a3,a4,a5')
            struct('name', 'Stable', 'code', 'stable', 'support', [-Inf Inf], 'plim', [realmin -1 realmin -Inf; 2 1 Inf Inf], 'pnames', 'alpha,beta,gam,delta');  ...
            struct('name', 't Location-Scale', 'code', 'tlocationscale', 'support', [-Inf Inf], 'plim', [-Inf realmin realmin; Inf Inf Inf], 'pnames', 'mu,sigma,nu');  ...
            struct('name','TaperedPareto','code','taperedpareto','support', [0 Inf], 'plim',[1,1E-30 , 0;Inf,Inf, Inf],'pnames', 'alpha,xmin,lambda'); ...
            struct('name', 'Uniform', 'code', 'uniform', 'support', [-Inf Inf], 'plim', [-Inf -Inf; Inf Inf], 'pnames', 'Lower,Upper');  ...
            struct('name', 'Weibull', 'code', 'weibull', 'support', [0 Inf], 'plim', [realmin realmin; Inf Inf], 'pnames', 'A,B')];
      
        pnames = regexp({info.pnames}, ',', 'split');
        for i = 1:length(pnames)
            info(i).pnames = pnames{i};
        end
       %struct('name', 'Hill', 'code', 'hill', 'support', [0 Inf], 'plim', prob.HillDistribution.plim, 'pnames','c,h,p');  ...
        % struct('name', 'Siler', 'code', 'siler', 'support', [0 Inf], 'plim', prob.SilerDistribution.plim, 'pnames','a1,a2,a3,a4,a5');  ...
        %struct('name', 'Hump', 'code', 'hump', 'support', [0 Inf], 'plim', prob.HumpDistribution.plim,'pnames','c,h,g,r');  ...
        %the first argument of the binomial (N) should be integer, this is not in the getInfo; 
        codes = {info(:).code};
        ndx = find(ismember(codes, {'pareto', 'taperedpareto'}));
        for i=1:numel(ndx)
        info(ndx(i)).range_pars={'xmin',''};
        end
        info(strcmp(codes, 'boundedpareto')).range_pars={'xmin','xmax'};
        info(strcmp(codes, 'uniform')).range_pars={'Lower','Upper'};
        ndx = strcmp(codes, 'binomial');
        info(ndx).pinteger = [true false];
        %discrete distributions
        ndx = ismember(codes, {'binomial', 'negativebinomial', 'poisson'});
        for i = 1:length(ndx)
            info(i).iscontinuous = ~ndx(i);
        end
        distrs = fitdist;
        ndx = find(~ismember(distrs, codes));
        for j = 1:length(ndx)
            try
                d1 = makedist(distrs{ndx(j)});
                info2 = d1.getInfo;
                if isfield(info2, 'pinteger')
                    pinteger = info2.pinteger;
                else
                    pinteger = [];
                end
                info2 = struct('name', info2.name, 'code', info2.code, 'support',  ...
                    info2.support,'range_pars',[] ,'plim', info2.plim, 'pinteger', pinteger, 'iscontinuous', info2.iscontinuous, 'pnames', {info2.pnames});
                info(end + 1) = info2;
            catch err
                if ~any(strcmp(err.identifier, {'stats:fitdist:NotMakeable', 'MATLAB:noSuchMethodOrField'}))
                    rethrow(err);
                end
            end
        end

    end

    ndx = strcmpi({info(:).name}, distrcode);
    if any(ndx)
        info1 = info(ndx);
    else
        ndx = strcmpi({info(:).code}, distrcode);
        if any(ndx)
            info1 = info(ndx);
        else
            pd = makedist(distrcode);
            info1 = pd.getInfo;
        end
    end

end
