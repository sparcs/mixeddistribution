function data = data_outside_range(range, InputData)
    data = [];
    if numel(range) == 1
        range = [range inf];
    end
    if ~isempty(InputData) && isfield(InputData, 'data')
        if ~isfield(InputData, 'cens')
            InputData.cens = [];
        end
        if ~isfield(InputData, 'freq')
            InputData.freq = [];
        end
        if any(InputData.freq > 1)
            [InputData.data, InputData.cens, InputData.freq] = shared.unfoldfreq(InputData.data, InputData.cens, InputData.freq);
        end
        intervalcens = ~isempty(InputData.cens) && (~islogical(InputData.cens) || all(ismember(InputData.cens, [0 1])));
        if ~isinf(range(1))
            ndx = InputData.data < range(1);
        else
            ndx = true(size(InputData.data));
        end
        if ~isinf(range(2))
            if intervalcens
                ndx = ndx | InputData.cens > range(2);
            else
                ndx = ndx | InputData.data > range(2);
            end
        end
        if ~isempty(InputData.cens) && any(InputData.cens(ndx))
            warning('ignored that some data are censored');
        end
        if ~intervalcens
            data = InputData.data(ndx);
        else
            data = (InputData.data(ndx) + InputData.cens(ndx)) / 2;
        end
    end
end
