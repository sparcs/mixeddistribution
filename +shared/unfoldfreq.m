function [data, cens, freq, truncdata] = unfoldfreq(data, cens, freq, truncdata)
    if nargin<4
        truncdata=[];
    end
    %unfold the frequencies in data and cens
    if nargin==1&&isstruct(data)
        %InputData struct
        cens=data.cens;
        freq=data.freq;
        opts=data.fitoptions;
        opts=reshape(opts,2,numel(opts)/2);
        ndx=strcmpi(opts(1,:),'truncdata');
        if any(ndx)
            truncdata=opts{2,ndx};
        else
            truncdata=[];
        end
        data=data.data;
        
    end
%     if isempty(freq)
%         freq=ones(size(data));
%     end
    if any(freq > 1)
        %we need to unfold frequencies for easier selection
        if isempty(cens)
            cens = false(size(data));
        end
        cens1 = logical([]);
        data1 = [];
        truncdata1 =[];
        for i = 1:length(freq)
            %maybe a bit slow, but runs only once
            data1 = [data1; repmat(data(i), freq(i), 1)]; %#ok<AGROW>
            if ~isempty(cens)
                cens1 = [cens1; repmat(cens(i), freq(i), 1)]; %#ok<AGROW>
            end
            if ~isempty(truncdata)
                truncdata1 = [truncdata1; repmat(truncdata(i), freq(i), 1)]; %#ok<AGROW>
            end
        end
        data = data1;
        cens = cens1;
        truncdata=truncdata1;
    elseif any(freq == 0)
        data = data(freq > 0);
        if ~isempty(cens)
            cens = cens(freq > 0);
        end
        if ~isempty(truncdata)
            truncdata = truncdata(freq > 0,:);
        end
    end
    if numel(truncdata)==2
        truncdata=repmat(truncdata(:)',numel(data),1);
    end
    freq = [];
end
