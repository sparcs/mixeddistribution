function [Fout, xlow, Flo, Fup, D] = my_ecdf(data, varargin)
    %MY_ECDF Empirical (Kaplan-Meier) cumulative distribution function.
    %   [F,X] = MY_ECDF(Y) calculates the Kaplan-Meier estimate of the
    %   cumulative distribution function (cdf), also known as the empirical
    %   cdf.  Y is a vector of data values.  F is a vector of values of the
    %   empirical cdf evaluated at X.
    %
    %   [F,X,FLO,FUP] = ECDF(Y) also returns lower and upper confidence
    %   bounds for the cdf.  These bounds are calculated using Greenwood's
    %   formula, and are not simultaneous confidence bounds.
    %
    %   ECDF(...) without output arguments produces a plot of the empirical
    %   cdf. Use the data cursor to read precise values from the plot.
    %
    %   ECDF(AX,...) plots into axes AX instead of GCA.
    %
    %   [...] = ECDF(Y,'PARAM1',VALUE1,'PARAM2',VALUE2,...) specifies
    %   additional parameter name/value pairs chosen from the following:
    %
    %      Name          Value
    %      'censoring'   A boolean vector of the same size as Y that is 1 for
    %                    observations that are right-censored and 0 for
    %                    observations that are observed exactly.  Default is
    %                    all observations observed exactly.
    %      'frequency'   A vector of the same size as Y containing non-negative
    %                    integer counts.  The jth element of this vector
    %                    gives the number of times the jth element of Y was
    %                    observed.  Default is 1 observation per Y element.
    %      'alpha'       A value between 0 and 1 for a confidence level of
    %                    100*(1-alpha)%.  Default is alpha=0.05 for 95% confidence.
    %      'function'    The type of function returned as the F output argument,
    %                    chosen from 'cdf' (the default), 'survivor', or
    %                    'cumulative hazard'.
    %      'bounds'      Either 'on' to include bounds or 'off' (the default)
    %                    to omit them.  Used only for plotting.
    %
    %
    %   Example:  Generate random failure times and random censoring times,
    %   and compare the empirical cdf with the known true cdf:
    %
    %       y = exprnd(10,50,1);     % random failure times are exponential(10)
    %       d = exprnd(20,50,1);     % drop-out times are exponential(20)
    %       t = min(y,d);            % we observe the minimum of these times
    %       censored = (y>d);        % we also observe whether the subject failed
    %
    %       % Calculate and plot the empirical cdf and confidence bounds
    %       [f,x,flo,fup] = ecdf(t,'censoring',censored);
    %       stairs(x,f);
    %       hold on;
    %       stairs(x,flo,'r:'); stairs(x,fup,'r:');
    %
    %       % Superimpose a plot of the known true cdf
    %       xx = 0:.1:max(t); yy = 1-exp(-xx/10); plot(xx,yy,'g-')
    %       hold off;
    %
    %   See also CDFPLOT, ECDFHIST.

    % References:
    %   [1] Cox, D.R. and D. Oakes (1984) Analysis of Survival Data,
    %       Chapman & Hall.
    %   [2] Lawless, J.F. (2003) Statistical Models and Methods for
    %       Lifetime Data, 2nd ed., Wiley.

    % Copyright 1993-2019 The MathWorks, Inc.
    if (nargin > 0) && isscalar(data) && ishghandle(data) && isequal(get(data, 'type'), 'axes')
        axarg = {data};
        if nargin > 1
            data = varargin{1};
            varargin(1) = [];
        else
            data = []; % error to be dealt with below
        end
    else
        axarg = {};
    end
    if (nargin>0) && ischar(data)
        varargin=[{data} varargin];
        data=[];
    end
    validoptions = {'data' 'cens', 'freq', 'censoring' 'frequency' 'alpha' 'function' 'bounds'};
    fitopts = reshape(varargin, 2, length(varargin) / 2);
    if ~isempty(fitopts)
        fitopts(1, :) = lower(fitopts(1, :));
    end
    if any(~ismember(fitopts(1, :), validoptions))
        error('my_ecdf:optionsnotvalid', 'Options invalid.\nValid options are: %s\n', sprintf('''%s'' ', validoptions{:}));
    end
    ndx = strcmp(fitopts(1, :), 'data');
    if any(ndx)
        data = fitopts{2, ndx};
        fitopts=fitopts(:,~ndx); %not allowed for ecdf
    end

    ndx = strncmp(fitopts(1, :), 'cens',4);
    if any(ndx)
        cens = fitopts{2, ndx};
    else
        cens = [];
    end

    if ~isempty(cens) && ~all(cens == 1 | cens == 0)
        right=cens;
        cens=right ~= data;
        rightcens = isinf(right);
        if all(cens == rightcens)
            %all interval censored data are right censored, we can use the
            %normal ecdf function
            cens = rightcens;
        else
            data=[data,right];
            cens=[];
        end
        if any(ndx)
           fitopts{2, ndx}=cens;
        end
    else
        cens=logical(cens);
    end

%     if ~verLessThan('matlab','9.1')&&~islogical(cens)
%         data=[data,xhigh];
%           ndx = strncmp(fitopts(1, :), 'cens',4);
%           fitopts=fitopts(:,~ndx);
%         if nargout == 0
%             ecdf(data,fitopts{:});
%         else
%             [Fout, xlow] = ecdf(data, fitopts{:});
%             xlow=xlow(:,1);
%         end
%         return
%     end
    if size(data,2)==1 %||~isMATLABReleaseOlderThan('R2021b')
        %if cens is a logical or empty use ecdf
        if nargout == 0
            ecdf(data,fitopts{:});
        elseif size(data,2)==1
             [Fout, xlow, Flo, Fup,D]=  ecdf(data, fitopts{:});
        else
            [Fout, xlow] = ecdf(data, fitopts{:});
        end
        return;
    end
    ndx = strncmp(fitopts(1, :), 'freq',4);
    if any(ndx)
        freq = fitopts{2, ndx};
    else
        freq = [];
    end
    ndx = strcmp(fitopts(1, :), 'alpha');
    if any(ndx)
        alpha = fitopts{2, ndx};
    else
        alpha = 0.05;
    end
    ndx = strcmp(fitopts(1, :), 'function');
    if any(ndx)
        fn = fitopts{2, ndx};
    else
        fn = 'cdf';
    end
    ndx = strcmp(fitopts(1, :), 'bounds');
    if any(ndx)
        bounds = fitopts{2, ndx};
    else
        bounds = 'off';
    end
   

    xlow=data(:,1);


    % Check arguments

    xhigh = data(:,2);
    if ~isequal(size(xlow), size(xhigh))
        error(message('stats:ecdf:InputSizeMismatch'));
    end


    if isempty(freq)
        freq = ones(size(xlow), "like", xlow);
    else
        freq = freq(:);
        if ~isequal(size(xlow), size(freq))
            error(message('stats:ecdf:InputSizeMismatch'));
        end
        if islogical(freq)
            freq = double(freq);
        end
    end
    if numel(alpha) ~= 1 || ~isnumeric(alpha) || alpha <= 0 || alpha >= 1
        error(message('stats:ecdf:BadAlpha'))
    end

    % Remove NaNs, so they will be treated as missing
    %[ignore1,ignore2,x,cens,freq] = statremovenan(x,cens,freq); %#ok<ASGLU>


    % Remove missing observations indicated by NaN's.
    t = ~isnan(xlow) & ~isnan(freq) & ~isnan(xhigh) & freq > 0;
    xlow = xlow(t);
    n = length(xlow);
    if n == 0
        error(message('stats:ecdf:NotEnoughData'));
    end
    xhigh = xhigh(t);
    freq = freq(t);

    % Sort observation data in ascending order.
    leftx = xlow;
    rightx = xhigh;
    [rightx, t] = sort(rightx);
    leftx = leftx(t);
    freq=freq(t);
    %rightx = [0; rightx];
    % leftx = [0; leftx(t)];
    % leftx(leftx < 0) = 0;
    % freq = [0; freq(t)];
    % Make sure freq and X have matching type
    freq = cast(freq, "like", xlow);
 
    % Compute cumulative sum of frequencies
    rightcens = isinf(rightx);
    %leftcens = leftx ~= rightx & leftx <= 0;
    intervalcens = leftx ~= rightx & ~rightcens;
    totcumfreq = cumsum(freq);
  %  obscumfreq = cumsum(freq);
    %     t = (diff(x) == 0);
    %     if any(t)
    %         x(t) = [];
    %         leftcumfreq(t)=[];
    %         totcumfreq(t) = [];
    %         obscumfreq(t) = [];
    %     end
    totalcount = totcumfreq(end);


    % Get number of deaths and number at risk at each unique X
    D = freq;
    %spread the inverval deaths
    f = find(intervalcens);
    for i = 1:length(f)
        n = freq(f(i));
        D(f(i))=0;
        l=leftx(f(i));
        r=rightx(f(i));
        if ~isinf(r)
          ndx = leftx>=l&rightx<=r;
          ntot = sum(ndx);
          D(ndx) = D(ndx) +  n / ntot;
        end
    end
    totcumfreq=cumsum(D);
    N = totalcount - [0; totcumfreq(1:end - 1)];

    % No change in function except at a death, so remove other points
    %     t = (D > 0);
    %     x = rightx(t);
    %     D = D(t);
    %     N = N(t);
   
    
    cdf_sf = any(strcmp({'cdf', 'survivor'}, fn));

    if cdf_sf % 'cdf' or 'survivor'
        % Use the product-limit (Kaplan-Meier) estimate of the survivor
        % function, transform to the CDF.
        S = cumprod(1 - D ./ N);
        if strcmp(fn, 'cdf')
            Func = 1 - S;
           % F0 = 0; % starting value of this function (at x=-Inf)
            funcdisplayname = 'F(x)';
        else % 'survivor'
            Func = S;
          %  F0 = 1;
            funcdisplayname = 'S(x)';
        end
    else % 'cumhazard'
        % Use the Nelson-Aalen estimate of the cumulative hazard function.
        Func = cumsum(D ./ N);
        F0 = 0;
        funcdisplayname = 'H(x)';
    end

    % Include a starting value; required for accurate staircase plot
    if ~isempty(D)
     %   x = [min(y); x];
        F =  Func;
    else
        % All data are censored, x and F should be empty; so, no need to include
        % the starting value
        warning(message('stats:ecdf:NoDeath'));
        F = Func;
    end
   %x = [leftx; right(end)];
   % F=[F;F(end)];
    xlow=leftx;
    if nargout > 2 || (nargout == 0 && isequal(bounds, 'on'))
        % Get standard error of requested function
        if cdf_sf % 'cdf' or 'survivor'
            se = NaN(size(D), "like", xlow);
            if ~isempty(D)
                if N(end) == D(end)
                    t = 1:length(N) - 1;
                else
                    t = 1:length(N);
                end
                se(t) = S(t) .* sqrt(cumsum(D(t) ./ (N(t) .* (N(t) - D(t)))));
            end
        else % 'cumhazard'
            se = sqrt(cumsum(D ./ (N .* N)));
        end

        if ~isempty(se)
            % Get confidence limits
            zalpha = -norminv(alpha / 2);
            halfwidth = zalpha * se;
            Flo = max(0, Func - halfwidth);
            Flo(isnan(halfwidth)) = NaN; % max drops NaNs, put them back
            if cdf_sf % 'cdf' or 'survivor'
                Fup = min(1, Func + halfwidth);
                Fup(isnan(halfwidth)) = NaN; % max drops NaNs
            else % 'cumhazard'
                Fup = Func + halfwidth; % no restriction on upper limit
            end
           % Flo = [NaN; Flo];
           % Fup = [NaN; Fup];
        else
            Flo = cast([], "like", xlow);
            Fup = Flo;
        end
    else
        Flo = cast([], "like", xlow);
        Fup = Flo;
    end

    % Plot if no return values are requested
    if nargout == 0
        h = stairs(axarg{:}, xlow, [F Flo Fup]);
        xlabel(axarg{:}, 'x');
        ylabel(axarg{:}, funcdisplayname);
        set(h, 'Tag', 'ecdf');
        %Set custom data cursor text for data line.
        hB = hggetbehavior(h(1), 'datacursor');
        set(hB, 'UpdateFcn', @ecdfDatatipCallback);
        setappdata(h(1), 'funcdisplayname', funcdisplayname);
        setappdata(h(1), 'D', [0; D]);
     
        if ~isempty(Flo) && ~isempty(Fup)
            % Set confidence bounds color and linestyle.
            set(h(2:3), 'Color', get(h(1), 'Color'), 'LineStyle', ':');
            set(h(2), 'Tag', 'lower');
            set(h(3), 'Tag', 'upper');
            % Store confidence bounds data with the data line, for use in
            % data cursor tip.
            setappdata(h(1), 'confbounds', [Flo Fup]);
            setappdata(h(1), 'alpha', alpha);
            % Set data cursor for confidence bounds lines.
            setappdata(h(2), 'hprimary', h(1))
            setappdata(h(3), 'hprimary', h(1))
            hB = hggetbehavior(h(2), 'datacursor');
            set(hB, 'UpdateFcn', @ecdfDatatipCallback);
            hB = hggetbehavior(h(3), 'datacursor');
            set(hB, 'UpdateFcn', @ecdfDatatipCallback);
        end
    else
        Fout = F;
    end

    % -----------------------------
end
function datatipTxt = ecdfDatatipCallback(obj, evt) %#ok<INUSL>

    target = get(evt, 'Target');
    ind = get(evt, 'DataIndex');

    % If a confidence bound clicked, redirect to main line.
    if isappdata(target, 'hprimary')
        target = getappdata(target, 'hprimary');
    end

    funcdisplayname = getappdata(target, 'funcdisplayname');
    D = getappdata(target, 'D');
    xdat = get(target, 'XData');
    ydat = get(target, 'YData');
    x = xdat(ind);
    y = ydat(ind);
    d = D(ind);
    datatipTxt = {
        sprintf('x: %s', num2str(x))  ...
        [funcdisplayname ': ' num2str(y)]  ...
        ''  ...
        getString(message('stats:ecdf:NumObs', num2str(d)))  ...
        };

    if isappdata(target, 'confbounds')
        confbounds = getappdata(target, 'confbounds');
        alpha = getappdata(target, 'alpha');
        bounds = confbounds(ind, :);
     
        datatipTxt{end + 1} = getString(message('stats:ecdf:ConfBounds',  ...
            num2str(100 * (1 - alpha)), num2str(bounds(1)), num2str(bounds(2))));
    end
end
