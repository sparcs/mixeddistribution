function pd = fit_mle(likefunct, makefittedfunct, plim, defaultstart, data, varargin)
    %FIT Fit from data using custommle
    % pd= fit_mle(likefunct,makefittedfunct,plim,defaultstart,data,varargin)
    %    likefunct, handle to likelihood function
    %    makefittedfuct, handle to makefitted function (leave empty to
    %       if there is not such function
    %    plim: parameter limits (see getInfo)
    %       defaultstart: a value to start (if the use does not provide it in
    %       the options
    %    data: basic data
    %       parameters in varargin
    %          'options'   (use Start field)
    %          'upperbound'
    %          'lowerbound'
    %          'censoring'    Boolean vector indicating censored x values
    %                         or upperranges
    %          'frequency'    Vector indicating frequencies of corresponding
    %                         x values
    %          'options'      Options structure for fitting, as create by
    %                         the STATSET function

    if nargin == 6 && isstruct(varargin{1})
        aa = varargin{1};
        varargin = [fieldnames(aa)'; struct2cell(aa)'];
        varargin = varargin(:)';
    end
    options = reshape(varargin, 2, length(varargin) / 2);
    ndx = strncmpi(options(1, :), 'freq', 4);
    if any(ndx)
        options{1, ndx} = 'frequency';
        freq = options{2, ndx};
        if isempty(freq)
            options{2, ndx} = ones(size(data));
        end
    else
        freq = ones(size(data));
    end
    ndx = strncmpi(options(1, :), 'cens', 4);
    if any(ndx)
        options{1, ndx} = 'censoring';
        if isempty(options{2, ndx})
            options{2, ndx} = false(size(data));
        %else
        %    options{2, ndx} = options{2, ndx};
        end
        cens = options{2, ndx};
    else
        cens = false(size(data));
    end

    ndx = strcmp(options(1, :), 'options');
    if ~any(ndx)
        options = [options, {'start'; defaultstart}];
    else
        opts = options{2, ndx};
        if isfield(opts, 'start')
            defaultstart = options{2, ndx}.start;
            options = [ options, {'start'; options{2, ndx}.start}];
        else
            options = [options, {'start'; defaultstart}];
        end

        if isfield(opts, 'lowerbound')
            options = [options {'lowerbound'; opts.lowerbound}];

        end
        if isfield(opts, 'upperbound')
            options = [options {'upperbound'; opts.upperbound}];
        end
    end
    ndx = strcmp(options(1, :), 'lowerbound');
    newstart=defaultstart;
    if ~any(ndx)
        options = [options {'lowerbound'; plim(1, :)}];
    else
        lb=max(plim(1,:),options{2,ndx});
        options{2,ndx}=lb;
        newstart(newstart<lb)=lb(newstart<lb);
    end
    ndx = strcmp(options(1, :), 'upperbound');
    if ~any(ndx)
        options = [options {'upperbound'; plim(2, :)}];
    else
        ub=min(plim(2,:),options{2,ndx});
        options{2,ndx}=ub;
        newstart(newstart>ub)=ub(newstart>ub);
    end
    if any(newstart~=defaultstart)
        ndx1 = strcmp(options(1, :), 'start');
        options{2,ndx1}=newstart;
    end
    
    try
        % [phat] = mle(data, 'pdf', pdffunct,'cdf', cdffunct, options{:});
        [phat] = mle(data, 'nloglf', likefunct, options{:});
    catch err
        if strcmp('stats:mle:NonpositivePdfVal', err.identifier)
            disp(err.message)
            phat = nan + distr.ParameterValues;
        else
            rethrow(err);
        end
    end
    cov = [];
    %loglikelihood
    nll = likefunct(phat, data, cens, freq);

    if isinf(nll)
        error('nll is inf');
    end
    % Create the distribution by calling the constructor.
    if ~isempty(makefittedfunct)
       pd = makefittedfunct(phat, nll, cov, data, cens, freq);
    else
       pd = struct('phat',phat,'nll', nll,'cov',cov,'InputData',struct('data',data,'cens', cens, 'freq',freq));
    end

end
