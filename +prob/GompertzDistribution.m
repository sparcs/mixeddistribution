classdef GompertzDistribution < prob.ToolboxFittableParametricDistribution
    %
    %    An object of the GompertzDistribution class represents a
    %    Linear-Exponential probability distribution with a specific location parameter MU and
    %    scale parameter SIGMA. This distribution object can be created directly
    %    using the MAKEDIST function or fit to data using the FITDIST function.
    %
    %    GompertzDistribution methods:
    %       cdf                   - Cumulative distribution function
    %       fit                   - Fit distribution to data
    %       icdf                  - Inverse cumulative distribution function
    %       iqr                   - Interquartile range
    %       mean                  - Mean
    %       median                - Median
    %       paramci               - Confidence intervals for parameters
    %       pdf                   - Probability density function
    %       proflik               - Profile likelihood function
    %       random                - Random number generation
    %       std                   - Standard deviation
    %       truncate              - Truncation distribution to an interval
    %       var                   - Variance
    %
    %    LaplaceDistribution properties:    
    %       DistributionName      - Name of the distribution
    %       mu                    - Value of the mu parameter
    %       sigma                 - Value of the sigma parameter
    %       NumParameters         - Number of parameters
    %       ParameterNames        - Names of parameters
    %       ParameterDescription  - Descriptions of parameters
    %       ParameterValues       - Vector of values of parameters
    %       Truncation            - Two-element vector indicating truncation limits
    %       IsTruncated           - Boolean flag indicating if distribution is truncated
    %       ParameterCovariance   - Covariance matrix of estimated parameters
    %       ParameterIsFixed      - Two-element boolean vector indicating fixed parameters
    %       InputData             - Structure containing data used to fit the distribution
    %       NegativeLogLikelihood - Value of negative log likelihood function
    %
    %    See also fitdist, makedist.

    % All ProbabilityDistribution objects must specify a DistributionName
    properties(Constant)
        %DistributionName Name of distribution
        %    DistributionName is the name of this distribution.
        DistributionName = 'Gompertz';
    end

    % Optionally add your own properties here. For this distribution it's convenient
    % to be able to refer to the mu and sigma parameters by name, and have them
    % connected to the proper element of the ParameterValues property. These are
    % dependent properties because they depend on ParameterValues.
    properties(Dependent = true)
        %R is exponential decay R=constant factor
        alpha
        
        %A Linear growth parameter A= growth factor
        beta
 
    end
    
    % All ParametricDistribution objects must specify values for the following
    % constant properties (they are the same for all instances of this class).
    properties(Constant)
        %NumParameters Number of parameters
        %    NumParameters is the number of parameters in this distribution.

        NumParameters = 2;
        
        %ParameterName Name of parameter
        %    ParameterName is a two-element cell array containing names
        %    of the parameters of this distribution.
        ParameterNames = {'alpha' 'beta'};
        
        %ParameterDescription Description of parameter
        %    ParameterDescription is a two-element cell array containing
        %    descriptions of the parameters of this distribution.
        ParameterDescription = { 'factor', 'exponent'};
        typicalpars = [0.001 0.01];
    end

    % All ParametricDistribution objects must include a ParameterValues property
    % whose value is a vector of the parameter values, in the same order as
    % given in the ParameterNames property above.
    properties(GetAccess = 'public', SetAccess = 'protected')
        %ParameterValues Values of the distribution parameters
        %    ParameterValues is a two-element vector containing the mu and sigma
        %    values of this distribution.
        ParameterValues
    end

    methods
        % The constructor for this class can be called with a set of parameter
        % values or it can supply default values. These values should be
        % checked to make sure they are valid. They should be stored in the
        % ParameterValues property.
        function pd = GompertzDistribution(alpha, beta)
            if nargin == 0
                alpha = 0.001;
                beta = 0.05;
            end
            checkargs(alpha, beta);
            
            pd.ParameterValues = [alpha beta];
            
            % All FittableParametricDistribution objects must assign values
            % to the following two properties. When an object is created by
            % the constructor, all parameters are fixed and the covariance
            % matrix is entirely zero.
            pd.ParameterIsFixed = [true true];
            pd.ParameterCovariance = zeros(pd.NumParameters);
        end
 
        
        % Implement methods to compute the mean, variance, and standard
        % deviation.
        function m = mean(this)
            %numerical mean using the pdf
            info = this.getInfo;
            m = integral(@(x)x.*this.pdf(x), info.support(1), info.support(2));
        end
        
        function s = std(this)
            s = sqrt(this.var);
        end
        function v = var(this)
            info = this.getInfo;
            m = this.mean;
            v = integral(@(x)(x - m).^2 .*this.pdf(x), info.support(1), info.support(2));
        end
        function haz = cumhazard(this, x)
            haz = this.alpha .* exp(this.beta .* x) ./ this.beta;
        end
        function haz = hazard(this, x)
            haz = this.alpha .* exp(this.beta .* x);
        end
        
    end
    methods
        % If this class defines dependent properties to represent parameter
        % values, their get and set methods must be defined. The set method
        % should mark the distribution as no longer fitted, because any
        % old results such as the covariance matrix are not valid when the
        % parameters are changed from their estimated values.
        function this = set.alpha(this, alpha)
            checkargs(alpha, this.beta);
            this.ParameterValues(1) = alpha;
            this = invalidateFit(this);
        end
        function this = set.beta(this, beta)
            checkargs(this.alpha, beta);
            this.ParameterValues(2) = beta;
            this = invalidateFit(this);
        end
        function alpha = get.alpha(this)
            alpha = this.ParameterValues(1);
        end
        function beta = get.beta(this)
            beta = this.ParameterValues(2);
        end

    end
    methods(Static)
        % All FittableDistribution classes must implement a fit method to fit
        % the distribution from data. This method is called by the FITDIST
        % function, and is not intended to be called directly
        function pd = fit(x, varargin)
            pd = shared.fit_mle(@prob.GompertzDistribution.likefunc,  ...
                @prob.GompertzDistribution.makeFitted,  ...
                prob.GompertzDistribution.getInfo.plim, prob.GompertzDistribution.typicalpars,  ...%typical parameter set
                x, varargin{:});
        end
        

        % The following static methods are required for the
        % ToolboxParametricDistribution class and are used by various
        % Statistics and Machine Learning Toolbox functions. These functions operate on
        % parameter values supplied as input arguments, not on the
        % parameter values stored in a LaplaceDistribution object. For
        % example, the cdf method implemented in a parent class invokes the
        % cdffunc static method and provides it with the parameter values.
        function [nll, acov] = likefunc(varargin) % likelihood function
            [nll, acov] = shared.likefunc_mydistrib(@prob.GompertzDistribution.pdffunc, @prob.GompertzDistribution.cdffunc,[], varargin{:});
        end

        function y = cdffunc(x, alpha, beta) % cumulative distribution function
            %One minus the solution of x'=-(lambda+alpha*exp(beta*t))*x
            if isempty(x)
                y = [];
                return;
            end
            lambda = 0;
            if beta == 0
                %exponential
                y = 1 - exp(-(alpha + lambda) .* x);
            else
                y = 1 - exp(-lambda .* x - (alpha .* exp(beta .* x)) ./ beta + alpha ./ beta);
            end
            y(x <= 0) = 0;
            y(isinf(x)) = 1;
            y(y >= 1 - 1E-10) = 1 - 1E-10;
 
 
            %numerical cdf using the pdf
            %             ran = prob.GompertzDistribution.GetRange(varargin);
            %             xs = linspace(ran(1), ran(2), 1000);
            %             numpdf = prob.GompertzDistribution.pdffunc(xs, varargin{:});
            %             cumcdf = cumtrapz(xs, numpdf);
            %             y = interp1(xs, cumcdf, x);
            %             y(isnan(x)) = NaN;
        end
        
        function y = pdffunc(x, alpha, beta) % probability density function has to be specified
            %derivative of cdffunc
            %essential to combine exp() functions, as the separate
            %factors can become infitine. Do not simplify the equation!
            lambda = 0;
            expon1 = -lambda .* x - (alpha .* exp(beta .* x)) ./ beta + alpha ./ beta;
            expon2 = expon1 + beta .* x;
            y = 1E-300 + exp(expon1) .* lambda + exp(expon2) .* alpha;
            % y = 1E-300 + exp(- lambda .* x - (alpha .* exp(beta .* x)) ./ beta) .* exp(alpha ./ beta) .* (lambda + alpha .* exp(beta .* x));
         
        end
        
        function y = invfunc(p, alpha, beta) % inverse cdf
            if isempty(p)
                y = [];
                return;
            end
            if exp(-alpha / beta) > 0
                y = log(-(beta .* log(-exp(-alpha ./ beta) .* (p - 1.0))) ./ alpha) ./ beta;
                y(p < 0 | 1 < p) = NaN;
            else
                if numel(p) < 10
                    y = zeros(size(p));
                    for i = 1:numel(p)
                        if p == 1
                            y(i) = inf;
                        else
                            %reduces to exponential fzero slightly more precise
                            y0 = -log(1 - p) ./ alpha;
                            y(i) = fzero(@(x)prob.GompertzDistribution.cdffunc(x,alpha,beta) - p(i), y0);
                        end
                    end
                    return;
                end
                p(p == 1) = 1 - 1e-5;
                %interpolate the cdf with x and y exchanged for speed
                xs = linspace(fzero(@(x)prob.GompertzDistribution.cdffunc(x,alpha,beta) - min(p), -log(1 - min(p)) ./ alpha),  ...
                    fzero(@(x)prob.GompertzDistribution.cdffunc(x,alpha,beta) - max(p), -log(1 - min(p)) ./ alpha), 2000);
                ys = prob.GompertzDistribution.cdffunc(xs, alpha, beta);
                [~, ndx] = unique(ys, 'stable');
                y = interp1(ys(ndx), xs(ndx), p, 'linear', 'extrap');
            end
            %             ran = prob.GompertzDistribution.GetRange(varargin);
            %             xs = linspace(ran(1), ran(2), 1000);
            %             numpdf = prob.GompertzDistribution.pdffunc(xs, varargin{:});
            %             cumcdf = cumtrapz(xs, numpdf);
            %             [~, ndx] = unique(cumcdf);
            %             y = interp1(cumcdf(ndx), xs(ndx), p);
            %             y(p < 0 | 1 < p) = NaN;
            %             hh=  @(p,alpha,beta)log(-(beta.*log(-exp(-alpha./beta).*(p-1.0)))./alpha)./beta;
            %             hh(p,varargin{:})
        end
        
        function y = randfunc(r, a, varargin) % random number generator
            y = prob.GompertzDistribution.invfunc(rand(varargin{:}), r, a);
        end

        function ran = GetRange(cpar, minpdf, info)
            ran = prob.GompertzDistribution.invfunc([0, 1 - 1E-6], cpar{:});
            %             if nargin < 3
            %                 info = prob.GompertzDistribution.getInfo;
            %             end
            %             if nargin < 2
            %                 minpdf = 1E-100;
            %             end
            %             ran = info.support;
            %             if isinf(ran(1)) && ~isinf(ran(2))
            %                 x = -logspace(5, -3, 1000);
            %             elseif isinf(ran(2)) && ~isinf(ran(1))
            %                 x1 = logspace(5, -3, 1000);
            %             else
            %                 x = [-logspace(5, -3, 1000), 0, logspace(-3, 5, 1000)];
            %                 x1 = fliplr(x);
            %             end
            %             if isinf(ran(1))
            %                 pdf = prob.GompertzDistribution.pdffunc(x,cpar{:});
            %                 ran(1) = x(find(pdf > minpdf, 1) - 1);
            %             end
            %             if isinf(ran(2))
            %                 pdf = prob.GompertzDistribution.pdffunc(x1,cpar{:});
            %                 ndx=pdf > minpdf;
            %                 ran(2) = x1(find(ndx, 1) - 1);
            %             end
        end

    end
    methods(Static, Hidden)
        % All ToolboxDistributions must implement a getInfo static method
        % so that Statistics and Machine Learning Toolbox functions can get information about
        % the distribution.
        function info = getInfo
            
            % First get default info from parent class
            info = getInfo@prob.ToolboxFittableParametricDistribution('prob.Gompertz');
            
            % Then override fields as necessary
            info.name = 'Gompertz';
            info.code = 'gompertz';
            info.pnames = prob.GompertzDistribution.ParameterNames;
            info.pdescription = prob.GompertzDistribution.ParameterDescription;

            info.likefunc = @prob.GompertzDistribution.likefunc;
            info.cdffunc = @prob.GompertzDistribution.cdffunc;
            info.pdffunc = @prob.GompertzDistribution.pdffunc;
            info.invfunc = @prob.GompertzDistribution.invfunc;
            info.randfunc = @prob.GompertzDistribution.randfunc;
            % info.pnames is obtained from the ParameterNames property
            % info.pdescription is obtained from the ParameterDescription property
            info.prequired = [true true]; % Change if any parameter must
            %          be specified before fitting.
            %          An example would be the N
            %          parameter of the binomial
            %          distribution.
            % info.hasconfbounds = false     % Set to true if the cdf and
            %          icdf methods can return
            %          lower and upper bounds as
            %          their 2nd and 3rd outputs.
            info.censoring = true; % Set to true if the fit
            %          method supports censoring.
            info.support = [0, Inf];
            %          Set to other lower and upper
            %          bounds if the distribution
            %          doesn't cover the whole real
            %          line. For example, for a
            %          distribution on positive
            %          values use [0, Inf].
            % info.closedbound = [false false] % Set the Jth value to
            %          true if the distribution
            %          allows x to be equal to the
            %          Jth element of the support
            %          vector.
            % info.iscontinuous = true       % Set to false if x can take
            %          only integer values.
            info.islocscale = true; % Set to true if this is a
            %          location/scale distribution
            %          (no other parameters).
            % info.uselogpp = false          % Set to true if a probability
            %          plot should be drawn on the
            %          log scale.
 
            info.fittable = true;
            info.optimopts = true; % Set to true if the fit
            %          method can be called with an
            %          options structure.
            info.logci = [false false]; % Set to true for a parameter
            %          that should have its Wald
            %          confidence interval computed
            %          using a normal approximation
            %          on the log scale.
            info.plim = [0 0; Inf Inf];
           % info.typicalpars = [0.001 0.01];
        end
        function pd = makeFitted(par, nll, cov, x, cens, freq)
 
            cpar = num2cell(par);
            pd = prob.GompertzDistribution(cpar{:});
            pd.NegativeLogLikelihood = nll;
            pd.ParameterCovariance = cov;
            pd.ParameterIsFixed = false;
            pd.ParameterIsFixed = [false false false];
            pd.InputData = struct('data', x, 'cens', cens, 'freq', freq);
        end
    end
end % classdef

% The following utilities check for valid parameter values
function checkargs(alpha, beta)
    if ~(isscalar(alpha) && isnumeric(alpha) && isreal(alpha) && alpha >= 0 && isfinite(alpha))
        error('alpha must be a positive finite numeric scalar.')
    end
    if ~(isscalar(beta) && isnumeric(beta) && isreal(beta) && beta >= 0 && isfinite(beta))
        error('beta must be a positive finite numeric scalar.')
    end
end
