function y = numinvfunc(fcdf, support, p, varargin) % inverse cdf
    if isempty(p)
        y = [];
        return;
    end
    if numel(p) < 10
        p(p > 1 - 1E-8) = 1 - 1E-8;
        y = zeros(size(p));
        for i = 1:numel(p)
            if p(i) == 1
                y(i) = support(2);
            elseif p == 0
                y(i) = support(1);
            else
                y(i) = fzero(@(x)fcdf(x) - p(i), 0.5);
            end
        end
        return;
    end
    %interpolate the cdf with x and y exchanged for speed
    if min(p(:)) < 0.1
        minx = 0;
    else
        minx = fzero(@(x)fcdf(x) - min(p(:)), 0.5);
    end
    %   haserror = false;
    maxp = max(p(:));
    if maxp > 1 - 1E-8
        maxp = 1 - 1E-8;
    end
    % try
    maxx = fzero(@(x)fcdf(x) - maxp, 0.5);
    %     catch
    %         maxx = 5000;
    %         haserror = true;
    %     end
    %     if isnan(maxx)
    %         maxx = 5000;
    %     end
    xs = linspace(minx, maxx, 5000);
    ys = fcdf(xs);
    if ys(end) < maxp
        ys = ys / ys(end) * maxp;
    end
    [~, ndx] = unique(ys, 'stable');
    y = interp1(ys(ndx), xs(ndx), p, 'linear');
    if minx ~= 0
        y(p == min(p)) = minx;
    end
    y(p == max(p)) = maxx;
    %     if haserror
    %         y(isnan(y)) = maxx;
    %     end
    y(p == 1) = support(2);
    y(p == 0) = support(1);
end
