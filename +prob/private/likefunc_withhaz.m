function [nll, acov] = likefunc_withhaz(hazfun, cdffun, params, data, cens, freq) % likelihood function
    % n = length(x);
    if nargin < 5
        cens = [];
    end
    if nargin < 6
        freq = [];
    end
    if isempty(freq)
        freq = ones(size(data));
    end
    if isempty(cens)
        cens = false(size(data));
    elseif ~islogical(cens)&&~all(cens==1|cens==0)
        %interval censoring
        [nll, acov] = shared.likefunc_mydistrib_interval(pdffun, cdffun,[], params, data, cens, freq);
        return;
    else
        cens=logical(cens);
    end
    %saves one inthazard 
    cpar = num2cell(params);
    rescdf = cdffun(data, cpar{:});
    respdf = (1 - rescdf) .* hazfun(data, cpar{:})+1E-300;

    nll = 0;
    if any(cens) % compute log survivor function for censoring points
        nll = nll - sum(freq(cens) .* log(1 - rescdf(cens)));
    end
    if any(~cens) % compute log pdf for observed data
        nll = nll - sum(freq(~cens) .* log(respdf(~cens)));
    end
    acov = nan * eye(numel(params));
end
