classdef BoundedParetoDistribution < prob.ToolboxFittableParametricDistribution
    %
    %    An object of the BoundedParetoDistribution class represents the Pareto
    %    distubution as implemented by Clauset et al. 2009
    %       pdf(x) = (alpha-1)*xmin^(alpha-1)*x^alpha
    %
    %    The parameter ALPHA defines the power and XMIN is the scale parameter. 
    %    This distribution object can be created directly
    %    using the MAKEDIST function or fit to data using the FITDIST function.
    %
    %    BoundedParetoDistribution methods:
    %       cdf                   - Cumulative distribution function
    %       fit                   - Fit distribution to data
    %       icdf                  - Inverse cumulative distribution function
    %       iqr                   - Interquartile range
    %       mean                  - Mean
    %       median                - Median
    %       paramci               - Confidence intervals for parameters
    %       pdf                   - Probability density function
    %       proflik               - Profile likelihood function
    %       random                - Random number generation
    %       std                   - Standard deviation
    %       truncate              - Truncation distribution to an interval
    %       var                   - Variance
    %
    %    BoundedParetoDistribution properties:    
    %       DistributionName      - Name of the distribution
    %       a                     - Value of the a parameter, increase of
    %                               hazard
    %       NumParameters         - Number of parameters
    %       ParameterNames        - Names of parameters
    %       ParameterDescription  - Descriptions of parameters
    %       ParameterValues       - Vector of values of parameters
    %       Truncation            - Two-element vector indicating truncation limits
    %       IsTruncated           - Boolean flag indicating if distribution is truncated
    %       ParameterCovariance   - Covariance matrix of estimated parameters
    %       ParameterIsFixed      - Two-element boolean vector indicating fixed parameters
    %       InputData             - Structure containing data used to fit the distribution
    %       NegativeLogLikelihood - Value of negative log likelihood function
    %
    %    See also fitdist, makedist.

    % All ProbabilityDistribution objects must specify a DistributionName
    properties(Constant)
        %DistributionName Name of distribution
        %    DistributionName is the name of this distribution.
        DistributionName = 'boundedpareto';
    end

    % Optionally add your own properties here. For this distribution it's convenient
    % to be able to refer to the mu and sigma parameters by name, and have them
    % connected to the proper element of the ParameterValues property. These are
    % dependent properties because they depend on ParameterValues.
    properties(Dependent = true)
  
        %A Linear growth parameter A= growth factor
        alpha
        xmin
        xmax
    end
 
 
    % All ParametricDistribution objects must specify values for the following
    % constant properties (they are the same for all instances of this class).
    properties(Constant)
        %NumParameters Number of parameters
        %    NumParameters is the number of parameters in this distribution.

        NumParameters = 3;
 
        %ParameterName Name of parameter
        %    ParameterName is a two-element cell array containing names
        %    of the parameters of this distribution.
        ParameterNames = {'alpha' 'xmin', 'xmax'};
 
        %ParameterDescription Description of parameter
        %    ParameterDescription is a two-element cell array containing
        %    descriptions of the parameters of this distribution.
        ParameterDescription = { 'power', 'minimum value of x', 'maximum value of x'};
        typicalpars = [1.4 1 1000];

    end

    % All ParametricDistribution objects must include a ParameterValues property
    % whose value is a vector of the parameter values, in the same order as
    % given in the ParameterNames property above.
    properties(GetAccess = 'public', SetAccess = 'protected')
        %ParameterValues Values of the distribution parameters
        %    ParameterValues is a two-element vector containing the mu and sigma
        %    values of this distribution.
        ParameterValues
    end

    methods
        % The constructor for this class can be called with a set of parameter
        % values or it can supply default values. These values should be
        % checked to make sure they are valid. They should be stored in the
        % ParameterValues property.
        function pd = BoundedParetoDistribution(alpha, xmin, xmax)
            if nargin == 0
                %prevent error xmax<xmin
                pd.ParameterValues = [1.4, 1, 1000];
            else
                checkargs(alpha, xmin, xmax);
                pd.ParameterValues = [alpha, xmin, xmax];
            end
            % All FittableParametricDistribution objects must assign values
            % to the following two properties. When an object is created by
            % the constructor, all parameters are fixed and the covariance
            % matrix is entirely zero.
            pd.ParameterIsFixed = [true, true, true];
            pd.ParameterCovariance = zeros(pd.NumParameters);
        end
        
 
        % Implement methods to compute the mean, variance, and standard
        % deviation.
        function m = mean(this)
            %numerical mean using the pdf
            %             info = this.getInfo;
            %             m = integral(@(x)x.*this.pdf(x), info.support(1), info.support(2))
            beta = this.alpha - 1;
            if beta == 1
                m = this.xmin * this.xmax / (this.xmax - this.xmin) * log(this.xmax / this.xmin);
            else
                m = this.xmin.^beta / (1 - (this.xmin / this.xmax)^beta) * beta / (beta - 1) * (this.xmin^(-beta + 1) - this.xmax^(-beta + 1));
            end
        end
 
        function s = std(this)
            s = sqrt(this.var);
        end
        
        function v = var(this)
            %nice analytical solution
            beta = this.alpha - 1;
            %v1=int(x^2*pdf(x),xmin,xmax)
            if beta == 1
                v1 = this.xmax * this.xmin;
            elseif beta == 2
                v1 = -(2 * this.xmin^2 * (log(this.xmax) - log(this.xmin))) / (this.xmin^2 / this.xmax^2 - 1);
            else
                v1 = -(beta * (this.xmax^beta * this.xmin^2 - this.xmax^2 * this.xmin^beta)) /  ...
                    (this.xmax^beta * (beta - 2) * ((this.xmin / this.xmax)^beta - 1));
            end
            v = v1 - this.mean^2;
        end
        
        %         function haz = hazard(this, x)
        %             haz = this.a .* x;
        %         end
 

    end
    methods
        % If this class defines dependent properties to represent parameter
        % values, their get and set methods must be defined. The set method
        % should mark the distribution as no longer fitted, because any
        % old results such as the covariance matrix are not valid when the
        % parameters are changed from their estimated values.
        function this = set.alpha(this, a)
            checkargs(a, this.xmin, this.xmax);
            this.ParameterValues(1) = a;
            this = invalidateFit(this);
        end
        function a = get.alpha(this)
            a = this.ParameterValues(1);
        end
        function this = set.xmin(this, xmin1)
            checkargs(this.alpha, xmin1, this.xmax);
            this.ParameterValues(2) = xmin1;
            this = invalidateFit(this);
        end
        
        function xmin1 = get.xmin(this)
            xmin1 = this.ParameterValues(2);
        end
        function this = set.xmax(this, xmax1)
            checkargs(this.alpha, this.xmin, xmax1);
            this.ParameterValues(3) = xmax1;
            this = invalidateFit(this);
        end
        
        function xmax1 = get.xmax(this)
            xmax1 = this.ParameterValues(3);
        end
        

    end
    methods(Static)
        % All FittableDistribution classes must implement a fit method to fit
        % the distribution from data. This method is called by the FITDIST
        % function, and is not intended to be called directly
        
        function pd = fit(x, varargin)
            %fits also with censored/interval censored data
            fitopts = reshape(varargin, 2, length(varargin) / 2);

            if ~isempty(fitopts)
                fitopts(1, :) = lower(fitopts(1, :));
            end
            cndx = strncmp(fitopts(1, :), 'cens', 4);
            if any(cndx)
                cens = fitopts{2, cndx};
            else
                cens = [];
            end
            fndx = strncmp(fitopts(1, :), 'freq', 4);
            if any(fndx)
                freq = fitopts{2, fndx};
            else
                freq = [];
            end
            %xmin is a problem for mle fitting, as the number of
            %observations is not constant
            pd1 = SingleDist('boundedpareto');
            %find median using the ecdf
            [F1, x1] = shared.my_ecdf(x, 'censoring', cens, 'frequency', freq);
            perc25 = x1(find(F1(:,1) >= 0.25, 1) + 1);
            perc75 = x1(find(F1(:,1) >= 0.75, 1) + 1);
            pd2 = pd1.set('xmax', max(x), true);
            pd = pd2.fitfixed(x, 'fixedpar', 'xmin', 'lowerpar', true, fitopts{:}, 'parrange', [min(x), perc25]);
            pd2 = pd.set('xmax', [], false, 'xmin', pd.d.xmin, true);
            ndx = x >= pd.d.xmin;
            x1 = x(ndx);
            if any(cndx) && ~isempty(cens)
                cens1 = cens(ndx);
                fitopts{2, cndx} = cens1;
            end
            if any(fndx) && ~isempty(freq)
                freq1 = freq(ndx);
                fitopts{2, fndx} = freq1;
            end
            pd = pd2.fitfixed(x1, 'fixedpar', 'xmax', 'upperpar', true, fitopts{:}, 'parrange', [perc75, max(x)]);
            pd = prob.BoundedParetoDistribution.makeFitted(pd.d.ParameterValues, pd.negloglik, [], x, cens, freq);
        end


        % The following static methods are required for the
        % ToolboxParametricDistribution class and are used by various
        % Statistics and Machine Learning Toolbox functions. These functions operate on
        % parameter values supplied as input arguments, not on the
        % parameter values stored in a LaplaceDistribution object. For
        % example, the cdf method implemented in a parent class invokes the
        % cdffunc static method and provides it with the parameter values.
        function [nll, acov] = likefunc(varargin) % likelihood function
            %    alpha=varargin{1};
            %    xmin=varargin{2};
            %    x=varargin{3};
            %    z = x(x >= xmin);
            %    n = length(z);
            %   L = n * log((alpha - 1) / xmin) - alpha .* sum(log(z ./ xmin))

            [nll, acov] = shared.likefunc_mydistrib(@prob.BoundedParetoDistribution.pdffunc, @prob.BoundedParetoDistribution.cdffunc,[], varargin{:});
        end

        function y = cdffunc(x, alpha, xmin, xmax) % cumulative distribution function
            %numerical cdf using the pdf
            if isempty(x)
                y = [];
                return;
            end
            if alpha == 1
                y = zeros(size(x));
            else
                beta = alpha - 1;
                y = (1 - xmin.^beta .* x.^ -beta) ./ (1 - (xmin ./ xmax).^beta);
                y(x < xmin) = 0;
                y(x > xmax) = 1;
            end
        end
 
        function y = pdffunc(x, alpha, xmin, xmax) % probability density function has to be specified
            if alpha == 1
                y = zeros(size(x));
            else
                y = -(xmin.^(alpha - 1) .* (alpha - 1)) ./ (x.^alpha .* ((xmin / xmax).^(alpha - 1) - 1));
                y(x < xmin | x > xmax) = 1E-300;
            end
        end
 
        function y = invfunc(p, alpha, xmin, xmax) % inverse cdf
            if isempty(p)
                y = [];
                return;
            end
            %                         ran = prob.BoundedParetoDistribution.GetRange(varargin);
            %                         xs = linspace(ran(1), ran(2), 1000);
            %                         numpdf = prob.BoundedParetoDistribution.pdffunc(xs, varargin{:});
            %                         cumcdf = cumtrapz(xs, numpdf);
            %                         [~, ndx] = unique(cumcdf);
            %                         y = interp1(cumcdf(ndx), xs(ndx), p);
            %                         y(p < 0 | 1 < p) = NaN;
            %                         a=varargin{1};
            %             y = (sqrt(2) .* sqrt(-log(1 - p))) ./ sqrt(a);
            %             y(p < 0 | 1 < p) = NaN;
            beta = alpha - 1;
            y = 1 ./ (1 ./ xmin.^beta + (p .* ((xmin ./ xmax).^beta - 1)) / xmin.^beta).^(1 ./ beta);

   
        end
 
        function y = randfunc(alpha, xmin, xmax, varargin) % random number generator
            y = prob.BoundedParetoDistribution.invfunc(rand(varargin{:}), alpha, xmin, xmax);
        end

        function ran = GetRange(cpar, minpdf, info)
            ran = prob.BoundedParetoDistribution.invfunc([0, 1 - 1E-7], cpar{:});
            % 
            %             if nargin < 3
            %                 info = prob.BoundedParetoDistribution.getInfo;
            %             end
            %             if nargin < 2
            %                 minpdf = 1E-100;
            %             end
            %             ran = info.support;
            %             if isinf(ran(1))
            %                 x = -100: -100: -100000;
            %                 pdf = prob.BoundedParetoDistribution.pdffunc(x, cpar{:});
            %                 ran(1) = x(find(pdf < minpdf, 1));
            %             end
            %             if isinf(ran(2))
            %                 x = 100:100:1000000;
            %                 pdf = prob.BoundedParetoDistribution.pdffunc(x, cpar{:});
            %                 ran(2) = x(find(pdf < minpdf, 1));
            %             end
        end
    end
    methods(Static, Hidden)
        % All ToolboxDistributions must implement a getInfo static method
        % so that Statistics and Machine Learning Toolbox functions can get information about
        % the distribution.
        function info = getInfo
 
            % First get default info from parent class
            info = getInfo@prob.ToolboxFittableParametricDistribution('prob.Linear');
 
            % Then override fields as necessary
            info.name = 'BoundedPareto';
            info.code = 'boundedpareto';
            info.pnames = prob.BoundedParetoDistribution.ParameterNames;
            info.pdescription = prob.BoundedParetoDistribution.ParameterDescription;

            % info.pnames is obtained from the ParameterNames property
            % info.pdescription is obtained from the ParameterDescription property
            info.likefunc = @prob.BoundedParetoDistribution.likefunc;
            info.cdffunc = @prob.BoundedParetoDistribution.cdffunc;
            info.pdffunc = @prob.BoundedParetoDistribution.pdffunc;
            info.invfunc = @prob.BoundedParetoDistribution.invfunc;
            info.randfunc = @prob.BoundedParetoDistribution.randfunc;
            info.prequired = [false false true]; % Change if any parameter must
            %          be specified before fitting.
            %          An example would be the N
            %          parameter of the binomial
            %          distribution.
            % info.hasconfbounds = false     % Set to true if the cdf and
            %          icdf methods can return
            %          lower and upper bounds as
            %          their 2nd and 3rd outputs.
            info.censoring = true; % Set to true if the fit
            %          method supports censoring.
            info.support = [0, Inf];
            %          Set to other lower and upper
            %          bounds if the distribution
            %          doesn't cover the whole real
            %          line. For example, for a
            %          distribution on positive
            %          values use [0, Inf].
            % info.closedbound = [false false] % Set the Jth value to
            %          true if the distribution
            %          allows x to be equal to the
            %          Jth element of the support
            %          vector.
            % info.iscontinuous = true       % Set to false if x can take
            %          only integer values.
            info.islocscale = true; % Set to true if this is a
            %          location/scale distribution
            %          (no other parameters).
            % info.uselogpp = false          % Set to true if a probability
            %          plot should be drawn on the
            %          log scale.
            
            info.fittable = true;
            info.optimopts = true; % Set to true if the fit
            %          method can be called with an
            %          options structure.
            info.logci = [false false false]; % Set to true for a parameter
            %          that should have its Wald
            %          confidence interval computed
            %          using a normal approximation
            %          on the log scale.
            info.plim = [1, 1E-30 1E-30; Inf, Inf, Inf];
        end
        function pd = makeFitted(par, nll, cov, x, cens, freq)
            cpar = num2cell(par);
            pd = prob.BoundedParetoDistribution(cpar{:});
            pd.NegativeLogLikelihood = nll;
            pd.ParameterCovariance = cov;

            pd.ParameterIsFixed = false(size(par));
            pd.InputData = struct('data', x, 'cens', cens, 'freq', freq);
        end
    end
end % classdef

% The following utilities check for valid parameter values
function checkargs(alpha, xmin, xmax)
    if ~(isscalar(alpha) && isnumeric(alpha) && isreal(alpha) && isfinite(alpha))
        error('alpha must be a positive finite numeric scalar.')
    end
    if ~(isscalar(xmin) && isnumeric(xmin) && isreal(xmin) && xmin > 0 && isfinite(xmin))
        error('xmin must be a positive finite numeric scalar.')
    end
    if ~(isscalar(xmax) && isnumeric(xmax) && isreal(xmax) && xmax > 0 && isfinite(xmax))
        error('xmax must be a positive finite numeric scalar.')
    end
    if xmax < xmin
        error('xmax must be a larger than xmin.')
    end
end
