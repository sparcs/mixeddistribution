classdef HumpDistribution < prob.ToolboxFittableParametricDistribution
    %
    %    An object of the FlexHazardDistribution class represents a probability 
    %    distribution of survival analysis. The distribution based on a 
    %    saturating hazard function h(t): 
    %       h(t)=c*t/(t + h)
    %    The survival function S is the solution of the following
    %    differential equation with x(0)=1:
    %        dx/dt=-(c*t/(t + h))
    %        S=exp(c*h*log(h + t) - c*t-c*h*log(h))
    %    The pdf of the distribution is S times the hazard function
    %
    %    The parameter C defines the maximum hazard. 
    %    The parameter H defines the half-saturation constant of the hazard function. 
    %    The parameter R is the constant base level of hazard
    %    This distribution object can be created directly
    %    using the MAKEDIST function or fit to data using the FITDIST function.    
    %
    %
    %    HumpDistribution methods:
    %       cdf                   - Cumulative distribution function
    %       fit                   - Fit distribution to data
    %       icdf                  - Inverse cumulative distribution function
    %       iqr                   - Interquartile range
    %       mean                  - Mean
    %       median                - Median
    %       paramci               - Confidence intervals for parameters
    %       pdf                   - Probability density function
    %       proflik               - Profile likelihood function
    %       random                - Random number generation
    %       std                   - Standard deviation
    %       truncate              - Truncation distribution to an interval
    %       var                   - Variance
    %
    %    LaplaceDistribution properties:    
    %       DistributionName      - Name of the distribution
    %       C                     - Value of the maximum hazard
    %       H                     - Value of the half-saturation constant
    %       R                     - Value of the constant hazard
    %       NumParameters         - Number of parameters
    %       ParameterNames        - Names of parameters
    %       ParameterDescription  - Descriptions of parameters
    %       ParameterValues       - Vector of values of parameters
    %       Truncation            - Two-element vector indicating truncation limits
    %       IsTruncated           - Boolean flag indicating if distribution is truncated
    %       ParameterCovariance   - Covariance matrix of estimated parameters
    %       ParameterIsFixed      - Two-element boolean vector indicating fixed parameters
    %       InputData             - Structure containing data used to fit the distribution
    %       NegativeLogLikelihood - Value of negative log likelihood function
    %
    %    See also fitdist, makedist.

    % All ProbabilityDistribution objects must specify a DistributionName
    properties (Constant = true)
        %DistributionName Name of distribution
        %    DistributionName is the name of this distribution.
        DistributionName = 'Hump';

    end
    
    properties (Dependent = true)
        c
        h
        g
        r
    end
    
    % All ParametricDistribution objects must specify values for the following
    % constant properties (they are the same for all instances of this class).


    % All ParametricDistribution objects must include a ParameterValues property
    % whose value is a vector of the parameter values, in the same order as
    % given in the ParameterNames property above.
    properties(GetAccess = 'public', SetAccess = 'protected')
        %ParameterValues Values of the distribution parameters
        %    ParameterValues is a two-element vector containing the mu and sigma
        %    values of this distribution.
        ParameterValues
    end

    methods
        % The constructor for this class can be called with a set of parameter
        % values or it can supply default values. These values should be
        % checked to make sure they are valid. They should be stored in the
        % ParameterValues property.
        function pd = HumpDistribution(varargin)
            if nargin == 0
                varargin = num2cell(pd.typicalpars);
            end
            if numel(varargin) ~= pd.NumParameters
                error('MixedDistribution:NumParameters', 'Wrong number of parameters');
            end
            pd.ParameterValues = [varargin{:}];
            if any(pd.ParameterValues < pd.plim(1, :)) || any(pd.ParameterValues > pd.plim(2, :))
                error('MixedDistribution:NumParameters', 'Parameter out of the correct range');
            end
            pd.ParameterIsFixed = true(1, pd.NumParameters);
            pd.ParameterCovariance = zeros(pd.NumParameters);
        end
 
        
        % Implement methods to compute the mean, variance, and standard
        % deviation.
        function m = mean(this)
            %numerical mean using the pdf
            info = this.getInfo;
            m = integral(@(x)x.*this.pdf(x), info.support(1), info.support(2));
        end
        
        function s = std(this)
            s = sqrt(this.var);
        end
        
        function v = var(this)
            info = this.getInfo;
            m = this.mean;
            v = integral(@(x)(x - m).^2 .*this.pdf(x), info.support(1), info.support(2));
        end
 
        function haz = hazard(this, x)
            cpar = num2cell(this.ParameterValues);
            haz = prob.HumpDistribution.HazardFunc(x, cpar{:});
        end
        
        function this = set.c(this, c)
            plim1 = this.plim(:, 1);
            if c < plim1(1) || c > plim1(2)
                error('HumpDistribution:validate', 'Parameter outside range');
            end
            this.ParameterValues(1) = c;
            this = invalidateFit(this);
        end
        
        function this = set.h(this, h)
            plim1 = this.plim(:, 2);
            if h < plim1(1) || h > plim1(2)
                error('HumpDistribution:validate', 'Parameter outside range');
            end
            this.ParameterValues(2) = h;
            this = invalidateFit(this);
        end
        
        function this = set.g(this, g)
            plim1 = this.plim(:, 3);
            if g < plim1(1) || g > plim1(2)
                error('HumpDistribution:validate', 'Parameter outside range');
            end
            this.ParameterValues(3) = g;
            this = invalidateFit(this);
        end
       
        function this = set.r(this, r)
            plim1 = this.plim(:, 4);
            if r < plim1(1) || r > plim1(2)
                error('HumpDistribution:validate', 'Parameter outside range');
            end
            this.ParameterValues(4) = r;
            this = invalidateFit(this);
        end
       
  
        function c = get.c(this)
            c = this.ParameterValues(1);
        end
        function h = get.h(this)
            h = this.ParameterValues(2);
        end
        function g = get.g(this)
            g = this.ParameterValues(3);
        end
        function r = get.r(this)
            r = this.ParameterValues(4);
        end
     
    end

    methods(Static)
        % All FittableDistribution classes must implement a fit method to fit
        % the distribution from data. This method is called by the FITDIST
        % function, and is not intended to be called directly
        function pd = fit(x, varargin)
            pd = shared.fit_mle(@prob.HumpDistribution.likefunc,  ...
                @prob.HumpDistribution.makeFitted,  ...
                prob.HumpDistribution.getInfo.plim, prob.HumpDistribution.typicalpars,  ...%typical parameter set
                x, varargin{:});
        end


        % The following static methods are required for the
        % ToolboxParametricDistribution class and are used by various
        % Statistics and Machine Learning Toolbox functions. These functions operate on
        % parameter values supplied as input arguments, not on the
        % parameter values stored in a LaplaceDistribution object. For
        % example, the cdf method implemented in a parent class invokes the
        % cdffunc static method and provides it with the parameter values.
        function [nll, acov] = likefunc(varargin) % likelihood function
            [nll, acov] = likefunc_withhaz(@prob.HumpDistribution.HazardFunc,  ...
                @prob.HumpDistribution.cdffunc, varargin{:});
            %              [nll, acov] = shared.likefunc_mydistrib(@prob.HumpDistribution.pdffunc,  ...
            %                 @prob.HumpDistribution.cdffunc, varargin{:});
        end

 
        function F = cdffunc(x, varargin) % cumulative distribution function
            %numerical cdf using the pdf
            %1-solution of x'=-(r+c*t/(t+h))*x
            if isempty(x)
                F = [];
                return;
            end
            function res = cdffun(t, c, h_t, g)
                res = -exp((c .* log(h_t + t + g .* t.^2) .* (-1.0 ./ 2.0)) ./ (g - g.^2 .* h_t .* 4.0)) .*  ...
                    exp((c .* atan(1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0) .* (g .* t .* 2.0 + 1.0)) .* 1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) ./ g) .*  ...
                    exp(-(c .* atan(1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) .* 1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) ./ g) .*  ...
                    exp((c .* h_t .* log(h_t) .* 2.0) ./ (g .* h_t .* 4.0 - 1.0)) .* exp((c .* log(h_t)) ./ (g .* 2.0 - g.^2 .* h_t .* 8.0))  ...
                    .* exp((c .* h_t .* log(h_t + t + g .* t.^2) .* -2.0) ./ (g .* h_t .* 4.0 - 1.0)) + 1.0;
            end
            % F = cdffun(x, varargin{:});
            %if any(isnan(F))
            F = 1 - exp(-prob.HumpDistribution.IntHazardFunc(x, varargin{:}));
            %end
            F(isinf(x)) = 1;
            F(x < 0) = 0;
            F(F < 0) = 0;
            F(F >= 1 - 1E-10) = 1 - 1E-10;
        end
 
        function f = pdffunc(x, varargin) % probability density function has to be specified
            %essential to combine both exp() functions, as the separate
            %factors can become infitine. Do not simplify the equation!
            %exp(c.*h.*log(h + x) - r.*x - c.*x).*exp(-c.*h.*log(h)).*(c + r - (c*h)./(h + x))
            %y = 1E-300 + exp(c .* h .* ln(h + x) - c .* x - c .* h .* ln(h)) .* (c - (c .* h) ./ (h + x));
            function res = pdffun(t, c, h_t, g)
                res = (c .* exp((c .* log(h_t + t + g .* t.^2) .* (-1.0 ./ 2.0)) ./ (g - g.^2 .* h_t .* 4.0)) .*  ...
                    exp((c .* atan(1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0) .* (g .* t .* 2.0 + 1.0)) .* 1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) ./ g) .*  ...
                    exp(-(c .* atan(1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) .* 1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) ./ g) .*  ...
                    exp((c .* h_t .* log(h_t) .* 2.0) ./ (g .* h_t .* 4.0 - 1.0)) .* exp((c .* log(h_t)) ./ (g .* 2.0 - g.^2 .* h_t .* 8.0)) .*  ...
                    exp((c .* h_t .* log(h_t + t + g .* t.^2) .* -2.0) ./ (g .* h_t .* 4.0 - 1.0)) .* -2.0) ./ ((g .* h_t .* 4.0 - 1.0) .*  ...
                    ((g .* t .* 2.0 + 1.0).^2 ./ (g .* h_t .* 4.0 - 1.0) + 1.0)) + (c .* exp((c .* log(h_t + t + g .* t.^2) .*  ...
                    (-1.0 ./ 2.0)) ./ (g - g.^2 .* h_t .* 4.0)) .* exp((c .* atan(1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0) .* (g .* t .* 2.0 + 1.0)) .*  ...
                    1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) ./ g) .* exp(-(c .* atan(1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) .*  ...
                    1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) ./ g) .* exp((c .* h_t .* log(h_t) .* 2.0) ./ (g .* h_t .* 4.0 - 1.0))  ...
                    .* exp((c .* log(h_t)) ./ (g .* 2.0 - g.^2 .* h_t .* 8.0)) .* exp((c .* h_t .* log(h_t + t + g .* t.^2) .* -2.0)  ...
                    ./ (g .* h_t .* 4.0 - 1.0)) .* (g .* t .* 2.0 + 1.0)) ./ ((g - g.^2 .* h_t .* 4.0) .* (h_t + t + g .* t.^2) .* 2.0)  ...
                    +(c .* h_t .* exp((c .* log(h_t + t + g .* t.^2) .* (-1.0 ./ 2.0)) ./ (g - g.^2 .* h_t .* 4.0)) .*  ...
                    exp((c .* atan(1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0) .* (g .* t .* 2.0 + 1.0)) .* 1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) ./ g) .*  ...
                    exp(-(c .* atan(1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) .* 1.0 ./ sqrt(g .* h_t .* 4.0 - 1.0)) ./ g) .*  ...
                    exp((c .* h_t .* log(h_t) .* 2.0) ./ (g .* h_t .* 4.0 - 1.0)) .* exp((c .* log(h_t)) ./ (g .* 2.0 - g.^2 .* h_t .* 8.0))  ...
                    .* exp((c .* h_t .* log(h_t + t + g .* t.^2) .* -2.0) ./ (g .* h_t .* 4.0 - 1.0)) .* (g .* t .* 2.0 + 1.0) .* 2.0)  ...
                    ./ ((g .* h_t .* 4.0 - 1.0) .* (h_t + t + g .* t.^2));
            end
            %f = 1E-300 + pdffun(x, varargin{:});
            %if any(isnan(f)) || any(f <= 0)
            f = 1E-300 + exp(-prob.HumpDistribution.IntHazardFunc(x, varargin{:})) .* prob.HumpDistribution.HazardFunc(x, varargin{:});
            %end
            f(f <= 0) = 1E-300;
        end
 
        function y = invfunc(p, varargin) % inverse cdf
            y = numinvfunc(@(x)prob.HumpDistribution.cdffunc(x, varargin{:})...
                , [0 inf], p, varargin);
        end

        
        function y = randfunc(varargin) % random number generator
            n = prob.HumpDistribution.NumParameters;
            y = prob.HumpDistribution.invfunc(rand(varargin{n + 1:end}), varargin{1:n});
        end

        function ran = GetRange(cpar, minpdf, info)
            if nargin < 3
                info = prob.HumpDistribution.getInfo;
            end
            if nargin < 2
                minpdf = 1E-100;
            end
            ran = info.support;
            if isinf(ran(1))
                x = -100: -100: -100000;
                pdf = prob.HumpDistribution.pdffunc(x, cpar{:});
                ran(1) = x(find(pdf < minpdf, 1));
            end
            if isinf(ran(2))
                x = 100:100:100000;
                pdf = prob.HumpDistribution.pdffunc(x, cpar{:});
                ran(2) = x(find(pdf < minpdf, 1));
            end
        end
    end
    methods(Static, Hidden)
        % All ToolboxDistributions must implement a getInfo static method
        % so that Statistics and Machine Learning Toolbox functions can get information about
        % the distribution.
        function info = getInfo
            
            % First get default info from parent class
            info = getInfo@prob.ToolboxFittableParametricDistribution('prob.Hump');
            
            % Then override fields as necessary
            info.name = 'Hump';
            info.code = 'hump';
 
            info.pnames = prob.HumpDistribution.ParameterNames;
            info.pdescription = prob.HumpDistribution.ParameterDescription;

            % info.pnames is obtained from the ParameterNames property
            % info.pdescription is obtained from the ParameterDescription property
            info.prequired = true(1, numel(prob.HumpDistribution.typicalpars)); % Change if any parameter must
            info.likefunc = @prob.HumpDistribution.likefunc;
            info.cdffunc = @prob.HumpDistribution.cdffunc;
            info.pdffunc = @prob.HumpDistribution.pdffunc;
            info.invfunc = @prob.HumpDistribution.invfunc;
            info.randfunc = @prob.HumpDistribution.randfunc;
            %          be specified before fitting.
            %          An example would be the N
            %          parameter of the binomial
            %          distribution.
            % info.hasconfbounds = false     % Set to true if the cdf and
            %          icdf methods can return
            %          lower and upper bounds as
            %          their 2nd and 3rd outputs.
            info.censoring = true; % Set to true if the fit
            %          method supports censoring.
            info.support = [0, Inf];
            %          Set to other lower and upper
            %          bounds if the distribution
            %          doesn't cover the whole real
            %          line. For example, for a
            %          distribution on positive
            %          values use [0, Inf].
            % info.closedbound = [false false] % Set the Jth value to
            %          true if the distribution
            %          allows x to be equal to the
            %          Jth element of the support
            %          vector.
            % info.iscontinuous = true       % Set to false if x can take
            %          only integer values.
            info.islocscale = true; % Set to true if this is a
            %          location/scale distribution
            %          (no other parameters).
            % info.uselogpp = false          % Set to true if a probability
            %          plot should be drawn on the
            %          log scale.
 
            info.fittable = true;
            info.optimopts = true; % Set to true if the fit
            %          method can be called with an
            %          options structure.
            info.logci = false(1, numel(prob.HumpDistribution.typicalpars)); % Set to true for a parameter
            %          that should have its Wald
            %          confidence interval computed
            %          using a normal approximation
            %          on the log scale.
            info.plim = prob.HumpDistribution.plim;
            %info.typicalpars = prob.HumpDistribution.typicalpars;
        end
        function pd = makeFitted(par, nll, cov, x, cens, freq)
            cpar = num2cell(par);
            pd = prob.HumpDistribution(cpar{:});
            pd.NegativeLogLikelihood = nll;
            pd.ParameterCovariance = cov;
            pd.ParameterIsFixed = false;
            pd.ParameterIsFixed = false(size(par));
            pd.InputData = struct('data', x, 'cens', cens, 'freq', freq);
        end
    end
    %ADAPT THIS FOR YOUR DISTRIBUTION
    %
 
    properties (Constant = true)
        %the limits for the parameters
        distname = 'Hump';
        plim = [0, 0, 0, 0; Inf, Inf, Inf, Inf];
        %typical parameter values
        typicalpars = [0.002 25 0.001, 0.001];
        %Parameter Names
        ParameterNames = {'c', 'h', 'g', 'r'};
        %the limits for the parameters
        %          plim = [0, ; Inf];
        %          %typical parameter values
        %          typicalpars = [0.01];
        %          %Parameter Names
        %          ParameterNames = {'r'};
        % r = 1E-8;

 
        %(you can leave this unchanged)
        NumParameters = length(prob.HumpDistribution.ParameterNames);
        ParameterDescription = prob.HumpDistribution.ParameterNames;
    end
 
    methods (Static, Hidden)

        % The following utilities needs to be adapted
        function res = HazardFunc(x, c, h, g, r)
            %*** enter here the Hazard function
            res = c .* x ./ (h + x) .* exp (-g .* x) + r;
            %we add a tiny constant to prevent that max(cdf)<1
            %res=zeros(size(x))+c;
        end
        
        function res = IntHazardFunc(x, varargin)
            %*** enter here the integral of the Hazard function (or use this code
            %for numerical approximation
            function res1 = IntHaz(t, c, h, g, r)
                %function includes the exponential integral Ei (expint)
                %Ei = integral from x to Inf of (exp(-t)/t) dt, for x > 0.
                %matlab's expint uses a fast algoritm: series of ca 20
                %iterations
                if g == 0
                    %g==0 gives nan as expint(0) = Inf
                    res1 = c .* t - c .* h .* log(h + t) + r .* t;
                else
                    %plus tiny constant
                    res1 = -(c .* exp(-g .* t)) ./ g + c .* h .* expint(g .* h + g .* t) .* exp(g .* h) + r .* t;
                end
            end
            res = IntHaz(x, varargin{:}) - IntHaz(0, varargin{:});
 
 
            if any(isnan(res))
                %ca. 7x slower, but better with numerical overflow
                res = shared.integrate_ode45(@prob.HumpDistribution.HazardFunc, x, [], varargin{:});
            end
        end
       
    end
end % classdef
