classdef SkewNormalDistribution < prob.ToolboxFittableParametricDistribution
    %
    %    An object of the SkewNormalDistribution class represents a
    %    Skew Normal probability distribution with a specific location parameter MU and
    %    scale parameter SIGMA and a skewness paramter LAMBDA. 
    %    This distribution is in fact a normal distribution that is made
    %    skew using the following trick:
    %    PDF(x) = normalPDF(x) * weighted normalCDF(x*lambda)
    %    PDF(x) = normalPDF(x) *(normalCDF(x*lambda).*normalPDF(x))
    %    PDF(x) = 2*normalPDF(x)* normalCDF(x*lambda)
    %    Note that at the average of the normal distribution normalCDF=0.5
    %    so with lambda=0 this equation reduces to a normal distribution.
    %
    %    This distribution object can be created directly
    %    using the MAKEDIST function or fit to data using the FITDIST function.
    %
    %    SkewNormalDistribution methods:
    %       cdf                   - Cumulative distribution function
    %       fit                   - Fit distribution to data
    %       icdf                  - Inverse cumulative distribution function
    %       iqr                   - Interquartile range
    %       mean                  - Mean
    %       median                - Median
    %       paramci               - Confidence intervals for parameters
    %       pdf                   - Probability density function
    %       proflik               - Profile likelihood function
    %       random                - Random number generation
    %       std                   - Standard deviation
    %       truncate              - Truncation distribution to an interval
    %       var                   - Variance
    %
    %    SkewNormalDistribution properties:    
    %       DistributionName      - Name of the distribution
    %       mu                    - Value of the location parameter
    %       sigma                 - Value of the standard deviation parameter
    %       lambda                - Value of the skewness parameter
    %       NumParameters         - Number of parameters
    %       ParameterNames        - Names of parameters
    %       ParameterDescription  - Descriptions of parameters
    %       ParameterValues       - Vector of values of parameters
    %       Truncation            - Two-element vector indicating truncation limits
    %       IsTruncated           - Boolean flag indicating if distribution is truncated
    %       ParameterCovariance   - Covariance matrix of estimated parameters
    %       ParameterIsFixed      - Two-element boolean vector indicating fixed parameters
    %       InputData             - Structure containing data used to fit the distribution
    %       NegativeLogLikelihood - Value of negative log likelihood function
    %
    %    See also fitdist, makedist.

    % All ProbabilityDistribution objects must specify a DistributionName
    properties(Constant)
        %DistributionName Name of distribution
        %    DistributionName is the name of this distribution.
        DistributionName = 'SkewNormal';
    end

    % Optionally add your own properties here. For this distribution it's convenient
    % to be able to refer to the mu and sigma parameters by name, and have them
    % connected to the proper element of the ParameterValues property. These are
    % dependent properties because they depend on ParameterValues.
    properties(Dependent = true)
        %R is exponential decay R=constant factor
        mu
 
        %A Linear growth parameter A= growth factor
        sigma
        
        lambda
    end
 
    % All ParametricDistribution objects must specify values for the following
    % constant properties (they are the same for all instances of this class).
    properties(Constant)
        %NumParameters Number of parameters
        %    NumParameters is the number of parameters in this distribution.

        NumParameters = 3;
 
        %ParameterName Name of parameter
        %    ParameterName is a two-element cell array containing names
        %    of the parameters of this distribution.
        ParameterNames = {'mu' 'sigma' 'lambda'};
 
        %ParameterDescription Description of parameter
        %    ParameterDescription is a two-element cell array containing
        %    descriptions of the parameters of this distribution.
        ParameterDescription = { 'location', 'scale', 'shape'};
    end

    % All ParametricDistribution objects must include a ParameterValues property
    % whose value is a vector of the parameter values, in the same order as
    % given in the ParameterNames property above.
    properties(GetAccess = 'public', SetAccess = 'protected')
        %ParameterValues Values of the distribution parameters
        %    ParameterValues is a two-element vector containing the mu and sigma
        %    values of this distribution.
        ParameterValues
    end

    methods
        % The constructor for this class can be called with a set of parameter
        % values or it can supply default values. These values should be
        % checked to make sure they are valid. They should be stored in the
        % ParameterValues property.
        function pd = SkewNormalDistribution(mu, sigma, lambda)
            if nargin == 0
                mu = 0;
                sigma = 1;
                lambda = 0;
            end
            checkargs(mu, sigma, lambda);
 
            pd.ParameterValues = [mu sigma lambda];
 
            % All FittableParametricDistribution objects must assign values
            % to the following two properties. When an object is created by
            % the constructor, all parameters are fixed and the covariance
            % matrix is entirely zero.
            pd.ParameterIsFixed = [true true true];
            pd.ParameterCovariance = zeros(pd.NumParameters);
        end
        
 
        % Implement methods to compute the mean, variance, and standard
        % deviation.
        function m = mean(this)
            %numerical mean using the pdf
            % info = this.getInfo;
            % m = integral(@(x)x.*this.pdf(x), info.support(1), info.support(2));
            m = this.mu + this.sigma * (this.lambda / sqrt(1 + this.lambda^2)) * sqrt(2 / pi);
        end
 
        function s = std(this)
            s = sqrt(this.var);
        end
        function v = var(this)
            %             info = this.getInfo;
            %             m = this.mean;
            %             v = integral(@(x)(x - m).^2 .*this.pdf(x), info.support(1), info.support(2));
            delta = this.lambda / sqrt(1 + this.lambda^2);
            v = this.sigma.^2 .* (1 - 2 / pi * delta^2);
        end
        
        function v = skewness(this)
            delta = this.lambda / sqrt(1 + this.lambda^2);
            v = (4 - pi) / 2 * (delta * sqrt(2 / pi))^3 / (1 - 2 * delta^2 / pi)^(3 / 2);
        end
 
    end
    methods
        % If this class defines dependent properties to represent parameter
        % values, their get and set methods must be defined. The set method
        % should mark the distribution as no longer fitted, because any
        % old results such as the covariance matrix are not valid when the
        % parameters are changed from their estimated values.
        function this = set.mu(this, mu)
            checkargs(mu, this.sigma, this.lambda);
            this.ParameterValues(1) = mu;
            this = invalidateFit(this);
        end
        function this = set.sigma(this, sigma)
            checkargs(this.mu, sigma, this.lambda);
            this.ParameterValues(2) = sigma;
            this = invalidateFit(this);
        end
        function this = set.lambda(this, lambda)
            checkargs(this.mu, this.sigma, lambda);
            this.ParameterValues(3) = lambda;
            this = invalidateFit(this);
        end
        function alpha = get.mu(this)
            alpha = this.ParameterValues(1);
        end
        function beta = get.sigma(this)
            beta = this.ParameterValues(2);
        end
        function lambda = get.lambda(this)
            lambda = this.ParameterValues(3);
        end
    end
    methods(Static)
        % All FittableDistribution classes must implement a fit method to fit
        % the distribution from data. This method is called by the FITDIST
        % function, and is not intended to be called directly
        function pd = fit(x, varargin)
            if nargin == 2 && isstruct(varargin{1})
                aa = varargin{1};
                varargin = [fieldnames(aa)'; struct2cell(aa)'];
                varargin = varargin(:)';
            end
            options = reshape(varargin, 2, length(varargin) / 2);
            ndx=strcmpi('options', options(1,:));
            if any(ndx)
                opt=options{2,ndx};
                domomentfit= isfield(opt,'method')&& strcmpi(opt.method, 'moment');
            else
                ndx = strncmpi(options(1, :), 'cens', 4);
                if any(ndx)
                    cens=options{2,:};
                    domomentfit=~isempty(cens)||any(cens);
                else
                    domomentfit = false;
                end
            end
            if ~domomentfit
                 pd = shared.fit_mle(@prob.SkewNormalDistribution.likefunc,  ...
                @prob.SkewNormalDistribution.makeFitted,  ...
                prob.SkewNormalDistribution.getInfo.plim, [mean(x), std(x) rand(1)-0.5],  ...%typical parameter set
                x, varargin{:});
            else
                %faster but not better usually
                pd = prob.SkewNormalDistribution.momentfit(x, varargin{:});
            end
        end
        function pd = momentfit(x, varargin)
            %FIT Fit from data using moments method
            %   P = prob.LaplaceDistribution.fit(x)
            %   P = prob.LaplaceDistribution.fit(x, NAME1,VAL1, NAME2,VAL2, ...)
            %   with the following optional parameter name/value pairs:
            %
            %          'start'
            %          'censoring'    not supported
            %          'frequency'    Vector indicating frequencies of corresponding
            %                         x values
            %          'options'      Options structure for fitting, as create by
            %                         the STATSET function

            if nargin == 2 && isstruct(varargin{1})
                aa = varargin{1};
                varargin = [fieldnames(aa)'; struct2cell(aa)'];
                varargin = varargin(:)';
            else
                options = varargin(1:2:end);
            end
            freq = [];
            if any(strcmpi(options, 'frequency'))
                freq = varargin{find(strcmp(options, 'frequency'), 1) * 2 - 1};
            elseif any(strcmpi(options, 'freq'))
                ndx = find(strcmp(options, 'freq'), 1) * 2 - 1;
                varargin{ndx} = 'frequency';
                freq = varargin{ndx + 1};
            end

            if ~isempty(freq)
                if max(freq) == 1
                    x1 = x(freq == 1);
                else
                    x = x(:);
                    x1 = [];
                    for i = 1:max(freq)
                        x2 = x(freq == i);
                        x2 = repmat(x2, i, 1);
                        x1 = [x1; x2]; %#ok<AGROW>
                    end
                end
                %ndx = freq / max(freq) > 0.5;
                m = mean(x1);
                v = var(x1);
                s = skewness(x1);
            else
                m = mean(x);
                v = var(x);
                s = skewness(x);
            end
            if isnan(s) && v == 0
                if isnan(m)
                    m = 0;
                end
                v = 0.001;
                s = 0;
            end


            delta = sign(s) * sqrt(pi / 2 * abs(s)^(2 / 3) / (abs(s)^(2 / 3) + ((4 - pi) / 2)^(2 / 3)));
            %
            if delta == 1
                delta = 0.999;
            end
            lambda1 = sign(s) * abs((delta * sqrt(-(delta - 1) * (delta + 1))) / (delta^2 - 1));
            sigma1 = sqrt(abs( v / (1 - 2 / pi * delta^2)));
            mu1 = m - sqrt(2) * delta * sigma1 * sqrt(1 / pi);
 
            if isnan(mu1) || isnan(sigma1) || isnan(lambda1)
                
                mu1 = 0;
                sigma1 = 1;
                lambda1 = 0;
            end
            phat = [mu1, sigma1, lambda1];
            cov = [];
            % Create the distribution by calling the constructor.
            nll = prob.SkewNormalDistribution.likefunc(phat, x, false(size(freq)), freq);

            pd = prob.SkewNormalDistribution.makeFitted(phat, nll, cov, x, [], freq);


        end
%         function pd = mlefit(x, varargin)
%             %FIT Fit from data
%             %   P = prob.LaplaceDistribution.fit(x)
%             %   P = prob.LaplaceDistribution.fit(x, NAME1,VAL1, NAME2,VAL2, ...)
%             %   with the following optional parameter name/value pairs:
%             %
%             %          'start'
%             %          'censoring'    Boolean vector indicating censored x values
%             %          'frequency'    Vector indicating frequencies of corresponding
%             %                         x values
%             %          'options'      Options structure for fitting, as create by
%             %                         the STATSET function
% 
%             if nargin == 2 && isstruct(varargin{1})
%                 aa = varargin{1};
%                 varargin = [fieldnames(aa)'; struct2cell(aa)'];
%                 varargin = varargin(:)';
%             else
%                 options = varargin(1:2:end);
%             end
%             freq = [];
%             if any(strcmpi(options, 'frequency'))
%                 freq = varargin{find(strcmp(options, 'frequency'), 1) * 2 - 1};
%             elseif any(strcmpi(options, 'freq'))
%                 ndx = find(strcmp(options, 'freq'), 1) * 2 - 1;
%                 varargin{ndx} = 'frequency';
%                 freq = varargin{ndx + 1};
%                 if isempty(freq)
%                     varargin{ndx + 1} = ones(size(x));
%                 end
%             end
%             if isempty(freq)
%                 freq = ones(size(x));
%             end
%             if any(strncmpi(options, 'cens', 4))
%                 ndx = find(strncmpi(options, 'cens', 4), 1) * 2 - 1;
%                 ndx1 = true(size(varargin));
%                 ndx1(ndx:ndx + 1) = false;
%                 varargin = varargin(ndx1);
%             end
%             if ~any(strcmp(options, 'start'))
%                 pd1 = prob.SkewNormalDistribution.momentfit(x, varargin{:});
%                 varargin = [varargin {'start', pd1.ParameterValues}];
%             end
%             if ~any(strcmp(options, 'lowerbound'))
%                 varargin = [varargin {'lowerbound', [-inf 0 -inf]}];
%             end
%             %if ~any(strcmp(options, 'upperbound'))
%             % varargin = [varargin {'upperbound', [0.1 1000]}];
%             %end
%   
%             try
%                 %phat = prob.SkewNormalDistribution.moments_fit(x, freq);
%                 [phat] = mle(x, 'pdf', @prob.SkewNormalDistribution.pdffunc, varargin{:});
%             catch err
%                 if strcmp('stats:mle:NonpositivePdfVal', err.identifier)
%                     disp(err.message)
%                     phat = zeros(1, 3);
%                 else
%                     rethrow(err);
%                 end
%             end
%             cov = [];
% 
%             % Create the distribution by calling the constructor.
%             nll = prob.SkewNormalDistribution.likefunc(phat, x);
% 
%             pd = prob.SkewNormalDistribution.makeFitted(phat, nll, cov, x, [], freq);
% 
%         end

        % The following static methods are required for the
        % ToolboxParametricDistribution class and are used by various
        % Statistics and Machine Learning Toolbox functions. These functions operate on
        % parameter values supplied as input arguments, not on the
        % parameter values stored in a LaplaceDistribution object. For
        % example, the cdf method implemented in a parent class invokes the
        % cdffunc static method and provides it with the parameter values.
        function [nll, acov] = likefunc(varargin) % likelihood function
             [nll, acov] = shared.likefunc_mydistrib(@prob.SkewNormalDistribution.pdffunc,@prob.SkewNormalDistribution.cdffunc,[],varargin{:});           
        end
        function y = cdffunc(x, mu, sigma, lambda) % cumulative distribution function
            if isempty(x)
                y = [];
                return;
            end
            %analytical cdf using the owen's T function (prob.owen_t)
            z = (x - mu) ./ sigma;
            y = 0.5 * (1 + erf(z / sqrt(2))) - 2 * owen_t(z, lambda);
            y(x==-inf)=0;
            y(x==inf)=1;
        end

        function y = pdffunc(x, mu, sigma, lambda) % probability density function has to be specified
            %the idea is that a skew distrbution is the multiplication of
            %the 2*pdf(x)*cdf(lambda*x) note that cdf(0)=0.5
            % (here of normal distributions can also be other distributions around zero)
            % so with lambda=0 this reduces to a normal distribution
            %
            function res = Normpdf(x)
                res = 1 ./ sqrt(2 .* pi) .* exp(-x.^2 ./ 2);
            end
            function res = Normcdf(x)
                res = 0.5 .* (1 + erf(x ./ sqrt(2)));
            end
            y = 1E-300 + 2 ./ sigma .* Normpdf((x - mu) ./ sigma) .* Normcdf(lambda .* (x - mu) ./ sigma);
            % y = 1E-300 + exp(- lambda .* x - (alpha .* exp(beta .* x)) ./ beta) .* exp(alpha ./ beta) .* (lambda + alpha .* exp(beta .* x));

        end
        
        function y = invfunc(p, varargin) % inverse cdf
            y= numinvfunc(@(x)prob.SkewNormalDistribution.cdffunc(x, varargin{:})...
                ,[-inf inf], p, varargin);
        end


        function y = randfunc(mu, sigma, lambda, varargin) % random number generator
            %   y = prob.SkewNormalDistribution.invfunc(rand(varargin{:}), mu, sigma,lambda);
            % we use the approach https://www.researchgate.net/publication/251696468_Skew_scale_mixtures_of_normal_distributions_Properties_and_estimation
            % equation 2
            % two random normal variables
            t0 = randn(varargin{:});
            t1 = randn(varargin{:});
            delta = lambda ./ sqrt(1 + lambda.^2);
            y = mu + sigma .* (delta .* abs(t0) + t1 .* sqrt(1 - delta.^2));
        end

        function ran = GetRange(cpar, minpdf, info)
            if nargin < 3
                info = prob.SkewNormalDistribution.getInfo;
            end
            if nargin < 2
                minpdf = 1E-100;
            end
            ran = info.support;
            if isinf(ran(1)) && ~isinf(ran(2))
                x = -logspace(5, -3, 1000);
            elseif isinf(ran(2)) && ~isinf(ran(1))
                x1 = logspace(5, -3, 1000);
            else
                x = [-logspace(5, -3, 1000), 0, logspace(-3, 5, 1000)];
                x1 = fliplr(x);
            end
            if isinf(ran(1))
                pdf = prob.SkewNormalDistribution.pdffunc(x, cpar{:});
                ran(1) = x(find(pdf > minpdf, 1) - 1);
            end
            if isinf(ran(2))
                pdf = prob.SkewNormalDistribution.pdffunc(x1, cpar{:});
                ndx = pdf > minpdf;
                ran(2) = x1(find(ndx, 1) - 1);
            end

            %             ran = info.support;
            %             if isinf(ran(1))
            %                 x = -100: -100: -100000;
            %                 pdf = prob.SkewNormalDistribution.pdffunc(x, cpar{:});
            %                 ran(1) = x(find(pdf < minpdf, 1));
            %             end
            %             if isinf(ran(2))
            %                 x = 100:100:100000;
            %                 pdf = prob.SkewNormalDistribution.pdffunc(x, cpar{:});
            %                 ran(2) = x(find(pdf < minpdf, 1));
            %             end
        end
    end
    methods(Static, Hidden)
        % All ToolboxDistributions must implement a getInfo static method
        % so that Statistics and Machine Learning Toolbox functions can get information about
        % the distribution.
        function info = getInfo

            % First get default info from parent class
            info = getInfo@prob.ToolboxFittableParametricDistribution('prob.SkewNormal');

            % Then override fields as necessary
            info.name = 'SkewNormal';
            info.code = 'skewnormal';
            % info.pnames is obtained from the ParameterNames property
            % info.pdescription is obtained from the ParameterDescription property
            info.pnames=prob.SkewNormalDistribution.ParameterNames;
            info.pdescription=prob.SkewNormalDistribution.ParameterDescription;


            info.prequired = [true true, true]; % Change if any parameter must
            info.likefunc = @prob.SkewNormalDistribution.likefunc;
            info.cdffunc = @prob.SkewNormalDistribution.cdffunc;
            info.pdffunc = @prob.SkewNormalDistribution.pdffunc;
            info.invfunc = @prob.SkewNormalDistribution.invfunc;
            info.randfunc = @prob.SkewNormalDistribution.randfunc;
            %          be specified before fitting.
            %          An example would be the N
            %          parameter of the binomial
            %          distribution.
            % info.hasconfbounds = false     % Set to true if the cdf and
            %          icdf methods can return
            %          lower and upper bounds as
            %          their 2nd and 3rd outputs.
            info.censoring = true;              % Set to true if the fit
            %          method supports censoring.
            info.support = [-Inf, Inf] ;
            %          Set to other lower and upper
            %          bounds if the distribution
            %          doesn't cover the whole real
            %          line. For example, for a
            %          distribution on positive
            %          values use [0, Inf].
            % info.closedbound = [false false] % Set the Jth value to
            %          true if the distribution
            %          allows x to be equal to the
            %          Jth element of the support
            %          vector.
            % info.iscontinuous = true       % Set to false if x can take
            %          only integer values.
            info.islocscale = true; % Set to true if this is a
            %          location/scale distribution
            %          (no other parameters).
            % info.uselogpp = false          % Set to true if a probability
            %          plot should be drawn on the
            %          log scale.
    
            info.fittable = true;
            info.optimopts = true; % Set to true if the fit
            %          method can be called with an
            %          options structure.
            info.logci = [false false false]; % Set to true for a parameter
            %          that should have its Wald
            %          confidence interval computed
            %          using a normal approximation
            %          on the log scale.
            info.plim = [-Inf 0 -Inf; Inf Inf Inf];
        end
        function pd = makeFitted(par, nll, cov, x, cens, freq)
    
            cpar = num2cell(par);
            pd = prob.SkewNormalDistribution(cpar{:});
            pd.NegativeLogLikelihood = nll;
            pd.ParameterCovariance = cov;
          %  pd.ParameterIsFixed = false;
            pd.ParameterIsFixed = false(size(par));
            pd.InputData = struct('data', x, 'cens', cens, 'freq', freq);
        end
    end
end % classdef

% The following utilities check for valid parameter values
function checkargs(mu, sigma, lambda)
    if ~(isscalar(mu) && isnumeric(mu) && isreal(mu) && isfinite(mu))
        error('mu must be a finite numeric scalar.')
    end
    if ~(isscalar(sigma) && isnumeric(sigma) && isreal(sigma) && sigma >= 0 && isfinite(sigma))
        error('sigma must be a positive finite numeric scalar.')
    end
    if ~(isscalar(lambda) && isnumeric(lambda) && isreal(lambda) && isfinite(lambda))
        error('lambda must be a finite numeric scalar.')
    end
end
