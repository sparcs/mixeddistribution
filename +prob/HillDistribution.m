classdef HillDistribution < prob.ToolboxFittableParametricDistribution
    %
    %    An object of the HillDistribution class represents a probability 
    %    distribution of survival analysis. The distribution based on a 
    %    saturating hazard function h(t): 
    %       h(t)=c*t/(t + h)
    %    The survival function S is the solution of the following
    %    differential equation with x(0)=1:
    %        dx/dt=-(c*t/(t + h))
    %        S=exp(c*h*log(h + t) - c*t-c*h*log(h))
    %    The pdf of the distribution is S times the hazard function
    %
    %    The parameter C defines the maximum hazard. 
    %    The parameter H defines the half-saturation constant of the hazard function. 
    %    The parameter R is the constant base level of hazard
    %    This distribution object can be created directly
    %    using the MAKEDIST function or fit to data using the FITDIST function.    
    %
    %
    %    HillDistribution methods:
    %       cdf                   - Cumulative distribution function
    %       fit                   - Fit distribution to data
    %       icdf                  - Inverse cumulative distribution function
    %       iqr                   - Interquartile range
    %       mean                  - Mean
    %       median                - Median
    %       paramci               - Confidence intervals for parameters
    %       pdf                   - Probability density function
    %       proflik               - Profile likelihood function
    %       random                - Random number generation
    %       std                   - Standard deviation
    %       truncate              - Truncation distribution to an interval
    %       var                   - Variance
    %
    %    LaplaceDistribution properties:    
    %       DistributionName      - Name of the distribution
    %       C                     - Value of the maximum hazard
    %       H                     - Value of the half-saturation constant
    %       R                     - Value of the constant hazard
    %       NumParameters         - Number of parameters
    %       ParameterNames        - Names of parameters
    %       ParameterDescription  - Descriptions of parameters
    %       ParameterValues       - Vector of values of parameters
    %       Truncation            - Two-element vector indicating truncation limits
    %       IsTruncated           - Boolean flag indicating if distribution is truncated
    %       ParameterCovariance   - Covariance matrix of estimated parameters
    %       ParameterIsFixed      - Two-element boolean vector indicating fixed parameters
    %       InputData             - Structure containing data used to fit the distribution
    %       NegativeLogLikelihood - Value of negative log likelihood function
    %
    %    See also fitdist, makedist.

    % All ProbabilityDistribution objects must specify a DistributionName
    properties (Constant = true)
        %DistributionName Name of distribution
        %    DistributionName is the name of this distribution.
        DistributionName = 'Hill';

    end
    
    % All ParametricDistribution objects must specify values for the following
    % constant properties (they are the same for all instances of this class).


    % All ParametricDistribution objects must include a ParameterValues property
    % whose value is a vector of the parameter values, in the same order as
    % given in the ParameterNames property above.
    properties(GetAccess = 'public', SetAccess = 'protected')
        %ParameterValues Values of the distribution parameters
        %    ParameterValues is a two-element vector containing the mu and sigma
        %    values of this distribution.
        ParameterValues
    end
 

    methods
        % The constructor for this class can be called with a set of parameter
        % values or it can supply default values. These values should be
        % checked to make sure they are valid. They should be stored in the
        % ParameterValues property.
        function pd = HillDistribution(varargin)
            if nargin == 0
                varargin = num2cell(pd.typicalpars);
            end
            if numel(varargin)~=pd.NumParameters
                error('MixedDistribution:NumParameters','Wrong number of parameters');
            end
            pd.ParameterValues = [varargin{:}];
            if any(pd.ParameterValues<pd.plim(1,:))||any(pd.ParameterValues>pd.plim(2,:))
                error('MixedDistribution:NumParameters','Parameter out of the correct range');
            end            
            pd.ParameterIsFixed = true(1, pd.NumParameters);
            pd.ParameterCovariance = zeros(pd.NumParameters);
        end
 
        
        % Implement methods to compute the mean, variance, and standard
        % deviation.
        function m = mean(this)
            %numerical mean using the pdf
            info = this.getInfo;
            m = integral(@(x)x.*this.pdf(x), info.support(1), info.support(2));
        end
        
        function s = std(this)
            s = sqrt(this.var);
        end
        function v = var(this)
            info = this.getInfo;
            m = this.mean;
            v = integral(@(x)(x - m).^2 .*this.pdf(x), info.support(1), info.support(2));
        end
 
        function haz = hazard(this, x)
            cpar = num2cell(this.ParameterValues);
            haz = prob.HillDistribution.HazardFunc(x, cpar{:});
        end
      
        
    end

    methods(Static)
        % All FittableDistribution classes must implement a fit method to fit
        % the distribution from data. This method is called by the FITDIST
        % function, and is not intended to be called directly
         function pd = fit(x, varargin)
            pd = shared.fit_mle(@prob.HillDistribution.likefunc,  ...
                @prob.HillDistribution.makeFitted,  ...
                prob.HillDistribution.getInfo.plim, prob.HillDistribution.typicalpars,  ...%typical parameter set
                x, varargin{:});
        end
        
       
        % The following static methods are required for the
        % ToolboxParametricDistribution class and are used by various
        % Statistics and Machine Learning Toolbox functions. These functions operate on
        % parameter values supplied as input arguments, not on the
        % parameter values stored in a LaplaceDistribution object. For
        % example, the cdf method implemented in a parent class invokes the
        % cdffunc static method and provides it with the parameter values.
        function [nll, acov] = likefunc(varargin) % likelihood function
            [nll, acov] = likefunc_withhaz(@prob.HillDistribution.HazardFunc,  ...
                @prob.HillDistribution.cdffunc, varargin{:});
        end

 
        function F = cdffunc(x, varargin) % cumulative distribution function
            %numerical cdf using the pdf
            %1-solution of x'=-(r+c*t/(t+h))*x
            if isempty(x)
                F = [];
                return;
            end
            x(x < 0) = 0;
            F = 1 - exp(-prob.HillDistribution.IntHazardFunc(x, varargin{:}));
            F(isinf(x)) = 1;
            F(x < 0) = 0;
            F(F < 0) = 0;
            F(F >= 1 - 1E-10) = 1 - 1E-10;
        end
 
        function f = pdffunc(x, varargin) % probability density function has to be specified
            %essential to combine both exp() functions, as the separate
            %factors can become infitine. Do not simplify the equation!
            %exp(c.*h.*log(h + x) - r.*x - c.*x).*exp(-c.*h.*log(h)).*(c + r - (c*h)./(h + x))
            %y = 1E-300 + exp(c .* h .* ln(h + x) - c .* x - c .* h .* ln(h)) .* (c - (c .* h) ./ (h + x));
            x(x < 0) = 0;
            f = 1E-300 + exp(-prob.HillDistribution.IntHazardFunc(x, varargin{:})) .* prob.HillDistribution.HazardFunc(x, varargin{:});
            f(f <= 0) = 1E-300;
            f(isnan(f))=1E-300;
        end
 
        function y = invfunc(p, varargin) % inverse cdf
            y = numinvfunc(@(x)prob.HillDistribution.cdffunc(x, varargin{:})...
                , [0 inf], p, varargin);
        end

        
        function y = randfunc(varargin) % random number generator
            n= prob.HillDistribution.NumParameters;
            y = prob.HillDistribution.invfunc(rand(varargin{n+1:end}), varargin{1:n});
        end

        function ran = GetRange(cpar, minpdf, info)
            if nargin < 3
                info = prob.HillDistribution.getInfo;
            end
            if nargin < 2
                minpdf = 1E-100;
            end
            ran = info.support;
            if isinf(ran(1))
                x = -100: -100: -100000;
                pdf = prob.HillDistribution.pdffunc(x, cpar{:});
                ran(1) = x(find(pdf < minpdf, 1));
            end
            if isinf(ran(2))
                x = 100:100:100000;
                pdf = prob.HillDistribution.pdffunc(x, cpar{:});
                ran(2) = x(find(pdf < minpdf, 1));
            end
        end
    end
    methods (Hidden,Access='private')
        function this = setparameter(this, n, value)
            plim1 = this.plim(:, n);
            if value < plim1(1) || value > plim1(2)
                error('HillDistribution:validate', 'Parameter outside range');
            end
            this.ParameterValues(n) = value;
            this = invalidateFit(this);
        end
    end
    methods(Static, Hidden)
        % All ToolboxDistributions must implement a getInfo static method
        % so that Statistics and Machine Learning Toolbox functions can get information about
        % the distribution.
        function info = getInfo
            
            % First get default info from parent class
            info = getInfo@prob.ToolboxFittableParametricDistribution('prob.Hill');
            
            % Then override fields as necessary
            info.name = 'Hill';
            info.code = 'hill';
            info.pnames=prob.HillDistribution.ParameterNames;
            info.pdescription=prob.HillDistribution.ParameterDescription;

            % info.pnames is obtained from the ParameterNames property
            % info.pdescription is obtained from the ParameterDescription property
            info.prequired = true(1, numel(prob.HillDistribution.typicalpars)); % Change if any parameter must
            info.likefunc = @prob.HillDistribution.likefunc;
            info.cdffunc = @prob.HillDistribution.cdffunc;
            info.pdffunc = @prob.HillDistribution.pdffunc;
            info.invfunc = @prob.HillDistribution.invfunc;
            info.randfunc = @prob.HillDistribution.randfunc;
            %          be specified before fitting.
            %          An example would be the N
            %          parameter of the binomial
            %          distribution.
            % info.hasconfbounds = false     % Set to true if the cdf and
            %          icdf methods can return
            %          lower and upper bounds as
            %          their 2nd and 3rd outputs.
            info.censoring = true; % Set to true if the fit
            %          method supports censoring.
            info.support = [0, Inf];
            %          Set to other lower and upper
            %          bounds if the distribution
            %          doesn't cover the whole real
            %          line. For example, for a
            %          distribution on positive
            %          values use [0, Inf].
            % info.closedbound = [false false] % Set the Jth value to
            %          true if the distribution
            %          allows x to be equal to the
            %          Jth element of the support
            %          vector.
            % info.iscontinuous = true       % Set to false if x can take
            %          only integer values.
            info.islocscale = true; % Set to true if this is a
            %          location/scale distribution
            %          (no other parameters).
            % info.uselogpp = false          % Set to true if a probability
            %          plot should be drawn on the
            %          log scale.
 
            info.fittable = true;
            info.optimopts = true; % Set to true if the fit
            %          method can be called with an
            %          options structure.
            info.logci = false(1, numel(prob.HillDistribution.typicalpars)); % Set to true for a parameter
            %          that should have its Wald
            %          confidence interval computed
            %          using a normal approximation
            %          on the log scale.
            info.plim = prob.HillDistribution.plim;
        end
        function pd = makeFitted(par, nll, cov, x, cens, freq)
            cpar = num2cell(par);
            pd = prob.HillDistribution(cpar{:});
            pd.NegativeLogLikelihood = nll;
            pd.ParameterCovariance = cov;
            pd.ParameterIsFixed = false;
            pd.ParameterIsFixed = false(size(par));
            pd.InputData = struct('data', x, 'cens', cens, 'freq', freq);
        end
    end
    %ADAPT THIS FOR YOUR DISTRIBUTION
    %
 
    properties (Constant = true)
        %the limits for the parameters
        distname = 'Hill';
        plim = [0, 0, 0; Inf, Inf, Inf];
        %typical parameter values
        typicalpars = [0.01 150 2];
        %Parameter Names
        ParameterNames = {'c', 'h', 'p'};
        %the limits for the parameters
        %          plim = [0, ; Inf];
        %          %typical parameter values
        %          typicalpars = [0.01];
        %          %Parameter Names
        %          ParameterNames = {'r'};

 
        %(you can leave this unchanged)
        NumParameters = length(prob.HillDistribution.ParameterNames);
        ParameterDescription = prob.HillDistribution.ParameterNames;
    end
    
    %optionally make parameters accessible
    properties (Dependent = true)
        c
        h
        p
    end
    methods
        function this = set.c(this, c)
            this=this.setparameter(1,c);
        end
        function this = set.h(this, h)
            this=this.setparameter(2,h);
        end
        function this = set.p(this, p)
            this=this.setparameter(3,p);
        end
        function c = get.c(this)
            c = this.ParameterValues(1);
        end
        function h = get.h(this)
            h = this.ParameterValues(2);
        end
        function p = get.p(this)
            p = this.ParameterValues(3);
        end
    end
 
    methods(Static, Hidden)

        % The following utilities needs to be adapted
        function res = HazardFunc(x, c, h, p)
            %*** enter here the Hazard function
            res = c .* x.^p ./ (h.^p + x.^p);
            %res=zeros(size(x))+c;
        end
       function res = IntHazardFunc(x, varargin)
            %*** enter here the integral of the Hazard function (or use this code
            %for numerical approximation
            res=shared.integrate_ode45(@prob.HillDistribution.HazardFunc,x,[], varargin{:});
       end
    end
end % classdef
