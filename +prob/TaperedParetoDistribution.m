classdef TaperedParetoDistribution < prob.ToolboxFittableParametricDistribution
    %
    %    An object of the TaperedParetoDistribution class represents the sligtly changed 
    %    Tapered Pareto distribution as implemented by Vaiciulis and Markovich
    %    (Automation and Remote Control, 2021, Vol. 82, No. 8, pp.
    %    1358–1377):
    %
    %       pdf(x) = ((alpha-1)/x+lambda)*(xmin./x)^(alpha-1)*exp(-lambda.*(x-xmin));
    %       cdf(x) =  1 - (xmin^(alpha-1)*exp((lambda*(xmin - x))))/x^(alpha-1)
    %
    %    The parameter ALPHA defines the power and XMIN is the scale parameter 
    %    and LAMBDA the amount of tapering. It uses the method of Clauset
    %    to find the optimal xmin (cannot be done with MLE).
    %    This distribution object can be created directly
    %    using the MAKEDIST function or fit to data using the FITDIST function.
    %
    %    TaperedParetoDistribution methods:
    %       cdf                   - Cumulative distribution function
    %       fit                   - Fit distribution to data
    %       icdf                  - Inverse cumulative distribution function
    %       iqr                   - Interquartile range
    %       mean                  - Mean
    %       median                - Median
    %       paramci               - Confidence intervals for parameters
    %       pdf                   - Probability density function
    %       proflik               - Profile likelihood function
    %       random                - Random number generation
    %       std                   - Standard deviation
    %       truncate              - Truncation distribution to an interval
    %       var                   - Variance
    %
    %    TaperedParetoDistribution properties:    
    %       DistributionName      - Name of the distribution
    %       alpha                 - slope of the power law, as defined by
    %                               Clauset et al. 2009 (i.e. the slope of 
    %                               Vaiciulis and Markovich = alpha-1)
    %       xmin                  - the minumum value of the powerlaw
    %       lambda                - the strength of the exponential
    %                               tapering
    %       NumParameters         - Number of parameters
    %       ParameterNames        - Names of parameters
    %       ParameterDescription  - Descriptions of parameters
    %       ParameterValues       - Vector of values of parameters
    %       Truncation            - Two-element vector indicating truncation limits
    %       IsTruncated           - Boolean flag indicating if distribution is truncated
    %       ParameterCovariance   - Covariance matrix of estimated parameters
    %       ParameterIsFixed      - Two-element boolean vector indicating fixed parameters
    %       InputData             - Structure containing data used to fit the distribution
    %       NegativeLogLikelihood - Value of negative log likelihood function
    %
    %    See also fitdist, makedist.

    % All ProbabilityDistribution objects must specify a DistributionName
    properties(Constant)
        %DistributionName Name of distribution
        %    DistributionName is the name of this distribution.
        DistributionName = 'taperedpareto';
    end

    % Optionally add your own properties here. For this distribution it's convenient
    % to be able to refer to the mu and sigma parameters by name, and have them
    % connected to the proper element of the ParameterValues property. These are
    % dependent properties because they depend on ParameterValues.
    properties(Dependent = true)
       
        %A Linear growth parameter A= growth factor
        alpha
        xmin
        lambda
    end
    
    
    % All ParametricDistribution objects must specify values for the following
    % constant properties (they are the same for all instances of this class).
    properties(Constant)
        %NumParameters Number of parameters
        %    NumParameters is the number of parameters in this distribution.

        NumParameters = 3;
        
        %ParameterName Name of parameter
        %    ParameterName is a two-element cell array containing names
        %    of the parameters of this distribution.
        ParameterNames = {'alpha' 'xmin', 'lambda'};
        
        %ParameterDescription Description of parameter
        %    ParameterDescription is a two-element cell array containing
        %    descriptions of the parameters of this distribution.
        ParameterDescription = { 'power', 'minimum value of x', 'truncation'};
        typicalpars = [1.4 1 0.001];

    end

    % All ParametricDistribution objects must include a ParameterValues property
    % whose value is a vector of the parameter values, in the same order as
    % given in the ParameterNames property above.
    properties(GetAccess = 'public', SetAccess = 'protected')
        %ParameterValues Values of the distribution parameters
        %    ParameterValues is a two-element vector containing the mu and sigma
        %    values of this distribution.
        ParameterValues
    end

    methods
        % The constructor for this class can be called with a set of parameter
        % values or it can supply default values. These values should be
        % checked to make sure they are valid. They should be stored in the
        % ParameterValues property.
        function pd = TaperedParetoDistribution(alpha, xmin, lambda)
            if nargin == 0
                alpha = 1.4;
                xmin = 1;
                lambda = 0.1;
            end
            checkargs(alpha, xmin, lambda);
            
            pd.ParameterValues = [alpha, xmin, lambda];
            
            % All FittableParametricDistribution objects must assign values
            % to the following two properties. When an object is created by
            % the constructor, all parameters are fixed and the covariance
            % matrix is entirely zero.
            pd.ParameterIsFixed = [true, true true];
            pd.ParameterCovariance = zeros(pd.NumParameters);
        end
 
        
        % Implement methods to compute the mean, variance, and standard
        % deviation.
        function m = mean(this)
            %numerical mean using the pdf
            %             info = this.getInfo;
            %             m = integral(@(x)x.*this.pdf(x), info.support(1), info.support(2))
            %if this.alpha > 2
            info = this.getInfo;
            m = integral(@(x)x.*this.pdf(x), info.support(1), info.support(2));
            %    m = (this.alpha - 1) * this.xmin / (this.alpha - 2);

            %             m = real(this.lambda^(1 - this.alpha) * (this.lambda^(this.alpha - 2) * gamma((2 - this.alpha)) +  ...
            %                 ((-1)^(this.alpha / 2 + 1 / 2) * this.lambda^(this.alpha / 2 - 3 / 2) * this.xmin^(1 / 2 -  ...
            %                 this.alpha / 2) * exp((-(this.lambda * this.xmin) / 2)) * whittakerM((this.alpha / 2 - 1 / 2),  ...
            %                 (1 - this.alpha / 2), (-this.lambda * this.xmin))) / (this.alpha - 2))) / igamma(1 - this.alpha, this.lambda * this.xmin);
            %  else
            %     m = Inf;
            % end
        end
        
        function s = std(this)
            s = sqrt(this.var);
        end
        function v = var(this)
            %nice analytical solution
            %             if this.alpha > 3
            %                 v=(this.xmin^2*(this.alpha - 1))/((this.alpha - 2)^2*(this.alpha - 3));
            %             else
            %    v=inf;
            info = this.getInfo;
            v = integral(@(x)x.^2 .*this.pdf(x), this.xmin, info.support(2)) - this.mean^2;
            %            end
        end
 
        function haz = hazard(this, x)
            haz = this.lambda + (this.alpha - 1) ./ x;
            haz(x < this.xmin) = nan;
        end
 
        %         function haz = hazard(this, x)
        %             haz = this.a .* x;
        %         end
        

    end
    methods
        % If this class defines dependent properties to represent parameter
        % values, their get and set methods must be defined. The set method
        % should mark the distribution as no longer fitted, because any
        % old results such as the covariance matrix are not valid when the
        % parameters are changed from their estimated values.
        function this = set.alpha(this, a)
            checkargs(a, this.xmin, this.lambda);
            this.ParameterValues(1) = a;
            this = invalidateFit(this);
        end
        function a = get.alpha(this)
            a = this.ParameterValues(1);
        end
        function this = set.xmin(this, xmin1)
            checkargs(this.alpha, xmin1, this.lambda);
            this.ParameterValues(2) = xmin1;
            this = invalidateFit(this);
        end
        function xmin1 = get.xmin(this)
            xmin1 = this.ParameterValues(2);
        end
        function this = set.lambda(this, lamda)
            checkargs(this.alpha, lamda, this.lambda);
            this.ParameterValues(3) = lamda;
            this = invalidateFit(this);
        end
        function lambda = get.lambda(this)
            lambda = this.ParameterValues(3);
        end
    end
    methods(Static)
        % All FittableDistribution classes must implement a fit method to fit
        % the distribution from data. This method is called by the FITDIST
        % function, and is not intended to be called directly

        function pd = fit(x, varargin)
            fitopts = reshape(varargin, 2, length(varargin) / 2);
  
            if ~isempty(fitopts)
                fitopts(1, :) = lower(fitopts(1, :));
            end
            ndx = strcmp(fitopts(1, :), 'lowerbound');
            if any(ndx)
                lowerbound = fitopts{2, ndx};
            else
                lowerbound = [0 0 0];
            end
            ndx = strcmp(fitopts(1, :), 'upperbound');
            if any(ndx)
                upperbound = fitopts{2, ndx};
            else
                %nan means that we use the dafault= median
                upperbound = [inf nan inf];
            end
            ndx = strncmp(fitopts(1, :), 'cens', 4);
            if any(ndx)
                cens = fitopts{2, ndx};
            else
                cens = [];
            end
            ndx = strncmp(fitopts(1, :), 'freq', 4);
            if any(ndx)
                freq = fitopts{2, ndx};
            else
                freq = [];
            end
            %xmin is a problem for mle fitting, as the number of
            %observations is not constant

            pd1 = SingleDist('taperedpareto');
            if isnan(upperbound(2))
                %find median using the ecdf
                if any(cens) || any(freq > 1)
                    [F1, x1] = shared.my_ecdf(x, 'censoring', cens, 'frequency', freq);
                    upperbound(2) = x1(find(F1(:, 1) >= 0.5, 1) + 1);
                else
                    upperbound(2) = median(x);
                end
            end
            pd = pd1.fitfixed(x, 'fixedpar', 'xmin', 'lowerpar', true, varargin{:}, 'parrange', x(x <= upperbound(2) & x >= lowerbound(2)));
            pd = prob.TaperedParetoDistribution.makeFitted(pd.d.ParameterValues, pd.negloglik, [], x, cens, freq);
        end
        
        

        % The following static methods are required for the
        % ToolboxParametricDistribution class and are used by various
        % Statistics and Machine Learning Toolbox functions. These functions operate on
        % parameter values supplied as input arguments, not on the
        % parameter values stored in a LaplaceDistribution object. For
        % example, the cdf method implemented in a parent class invokes the
        % cdffunc static method and provides it with the parameter values.
        function [nll, acov] = likefunc(varargin) % likelihood function
            %    alpha=varargin{1};
            %    xmin=varargin{2};
            %    x=varargin{3};
            %    z = x(x >= xmin);
            %    n = length(z);
            %   L = n * log((alpha - 1) / xmin) - alpha .* sum(log(z ./ xmin))

            [nll, acov] = shared.likefunc_mydistrib(@prob.TaperedParetoDistribution.pdffunc, @prob.TaperedParetoDistribution.cdffunc, [], varargin{:});
        end

        function y = cdffunc(x, alpha, xmin, lambda) % cumulative distribution function
            %numerical cdf using the pdf
            if isempty(x)
                y = [];
                return;
            end
            y = 1 - (xmin.^(alpha - 1) .* exp((lambda .* (xmin - x)))) ./ x.^(alpha - 1);
            y(x <= xmin) = 0;
        end
        
        function y = pdffunc(x, alpha, xmin, lambda) % probability density function has to be specified
            if alpha == 1
                y = zeros(size(x));
            else
                y = ((alpha - 1) ./ x + lambda) .* (xmin ./ x).^(alpha - 1) .* exp(-lambda .* (x - xmin));
                y(x < xmin) = 1E-300;
            end
        end
        
        function y = invfunc(p, alpha, xmin, lambda) % inverse cdf
            if isempty(p)
                y = [];
                return;
            end
            if lambda == 0
                y = 1 ./ (-(xmin .* (p - 1)) ./ xmin.^alpha).^(1 ./ (alpha - 1));
            else
                y = ((alpha - 1) .* lambertw(0, (lambda ./ ((alpha - 1) .* (-(exp((-lambda .* xmin)) .* (p - 1)) ./ xmin.^(alpha - 1)).^(1 ./ (alpha - 1)))))) ./ lambda;
            end
            %  y = 1 ./ (-(xmin .* (p - 1)) ./ xmin.^alpha).^(1 ./ (alpha - 1));
        end
        
        function y = randfunc(alpha, xmin, lambda, varargin) % random number generator
            %both methods give very similar results
            %  y = min(prob.ParetoDistribution.randfunc(alpha,xmin,varargin{:}),exprnd(1 ./lambda,varargin{:})+xmin);
            y = prob.TaperedParetoDistribution.invfunc(1 - rand(varargin{:}), alpha, xmin, lambda);
        end

        function ran = GetRange(cpar)
            persistent oldpar oldran;
            if isequal(cpar, oldpar)
                ran = oldran;
            else
                ran = zeros(2, 1);
                ran(1) = cpar{2};
                maxcdf = 1 - 1E-10;
                % tic
                ma = cpar{2} +100000;
                mi = cpar{2} +0.01;
                while ma - mi > ma / 10
                    x = (ma + mi) / 2;
                    if prob.TaperedParetoDistribution.cdffunc(x, cpar{:}) < maxcdf
                        mi = x;
                    else
                        ma = x;
                    end
                    % fprintf('%g   %g   %g   %g   \n', prob.TaperedParetoDistribution.cdffunc(x, cpar{:}), x, mi, ma);
                end
                ran(2) = x;
                % prob.TaperedParetoDistribution.cdffunc(x, cpar{:})
                %toc
                oldpar = cpar;
                oldran = ran;
            end
        end
    end
    methods(Static, Hidden)
        % All ToolboxDistributions must implement a getInfo static method
        % so that Statistics and Machine Learning Toolbox functions can get information about
        % the distribution.
        function info = getInfo
            
            % First get default info from parent class
            info = getInfo@prob.ToolboxFittableParametricDistribution('prob.Linear');
            
            % Then override fields as necessary
            info.name = 'TaperedPareto';
            info.code = 'taperedpareto';
            info.pnames = prob.TaperedParetoDistribution.ParameterNames;
            info.pdescription = prob.TaperedParetoDistribution.ParameterDescription;

            % info.pnames is obtained from the ParameterNames property
            % info.pdescription is obtained from the ParameterDescription property
            info.likefunc = @prob.TaperedParetoDistribution.likefunc;
            info.cdffunc = @prob.TaperedParetoDistribution.cdffunc;
            info.pdffunc = @prob.TaperedParetoDistribution.pdffunc;
            info.invfunc = @prob.TaperedParetoDistribution.invfunc;
            info.randfunc = @prob.TaperedParetoDistribution.randfunc;
            info.prequired = [false false false]; % Change if any parameter must
            %          be specified before fitting.
            %          An example would be the N
            %          parameter of the binomial
            %          distribution.
            % info.hasconfbounds = false     % Set to true if the cdf and
            %          icdf methods can return
            %          lower and upper bounds as
            %          their 2nd and 3rd outputs.
            info.censoring = true; % Set to true if the fit
            %          method supports censoring.
            info.support = [0, Inf];
            %          Set to other lower and upper
            %          bounds if the distribution
            %          doesn't cover the whole real
            %          line. For example, for a
            %          distribution on positive
            %          values use [0, Inf].
            % info.closedbound = [false false] % Set the Jth value to
            %          true if the distribution
            %          allows x to be equal to the
            %          Jth element of the support
            %          vector.
            % info.iscontinuous = true       % Set to false if x can take
            %          only integer values.
            info.islocscale = true; % Set to true if this is a
            %          location/scale distribution
            %          (no other parameters).
            % info.uselogpp = false          % Set to true if a probability
            %          plot should be drawn on the
            %          log scale.
 
            info.fittable = true;
            info.optimopts = true; % Set to true if the fit
            %          method can be called with an
            %          options structure.
            info.logci = [false false false]; % Set to true for a parameter
            %          that should have its Wald
            %          confidence interval computed
            %          using a normal approximation
            %          on the log scale.
            info.plim = [0 0 0; Inf, Inf Inf];
        end
        function pd = makeFitted(par, nll, cov, x, cens, freq)
            cpar = num2cell(par);
            pd = prob.TaperedParetoDistribution(cpar{:});
            pd.NegativeLogLikelihood = nll;
            pd.ParameterCovariance = cov;

            pd.ParameterIsFixed = false(size(par));
            pd.InputData = struct('data', x, 'cens', cens, 'freq', freq);
        end
    end
end % classdef

% The following utilities check for valid parameter values
function checkargs(alpha, xmin, lambda)
    if ~(isscalar(alpha) && isnumeric(alpha) && isreal(alpha) && isfinite(alpha) && alpha >= 0)
        error('alpha must be a positive finite numeric scalar.')
    end
    if ~(isscalar(xmin) && isnumeric(xmin) && isreal(xmin) && xmin >= 0 && isfinite(xmin))
        error('xmin must be a positive finite numeric scalar.')
    end
    if ~(isscalar(lambda) && isnumeric(lambda) && isreal(lambda) && isfinite(lambda) && lambda >= 0)
        error('lambda must be a positive finite numeric scalar.')
    end

end
