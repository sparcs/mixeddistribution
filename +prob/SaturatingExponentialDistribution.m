classdef SaturatingExponentialDistribution < prob.ToolboxFittableParametricDistribution
    %
    %    An object of the SaturationExponentiaDistribution class represents a probability 
    %    distribution of survival analysis. The distribution based on a 
    %    saturating hazard function h(t) with intercept: 
    %       h(t)=r + c*t/(t + h)
    %    The survival function S is the solution of the following
    %    differential equation with x(0)=1:
    %        dx/dt=-(r + c*t/(t + h))
    %        S=exp(c*h*log(h + t) - r*t - c*t-c*h*log(h))
    %    The pdf of the distribution is S times the hazard function
    %
    %    The parameter C defines the maximum hazard. 
    %    The parameter H defines the half-saturation constant of the hazard function. 
    %    The parameter R is the constant base level of hazard
    %    This distribution object can be created directly
    %    using the MAKEDIST function or fit to data using the FITDIST function.    
    %
    %    SaturatingExponentialDistribution methods:
    %       cdf                   - Cumulative distribution function
    %       fit                   - Fit distribution to data
    %       icdf                  - Inverse cumulative distribution function
    %       iqr                   - Interquartile range
    %       mean                  - Mean
    %       median                - Median
    %       paramci               - Confidence intervals for parameters
    %       pdf                   - Probability density function
    %       proflik               - Profile likelihood function
    %       random                - Random number generation
    %       std                   - Standard deviation
    %       truncate              - Truncation distribution to an interval
    %       var                   - Variance
    %
    %    LaplaceDistribution properties:    
    %       DistributionName      - Name of the distribution
    %       C                     - Value of the maximum hazard
    %       H                     - Value of the half-saturation constant
    %       R                     - Value of the constant hazard
    %       NumParameters         - Number of parameters
    %       ParameterNames        - Names of parameters
    %       ParameterDescription  - Descriptions of parameters
    %       ParameterValues       - Vector of values of parameters
    %       Truncation            - Two-element vector indicating truncation limits
    %       IsTruncated           - Boolean flag indicating if distribution is truncated
    %       ParameterCovariance   - Covariance matrix of estimated parameters
    %       ParameterIsFixed      - Two-element boolean vector indicating fixed parameters
    %       InputData             - Structure containing data used to fit the distribution
    %       NegativeLogLikelihood - Value of negative log likelihood function
    %
    %    See also fitdist, makedist.

    % All ProbabilityDistribution objects must specify a DistributionName
    properties(Constant)
        %DistributionName Name of distribution
        %    DistributionName is the name of this distribution.
        DistributionName = 'SaturatingExponential';
    end

    % Optionally add your own properties here. For this distribution it's convenient
    % to be able to refer to the mu and sigma parameters by name, and have them
    % connected to the proper element of the ParameterValues property. These are
    % dependent properties because they depend on ParameterValues.
    properties(Dependent = true)
        %R is exponential decay R=constant factor
        c
        
        %A Linear growth parameter A= growth factor
        h
        r
    end
    
    % All ParametricDistribution objects must specify values for the following
    % constant properties (they are the same for all instances of this class).
    properties(Constant)
        %NumParameters Number of parameters
        %    NumParameters is the number of parameters in this distribution.

        NumParameters = 3;
        
        %ParameterName Name of parameter
        %    ParameterName is a two-element cell array containing names
        %    of the parameters of this distribution.
        ParameterNames = {'c' 'h' 'r'};
        
        %ParameterDescription Description of parameter
        %    ParameterDescription is a two-element cell array containing
        %    descriptions of the parameters of this distribution.
        ParameterDescription = { 'maximal decay', 'half saturation time', 'constant decay'};
        typicalpars=   [0.01 150,0];

    end

    % All ParametricDistribution objects must include a ParameterValues property
    % whose value is a vector of the parameter values, in the same order as
    % given in the ParameterNames property above.
    properties(GetAccess = 'public', SetAccess = 'protected')
        %ParameterValues Values of the distribution parameters
        %    ParameterValues is a two-element vector containing the mu and sigma
        %    values of this distribution.
        ParameterValues
    end

    methods
        % The constructor for this class can be called with a set of parameter
        % values or it can supply default values. These values should be
        % checked to make sure they are valid. They should be stored in the
        % ParameterValues property.
        function pd = SaturatingExponentialDistribution(c, h, r)
            if nargin == 0
                c = 0.01;
                h = 150;
                r = 0.00001;
            end
            checkargs(c, h, r);
            
            pd.ParameterValues = [c h, r];
            
            % All FittableParametricDistribution objects must assign values
            % to the following two properties. When an object is created by
            % the constructor, all parameters are fixed and the covariance
            % matrix is entirely zero.
            pd.ParameterIsFixed = [true true true];
            pd.ParameterCovariance = zeros(pd.NumParameters);
        end
 
        
        % Implement methods to compute the mean, variance, and standard
        % deviation.
        function m = mean(this)
            %numerical mean using the pdf
            info = this.getInfo;
            m = integral(@(x)x.*this.pdf(x), info.support(1), info.support(2));
        end
        
        function s = std(this)
            s = sqrt(this.var);
        end
        function v = var(this)
            info = this.getInfo;
            m = this.mean;
            v = integral(@(x)(x - m).^2 .*this.pdf(x), info.support(1), info.support(2));
        end
        
        function haz=hazard(this,x)
            haz=this.r+this.c.*x./(x + this.h);
        end
        
    end
    methods
        % If this class defines dependent properties to represent parameter
        % values, their get and set methods must be defined. The set method
        % should mark the distribution as no longer fitted, because any
        % old results such as the covariance matrix are not valid when the
        % parameters are changed from their estimated values.
        function this = set.c(this, c)
            checkargs(c, this.h, this.r);
            this.ParameterValues(1) = c;
            this = invalidateFit(this);
        end
        function this = set.h(this, h)
            checkargs(this.c, h, this.r);
            this.ParameterValues(2) = h;
            this = invalidateFit(this);
        end
        function this = set.r(this, r)
            checkargs(this.c, this.h, r);
            this.ParameterValues(3) = r;
            this = invalidateFit(this);
        end
        function c = get.c(this)
            c = this.ParameterValues(1);
        end
        function h = get.h(this)
            h = this.ParameterValues(2);
        end
        function r = get.r(this)
            r = this.ParameterValues(3);
        end
    end
    methods(Static)
        % All FittableDistribution classes must implement a fit method to fit
        % the distribution from data. This method is called by the FITDIST
        % function, and is not intended to be called directly
         function pd = fit(x, varargin)
            pd = shared.fit_mle(@prob.SaturatingExponentialDistribution.likefunc,  ...
                @prob.SaturatingExponentialDistribution.makeFitted,  ...
                prob.SaturatingExponentialDistribution.getInfo.plim, prob.SaturatingExponentialDistribution.typicalpars,  ...%typical parameter set
                x, varargin{:});
        end
        
       
        % The following static methods are required for the
        % ToolboxParametricDistribution class and are used by various
        % Statistics and Machine Learning Toolbox functions. These functions operate on
        % parameter values supplied as input arguments, not on the
        % parameter values stored in a LaplaceDistribution object. For
        % example, the cdf method implemented in a parent class invokes the
        % cdffunc static method and provides it with the parameter values.
        function [nll, acov] = likefunc(varargin) % likelihood function
            [nll, acov] = shared.likefunc_mydistrib(@prob.SaturatingExponentialDistribution.pdffunc,@prob.SaturatingExponentialDistribution.cdffunc,[],varargin{:});           
        end

        function F = cdffunc(x, c, h, r) % cumulative distribution function
            %numerical cdf using the pdf
            %1-solution of x'=-(c*t/(t+h))*x
            if isempty(x)
                F = [];
                return;
            end
            function res = inthz(x)
                %integral of the Hazard function
                res = c .* x + r .* x - c .* h .* log(h + x);
            end
            F = 1 - exp(-(inthz(x) - inthz(0)));
            F(isinf(x)) = 1;
            F(x < 0) = 0;
            F(F<0)=0;
            F(F>=1-1E-10)=1-1E-10;
        end
        
        function f = pdffunc(x, c, h, r) % probability density function has to be specified
            %essential to combine both exp() functions, as the separate
            %factors can become infitine. Do not simplify the equation!
            %exp(c.*h.*log(h + x) - r.*x - c.*x).*exp(-c.*h.*log(h)).*(c + r - (c*h)./(h + x))
            function res = inthz(x)
                %integral of the Hazard function
                res = c .* x + r .* x - c .* h .* log(h + x);
            end
            %hazard function
            hz = r + c .* x ./ (x + h);
            f = 1E-300 + exp(-(inthz(x) - inthz(0))) .* hz;
            f(x < 0) = 0;
            %y = 1E-300 + exp(-c .* h .* ln(h)) .* exp(c .* h .* ln(h + x) - c .* x) .* (c - (c .* h) ./ (h + x));
        end
        
        function y = invfunc(p, varargin) % inverse cdf
            y= numinvfunc(@(x)prob.SaturatingExponentialDistribution.cdffunc(x, varargin{:})...
                ,[0 inf], p, varargin);
        end
        
        function y = randfunc(c, h, r, varargin) % random number generator
            y = prob.SaturatingExponentialDistribution.invfunc(rand(varargin{:}), c, h, r);
        end

        function ran = GetRange(cpar, minpdf, info)
            if nargin < 3
                info = prob.SaturatingExponentialDistribution.getInfo;
            end
            if nargin < 2
                minpdf = 1E-100;
            end
            ran = info.support;
            if isinf(ran(1))
                x = -100: -100: -100000;
                pdf = prob.SaturatingExponentialDistribution.pdffunc(x, cpar{:});
                ran(1) = x(find(pdf < minpdf, 1));
            end
            if isinf(ran(2))
                x = 100:100:100000;
                pdf = prob.SaturatingExponentialDistribution.pdffunc(x, cpar{:});
                ran(2) = x(find(pdf < minpdf, 1));
            end
        end
    end
    methods(Static, Hidden)
        % All ToolboxDistributions must implement a getInfo static method
        % so that Statistics and Machine Learning Toolbox functions can get information about
        % the distribution.
        function info = getInfo
            
            % First get default info from parent class
            info = getInfo@prob.ToolboxFittableParametricDistribution('prob.SaturatingExponential');
            
            % Then override fields as necessary
            info.name = 'SaturatingExponential';
            info.code = 'saturatingexponential';
                        info.pnames=prob.SaturatingExponentialDistribution.ParameterNames;
            info.pdescription=prob.SaturatingExponentialDistribution.ParameterDescription;


            % info.pnames is obtained from the ParameterNames property
            % info.pdescription is obtained from the ParameterDescription property
            info.prequired = [true true true]; % Change if any parameter must
            info.likefunc = @prob.SaturatingExponentialDistribution.likefunc;
            info.cdffunc = @prob.SaturatingExponentialDistribution.cdffunc;
            info.pdffunc = @prob.SaturatingExponentialDistribution.pdffunc;
            info.invfunc = @prob.SaturatingExponentialDistribution.invfunc;
            info.randfunc = @prob.SaturatingExponentialDistribution.randfunc;
            %          be specified before fitting.
            %          An example would be the N
            %          parameter of the binomial
            %          distribution.
            % info.hasconfbounds = false     % Set to true if the cdf and
            %          icdf methods can return
            %          lower and upper bounds as
            %          their 2nd and 3rd outputs.
            info.censoring = true;              % Set to true if the fit
            %          method supports censoring.
            info.support = [0, Inf] ;
            %          Set to other lower and upper
            %          bounds if the distribution
            %          doesn't cover the whole real
            %          line. For example, for a
            %          distribution on positive
            %          values use [0, Inf].
            % info.closedbound = [false false] % Set the Jth value to
            %          true if the distribution
            %          allows x to be equal to the
            %          Jth element of the support
            %          vector.
            % info.iscontinuous = true       % Set to false if x can take
            %          only integer values.
            info.islocscale = true; % Set to true if this is a
            %          location/scale distribution
            %          (no other parameters).
            % info.uselogpp = false          % Set to true if a probability
            %          plot should be drawn on the
            %          log scale.
 
            info.fittable = true;
            info.optimopts = true; % Set to true if the fit
            %          method can be called with an
            %          options structure.
            info.logci = [false false false]; % Set to true for a parameter
            %          that should have its Wald
            %          confidence interval computed
            %          using a normal approximation
            %          on the log scale.
            info.plim = [0 0 0; Inf Inf Inf];
        end
        function pd = makeFitted(par, nll, cov, x, cens, freq)
            cpar = num2cell(par);
            pd = prob.SaturatingExponentialDistribution(cpar{:});
            pd.NegativeLogLikelihood = nll;
            pd.ParameterCovariance = cov;
            %pd.ParameterIsFixed = false;
            pd.ParameterIsFixed = [false false false];
            pd.InputData = struct('data', x, 'cens', cens, 'freq', freq);
        end
    end
end % classdef

% The following utilities check for valid parameter values
function checkargs(c, h, r)
    if ~(isscalar(c) && isnumeric(c) && isreal(c) && c >= 0 && isfinite(c))
        error('C must be a positive finite numeric scalar.')
    end
    if ~(isscalar(h) && isnumeric(h) && isreal(h) && h >= 0 && isfinite(h))
        error('H must be a positive finite numeric scalar.')
    end
    if ~(isscalar(r) && isnumeric(r) && isreal(r) && r >= 0 && isfinite(r))
        error('R must be a positive finite numeric scalar.')
    end
end
