classdef LinearExponentialDistribution < prob.ToolboxFittableParametricDistribution
    %
    %    An object of the LinearExponentiaDistribution class represents a probability 
    %    distribution of survival analysis. The distribution based on a 
    %    linear increasing hazard function h(t) with intercept: 
    %       h(t)=a*t +r
    %    The survival function S is the solution of the following
    %    differential equation with x(0)=1:
    %        dx/dt=-(a*t +r)
    %        S=exp(- (a*t^2)/2 - r*t)
    %    The pdf of the distribution is S times the hazard function
    %
    %    The parameter A defines the steepness of the increase of the hazard. 
    %    The parameter R is the constant base level of hazard
    %    This distribution object can be created directly
    %    using the MAKEDIST function or fit to data using the FITDIST function.
    %
    %    LinearExponentialDistribution methods:
    %       cdf                   - Cumulative distribution function
    %       fit                   - Fit distribution to data
    %       icdf                  - Inverse cumulative distribution function
    %       iqr                   - Interquartile range
    %       mean                  - Mean
    %       median                - Median
    %       paramci               - Confidence intervals for parameters
    %       pdf                   - Probability density function
    %       proflik               - Profile likelihood function
    %       random                - Random number generation
    %       std                   - Standard deviation
    %       truncate              - Truncation distribution to an interval
    %       var                   - Variance
    %
    %    LaplaceDistribution properties:    
    %       DistributionName      - Name of the distribution
    %       a                     - Value of the "a" parameter, increase of
    %                               hazard
    %       r                     - Value of the r parameter, constant
    %                               hazard
    %       NumParameters         - Number of parameters
    %       ParameterNames        - Names of parameters
    %       ParameterDescription  - Descriptions of parameters
    %       ParameterValues       - Vector of values of parameters
    %       Truncation            - Two-element vector indicating truncation limits
    %       IsTruncated           - Boolean flag indicating if distribution is truncated
    %       ParameterCovariance   - Covariance matrix of estimated parameters
    %       ParameterIsFixed      - Two-element boolean vector indicating fixed parameters
    %       InputData             - Structure containing data used to fit the distribution
    %       NegativeLogLikelihood - Value of negative log likelihood function
    %
    %    See also fitdist, makedist.

    % All ProbabilityDistribution objects must specify a DistributionName
    properties(Constant)
        %DistributionName Name of distribution
        %    DistributionName is the name of this distribution.
        DistributionName = 'LinearExponential';
    end

    % Optionally add your own properties here. For this distribution it's convenient
    % to be able to refer to the mu and sigma parameters by name, and have them
    % connected to the proper element of the ParameterValues property. These are
    % dependent properties because they depend on ParameterValues.
    properties(Dependent = true)
        %R is exponential decay R=constant factor
        r
        
        %A Linear growth parameter A= growth factor
        a
    end
    
    % All ParametricDistribution objects must specify values for the following
    % constant properties (they are the same for all instances of this class).
    properties(Constant)
        %NumParameters Number of parameters
        %    NumParameters is the number of parameters in this distribution.

        NumParameters = 2;
        
        %ParameterName Name of parameter
        %    ParameterName is a two-element cell array containing names
        %    of the parameters of this distribution.
        ParameterNames = {'r' 'a'};
        
        %ParameterDescription Description of parameter
        %    ParameterDescription is a two-element cell array containing
        %    descriptions of the parameters of this distribution.
        ParameterDescription = { 'growth factor', 'constant factor'};
        typicalpars = [5E-4 1E-8];
    end

    % All ParametricDistribution objects must include a ParameterValues property
    % whose value is a vector of the parameter values, in the same order as
    % given in the ParameterNames property above.
    properties(GetAccess = 'public', SetAccess = 'protected')
        %ParameterValues Values of the distribution parameters
        %    ParameterValues is a two-element vector containing the mu and sigma
        %    values of this distribution.
        ParameterValues
    end

    methods
        % The constructor for this class can be called with a set of parameter
        % values or it can supply default values. These values should be
        % checked to make sure they are valid. They should be stored in the
        % ParameterValues property.
        function pd = LinearExponentialDistribution(r, a)
            if nargin == 0
                r = 5E-4;
                a = 1E-8;
            end
            checkargs(r, a);
            
            pd.ParameterValues = [r a];
            
            % All FittableParametricDistribution objects must assign values
            % to the following two properties. When an object is created by
            % the constructor, all parameters are fixed and the covariance
            % matrix is entirely zero.
            pd.ParameterIsFixed = [true true];
            pd.ParameterCovariance = zeros(pd.NumParameters);
        end
 
        
        % Implement methods to compute the mean, variance, and standard
        % deviation.
        function m = mean(this)
            %numerical mean using the pdf
            function m = meanfunc(a, r)
                m = -exp(r.^2 ./ (a .* 2.0)) .* (erf(r .* 1.0 ./ sqrt(a .* 2.0)) - 1.0) .* 1.0 ./ sqrt((a .* 2.0) ./ pi);
                if any(isnan(m)) || any(m == 0)
                    %for large r values the equation becomes zero or na 
                    %it reduces then to an exomential model
                    %   approx. 1 / r;
                    %we use the integral 
                    m = integral(@(x)x.*this.pdf(x), 0, inf);
                end
            end
            m = meanfunc(this.a, this.r);
        end
        
        function s = std(this)
            s = sqrt(this.var);
        end
 
        function v = var(this)
            function v = varfunc(a, r)
                v = 2 ./ a - (pi .* exp(r.^2 ./ a) .* (erf((sqrt(2) .* 1  ...
                    ./ sqrt(a) .* r) ./ 2.0) - 1.0).^2) ./ (a .* 2) - sqrt(2) .*  ...
                    1 ./ a.^(3 ./ 2) .* r .* sqrt(pi) .* exp(r.^2 ./ (a .* 2)) + sqrt(2)  ...
                    .* 1 ./ a.^(3 ./ 2) .* r .* sqrt(pi) .* erf((sqrt(2) .*  ...
                    1 ./ sqrt(a) .* r) ./ 2) .* exp(r.^2 ./ (a .* 2));
                if any(isnan(v)) || any(v == 0)
                    info = this.getInfo;
                    m = this.mean;
                    v = integral(@(x)(x - m).^2 .*this.pdf(x), info.support(1), info.support(2));
                end
            end
            v = varfunc(this.a, this.r);
        end

        function haz = hazard(this, x)
            haz = this.r + this.a .* x;
        end
    end
    methods
        % If this class defines dependent properties to represent parameter
        % values, their get and set methods must be defined. The set method
        % should mark the distribution as no longer fitted, because any
        % old results such as the covariance matrix are not valid when the
        % parameters are changed from their estimated values.
        function this = set.r(this, r)
            checkargs(r, this.a);
            this.ParameterValues(1) = r;
            this = invalidateFit(this);
        end
        function this = set.a(this, a)
            checkargs(this.r, a);
            this.ParameterValues(2) = a;
            this = invalidateFit(this);
        end
        function r = get.r(this)
            r = this.ParameterValues(1);
        end
        function a = get.a(this)
            a = this.ParameterValues(2);
        end
    end
    methods(Static)
        % All FittableDistribution classes must implement a fit method to fit
        % the distribution from data. This method is called by the FITDIST
        % function, and is not intended to be called directly
         function pd = fit(x, varargin)
            pd = shared.fit_mle(@prob.LinearExponentialDistribution.likefunc,  ...
                @prob.LinearExponentialDistribution.makeFitted,  ...
                prob.LinearExponentialDistribution.getInfo.plim, prob.LinearExponentialDistribution.typicalpars,  ...%typical parameter set
                x, varargin{:});
        end
        
       
        % The following static methods are required for the
        % ToolboxParametricDistribution class and are used by various
        % Statistics and Machine Learning Toolbox functions. These functions operate on
        % parameter values supplied as input arguments, not on the
        % parameter values stored in a LaplaceDistribution object. For
        % example, the cdf method implemented in a parent class invokes the
        % cdffunc static method and provides it with the parameter values.
        function [nll, acov] = likefunc(varargin) % likelihood function
            [nll, acov] = shared.likefunc_mydistrib(@prob.LinearExponentialDistribution.pdffunc,@prob.LinearExponentialDistribution.cdffunc,[],varargin{:});           
        end

        function y = cdffunc(x, r, a) % cumulative distribution function
            %numerical cdf using the pdf
            if isempty(x)
                y = [];
                return;
            end
            %             ran = prob.LinearExponentialDistribution.GetRange(varargin);
            %             xs = linspace(ran(1), ran(2), 1000);
            %             numpdf = prob.LinearExponentialDistribution.pdffunc(xs, varargin{:});
            %             cumcdf = cumtrapz(xs, numpdf);
            %             y = interp1(xs, cumcdf, x);
            %             y(isnan(x)) = NaN;
            y = 1 - exp(-(a .* x.^2) ./ 2 - r .* x);
            y(y >= 1 - 1E-10) = 1 - 1E-10;
        end
        
        function y = pdffunc(x, r, a) % probability density function has to be specified
            y = 1E-300 + exp(-(a .* x.^2) ./ 2 - r .* x) .* (r + a .* x);
        end
        
        function y = invfunc(p, r, a) % inverse cdf
            if isempty(p)
                y = [];
                return;
            end
            %             ran = prob.LinearExponentialDistribution.GetRange(varargin);
            %             xs = linspace(ran(1), ran(2), 1000);
            %             numpdf = prob.LinearExponentialDistribution.pdffunc(xs, varargin{:});
            %             cumcdf = cumtrapz(xs, numpdf);
            %             [~, ndx] = unique(cumcdf);
            %             y = interp1(cumcdf(ndx), xs(ndx), p);
            y = -(r - sqrt(r.^2 - 2 .* a .* log(1 - p))) ./ a;
            y(p < 0 | 1 < p) = NaN;
        end
        
        function y = randfunc(r, a, varargin) % random number generator
            y = prob.LinearExponentialDistribution.invfunc(rand(varargin{:}), r, a);
        end

        function ran = GetRange(cpar, minpdf, info)
            ran = prob.LinearDistribution.invfunc([0, 1 - 1E-7], cpar{:});
            % 
            %             if nargin < 3
            %                 info = prob.LinearExponentialDistribution.getInfo;
            %             end
            %             if nargin < 2
            %                 minpdf = 1E-100;
            %             end
            %             
            %             ran = info.support;
            %             if isinf(ran(1))
            %                 x = -100: -100: -100000;
            %                 pdf = prob.LinearExponentialDistribution.pdffunc(x, cpar{:});
            %                 ran(1) = x(find(pdf < minpdf, 1));
            %             end
            %             if isinf(ran(2))
            %                 x = 100:100:1000000;
            %                 pdf = prob.LinearExponentialDistribution.pdffunc(x, cpar{:});
            %                 ran(2) = x(find(pdf < minpdf, 1));
            %             end
        end
    end
    methods(Static, Hidden)
        % All ToolboxDistributions must implement a getInfo static method
        % so that Statistics and Machine Learning Toolbox functions can get information about
        % the distribution.
        function info = getInfo
            
            % First get default info from parent class
            info = getInfo@prob.ToolboxFittableParametricDistribution('prob.LinearExponential');
            
            % Then override fields as necessary
            info.name = 'LinearExponential';
            info.code = 'linearexponential';
            info.pnames=prob.LinearExponentialDistribution.ParameterNames;
            info.pdescription=prob.LinearExponentialDistribution.ParameterDescription;
            % info.pnames is obtained from the ParameterNames property
            % info.pdescription is obtained from the ParameterDescription property
            info.prequired = [true true]; % Change if any parameter must
            info.likefunc = @prob.LinearExponentialDistribution.likefunc;
            info.cdffunc = @prob.LinearExponentialDistribution.cdffunc;
            info.pdffunc = @prob.LinearExponentialDistribution.pdffunc;
            info.invfunc = @prob.LinearExponentialDistribution.invfunc;
            info.randfunc = @prob.LinearExponentialDistribution.randfunc;
            %          be specified before fitting.
            %          An example would be the N
            %          parameter of the binomial
            %          distribution.
            % info.hasconfbounds = false     % Set to true if the cdf and
            %          icdf methods can return
            %          lower and upper bounds as
            %          their 2nd and 3rd outputs.
            info.censoring = true; % Set to true if the fit
            %          method supports censoring.
            info.support = [0, Inf];
            %          Set to other lower and upper
            %          bounds if the distribution
            %          doesn't cover the whole real
            %          line. For example, for a
            %          distribution on positive
            %          values use [0, Inf].
            % info.closedbound = [false false] % Set the Jth value to
            %          true if the distribution
            %          allows x to be equal to the
            %          Jth element of the support
            %          vector.
            % info.iscontinuous = true       % Set to false if x can take
            %          only integer values.
            info.islocscale = true; % Set to true if this is a
            %          location/scale distribution
            %          (no other parameters).
            % info.uselogpp = false          % Set to true if a probability
            %          plot should be drawn on the
            %          log scale.
 
            info.fittable = true;
            info.optimopts = true; % Set to true if the fit
            %          method can be called with an
            %          options structure.
            info.logci = [false false]; % Set to true for a parameter
            %          that should have its Wald
            %          confidence interval computed
            %          using a normal approximation
            %          on the log scale.
            info.plim = [0 0; Inf Inf];
 
            
        end
        function pd = makeFitted(par, nll, cov, x, cens, freq)
            cpar = num2cell(par);
            pd = prob.LinearExponentialDistribution(cpar{:});
            pd.NegativeLogLikelihood = nll;
            pd.ParameterCovariance = cov;
            pd.ParameterIsFixed = false;
            pd.ParameterIsFixed = [false false];
            pd.InputData = struct('data', x, 'cens', cens, 'freq', freq);
        end
    end
end % classdef

% The following utilities check for valid parameter values
function checkargs(r, a)
    if ~(isscalar(r) && isnumeric(r) && isreal(r) && r >= 0 && isfinite(r))
        error('R must be a positive finite numeric scalar.')
    end
    if ~(isscalar(a) && isnumeric(a) && isreal(a) && a >= 0 && isfinite(a))
        error('A must be a positive finite numeric scalar.')
    end
end
