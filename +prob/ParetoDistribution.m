classdef ParetoDistribution < prob.ToolboxFittableParametricDistribution
    %
    %    An object of the ParetoDistribution class represents the Pareto
    %    distubution as implemented by Clauset et al. 2009
    %       pdf(x) = (alpha-1)*xmin^(alpha-1)*x^alpha
    %
    %    The parameter ALPHA defines the power and XMIN is the scale parameter. 
    %    This distribution object can be created directly
    %    using the MAKEDIST function or fit to data using the FITDIST function.
    %
    %    ParetoDistribution methods:
    %       cdf                   - Cumulative distribution function
    %       fit                   - Fit distribution to data
    %       icdf                  - Inverse cumulative distribution function
    %       iqr                   - Interquartile range
    %       mean                  - Mean
    %       median                - Median
    %       paramci               - Confidence intervals for parameters
    %       pdf                   - Probability density function
    %       proflik               - Profile likelihood function
    %       random                - Random number generation
    %       std                   - Standard deviation
    %       truncate              - Truncation distribution to an interval
    %       var                   - Variance
    %
    %    ParetoDistribution properties:    
    %       DistributionName      - Name of the distribution
    %       a                     - Value of the a parameter, increase of
    %                               hazard
    %       NumParameters         - Number of parameters
    %       ParameterNames        - Names of parameters
    %       ParameterDescription  - Descriptions of parameters
    %       ParameterValues       - Vector of values of parameters
    %       Truncation            - Two-element vector indicating truncation limits
    %       IsTruncated           - Boolean flag indicating if distribution is truncated
    %       ParameterCovariance   - Covariance matrix of estimated parameters
    %       ParameterIsFixed      - Two-element boolean vector indicating fixed parameters
    %       InputData             - Structure containing data used to fit the distribution
    %       NegativeLogLikelihood - Value of negative log likelihood function
    %
    %    See also fitdist, makedist.

    % All ProbabilityDistribution objects must specify a DistributionName
    properties(Constant)
        %DistributionName Name of distribution
        %    DistributionName is the name of this distribution.
        DistributionName = 'pareto';
    end

    % Optionally add your own properties here. For this distribution it's convenient
    % to be able to refer to the mu and sigma parameters by name, and have them
    % connected to the proper element of the ParameterValues property. These are
    % dependent properties because they depend on ParameterValues.
    properties(Dependent = true)
       
        %A Linear growth parameter A= growth factor
        alpha
        xmin
    end
    
    
    % All ParametricDistribution objects must specify values for the following
    % constant properties (they are the same for all instances of this class).
    properties(Constant)
        %NumParameters Number of parameters
        %    NumParameters is the number of parameters in this distribution.

        NumParameters = 2;
        
        %ParameterName Name of parameter
        %    ParameterName is a two-element cell array containing names
        %    of the parameters of this distribution.
        ParameterNames = {'alpha' 'xmin'};
        
        %ParameterDescription Description of parameter
        %    ParameterDescription is a two-element cell array containing
        %    descriptions of the parameters of this distribution.
        ParameterDescription = { 'power', 'minimum value of x'};
        typicalpars = [1.4 1];

    end

    % All ParametricDistribution objects must include a ParameterValues property
    % whose value is a vector of the parameter values, in the same order as
    % given in the ParameterNames property above.
    properties(GetAccess = 'public', SetAccess = 'protected')
        %ParameterValues Values of the distribution parameters
        %    ParameterValues is a two-element vector containing the mu and sigma
        %    values of this distribution.
        ParameterValues
    end

    methods
        % The constructor for this class can be called with a set of parameter
        % values or it can supply default values. These values should be
        % checked to make sure they are valid. They should be stored in the
        % ParameterValues property.
        function pd = ParetoDistribution(alpha, xmin)
            if nargin == 0
                alpha = 1.4;
                xmin = 1;
            end
            checkargs(alpha, xmin);
            
            pd.ParameterValues = [alpha, xmin];
            
            % All FittableParametricDistribution objects must assign values
            % to the following two properties. When an object is created by
            % the constructor, all parameters are fixed and the covariance
            % matrix is entirely zero.
            pd.ParameterIsFixed = [true, true];
            pd.ParameterCovariance = zeros(pd.NumParameters);
        end
 
        
        % Implement methods to compute the mean, variance, and standard
        % deviation.
        function m = mean(this)
            %numerical mean using the pdf
            %             info = this.getInfo;
            %             m = integral(@(x)x.*this.pdf(x), info.support(1), info.support(2))
            if this.alpha > 2
                m = (this.alpha - 1) * this.xmin / (this.alpha - 2);
            else
                m = Inf;
            end
        end
        
        function s = std(this)
            s = sqrt(this.var);
        end
        function v = var(this)
            %nice analytical solution
            if this.alpha > 3
                v = (this.xmin^2 * (this.alpha - 1)) / ((this.alpha - 2)^2 * (this.alpha - 3));
            else
                v = Inf;
            end
        end
 
        %         function haz = hazard(this, x)
        %             haz = this.a .* x;
        %         end
        

    end
    methods
        % If this class defines dependent properties to represent parameter
        % values, their get and set methods must be defined. The set method
        % should mark the distribution as no longer fitted, because any
        % old results such as the covariance matrix are not valid when the
        % parameters are changed from their estimated values.
        function this = set.alpha(this, a)
            checkargs(a, this.xmin);
            this.ParameterValues(1) = a;
            this = invalidateFit(this);
        end
        function a = get.alpha(this)
            a = this.ParameterValues(1);
        end
        function this = set.xmin(this, xmin1)
            checkargs(this.alpha, xmin1);
            this.ParameterValues(2) = xmin1;
            this = invalidateFit(this);
        end
 
        function xmin1 = get.xmin(this)
            xmin1 = this.ParameterValues(2);
        end
 
        function haz = hazard(this, x)
            haz = (this.alpha - 1) ./ x;
            haz(x < this.xmin) = nan;
        end

    end
    methods(Static)
        % All FittableDistribution classes must implement a fit method to fit
        % the distribution from data. This method is called by the FITDIST
        % function, and is not intended to be called directly
 
        function pd = fit(x, varargin)
            %fits also censored data
            fitopts = reshape(varargin, 2, length(varargin) / 2);

            if ~isempty(fitopts)
                fitopts(1, :) = lower(fitopts(1, :));
            end
            ndx = strcmp(fitopts(1, :), 'lowerbound');
            if any(ndx)
                lowerbound = fitopts{2, ndx};
            else
                lowerbound = [0 0];
            end
            ndx = strcmp(fitopts(1, :), 'upperbound');
            if any(ndx)
                upperbound = fitopts{2, ndx};
            else
                %nan means that we use the dafault= median
                upperbound = [inf nan];
            end
            ndx = strncmp(fitopts(1, :), 'cens', 4);
            if any(ndx)
                cens = fitopts{2, ndx};
            else
                cens = [];
            end
            ndx = strncmp(fitopts(1, :), 'freq', 4);
            if any(ndx)
                freq = fitopts{2, ndx};
            else
                freq = [];
            end
            ndx = strcmp(fitopts(1, :), 'truncdata');
            if any(ndx)
                truncdata = fitopts{2, ndx};
                if ~isempty(truncdata) %check if none of the truncdata is limiting
                    if all(truncdata(:, 1) <= 0) && all(truncdata(:, 2) == inf)
                        truncdata = [];
                    end
                end
            else
                truncdata = [];
            end
            if isempty(cens) || all(~cens) && isempty(truncdata) && lowerbound(1) <= 1 && lowerbound(2) == inf
                %if there is no censoring we can use the fast method of
                %Clauset (adapted for using freq)
                %[x,cens,freq] = shared.unfoldfreq(x, cens, freq);
                if isnan(upperbound(2))
                    upperbound(2) = median(x);
                end
                [a, xm, L] = plfit(x, 'datatype', 'REAL', 'limit', upperbound(2), 'freq', freq);
 
                pd = prob.ParetoDistribution.makeFitted([a, xm], -L, [], x, cens, freq);
            else
                %xmin is a problem for mle fitting, as the number of
                %observations is not constant
                pd1 = SingleDist('pareto');
                %find median using the ecdf
                if isnan(upperbound(2))
                    [F1, x1] = shared.my_ecdf(x, 'censoring', cens, 'frequency', freq);
                    upperbound(2) = x1(find(F1(:, 1) >= 0.5, 1) + 1);
                end
                pd = pd1.fitfixed(x, 'fixedpar', 'xmin', 'lowerpar', true, varargin{:}, 'parrange', x(x <= upperbound(2) & x > lowerbound(2)));
                pd = prob.ParetoDistribution.makeFitted(pd.d.ParameterValues, pd.negloglik, [], x, cens, freq);
            end
        end


        % The following static methods are required for the
        % ToolboxParametricDistribution class and are used by various
        % Statistics and Machine Learning Toolbox functions. These functions operate on
        % parameter values supplied as input arguments, not on the
        % parameter values stored in a LaplaceDistribution object. For
        % example, the cdf method implemented in a parent class invokes the
        % cdffunc static method and provides it with the parameter values.
        function [nll, acov] = likefunc(varargin) % likelihood function
            %    alpha=varargin{1};
            %    xmin=varargin{2};
            %    x=varargin{3};
            %    z = x(x >= xmin);
            %    n = length(z);
            %   L = n * log((alpha - 1) / xmin) - alpha .* sum(log(z ./ xmin))

            [nll, acov] = shared.likefunc_mydistrib(@prob.ParetoDistribution.pdffunc, @prob.ParetoDistribution.cdffunc, [], varargin{:});
        end

        function y = cdffunc(x, alpha, xmin) % cumulative distribution function
            %numerical cdf using the pdf
            if isempty(x)
                y = [];
                return;
            end
            if alpha == 1
                y = zeros(size(x));
            else
                y = 1 - (x.^(1 - alpha)) * xmin^(alpha - 1);
                y(x < xmin) = 0;
            end
        end
        
        function y = pdffunc(x, alpha, xmin) % probability density function has to be specified
            if alpha == 1
                y = zeros(size(x));
            else
                y = (alpha - 1) / xmin .* (x ./ xmin).^ -alpha;
                y(x < xmin) = 1E-300;
            end
        end
        
        function y = invfunc(p, alpha, xmin) % inverse cdf
            if isempty(p)
                y = [];
                return;
            end
            %                         ran = prob.ParetoDistribution.GetRange(varargin);
            %                         xs = linspace(ran(1), ran(2), 1000);
            %                         numpdf = prob.ParetoDistribution.pdffunc(xs, varargin{:});
            %                         cumcdf = cumtrapz(xs, numpdf);
            %                         [~, ndx] = unique(cumcdf);
            %                         y = interp1(cumcdf(ndx), xs(ndx), p);
            %                         y(p < 0 | 1 < p) = NaN;
            %                         a=varargin{1};
            %             y = (sqrt(2) .* sqrt(-log(1 - p))) ./ sqrt(a);
            %             y(p < 0 | 1 < p) = NaN;
            y = 1 ./ (-(xmin .* (p - 1)) ./ xmin.^alpha).^(1 ./ (alpha - 1));

          
        end
        
        function y = randfunc(alpha, xmin, varargin) % random number generator
            y = prob.ParetoDistribution.invfunc(rand(varargin{:}), alpha, xmin);
        end

        function ran = GetRange(cpar, minpdf, info)
            ran = prob.ParetoDistribution.invfunc([0, 1 - 1E-7], cpar{:});
            % 
            %             if nargin < 3
            %                 info = prob.ParetoDistribution.getInfo;
            %             end
            %             if nargin < 2
            %                 minpdf = 1E-100;
            %             end
            %             ran = info.support;
            %             if isinf(ran(1))
            %                 x = -100: -100: -100000;
            %                 pdf = prob.ParetoDistribution.pdffunc(x, cpar{:});
            %                 ran(1) = x(find(pdf < minpdf, 1));
            %             end
            %             if isinf(ran(2))
            %                 x = 100:100:1000000;
            %                 pdf = prob.ParetoDistribution.pdffunc(x, cpar{:});
            %                 ran(2) = x(find(pdf < minpdf, 1));
            %             end
        end
    end
    methods(Static, Hidden)
        % All ToolboxDistributions must implement a getInfo static method
        % so that Statistics and Machine Learning Toolbox functions can get information about
        % the distribution.
        function info = getInfo
            
            % First get default info from parent class
            info = getInfo@prob.ToolboxFittableParametricDistribution('prob.Linear');
            
            % Then override fields as necessary
            info.name = 'Pareto';
            info.code = 'pareto';
            info.pnames = prob.ParetoDistribution.ParameterNames;
            info.pdescription = prob.ParetoDistribution.ParameterDescription;

            % info.pnames is obtained from the ParameterNames property
            % info.pdescription is obtained from the ParameterDescription property
            info.likefunc = @prob.ParetoDistribution.likefunc;
            info.cdffunc = @prob.ParetoDistribution.cdffunc;
            info.pdffunc = @prob.ParetoDistribution.pdffunc;
            info.invfunc = @prob.ParetoDistribution.invfunc;
            info.randfunc = @prob.ParetoDistribution.randfunc;
            info.prequired = [false false]; % Change if any parameter must
            %          be specified before fitting.
            %          An example would be the N
            %          parameter of the binomial
            %          distribution.
            % info.hasconfbounds = false     % Set to true if the cdf and
            %          icdf methods can return
            %          lower and upper bounds as
            %          their 2nd and 3rd outputs.
            info.censoring = true; % Set to true if the fit
            %          method supports censoring.
            info.support = [0, Inf];
            %          Set to other lower and upper
            %          bounds if the distribution
            %          doesn't cover the whole real
            %          line. For example, for a
            %          distribution on positive
            %          values use [0, Inf].
            % info.closedbound = [false false] % Set the Jth value to
            %          true if the distribution
            %          allows x to be equal to the
            %          Jth element of the support
            %          vector.
            % info.iscontinuous = true       % Set to false if x can take
            %          only integer values.
            info.islocscale = true; % Set to true if this is a
            %          location/scale distribution
            %          (no other parameters).
            % info.uselogpp = false          % Set to true if a probability
            %          plot should be drawn on the
            %          log scale.
 
            info.fittable = true;
            info.optimopts = true; % Set to true if the fit
            %          method can be called with an
            %          options structure.
            info.logci = [false false]; % Set to true for a parameter
            %          that should have its Wald
            %          confidence interval computed
            %          using a normal approximation
            %          on the log scale.
            info.plim = [1, 1E-30; Inf, Inf];
        end
        function pd = makeFitted(par, nll, cov, x, cens, freq)
            cpar = num2cell(par);
            pd = prob.ParetoDistribution(cpar{:});
            pd.NegativeLogLikelihood = nll;
            pd.ParameterCovariance = cov;

            pd.ParameterIsFixed = false(size(par));
            pd.InputData = struct('data', x, 'cens', cens, 'freq', freq);
        end
    end
end % classdef

% The following utilities check for valid parameter values
function checkargs(alpha, xmin)
    if ~(isscalar(alpha) && isnumeric(alpha) && isreal(alpha) && isfinite(alpha))
        error('alpha must be a positive finite numeric scalar.')
    end
    if ~(isscalar(xmin) && isnumeric(xmin) && isreal(xmin) && xmin >= 0 && isfinite(xmin))
        error('xmin must be a positive finite numeric scalar.')
    end
end
