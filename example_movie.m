function res = example_movie(distrib, start, savemovie)
    %you can animate the progress of mle with/without censoring
    if nargin < 1
        distrib = 'linear';
    end
    if nargin < 2
        start = [];
    elseif ~isnumeric(start)
        error('Invalid start given, type should be numeric')
    end
    if nargin < 3
        savemovie = false;
    end
    if savemovie
        files = {'video1', 'video2'};
    else
        files = {'', ''};
    end
 
    if ischar(distrib)
        m = MixedDistribution(distrib);
        m = m.randompars;
    else
        m = distrib;
    end
    opt = statset;
    if ~isempty(start)
        if numel(start) ~= m.NumParameters
            error('Invalid start given, size of start is not correct');
        end
        opt.start = start;
    end
    [m2, avidata] = m.test_fit('options', opt);
    %select one of the replicates (as they are all the same)
    [~, ndx] = min([avidata.ll]);
    avidata = avidata(ndx);
    data = m2.InputData.data;
    data = data(:);
    figure
    plotdist(m2);
    xlim1 = get(gca, 'xlim');
    supp = m.support;
    if xlim1(1) < supp(1)
        xlim1(1) = supp(1);
    end
    if xlim1(2) > supp(2)
        xlim1(2) = supp(2);
    end
    hold on
    h = plotdist(m);
    set(h.hmain, 'color', 'b');
    figure
    res.frames1 = plotdist(m2, 'movie', avidata, 'mp4file', files{1}, 'xlim', xlim1);

    maxage = quantile(data, 0.75);
    cens = data > maxage;
    data(data > maxage) = maxage;
    figure
    plotdist(m, 'data', data, 'censoring', cens, 'xlim', xlim1);
    figure
    plotdist(m, 'data', data, 'censoring', cens, 'xlim', xlim1, 'spreadcens', xlim1(2));

    m4 = m.fit(data(~cens));
    figure
    plotdist(m4, 'xlim', xlim1)
    hold on
    h = plotdist(m, 'xlim', xlim1);
    set(h.hmain, 'color', 'b');

    [m3, avidata_c] = m.fit(data, 'censoring', cens, 'options', opt);
    [~, ndx] = min([avidata_c.ll]);
    avidata_c = avidata_c(ndx);
    figure
    res.frames2 = plotdist(m3, 'movie', avidata_c, 'mp4file', files{2}, 'xlim', xlim1, 'spreadcens', xlim1(2));

    figure
    plotdist(m3, 'xlim', xlim1);
    hold on
    h = plotdist(m, 'xlim', xlim1);
    set(h.hmain, 'color', 'b');
    figure
    plotdist(m3, 'xlim', xlim1, 'spreadcens', xlim1(2));
    hold on
    h = plotdist(m, 'xlim', xlim1);
    set(h.hmain, 'color', 'b');
end
