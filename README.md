# MixedDistribution
MATLAB code for fitting mixed distributions beyond Gaussian mixtures. It also has new distributions classes based on survival analysis

## Requirements

Needed for running the code\
*MATLAB version R2020a or later is needed.\
*Statistics toolbox\
*Parallel computing supported (replace parfor with for if needed)


## Description
This package has a class MixedDistribution that can fit mixtures of distributions to data. It can combine all fittable MATLAB distributions (but obviously not a combination of continuous and discrete distributions). It uses an Expectation-Maximalization algorithm for fitting, and you can plot the results in different ways.\
It also has tools for Survival analysis and some extra distributions based on assumed hazard functions.\
In addition to MATLAB's right [censoring](https://en.wikipedia.org/wiki/Censoring_(statistics)) (before version R2021), it also supports "interval censoring" and "left censoring" and "data truncation".

New support for two power law distributions: the Pareto and Tapered Pareto distribution (including fitting xmin).



## Getting Started
See example.mlx for different ways to use the code. Also test\test_mixeddistr.m can be helpful.

 
## Author
Egbert van Nes (egbert.vanNes@wur.nl)\
Wageningen University\
Aquatic Ecology and Water Quality Management group\
PO Box 47\
6700 AA Wageningen \
The Netherlands\
