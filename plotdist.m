function handles = plotdist(this, varargin)
    %function plotdist - plot distribution in various ways
    %To be used for MixedDistributions or other distributions objects 
    %
    % h = plotdist(obj, param, value, param,values...)
    %    obj is the MixedDistribution, gmdistribution or
    %    ParametericDistribution object.
    %
    %   param, value are the parameter, value pairs. The following parameters
    %   are supported:
    %     'aic'       - show AIC
    %     'bootstrap' - show range of bootstrap (if available)
    %     'censoring' - true if data are right censored
    %     'data'      - the data (if not included in the object)
    %     'frequency' - frequency of the data
    %     'movie'     - give parameter values to run a movie, save the
    %                   frames
    %     'mp4file'   - give name of the mp4 file to store the movie (empty = no save)
    %     'kernel'    - plot a kernel using these options (pdf only)
    %     'negloglik' - plot negative log likelihood
    %     'numbins'   - number of bins for histograms
    %     'binedges'  - the edges of the bins of the histogram of data
    %     'binscale'  - the scale of the bins: 'linear' or 'log'
    %     'pars'      - override ParameterValues
    %     'title'     - main title
    %     'title2'    - sub title
    %     'type'      - the type of plot: pdf (default), cdf, powerlaw,  survivor or hazard
    %     'scaletodata' - specific option for powerlaw distribution, to
    %                  scale cdf to the cdf of the full data (true/false)
    %     'xlim'      - the limits of the xaxis
    %     'ylim'      - the limits of the yaxis
    %     'spreadcens'- spread the right-censored data over the size of the histogram
    %     'fastupdate'- update the fitted distribution only (used for movie)
    %     'linecolor' - color of lines
    %    
    % 
    % 

    %cells must become double cells otherwise struct is not working
    if isstruct(this) && isfield(this, 'frames')
        %save movie frames
        if ~isempty(varargin)
            this.mp4file = varargin{1};
        elseif ~isfield(this, 'mp4file')
            this.mp4file = '';
        end
        if isempty(this.mp4file)
            disp('showing video frames, save to mp4 by specifying filename as extra argument');
            %figure(gcf);
            playmovie(this.frames);
        else
            fprintf('writing movie to "%s"\n', this.mp4file)
            writer = VideoWriter(this.mp4file, 'MPEG-4');
            writer.open;
            writeVideo(writer, this.frames);
            writer.close;
        end
        return
    end
    if ishold
        holdstate = 'on';
    else
        holdstate = 'off';
    end
    [options, res] = shared.gatherplotdata(this, varargin{:});
    if isfield(res, 'handles')
        handles = res.handles;
        return;
    end
    
    if strcmp(options.binscale,'log')&&isempty(options.binedges)
        options.binedges=logspace(log10(min(options.data)),log10(max(options.data)),options.numbins);
    end
        
    
    if ~isempty(options.data) && ~options.fastupdate
        switch options.type
            case 'pdf'
                if ~res.info.iscontinuous
                    handles1.hdata = histogram(options.data(~options.censoring), 'Normalization', 'pdf', 'BinEdges', res.t);
                else
                    if any(options.censoring) || res.intervalcens
                        if ~isempty(options.binedges)
                            handles1.hdata = shared.hist_cens(options.data, options.censoring, 'BinEdges', options.binedges);
                        else
                            handles1.hdata = shared.hist_cens(options.data, options.censoring, 'NumBins', options.numbins, 'spreadcens', options.spreadcens);
                        end
                        set(handles1.hdata(2), 'FaceColor', [0.9 0.7 0.7], 'EdgeColor', [0.9 0.6 0.6]);
                    else
                        if isempty(options.binedges)
                            handles1.hdata = histogram(options.data(~options.censoring), 'Normalization', 'pdf', 'NumBins', options.numbins);
                        else
                            handles1.hdata = histogram(options.data(~options.censoring), 'Normalization', 'pdf', 'BinEdges', options.binedges);
                        end
                    end
                end
                hold on
                set(handles1.hdata(1), 'FaceColor', [0.6235 0.8471 0.9294], 'EdgeColor', [0.5 0.7 0.9294]);
            case {'survivor', 'survival'}
                if ~res.intervalcens
                    [f, xecdf, flo, fup] = shared.my_ecdf(options.data, 'censoring', options.censoring, 'Function', 'survivor', 'Bounds', 'on');
                    hold on
                    flo(1) = f(1);
                    fup(1) = f(1);
                    flo(end) = f(end);
                    fup(end) = f(end);
                    handles1.hecdf_range = fillstairs(xecdf, xecdf, flo, fup, [1 0.7 0.7], 'EdgeColor', 'none');
                    handles1.hecdf = stairs(xecdf, f, 'b-');
                else
                    error('ecdf not yet implemented for interval censoring');
                end
            case 'cdf'
                if ~res.info.iscontinuous
                    handles1.hdata = histogram(options.data(~options.censoring), 'Normalization', 'cdf', 'BinEdges', res.t);
                    set(handles1.hdata, 'FaceColor', [0.6235 0.8471 0.9294], 'EdgeColor', [0.5 0.7 0.9294]);
                else
                    if ~res.intervalcens
                        handles1.hdata = histogram(options.data(~options.censoring), 'Normalization', 'cdf', 'NumBins', options.numbins);
                        set(handles1.hdata, 'FaceColor', [0.6235 0.8471 0.9294], 'EdgeColor', [0.5 0.7 0.9294]);
                    else
                        warning('Cannot show data with interval censoring')
                    end
                end
               
                hold on
            case 'powerlaw'
                if ~isempty(options.binedges)
                    handles1.hdata = stairs(res.t, res.c, '-');
                    hold on
                    set(gca, 'xscale', 'log', 'yscale', 'log');
                else
                    ndx = ~options.censoring;
                    marker = 'bo';
                    handles1.hdata = loglog(res.t(ndx), res.c(ndx), marker, 'MarkerSize', 3, 'MarkerFaceColor', [1 1 1], 'tag', 'powerlaw1');
                    hold on
                    handles1.hdata = loglog(res.t(~ndx), res.c(~ndx), marker, 'MarkerSize', 3, 'MarkerFaceColor', [1 0 0], 'tag', 'powerlaw2');
                end
                set(gca, 'xscale', 'log', 'yscale', 'log');
        end
        if options.bootstrap && ~isempty(res.bootdata)
            tt = [res.t(:)', fliplr(res.t(:)')];
            cc = [res.bootdata.CL(1, :), fliplr(res.bootdata.CL(2, :))];
            ndx = ~isnan(cc);
            handles1.hboot = fill(tt(ndx), cc(ndx), [0.7 0.7 1], 'EdgeColor', 'none');
            alpha(0.7);
            if strcmp(options.type, 'powerlaw')
                set(gca, 'xscale', 'log', 'yscale', 'log');
            end
        end
        
        % fprintf('Minus log-likelihood = %g\n', this.negloglik);
        hold on
    elseif options.fastupdate
        hax = gca;
        h = findobj(hax, 'tag', 'hcomps');
        delete(h);
        h = findobj(hax, 'tag', 'hmain');
        delete(h);
        hold on
    else
        hold(holdstate);
    end


    if ~isempty(options.title) && ~options.fastupdate
        title(options.title);
    end
    if options.negloglik
        handles1.hnegloglik = text(0.8, 0.9, sprintf('L=%4g', res.negloglik), 'units', 'normalized', 'FontSize', 8, 'tag', 'small-text');
    end
    if ~isempty(options.rtext)
        handles1.hrtext = text(0.97, 0.9, options.rtext, 'units', 'normalized', 'FontSize', 8, 'tag', 'small-text', 'HorizontalAlignment', 'right');
    end


    if options.aic
        if isempty(options.rtext)
            handles1.haic = text(0.97, 0.9, sprintf('AIC=%d', round(res.aic)), 'units', 'normalized', 'FontSize', 8, 'tag', 'small-text', 'HorizontalAlignment', 'right');
        elseif ~isempty(options.title2)
            if shared.strcontains(options.title2, newline)
                options.title2 = sprintf('%s AIC=%d', options.title2, round(res.aic));
            else
                options.title2 = sprintf('%s\nAIC=%d', options.title2, round(res.aic));
            end
        else
            options.title2 = sprintf('AIC=%d', round(res.aic));
        end
    end

    if ~isempty(options.title2)
        handles1.htitle2 = text(0.05, 0.95, options.title2, 'units', 'normalized', 'FontSize', 8, 'tag', 'small-text', 'VerticalAlignment', 'top');
    end
    
    if ~options.fastupdate
        ylabel(res.y_label);
    end
    if strcmp(options.type, 'powerlaw')
        f = find(res.overallfun == 1, 1, 'last');
        if ~isempty(f)
            ndx = true(size(res.t));
            ndx(1:f) = false;
            res.t = res.t(ndx);
            res.overallfun = res.overallfun(ndx);
            res.compsfun = res.compsfun(:, ndx);
        end
        for i = 1:size(res.compsfun, 1)
            f = find(res.overallfun == 1, 1, 'last');
            ndx = true(size(res.t));
            ndx(1:f) = false;
            res.compsfun(i, ~ndx) = nan;
        end
        if ~isempty(res.overallfun)
            handles1.hmain = loglog(res.t, res.overallfun, '-','color', options.linecolor, 'LineWidth', 2);
        end
        hold on;
        yl = ylim;
        if res.NumComponents > 1
            handles1.hcomps = loglog(res.t, res.compsfun, '--','color','b', 'tag', 'hcomps');
        end
        ylim(yl);
        xlabel('x')
    elseif ~isempty(res.overallfun)
        if res.NumComponents > 1
            handles1.hcomps = plot(res.t, res.compsfun, '--','color','b', 'tag', 'hcomps');
            hold on;
        end
        handles1.hmain = plot(res.t, res.overallfun, '-','color', options.linecolor, 'tag', 'hmain');
    end
    hold(holdstate);
    if ~isempty(options.ylim)
        ylim(options.ylim);
    end
    if ~isempty(options.xlim)
        xlim(options.xlim);
    end

    if nargout > 0
        handles = handles1;
    end
  

    function h = fillstairs(x, xback, y, yback, varargin)
        x = repmat(x(:), 1, 2)';
        x = x(:);
        x = x(2:end);
        xback = repmat(xback(:), 1, 2)';
        xback = xback(:);
        xback = xback(2:end);
        y = repmat(y(:), 1, 2)';
        y = y(:);
        y = y(1:end - 1);
        yback = repmat(yback(:), 1, 2)';
        yback = yback(:);
        yback = yback(1:end - 1);
        h = fill([x; flipud(xback)], [y; flipud(yback)], varargin{:});
    end

end


