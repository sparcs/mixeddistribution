function res = lilliefors(this, varargin)
    %LILLIEFORS test whether the data that were used to fit a distribution
    %are from that distribution. The Lilliefors test uses bootstrapping to
    %find the ranges of the ranges of the larges distances between cdf's
    %(i.e. Kolmogorov-Smirnov statistic). For some distributions (normal,	
    %exponential, extreme value, lognormal and Weibull) you can 
    %also perform the matlab version (lillietest), which uses fixed tables,
    %but can alternatively also do bootstrapping (see 'mctol').
    %
    %res=pd.lilliefors('alpha',0.05,'mctol',0.01)
    %'alpha' single-sided confidence limit (0.05)
    %'mctol' tolerated SE for p runs till the desired tolerance is found
    %- can be very slow! (empty for no checking)   
    %
    %Null hypothesis: the data is from this distribution
    %so if rejected it is from another distribution.
    %H=1 null hypothesis rejected, different distribution
    %H=0 null hypothesis accepted, no significant difference
    %
    if ~isfield(this.InputData, 'data') || isempty(this.InputData.data)
        error('MixedDistribution:lilliefors', 'First fit to data and run bootstrap before running lillifors');
    elseif ~isempty(this.InputData.cens) && any(this.InputData.cens)
        warning('MixedDistribution:lilliefors', 'Censoring not supported in the lilliefors test, censored data ignored');
    end
    data=this.InputData.data;
    if ~isempty(this.InputData.cens)
        data=data(~this.InputData.cens);
    end
    s=this.mixedname(this.Distributions, this.ComponentProportion, true);

    res=struct('lillietest',[],'bootstrap',[],'distribution',s,'nboot',[],'alpha',[],'KSstat',[],'criticalValue',[],'p',[],'SE_p',[]);
    opts = reshape(varargin, 2, length(varargin) / 2);
    opts(1, :) = lower(opts(1, :));
    allopts = {'alpha','mctol'};
    if any(~ismember(opts(1, :), allopts))
        error('MixedDistribution:lilliefors', 'Option unknown, valid options are: %s', sprintf('''%s'' ', allopts{:}));
    end
    ndx = strcmp(opts(1, :), 'alpha');
    if any(ndx)
        alpha = opts{2, ndx};
    else
        alpha = 0.05;
    end
    ndx = strcmp(opts(1, :), 'mctol');
    if any(ndx)
        mctol = opts{2, ndx};
    else
        mctol = [];
    end
    if this.NumComponents == 1 && any(strcmpi(this.Components{1}.DistributionName, {'normal', 'exponential', 'extreme value'}))
        [H, pValue, KSstatistic, criticalValue] = lillietest(data, 'Distribution', this.Components{1}.DistributionName, 'alpha', alpha);
        res.lillietest = struct('H', H, 'p', pValue, 'KSstat', KSstatistic, 'critval', criticalValue, 'alpha', alpha);
    end
    if this.NumComponents == 1 && any(strcmpi(this.Components{1}.DistributionName, {'lognormal', 'weibull'}))
        ndx = strcmpi(this.Components{1}.DistributionName, {'lognormal', 'weibull'});
        distribs = {'normal', 'extreme value'};
        [H, pValue, KSstatistic, criticalValue] = lillietest(log(data), 'Distribution', distribs{ndx}, 'alpha', alpha);
        res.lillietest = struct('H', H, 'p', pValue, 'KSstat', KSstatistic, 'critval', criticalValue, 'alpha', alpha);
    end
    % distributions that are supported by lillietest:
    % 'normal'	Normal distribution
    % 'exponential'	Exponential distribution
    % 'extreme value'	Extreme value distribution
    % 'lognormal' To test x for a lognormal distribution, test if log(x) has a normal distribution.
    % 'Weibull' To test x for a Weibull distribution, test if log(x) has an extreme value distribution.


    if isempty(this.BootstrapData)
        if ~isempty(res)
            return;
        end
        error('MixedDistribution:lilliefors', 'First run a bootstrap before running lillifors');
    end
    [~, ~, res.KSstat] = kstest(data, 'cdf', [data, this.cdf(data)]);
    if ~isempty(mctol)
        [res.criticalValue, res.p, res.bootstrap.allvalues] = lillieMC1(res.KSstat, numel(data), alpha, this, mctol);
        res.nboot=numel(res.bootstrap.allvalues);
    else
        res.bootstrap = this.bootCL('ksstat', 'alpha', alpha, 'tails', 'right');
        res.nboot = numel(res.bootstrap.allvalues);
        res.p = max(sum(res.bootstrap.allvalues > res.KSstat) / res.nboot,1/res.nboot);
        res.criticalValue = res.bootstrap.CL;
    end
    res.alpha = alpha;
    res.H = res.p < alpha;
    res.SE_p = sqrt(max(res.p * (1 - res.p) / res.nboot, 1 / res.nboot^2));

end

function [crit, p, KSstatTot] = lillieMC1(KSstat, n, alpha, distr, mctol)
    %LILLIEMC Simulated critical values and p-values for Lilliefors' test.
    %   [CRIT,P] = LILLIEMC(KSSTAT,N,ALPHA,DISTR,MCTOL) returns the critical value
    %   CRIT and p-value P for Lilliefors' test of the null hypothesis that data
    %   were drawn from a distribution in the family DISTR, for a sample size N
    %   and confidence level 100*(1-ALPHA)%.  P is the p-value for the observed
    %   value KSSTAT of the Kolmogorov-Smirnov statistic.  DISTR is 'norm', 'exp',
    %   'or 'ev'. ALPHA is a scalar or vector.  LILLIEMC uses Monte-Carlo
    %   simulation to approximate CRIT and P, and chooses the number of MC
    %   replications, MCREPS, large enough to make the standard error for P,
    %   SQRT(P*(1-P)/MCREPS), less than MCTOL.

    vartol = mctol^2;

    crit = 0;
    p = 0;
    mcRepsTot = 0;
    mcRepsMin = 1000;
    KSstatTot=[];
    while true
        mcRepsOld = mcRepsTot;
        mcReps = ceil(mcRepsMin - mcRepsOld);
        KSstatMC = zeros(mcReps, 1);
        
        %calculates new bootstrapvalues
        parfor rep = 1:length(KSstatMC)
            pd = distr;
            yCDF = (0:n)' / n;
            x = pd.random(n, 1);
            pd1 = pd.fit(x, 'replicates', 1, 'silent', true); % MLE fit to the data
            xCDF = sort(x); % unique values, no need for ECDF
            nullCDF = pd1.cdf(xCDF);
            delta1 = yCDF(1:end - 1) - nullCDF;
            delta2 = yCDF(2:end) - nullCDF;
            KSstatMC(rep) = max(abs([delta1; delta2]));
        end
       
        critMC = prctile(KSstatMC, 100 * (1 - alpha));
        pMC = sum(KSstatMC > KSstat) ./ mcReps;
        KSstatTot=[KSstatTot;KSstatMC]; %#ok<AGROW>
        mcRepsTot = mcRepsOld + mcReps;
        crit = (mcRepsOld * crit + mcReps * critMC) / mcRepsTot;
        p = (mcRepsOld * p + mcReps * pMC) / mcRepsTot;

        % Compute a std err for p, with lower bound (1/N)*(1-1/N)/N when p==0.
        sepsq = max(p * (1 - p) / mcRepsTot, 1 / mcRepsTot^2);
        if sepsq < vartol
            break
        end

        % Based on the current estimate, find the number of trials needed to
        % make the MC std err less than the specified tolerance.
        mcRepsMin = 1.2 * (mcRepsTot * sepsq) / vartol;
    end

    if isnan(KSstat)
        p = 0;
    end
end
