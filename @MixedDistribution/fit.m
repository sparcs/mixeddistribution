function [this, results1] = fit(this, data, varargin)
    %     MixedDistribution/fit
    %   
    %     This is the main iterative EM algorithm. It has two steps Expectation and
    %     Maximization. The loop stops when there is no improvement in
    %     log-likelihood.
    %     First define a 1D mixture components by creating a
    %     MixedDistribution.
    % 
    %     Usage: 
    %     res = pd.fit(data, param, value, param, value...)
    %     pd = MixedDistribution object, parameters will change during fit
    %     data = vector with 1D data
    %     param,value extra options 
    %     'replicates' Number of replicate searches. As the method can easily hit a local
    %                optimum it is recommended to have at least 10 replicates. (default = 10)
    %     'ínitial'  Vector of initial parameter values or a
    %                MixedDistribution object defining the parameters. This will be
    %                the initial conditions of the first replicate run.
    %     'lowerbound' vector with the lower bounds for the parameters.
    %     'upperbound' vector with the upper bounds for the parameters.
    %     'initfun'  Kind of drawing of initial parameters, ('kmeans','range','quantiles')
    %                See the method initfun for details. default 'kmeans'
    %     'truncate' range of trucation of the resulting distribution
    %                []=none
    %     'truncdata' detection range for each data point. matrix 2xn [min max]
    %                []=none (default) inf = max, -inf = min
    %     'censoring' vector with true for right censored data (default all false)
    %     'frequency' vector with frequencies of data (default all 1)
    %     'censmethod' Way of handling censored data (for testing)
    %                'ignore','remove','deal', 'deal' (=default) is by far
    %                the best method as the other ways are very biased.
    %     'fitgmdist' logical which is true if fitgmdist is used (for
    %                 Normal distributions only)
    %     'options'  a struct created with statset. The TolFun option is
    %                used only. Additional fields 'start',
    %     <name of parameter> value: fix the value the parameter (See
    %     the property ParameterNames for a list of parameter names).
    %
    %     output:
    %     res        struct with some information about the runs.
    %               'pars'  parameter and mixture values (vector)
    %               'll'    log-likelihood
    %               'ncomp' Number of efffectively different components.
    %               (the method can converge with some duplicate components
    %               or with some components with almost zero probability)
    %
    %
    %     This code based on the EM code for gausian mixtures by Reza Ahmadzadeh
    %     and partly on MATLAB's gmdistribution

    warning('off', 'stats:mle:EvalLimit');
    warning('off', 'stats:mle:IterLimit');
    ndx = find(cellfun(@iscell, varargin));
    for i = ndx
        varargin{i} = varargin(i);
    end
    %Valid options (copy changes to fit_steps and (except 'censoring' and 'frequency') to bootstrap and test_fit!)
    validoptions = [{'censoring', 'data', 'datarange', 'truncdata', 'lowerbound', 'upperbound', 'fitgmdist', 'frequency', 'replicates', 'initial', 'options', 'initfun', 'censmethod', 'silent', 'truncate', 'maxweight'}, this.ParameterNames(:)'];
  
    if nargin == 2 && ischar(data) && strcmp(data, '-validoptions')
        this = validoptions;
        return;
    end

    if ischar(data)
        varargin = [{data}, varargin];
        data = [];
    end
    origdata = data;
    fitopts = reshape(varargin, 2, length(varargin) / 2);
 
    if ~isempty(fitopts)
        parnames = this.ParameterNames;
        for i = 1:numel(parnames)
            ndx = strcmp(parnames{i}, fitopts(1, :));
            if any(ndx)
                par_comp = regexp(parnames{i}, '_', 'split');
                if strcmp(par_comp{1}, 'Pi')
                    error('MixedDistribution:fit', 'Cannot fix component proportions for fit (%s)', parnames{i});
                end
                if numel(par_comp) == 1
                    compnr = 1;
                else
                    compnr = str2double(par_comp{2});
                end
                this.Components{compnr} = this.Components{compnr}.set(par_comp{1}, fitopts{2, ndx}, [], true);
                fitopts = fitopts(:, ~ndx);
            end
        end
        fitopts(1, :) = lower(fitopts(1, :));
    end
    passingopts = repmat({{}}, 1, this.NumComponents);
    lowerPi = zeros(1, this.NumComponents);
    upperPi = lowerPi + 1;
    allpars = this.AllParameters;
    ndx = strcmp(fitopts(1, :), 'upperbound');
    if any(ndx)
        ub = fitopts{2, ndx};
        if numel(ub) ~= numel(allpars(~[allpars.isfixed]))
            s = this.ParameterNames;
            s = sprintf('%s,', s{:});
            error('Size of upperbound should match the total number of parameters(%d)\nThe order is %s', numel(allpars), s(1:end - 1));
        end
        Pindx = strcmp({allpars.realname}, 'Pi');
        upperPi(1:end - 1) = ub(Pindx);
        for m = 1:this.NumComponents
            passingopts{m} = [passingopts{m} {'upperbound', ub(~Pindx & [allpars.component] == m)}];
        end
        if any(sign(ub) == -1 & isinf(ub))
            error('upperbound cannot be -inf');
        end
    else
        ub = [];
    end
    ndx = strcmp(fitopts(1, :), 'truncdata');
    if any(ndx)
        truncdata = fitopts{2, ndx};
        for m = 1:this.NumComponents
            passingopts{m} = [passingopts{m} {'truncdata', truncdata}];
        end
    end
    ndx = strcmp(fitopts(1, :), 'lowerbound');
    if any(ndx)
        lb = fitopts{2, ndx};
        if numel(lb) ~= numel(allpars(~[allpars.isfixed]))
            s = this.ParameterNames;
            s = sprintf('%s,', s{:});
            error('Size of lowerbound vector should match the total number of parameters(%d)\nThe order is %s', numel(allpars), s(1:end - 1));
        end
        Pindx = strcmp({allpars.realname}, 'Pi');
        lowerPi(1:end - 1) = lb(Pindx);
        for m = 1:this.NumComponents
            passingopts{m} = [passingopts{m} {'lowerbound', lb(~Pindx & [allpars.component] == m)}];
        end
        if any(sign(lb) == 1 & isinf(lb))
            error('lowerbound cannot be +inf');
        end
        if ~isempty(ub) && any(lb > ub)
            error('lowerbound should be smaller than or equal to the upperbound')
        end
    end
 
    ndx = strcmp(fitopts(1, :), 'fitgmdist');
    if any(ndx)
        %use fitgmdist
        logfit = false;
        if all(strcmpi(this.Distributions, 'Lognormal'))
            origdata = data;
            data = log(data);
            logfit = true;
        elseif any(~strcmp(this.Distributions, 'Normal'))
            error('MixedDistribution:fit:type', 'fitgmdist cannot fit other distributions than Normal or Lognormal');
        end
        run_fitgmdist = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
        if run_fitgmdist
            ndx = strcmp(fitopts(1, :), 'censoring');
            if any(ndx) && all(~fitopts{2, ndx})
                fitopts = fitopts(:, ~ndx);
            end
            ndx = strcmp(fitopts(1, :), 'frequency');
            if any(ndx) && all(fitopts{2, ndx} == 1)
                fitopts = fitopts(:, ~ndx);
            end

            ndx = strcmp(fitopts(1, :), 'k');
            if any(ndx)
                k = fitopts{2, ndx};
                fitopts = fitopts(:, ~ndx);
            else
                k = this.NumComponents;
            end
            gm = fitgmdist(data, k, fitopts{:});
            this = MixedDistribution(gm, logfit);
            if logfit
                %the loglikelihood seems to be different
                this.negloglik = this.likefunc(origdata);
                data = origdata;
            end
            this.InputData = struct('data', data, 'cens', [], 'freq', [], 'fitoptions', {varargin});
            results1 = gm;
            return;
        end
    end
    
    
    if any(~ismember(fitopts(1, :), validoptions))
        error('MixedDistribution:fit:optionsnotvalid', 'Options invalid.\nValid options are: %s\n', sprintf('''%s'' ', validoptions{:}));
    end
    ndx = strcmp(fitopts(1, :), 'data');
    if any(ndx)
        data = fitopts{2, ndx};
        origdata = data;
        fitopts = fitopts(:, ~ndx);
    end
    origcens = [];
    origfreq = [];
    ndx = strcmp(fitopts(1, :), 'datarange');
    %alternative way of entering interval censoring
    if any(ndx)
        datarange = fitopts{2, ndx};
        if size(datarange, 2) == 2
            data = datarange(:, 1);
            cens = datarange(:, 2);
        end
        
        fitopts = fitopts(:, ~ndx);
    else
        cens = [];
    end

    ndx = strcmp(fitopts(1, :), 'truncate');
    if any(ndx)
        truncat = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
        this.Truncation = truncat;
    end
    if this.IsTruncated
        %          %two step process, first without truncation, then finetuning with
        %          %truncation;
%         trunc = this.Truncation;
%         if any(data <= max(trunc(:, 1)) | data >= min(trunc(:, 2)))
%             warning('fit_truncated:invaliddata', 'There is some data outside the truncation range of the distribution, removed');
%             data = data(data > max(trunc(:, 1)) & data < min(trunc(:, 2)), :);
%         end
%         disp('Step 1: Fitting without truncation')
%         notrunc = MixedDistribution(this.Distributions);
%         notrunc.ParameterValues = this.ParameterValues;
%         notrunc.Truncation = [];
%         notrunc = notrunc.fit(data, varargin{:});
%         this.ParameterValues = notrunc.ParameterValues;
%         disp('Step 2: Fitting with truncation');
    end
    save_details = nargout > 0;
    if save_details && this.NumComponents == 1
        ndx = strcmp(fitopts(1, :), 'options');
        if any(ndx)
            opts = fitopts{2, ndx};
        else
            ndx = size(fitopts, 2) + 1;
            opts = statset;
        end
        if isempty(opts) || isempty(statget(opts, 'outputfcn'))
            %if you use statset extra options are removed
            opts.OutputFcn = @saveprogress_mle;
            fitopts(:, ndx) = {'options'; opts};
        end
    end
    ndx = strcmp(fitopts(1, :), 'replicates');
    if any(ndx)
        nrep = fitopts{2, ndx};
    else
        nrep = 10;
    end
    ndx = strcmp(fitopts(1, :), 'silent');
    if any(ndx)
        silent = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
    else
        silent = false;
    end
    
    ndx = strcmp(fitopts(1, :), 'maxweight');
    if any(ndx)
        maxweight = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
    else
        maxweight = 10;
    end
    if length(maxweight) == 1
        maxweight = [maxweight maxweight];
    end
    maxweight = log(maxweight);
    ndx = strcmp(fitopts(1, :), 'censoring');
    if any(ndx)
        cens = fitopts{2, ndx};
        if size(cens, 1) < size(cens, 2)
            cens = cens';
        end
        origcens = cens;
        if ~isempty(cens) && all(cens == 1 | cens == 0) %else interval censoring
            cens = logical(cens);
        end
        fitopts = fitopts(:, ~ndx);
    end
    ndx = strcmp(fitopts(1, :), 'frequency');
    if ~any(ndx)
        freq = [];
    else
        freq = fitopts{2, ndx};
        if size(freq, 1) < size(freq, 2)
            freq = freq';
        end
        origfreq = freq;
        fitopts = fitopts(:, ~ndx);
        if ~isempty(freq) && this.NumComponents > 1
            [data, cens, freq] = shared.unfoldfreq(data, cens, freq);
            %             if any(freq > 1)
            %                 %we need to unfold frequencies for easier selection
            %                 cens1 = logical([]);
            %                 data1 = [];
            %                 for i = 1:length(freq)
            %                     %maybe a bit slow, but runs only once
            %                     data1 = [data1; repmat(data(i), freq(i), 1)]; %#ok<AGROW>
            %                     if ~isempty(cens)
            %                         cens1 = [cens1; repmat(cens(i), freq(i), 1)]; %#ok<AGROW>
            %                     end
            %                 end
            %                 data = data1;
            %                 cens = cens1;
            %             elseif any(freq == 0)
            %                 data = data(freq > 0);
            %                 if ~isempty(cens)
            %                     cens = cens(freq > 0);
            %                 end
            %             end
            %             freq = [];
        end
    end
    ndx = strcmp(fitopts(1, :), 'censmethod');
    if any(ndx)
        if strcmp(fitopts{2, ndx}, 'ignore')
            if ~islogical(cens)
                ndx = ~(isinf(data) | isinf(cens));
                data(ndx) = (data(ndx) + cens(ndx)) / 2;
                data(isinf(data)) = cens(isinf(data)); %left censoring
            end
            cens = [];
        elseif strcmp(fitopts{2, ndx}, 'remove') && ~isempty(cens)
            if ~islogical(cens)
                cens = cens > data;
            end
            data = data(~cens);
            cens = [];
        end
    end
    ndx = strcmp(fitopts(1, :), 'initial');
    if ~any(ndx)
        initial = [];
    else
        initial = fitopts{2, ndx};
        if isa(initial, 'MixedDistribution')
            initial = initial.ParameterValues;
        end
    end
    
    ndx = strcmp(fitopts(1, :), 'initfun');
    if ~any(ndx)
        opts_initfun = ''; %empty gives default defined in this.initfun
    else
        opts_initfun = fitopts{2, ndx};
    end
    if ~isstruct(opts_initfun) % you can also enter the functions as a struct
        if ~iscell(opts_initfun)
            opts_initfun = this.initfun(opts_initfun);
        else
            opts_initfun = this.initfun(opts_initfun{:});
        end
    end
    ndx = strcmp(fitopts(1, :), 'options');
    if any(ndx)
        options = fitopts{2, ndx};
    else
        options = statset;
    end
    if size(data, 1) < size(data, 2)
        data = data';
        origdata = origdata';
    end
    %shift = 10000; % a big number
    if ~isfield(options, 'TolFun') || isempty(options.TolFun)
        options.TolFun = 1E-13;
    end
    if isempty(cens)
        cens = false(size(data));
    end
    if isempty(freq)
        freq = ones(size(data));
    end
    initfun1 = this.initfun('-start', opts_initfun, data, cens, freq);
    %this = initialize_fit(this, data, varargin);
    results = struct('pars', cell(nrep, 1), 'll', 0, 'ncomp', 1, 'details', []);
    TolFun = options.TolFun;
    for m = 1:this.NumComponents
        passingopts{m} = [passingopts{m} {'options', options}];
    end
    if this.NumComponents == 1 %if there is only one component, we can simply fit the component
        %We do not need to do the EM algorithm
        for j = 1:nrep
            this1 = this;
            saveprogress_mle('-reset');
            if ~isempty(initial) && j == 1
                if ~silent
                    fprintf('Replicate 1 uses given initial settings: %s\n', mat2str(initial, 4))
                end
                this1.ParameterValues = initial;
            else
                this1 = this1.initfun('-init', initfun1);
                % this1 = initializepars(this1);
            end
            this1.Components{1} = this1.Components{1}.fit(data, 'censoring', cens, 'frequency', freq, passingopts{1}{:});
            ll = this1.Components{1}.negloglik;
            %code for testing
            %             if any(strcmp(this1.Components{1}.code, {'pareto', 'taperedpareto'}))
            %                 ndx = (data >= this1.Components{1}.d.xmin);
            %                 ll2 = this1.likefunc(data(ndx), cens(ndx), freq(ndx));
            %             else
            %                 ll2 = this1.likefunc(data, cens, freq);
            %             end
            %             fprintf('SingleDistr loglik: %g    Mixeddistr loglik: %g\n',ll,ll2)
            %             assert(abs(ll2 - ll) < 1E-7, 'error negloglik')
            %end code for testing
            details = [];
            if save_details
                try
                    details = saveprogress_mle;
                catch
                    details = [];
                end
            end
            results(j) = struct('pars', this1.ParameterValues, 'll', ll, 'ncomp', 1, 'details', details);
            if ~silent
                fprintf('Replicate %d: neg.log-likelihood %g\n', j, ll)
            end
        end
    else
        %main loop (parfor)
        %for debugging replace parfor with for
        parfor j = 1:nrep
            %for j = 1:nrep
            this1 = this; %We copy to minimize overhead 
            if ~isempty(initial) && j == 1
                if ~silent
                    fprintf('Replicate 1 uses given initial settings: %s\n', mat2str(initial, 4))
                end
                this1.ParameterValues = initial;
            else
                this1 = this1.initfun('-init', initfun1);
                % this1 = initializepars(this1)
            end
            ll = 0;
            llDiff = -1;
            iter = 0;
            detail = this1.ParameterValues;
            bestpar = detail;
            bestll = Inf;
            lastbest = 0;
            while ~(llDiff >= 0 && llDiff < TolFun * abs(ll) || iter > 1000 || iter - lastbest > 50)
                ll_old = ll;
                iter = iter + 1;
                % E-step
                post_clus = expectation(this1, data, cens);
        
                % M-step
                this1 = maximization(this1, data, post_clus, cens, exp(maxweight(1) + (iter - 1) ./ 1000 .* (maxweight(2) - maxweight(1))), passingopts); %#ok<PFBNS>
                this1.ComponentProportion(this1.ComponentProportion < lowerPi) = lowerPi(this1.ComponentProportion < lowerPi);
                this1.ComponentProportion(this1.ComponentProportion > upperPi) = upperPi(this1.ComponentProportion > upperPi);
                if save_details
                    detail(end + 1, :) = this1.ParameterValues; %#ok<AGROW>
                end
                % calculate the distance/error from the previous set of params
                ll = this1.likefunc(data, cens);
                if ll < bestll
                    bestll = ll;
                    bestpar = this1.ParameterValues;
                    lastbest = iter;
                end
                llDiff = ll - ll_old;
            end
            this1.ParameterValues = bestpar;
            results(j) = struct('pars', this1.ParameterValues, 'll', bestll, 'ncomp', this1.NDifferentComp, 'details', detail);
            if ~silent
                fprintf('Replicate %d: neg.log-likelihood %g  n-comps %d\n', j, bestll, results(j).ncomp)
            end
        end
    end
    if nargout > 0
        results1 = results;
    end
    ndx1 = [results(:).ncomp] == max([results(:).ncomp]);
    results = results(ndx1);
    results = results(~isnan([results.ll]));
    if isempty(results) || results(1).ncomp < this.NumComponents
        warning('MixedDistribution:fail:ncomp', 'Failed to fit all componentes');
    end
    if ~isempty(results)
        [~, ndx] = min([results.ll]);
        this.ParameterValues = results(ndx).pars;
     
        this.negloglik = results(ndx).ll;
    end
    %this.Posteriors = expectation(this, data);
    %opt=[fieldnames(fitoptions) struct2cell(fitoptions)]';
    fitopts = fitopts(:)';
    this.InputData = struct('data', origdata, 'cens', origcens, 'freq', origfreq, 'fitoptions', {fitopts});
end


function this = maximization(this, data, post_clus, cens, maxweight, passingopts)
    %returns the probabilities of being in each of the components
    for i = 1:this.NumComponents
        this.Components{i} = this.Components{i}.fit(data, 'censoring', cens, 'frequency', round(post_clus(:, i) * maxweight), passingopts{i}{:});
        % this.Components{i} = fit_component(this, i, data, cens, round(post_clus(:, i) * maxweight));
    end
    propor = sum(post_clus) ./ numel(data);
    if ~any(isnan(propor))
        this.ComponentProportion = propor;
    end
end


function post_clus = expectation(this, data, cens)
    %returns the probabilities of being in each of the components
    if nargin < 3
        cens = [];
    end
 
    if isempty(cens)
        cens = false(size(data));
    end
    if ~islogical(cens)
        %interval censoring
        maxdata = cens;
        cens = cens > data;
    else
        %standard right censoring
        maxdata = [];
    end
 
    total_xdf = zeros(size(data));
    post_clus = zeros(size(data, 1), this.NumComponents);
    if any(~cens)
        [total_pdf, post_clus1] = this.pdf(data(~cens));
        total_xdf(~cens) = total_pdf;
        post_clus(~cens, :) = post_clus1;
    end
    if any(cens)
        if isempty(maxdata)
            %standard right censoring
            [~, post_clus2] = this.cdf(data(cens));
            post_clus(cens, :) = 1 - post_clus2;
            total_xdf(cens) = sum(1 - post_clus2, 2);
        else
            %interval/left/right censoring
            [~, minpost_clus2] = this.cdf(data(cens));
            [~, maxpost_clus2] = this.cdf(maxdata(cens));
            diff_c = maxpost_clus2 - minpost_clus2;
            diff_c(diff_c < 1E-200) = 1E-200; %otherwise division by zero possible
            post_clus(cens, :) = diff_c;
            total_xdf(cens) = sum(diff_c, 2);
        end
    end
    for i = 1:this.NumComponents
        post_clus(:, i) = post_clus(:, i) ./ total_xdf;
    end
end

function res = saveprogress_mle(varargin)
    %use this as OutputFcn to save the progress of mle
    %
    % run seperately without arguments to get the results
    persistent saveprogress_parvalues
    if nargin == 0
        %return the parameter values after fitting
        res = saveprogress_parvalues;
        return;
    elseif nargin == 1
        if strcmp(varargin{1}, '-reset')
            saveprogress_parvalues = [];
            return;
        end
    end
    %add current parameters
    saveprogress_parvalues = [saveprogress_parvalues; varargin{1}];
 
    %make true to stop during fitting
    res = false;
end
