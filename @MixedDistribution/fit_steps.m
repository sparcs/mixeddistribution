function res = fit_steps(this, data, varargin)
    %fit_steps
    %use argument k to specify a list of numbers of components to fit.
    %for instance 
    % m.fit_steps(data,'k',1:5)
    % m.fit_steps(data,'k',1:5, name,par) (add other fit options)
    %
    %Fits all combinations of different distributions.
    %  'Normal','LogNormal' and k=2 fits 3 distributions:
    %  1: 'Normal','Normal'
    %  2: 'Normal','LogNormal'
    %  3: 'LogNormal','LogNormal'
    %Note that this can become many combinations!
    %
    %Returns fitted distributions in a structure with the following fields:
    %  res.pd  list of fitted distributions
    %  res.k   the number of components per distribution
    %  res.AIC the Akaike Information Criterion
    %  res.BIC the Bayesian Information Criterion
    if ischar(data)&&strncmpi('-c',data,2)
        distribs=unique(varargin{1},'stable');
        k=varargin{2};
        res={};
        for i=1:length(k)
            resk=nmultichoosek(distribs,k(i));
            res1=cell(size(resk,1),1);
            for j=1:length(res1)
                res1{j}=resk(j,:);
            end
            res=[res;res1];
        end
        return;
    end
        
    coms = reshape(varargin, 2, length(varargin) / 2);
    coms(1,:)=lower(coms(1,:));
    
    validoptions=['k',this.fit('-validoptions')];
    if any(~ismember(coms(1,:),validoptions))
        error('MixedDistribution:fit_steps:optionsnotvalid','Options invalid.\nValid options are: %s\n',sprintf('''%s'' ',validoptions{:}));
    end
%     for i = 1:2:length(varargin)
%         varargin{i} = lower(varargin{i});
%     end
%     coms = varargin(1:2:end);
    ndx = strcmp(coms(1,:), 'k');
    if any(ndx)
        k = coms{2,ndx};%varargin{1 + (ndx - 1) * 2 + 1};
        coms=coms(:,~ndx);
    else
        k = 1:5;
    end
    distrs = unique(this.Distributions);
    if length(distrs) == 1
        % pds = cell(numel(k), 1);
        ncomb = numel(k);
    else
        ncomb = 0;
        for i = k
            if i == 1
                ncomb = ncomb + length(distrs);
            else
                %returns the number of combinations with replicates
                ncomb = ncomb + nmultichoosek(length(distrs), i);
            end
        end
    end
    ndx = strcmp(coms(1,:), 'replicates');
    if any(ndx)
        nreplicates = int2str(coms{2,ndx});
    else
        nreplicates = 'default number of';
    end
    fprintf('Fitting %d models, with %s replicates\n\n', ncomb, nreplicates);
    pds = cell(ncomb, 1);
    ks = zeros(size(pds)) + NaN;
    AICs = ks;
    BICs = ks;
    ii = 0;
    for i = k
        %returns all combinations of sets of i distributions (with replicates)
        combs = nmultichoosek(1:length(distrs), i);
        for j = 1:size(combs, 1)
            ii = ii + 1;
            %fit the data to the current combination of distributions
            pd = MixedDistribution(distrs(combs(j, :)));
            
            pd=pd.fit(data, coms{:});
            
            %store the results
            pds{ii} = pd;
            ks(ii) = i;
            AICs(ii) = pd.AIC;
            BICs(ii) = pd.BIC;
        end
    end
    %save the results as a struct, store k, AIC and BIC for convenience
    res = struct('pd', {pds}, 'k', ks, 'AIC', AICs, 'BIC', BICs);
end

function combs = nmultichoosek(values, k)
    %// Return number of multisubsets or actual multisubsets.
    if numel(values) == 1
        if values == 1
            combs = ones(1, k);
        else
            n = values;
            combs = nchoosek(n + k - 1, k);
        end
    else
        n = numel(values);
        combs = bsxfun(@minus, nchoosek(1:n + k - 1, k), 0:k - 1);
        combs = reshape(values(combs), [], k);
    end
end

