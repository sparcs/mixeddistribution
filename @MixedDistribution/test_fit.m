function [m, details] = test_fit(this, varargin)
    %test_fit
    %First generate data then refit the mixture distributions and show the
    %results in histograms
    %All options of fit can also be used here
    % extra options:
    % 'xlim', the range for the figures
    % 'fract_cens' fractionnumber of right-censored data
    % 'maxfreq' maximum frequency
    % 'survival' add survivor and hazard plots
    % 'discretize' number of classes to test interval censoring
    % 'showmovie' true to show the fit as movie
    ndx = find(cellfun(@iscell, varargin));
    for i = ndx
        varargin{i} = varargin(i);
    end
    opts = reshape(varargin, 2, length(varargin) / 2);
    opts(1, :) = lower(opts(1, :));
    validoptions = [{'fract_cens', 'maxfreq', 'xlim', 'showmovie', 'survival', 'discretize'}, this.fit('-validoptions')];
    ndx = ismember(validoptions, {'censoring', 'frequency'}); %thses options cannot be used here
    validoptions = validoptions(~ndx);
    if any(~ismember(opts(1, :), validoptions))
        error('MixedDistribution:fit_steps:optionsnotvalid', 'Options invalid.\nValid options are: %s\n', sprintf('''%s'' ', validoptions{:}));
    end
    ndx = strcmp(opts(1, :), 'showmovie');
    if ~any(ndx)
        showmovie = false;
    else
        showmovie = opts{2, ndx};
        opts = opts(:, ~ndx);
    end
    ndx = strcmp(opts(1, :), 'xlim');
    if ~any(ndx)
        opt_xlim = [];
    else
        opt_xlim = opts{2, ndx};
        opts = opts(:, ~ndx);
    end
    ndx = strcmp(opts(1, :), 'survival');
    if ~any(ndx)
        surv = false;
    else
        surv = opts{2, ndx};
        opts = opts(:, ~ndx);
    end
    figure
    data = this.random(3000, 1);
    fprintf('%s\n', this.mixedname(this.Distributions, this.ComponentProportion))
    fprintf('Mean %g    in drawn data:%g\n', this.mean, mean(data));
    fprintf('std %g     in drawn data:%g\n', this.std, std(data));
 
    ndx = strcmp(opts(1, :), 'maxfreq');
    if any(ndx) && ~isempty(opts{2, ndx})
        freq = randi(opts{2, ndx} +1, size(data)) - 1;
        opts = opts(:, ~ndx);
    else
        freq = [];
    end
 
    ndx = strcmp(opts(1, :), 'discretize');
    if any(ndx)
        ndx1 = strcmp(opts(1, :), 'fract_cens');
        if any(ndx1)
            fractcens = opts{2, ndx1};
            opts = opts(:, ~ndx1);
            ndx = strcmp(opts(1, :), 'discretize');
        else
            fractcens = 1;
        end
        [indices, bins] = discretize(data, opts{2, ndx});
        data1 = bins(indices);
        cens1 = bins(indices + 1);
        censndx = rand(size(data)) < fractcens;
        cens = data;
        data(censndx) = data1(censndx);
        cens(censndx) = cens1(censndx);
        opts = opts(:, ~ndx);
    else
        ndx = strcmp(opts(1, :), 'fract_cens');
        if any(ndx) && ~isempty(opts{2, ndx})
            cens = rand(size(data)) < opts{2, ndx};
            data(cens) = data(cens) .* rand(size(data(cens)));
            opts = opts(:, ~ndx);
        else
            cens = [];
        end
    end
 
    m = MixedDistribution(this.Distributions);
   
    iscontin=shared.basic_info(this.Components{1}.d.DistributionName).iscontinuous;
    %we cannot mix continuous and discontinous, so if the first oneis
    %continuous the rest should also be
    if ~iscontin
        data=round(data);
        if ~islogical(cens)
            cens=round(cens);
        end
    end

    if this.IsTruncated
        m.Truncation = this.Truncation;
    end
    if nargout > 1 || showmovie
        [m, details] = m.fit(data, 'censoring', cens, 'frequency', freq, opts{:});
    else
        m = m.fit(data, 'censoring', cens, 'frequency', freq, opts{:});
    end
    if surv
        figure
        subplot(3, 1, 1)
        plotdist(this, 'data', data, 'censoring', cens, 'frequency', freq, 'xlim', opt_xlim, 'title', sprintf('model: %s', this.mixedname(this.Distributions)));
        subplot(3, 1, 2)
        plotdist(this, 'xlim', opt_xlim, 'type', 'survivor', 'title', '');
        subplot(3, 1, 3)
        plotdist(this, 'xlim', opt_xlim, 'type', 'hazard', 'title', '');
        figure
        subplot(3, 1, 1)
        plotdist(m, 'xlim', opt_xlim, 'censoring', cens, 'title', sprintf('model: %s', this.mixedname(this.Distributions)));
        subplot(3, 1, 2)
        plotdist(m, 'xlim', opt_xlim, 'type', 'survival', 'title', '');
        subplot(3, 1, 3)
        plotdist(m, 'xlim', opt_xlim, 'type', 'hazard', 'title', '');
    else
        udistr = unique(this.Distributions);
        if length(udistr) == 1 && any(strcmp(udistr, {'Lognormal', 'Normal'}))
            isgm = true;
            nfig = 3;
        else
            isgm = false;
            nfig = 2;
        end
        subplot(nfig, 1, 1)
        titl1 = sprintf('model: %s', this.mixedname(this.Distributions));
        titl1(titl1 == '_') = '-';
        plotdist(this, 'data', data, 'censoring', cens, 'frequency', freq, 'xlim', opt_xlim, 'negloglik', this.likefunc(data, cens, freq), 'title', titl1);

        %  title(titl1);
        subplot(nfig, 1, 2)
        plotdist(m, 'xlim', opt_xlim, 'negloglik', m.negloglik, 'title', 'fitted');
        %   title('fitted')
        if isgm&&(isempty(cens)||~any(cens))
            %fit/plot also gmdistribution to compare only if all distributions
            %are normal or lognormal
            gm = m.fit(data, 'fitgmdist', true);
            %gm = fitgmdist(data, this.NumComponents);
            subplot(nfig, 1, 3);
            plotdist(gm, 'data', data, 'negloglik', gm.negloglik) %NegativeLogLikelihood)
            title('fitted by gmdistribution')
        end
        if showmovie
            subplot(nfig, 1, 2)
            title2 = 'Best fitted model';
            plotdist(m, 'xlim', opt_xlim, 'movie', details, 'negloglik', true, 'title', title2);
        end

    end
end
