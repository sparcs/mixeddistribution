function this = bootstrap(this, varargin)
    %MixedDistibution.bootstrap this function implements bootstrap methods
    %
    %The fitted distribution is used to generate nboot sets of random
    %numbers with the same size as the used InputData. If there were
    %censored data, the same fraction of censored data is randomly
    %assigend. The bootstrapped data values corresponding with censored
    %data are lowered randomly using a uniform distribution. 
    %Alternatively it features also simply resampling with replacement of both 
    %the data and the censoring.
    %
    %Usage:
    %  pd = pd.bootstap('nboot',nboot) - default nboot=100, fills the BootstrapData field
    %  with fields:
    %     'BootstrapData.bootdata.seed' random seed
    %     'BootstrapData.bootdata.nboot' number of bootstrap samples
    %     'BootstrapData.bootdata.method' bootstrap method: either 'resampling' or 'parametric'
    %     'BootstrapData.bootdata.ndata' number of data points in original set
    %     'BootstrapData.bootdata.censfract' fraction censored data
    %     'BootstrapData.parvalues', fitted parameters of the bootstrap sets (use
    %          quantiles to find confidence limits)
    %
    %  pd = pd.bootstrap('option',value)
    %      options:
    %       'method' the method of bootstrapping: 'parametric' (default) or
    %          or 'resampling'
    %      'nboot'      the number of bootstrap samples (default 1000)
    %      'ndata',     the number of genereted data (default the same as the data
    %      'replicates',  by default the same settings are used as during fitting.
    %                 However these can be overwritten.
    %      'totalperiod' - the assumed total period (default 2x the maximum
    %                      age)
    %      'resample_data' - data for hybrid method, used resampling while the rest is parametric.
    %             This can be used to add data outside of the range to be drawn
    %             (e.g. <xmin in pareto distribution)
    %      'censfraction' - for testing set the fraction censored data
    %
    %  bootdata=pd.bootCL('data') - recreate the raw boostrapped data as a structure 
    %      (fields: bootdata, bootcens). res is a structure created earlier using this method.
    %  meanvalues=pd.bootCL('mean')  %see bootCL for other
    %  options
    %
    
    if nargin == 2 && isstruct(varargin{1})
        %recreate the bootstrapped data (used internally, use bootCL
        opt = varargin{1};
        if isfield(opt, 'bootdata')
            opt = opt.bootdata;
        end
        rng(opt.seed);
        [bootdata, bootcens, bootfreq, boottrunc] = this.draw_bootstrap(opt);
        %[bootdata, bootcens] = drawdata(opt);
        this = struct('bootdata', bootdata, 'bootcens', bootcens, 'bootfreq', bootfreq, 'boottrunc', {boottrunc});
        return;
    elseif nargin == 2 && isempty(varargin{1})
        error('MixedDistribution:bootstrap:nobootstrap', 'There is no bootstrap data')
    end
    disp('Running bootstrap')
    nboot = 100; %default
    opts = reshape(varargin, 2, length(varargin) / 2);
    validoptions = [{'nboot', 'ndata', 'method', 'totalperiod', 'censfraction', 'resample_data', }, this.fit('-validoptions')];
    ndx = ismember(validoptions, {'censoring', 'frequency'}); %these options cannot be used here
    validoptions = validoptions(~ndx);
    %validoptions = {'nboot', 'method', 'totalperiod', 'censfraction', 'replicates', 'initial', 'options', 'initfun', 'censmethod', 'silent'};
    if any(~ismember(opts(1, :), validoptions))
        error('MixedDistribution:bootstrap:optionsnotvalid', 'Options invalid.\nValid options are: %s\n', sprintf('''%s'' ', validoptions{:}));
    end
    ndx = strcmpi(opts(1, :), 'resample_data');
    if any(ndx)
        res.resample_data = opts{2, ndx};
        opts = opts(:, ~ndx);
    else
        res.resample_data = [];
    end
    ndx = strcmpi(opts(1, :), 'ndata');
    if any(ndx)
        ndata = opts{2, ndx};
        opts = opts(:, ~ndx);
        maxdata = max(this.random(ndata, 1));
        fitoptions = {};
        censfract_data = 0;
        res.intervalcens = false;
    else
        if isempty(this.InputData) || isempty(this.InputData.data)
            error('MixedDistribution:bootstrap:nodata', 'The distribution should first be fitted to data (use the method "fit" or "fitmixdist"');
        end
        if ~isempty(this.InputData.freq)
            ndata = sum(this.InputData.freq);
        else
            ndata = numel(this.InputData.data);
        end
        maxdata = max(this.InputData.data);
        fitoptions = this.InputData.fitoptions;
        res.intervalcens = ~isempty(this.InputData.cens) && (~islogical(this.InputData.cens) || ~all(this.InputData.cens == 0 | this.InputData.cens == 1));
        if isempty(this.InputData.cens) || ~islogical(this.InputData.cens)
            censfract_data = 0;
        else
            censfract_data = sum(this.InputData.cens) / length(this.InputData.cens);
        end
    end
    ndx = strcmpi(opts(1, :), 'nboot');
    if any(ndx)
        nboot = opts{2, ndx};
        opts = opts(:, ~ndx);
    end
    method = 'parametric';
    ndx = strcmpi(opts(1, :), 'method');
    if any(ndx)
        method = opts{2, ndx};
        methods = {'parametric', 'resampling'};
        if ~any(strcmp(method, methods))
            error('MixedDistribution:bootstrap:methodnotvalid', 'Method "%s" not valid.\nValid methods are: %s\n', method, sprintf('''%s'' ', methods{:}));
        end
        opts = opts(:, ~ndx);
    end

    ndx = strcmpi(opts(1, :), 'censfraction');
    if any(ndx)
        censfraction = opts{2, ndx};
        opts = opts(:, ~ndx);
    else
        censfraction = [];
    end
    ndx = strcmpi(opts(1, :), 'totalperiod');
    if any(ndx)
        totalperiod = opts{2, ndx};
        opts = opts(:, ~ndx);
    elseif censfraction < 0.5
        totalperiod = 2 * maxdata;
    else
        totalperiod = maxdata;
    end
    res.truncdata = [];
    fitopts = reshape(fitoptions, 2, length(fitoptions) / 2);
    if ~isempty(opts)
        %merge with options during fit of "this"
        for i = 1:size(opts, 2)
            ndx = strcmpi(fitopts(1, :), opts{1, i});
            if any(ndx)
                fitopts(:, ndx) = opts(:, i);
                fprintf('Original fit option "%s" overridden\n', fitopts{1, ndx});
            else
                fitopts = [fitopts, opts(:, i)];
            end
        end
    end
    ndx = strcmpi(fitopts(1, :), 'truncdata');
    if any(ndx)
        res.truncdata = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
    end
    fitopts=fitopts(:);
    res.seed = rng;
    res.nboot = nboot;
    res.method = lower(method);
    res.ndata = ndata;
    if strcmp(res.method, 'parametric')
        res.totalperiod = totalperiod;
        if ~isempty(censfraction)
            res.censfract = censfraction;
        else
            res.censfract = censfract_data;
        end
        this.InputData.fitoptions = fitopts;
    end

    [bootdata, bootcens, bootfreq, boottrunc] = this.draw_bootstrap(res);
    parvalues = zeros(nboot, this.NumParameters);
    han = @(data,cens,freq,trunc)this.fit(data,'censoring',cens,'frequency',freq,'truncdata',trunc,fitopts{:});

    %for debugging replace parfor with for
    parfor i = 1:nboot
        try
            pd1 = han(bootdata(:, i), bootcens(:, i), bootfreq(:, i), boottrunc{i});
            parvalues(i, :) = pd1.ParameterValues;
        catch
            parvalues(i, :) = nan;
        end
    end
    %one round of repairing failed fits
    ndx = any(isnan(parvalues), 2);
    if any(ndx)
        f = find(ndx);
        [bootdata2, bootcens2, bootfreq2, boottrunc2] = this.draw_bootstrap(res);
        for i = 1:length(f)
            try
                pd2 = han(bootdata2(:, i), bootcens2(:, i), bootfreq2(:, i), boottrunc2{i});
                parvalues(f(i), :) = pd2.ParameterValues;
            catch
                %
            end
        end
    end
    this.BootstrapData = struct('bootdata', res, 'parvalues', parvalues);
end
