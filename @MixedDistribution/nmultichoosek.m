function combs = nmultichoosek(values, k)
    %// Return number of multisubsets or actual multisubsets.
    if numel(values) == 1
        if values == 1
            combs = ones(1, k);
        else
            n = values;
            combs = nchoosek(n + k - 1, k);
        end
    else
        n = numel(values);
        combs = bsxfun(@minus, nchoosek(1:n + k - 1, k), 0:k - 1);
        combs = reshape(values(combs), [], k);
    end
end