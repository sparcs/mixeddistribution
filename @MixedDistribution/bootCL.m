function res = bootCL(this, func, varargin)
    %res=pd.bootCL('mean') get the bootstrap confidence limits for the mean
    %Valid functions
    %'mean'      - mean of the distribution
    %'median'    - median of the distribution
    %'std'       - standard deviation
    %'var'       - variance
    %'cdf'       - cumulative distribution (use option 'x' for values)
    %'ksstat'    - Kolmogorov-Smirnov statistic D
    %'icdf'      - inverse cumulative distribution (use option 'p' for pvalues)
    %'pdf'       - density of the distribution (use option 'x' for values)
    %'hazard'    - hazard function (only if support=0,inf) (use option 'x' for values)
    %'cumhazard'    - hazard function (only if support=0,inf) (use option 'x' for values)
    %'survivor'  - survivor function (1-cdf) (use option 'x' for values)
    %'bootdata'  - the raw bootstrap data
    %'parvalues' - the parameter values fitted to the bootstrap data
    %
    %res=pd.bootCL('pdf','x',xvalues,'alpha',0.05)
    % options:
    %   'x' for some functions the x values are needed
    %     (cdf,pdf,hazard,survivor)
    %   'p' probabilities for inverse cumulative distribution, needed for
    %      icdf
    %   'quantiles', directly the quantiles
    %   'alpha' two-sided confidence level: default 0.05 
    %   'tails', set to 'left' or 'right' for one sided test, and 'both'
    %       for twosided test
    %   'ignorenan', logical
    %
    %   
    %   
    if nargin < 2
        func = 'mean';
    end
    if mod(length(varargin), 2) > 0
        error('MixedDistribution:bootCL', 'Use parameter/value pairs');
    end
    opts = reshape(varargin, 2, length(varargin) / 2);
    opts(1, :) = lower(opts(1, :));
    allopts = {'alpha', 'x', 'p', 'ignorenan','quantiles', 'tails'};
    if any(~ismember(opts(1, :), allopts))
        error('MixedDistribution:bootCL', 'Option unknown, valid options are: %s', sprintf('''%s'' ', allopts{:}));
    end
    ndx = strcmp(opts(1, :), 'alpha');
    if any(ndx)
        res.alpha = opts{2, ndx};
    else
        res.alpha = 0.05;
    end
    ndx = strcmp(opts(1, :), 'tails');
    if any(ndx)
        res.tails = opts{2, ndx};
    end
    ndx = strcmp(opts(1, :), 'ignorenan');
    if any(ndx)
        ignorenan = opts{2, ndx};
    else
        ignorenan=false;
    end
    ndx = strcmp(opts(1, :), 'quantiles');
    if any(ndx)
        res = rmfield(res, 'alpha');
        res.quantiles = opts{2, ndx};
    end
    ndx = strcmp(opts(1, :), 'x');
    if any(ndx)
        x = opts{2, ndx};
    else
        x = [];
    end
    ndx = strcmp(opts(1, :), 'p');
    if any(ndx)
        p = opts{2, ndx};
    else
        p = [];
    end
    res.func = func;
    if isfield(res, 'alpha')
        if isfield(res,'tails')&&strcmpi(res.tails,'right')
            quantiles=1-res.alpha;
        elseif isfield(res,'tails')&&strcmpi(res.tails,'left')
            quantiles=res.alpha;
        else
            quantiles = [res.alpha / 2, (1 - res.alpha / 2)];        
        end
    else
        quantiles = res.quantiles;
    end
    res.quantiles=quantiles;
    parvalues = this.BootstrapData.parvalues;
    if ignorenan
        ndx=any(isnan(parvalues),2);
        if any(ndx)
            parvalues=parvalues(~ndx,:);
            fprintf('Removed %d parameter sets with NaN values\n',sum(ndx));
        end
    end
        
    %this=bootstraps.pd;
    %parvalues = bootstraps.parvalues;
    if any(strcmp(func, {'mean', 'median', 'std', 'var','ksstat'}))
        boot_res = zeros(size(this.BootstrapData.parvalues, 1), 1);
    elseif any(strcmp(func, {'cdf', 'pdf', 'hazard', 'cumhazard', 'survivor','powerlaw'}))
        if isempty(x)
            range_x = this.GetRange;
            x = linspace(range_x(1), range_x(2), 400);
        end
        res.x = x(:)';
        boot_res = zeros(size(parvalues, 1), length(x));
    elseif strcmp(func, 'icdf')
        if isempty(p)
            p = linspace(0, 1, 400);
        end
        res.p = p(:)';
        boot_res = zeros(size(parvalues, 1), length(p));
    elseif ~any(strcmp(func, {'bootdata', 'parvalues'}))
        error('MixedDistribution:bootCL', ['Kind of bootstrap result not supported:\n',  ...
            '''mean'', ''median'', ''std'', ''var'', ''cdf'', ''icdf'', ''pdf'', ''hazard'', ''cumhazard'', ''survivor'', ''ksstat'', ''bootdata'', ''parvalues'' are valid options']);
    end
    if strcmp(func, 'bootdata')
        %original boostrapdata
        res = this.bootstrap(this.BootstrapData.bootdata);
    elseif strcmp(func, 'parvalues')
        res.CL = quantile(parvalues, quantiles);
        res.allvalues = parvalues;
    elseif strcmp(func, 'ksstat')
        %fast code adapted from lillieML
        %original boostrap data
        bootdata = this.bootstrap(this.BootstrapData.bootdata);
        bootdata=bootdata.bootdata;
        pd = this;      
        n=size(bootdata,1);
        yCDF = (0:n)' / n;
        for i = 1:size(parvalues, 1)
            pd.ParameterValues = parvalues(i, :); %use the fitted distribution
            xCDF = sort(bootdata(:, i)); % unique values, no need for ECDF
            nullCDF = pd.cdf(xCDF);
            delta1 = yCDF(1:end - 1) - nullCDF;
            delta2 = yCDF(2:end) - nullCDF;
            %KS statistic = max distance between CDF's
            boot_res(i) = max(abs([delta1; delta2]));
        end
        res.CL = quantile(boot_res, quantiles);
        res.allvalues = boot_res;
    else
        pd = this;
        for i = 1:size(parvalues, 1)
            pd.ParameterValues = parvalues(i, :);
            switch func
                case 'mean'
                    boot_res(i) = pd.mean;
                case 'std'
                    boot_res(i) = pd.std;
                case 'var'
                    boot_res(i) = pd.var;
                case 'median'
                    boot_res(i) = pd.median;
                case 'icdf'
                    boot_res(i, :) = pd.icdf(p);
                case 'cdf'
                    boot_res(i, :) = pd.cdf(x);
                case 'pdf'
                    boot_res(i, :) = pd.pdf(x);
                case 'cumhazard'
                    boot_res(i, :) = pd.cumhazard(x);
                case 'hazard'
                    boot_res(i, :) = pd.hazard(x);
                case {'survivor','powerlaw'}
                    boot_res(i, :) = 1 - pd.cdf(x);
            end
        end
        res.CL = quantile(boot_res, quantiles);
        res.allvalues = boot_res;
    end
end
