classdef MixedDistribution
    %<prob.ProbabilityDistribution
    %MixedDistribution class. A generalization of the gmdistribution class
    %describing a finite mixture of distributions (up till now only univariate variable is supported).
    %Can fit the distributions using an Expectation–Maximization algorithm.
    %Any combination of MATLAB distributions (including custom made) can be
    %in principle be fitted. (except for a combination of continuous and discontinuous
    %distributions). The algorithm is not always able to distinguish
    %overlapping distibutions and will not always converge.
    %
    %Properties:
    %   AIC                 AIC after fitting
    %   BIC                 BIC after fitting
    %   ComponentProportion the proportions of the mixture components
    %   Components          the objects of the components
    %   DistributionName    Name of the distribution
    %   Distributions       List of distribution names of the components
    %   InputData           data used for fitting
    %   IsTruncated         true if the distributions are truncated
    %   negloglik           the negative log likelihood after fitting
    %   NumComponents       Number of components
    %   NumParameters       Number of parameters
    %   ParameterInit       Initialization method during fitting
    %   ParameterNames      Names of all parameters in the vector
    %   ParameterValues     All parameters as vector
    %   Posteriors          Posteriors to define latent classes
    %   Truncation          range for truncation (or empty)
    %
    %
    %Methods
    %   cdf                Compound cumulative density function
    %   disp               Display the components
    %   fit                Fit the distribution to data using the EM method
    %   fit_steps          Fit all combinations of components
    %   icdf               Compound inverse cumulative density
    %   likefunc           Likelihood function
    %   mean               Mean of the compound distribution
    %   median             Median of the compound distribution
    %   MixedDistribution  The constructor of the class
    %   NDifferentComp     How many components are really different?
    %   initfun            Define how to intialize parameters for fitting 
    %   pdf                The compound probability density function
    %   random             Draw random numbers from the mixture
    %   randompars         Get random parameters
    %   std                Standard deviation of the distribution
    %   test_fit           Test fitting by generating data and fitting back
    %   truncate           Truncate the compound distribution
    %   var                Variance of the compound distribution
    %
    % 

 
    properties(Constant)
        %DistributionName Name of distribution
        %To be consistent with other distributions.
        DistributionName = 'Mixed';
        %   DistName = 'mixed';
    end
 
    %dynamic properties:
    properties
        %objects for each of the components of the mixture
        Components = {};
  
        %The component proportions
        ComponentProportion = [];
 
        %optional truncation
 
        %only filled after fitting:
        %negative log likelihood
        negloglik = [];
 
        %input data (structure with 'data', 'frequency' and 'cens' fields)
        %the InputData of the components are deleted to minimize space
        InputData = [];
        %only filled after bootstrap
        BootstrapData = [];
    end

    %we have a lot of dependend properties. They can only be read and are
    %calculated in the get function
    properties (Dependent = true)
        NumParameters
        Truncation

        %ParameterName Name of parameter
        %    ParameterName is a two-element cell array containing names
        %    of the parameters of this distribution.
        ParameterNames
 
        %ParameterDescription Description of parameter
        %    ParameterDescription is a two-element cell array containing
        %    descriptions of the parameters of this distribution.
        Posteriors
        ParameterValues
        AllParameters
        IsTruncated
        Distributions
        NumComponents
        BIC
        AIC
        support
    end

    methods
        [this, results1] = fit(this, data, varargin)
        this = bootstrap(this, varargin)
        res = bootCL(this, varargin)
        [m, details] = test_fit(this, varargin)
        res = fit_steps(this, data, varargin)
        res = lilliefors(this, varargin)
    end
 
    
    methods (Static, Access = protected)
        combs = nmultichoosek(values, k)
        

        function dist = name2code(distr)
            info = shared.basic_info(distr);
            dist = info.code;
        end
        
    end
    methods (Static)
        function s = mixedname(distrs, proport, short)
            if nargin == 1
                proport = [];
            end
            if nargin < 3
                short = false;
            end
            if isa(distrs, 'MixedDistribution')
                this = distrs;
                distrs = this.Distributions;
                if nargin > 1
                    short = proport;
                end
                proport = this.ComponentProportion;
            end
            s = '';
            if length(proport) <= 1 || short
                udistr = unique(distrs);
                for i = 1:length(udistr)
                    k_i = sum(strcmp(distrs, udistr{i}));
                    if k_i == 1
                        s = sprintf('%s%s ', s, udistr{i});
                    else
                        s = sprintf('%s%dx%s ', s, k_i, udistr{i});
                    end
                end
            else
                for i = 1:length(distrs)
                    s = sprintf('%s%d%% %s ', s, round(proport(i) * 100), distrs{i});
                end
            end
            s = s(1:end - 1);
        end

        %         function info1 = myinfo(distrcode)
        %             %the distrcode can either be the code for histdist or the distributionName
        %             %the information by MATLAB is not accurate: here are corrected
        %             %data, especially for the plim and support
        %  
        %             %use shared.basic_info for fields code, support, plim and pinteger (MATLAB getInfo is rather slow)
        %             
        %             %copied from histdist
        %           
        %             if nargin == 0
        %                 info1 = shared.basic_info;
        %                 return;
        %             end
        %             if ~ischar(distrcode)
        %                 info1 = distrcode.getInfo;
        %                 if isa(distrcode, 'MixedDistribution')
        %                     return;
        %                 end
        %                 distrcode = distrcode.DistributionName;
        %             else
        %                 info1 = [];
        %             end
        % 
        %             if strcmp(distrcode, '-reset')
        %                 shared.basic_info('-reset');
        %                 return;
        %             end
        % 
        %             if nargin == 1
        %                 info2 = shared.basic_info(distrcode);
        %                 if isempty(info2)
        %                     error('mixeddistribution:unknowndistr', '"%s" is not a valid distribution name.', distrcode)
        %                 end
        %             end
        %             if isempty(info1)
        %                 try
        %                     d1 = makedist(distrcode);
        %                     info1 = d1.getInfo;
        %                 catch
        %                     info1 = info2;
        %                 end
        %             end
        %             info1.support = info2.support;
        %             info1.plim = info2.plim;
        %             info1.pinteger = info2.pinteger;
        %         
        %         end

    end



    methods

        function [this, KS] = fitfixed(this, data, varargin)
            if this.NumComponents == 1
                [s, KS] = this.Components{1}.fitfixed(data, varargin{:});
                this.Components{1} = s;
                this.InputData = this.Components{1}.InputData;
            else
                error('fitfixed not yet implemented for mixtures');
            end
        end


        function res = get.Truncation(this)
            res = zeros(this.NumComponents, 2);
            for i = 1:this.NumComponents
                trunc = this.Components{i}.Truncation;
                if isempty(trunc)
                    res(i, :) = [-Inf; Inf];
                else
                    res(i, :) = trunc;
                end
            end
            res1 = unique(res, 'rows');
            if size(res1, 1) == 1
                res = res1;
            end
        end

        function this = set.Truncation(this, val)
            if numel(val) == 2
                val = val(:).';
                val = repmat(val, this.NumComponents, 1);
            end
            for i = 1:this.NumComponents
                if isempty(val)
                    this.Components{i}.Truncation = [-Inf, Inf];
                else
                    this.Components{i}.Truncation = val(i, :);
                end
            end
        end


        function [dens, xi, ksinfo] = kernel(this, varargin)
            %wrapper around ksdensity
            if isempty(this.InputData)
                error('MixedDistribution:kernel:nodata', 'This function can only be applied after fitting distributions to data.')
            end
            if ~any(cellfun(@(x)ischar(x)&&strcmpi(x,'censoring'), varargin))
                if ~isempty(this.InputData.cens) && any(this.InputData.cens)
                    varargin = [varargin {'censoring', this.InputData.cens}];
                end
            end
            if ~any(cellfun(@(x)ischar(x)&&strcmpi(x,'support'), varargin))
                % info = this.getInfo;
                varargin = [varargin {'support', this.support}];
            end
            if ~any(cellfun(@(x)ischar(x)&&strcmpi(x,'function'), varargin)) && nargout > 2
                %calculate negloglik
                if ~isempty(varargin) && ~ischar(varargin{1})
                    args = varargin(2:end);
                else
                    args = varargin;
                end
                cdffun = @(x)ksdensity(this.InputData.data, x,args{:},'function','cdf');
                pdffun = @(x)ksdensity(this.InputData.data, x,args{:},'function','pdf');
                loglik = shared.likefunc_mydistrib(pdffun, cdffun, [], this.InputData.data, this.InputData.cens, this.InputData.freq); %
            else
                loglik = [];
            end
            [dens, xi, bandwidth, ksinfo] = ksdensity(this.InputData.data, varargin{:});
            ksinfo.bandwidth = bandwidth;
            ksinfo.negloglik = loglik;
        end
        
        function supp = get.support(this)
            supp = zeros(this.NumComponents, 2);
            for i = 1:this.NumComponents
                supp(i, :) = this.Components{i}.support;
            end
            supp = [max(supp(:, 1)), min(supp(:, 2))];
        end

        function res = get.IsTruncated(this)
            res = false;
            for i = 1:length(this.Components)
                if this.Components{i}.IsTruncated
                    res = true;
                end
            end
        end

        function post_clus = get.Posteriors(this)
            if isempty(this.InputData)
                post_clus = [];
                return;
            end
            [total_pdf, post_clus] = this.pdf(this.InputData.data);
            for i = 1:this.NumComponents
                post_clus(:, i) = post_clus(:, i) ./ total_pdf;
            end
        end

        function distr = get.Distributions(this)
            distr = cell(size(this.NumComponents));
            for i = 1:this.NumComponents
                distr{i} = this.Components{i}.DistributionName;
            end
        end

        function n = get.NumComponents(this)
            n = numel(this.Components);
        end

        function n = get.NumParameters(this)
            %univarate assumed!
            n = 0;
            for i = 1:numel(this.Components)
                %add the number of parmeters for each component
                n = n + this.Components{i}.NumParameters;
            end
            n = n + this.NumComponents - 1; %add number of ComponentProportions - 1
            %as the last one is always 1
        end

 
        function this = set.AllParameters(this, allpars)
            ndx = strcmp({allpars.realname}, 'Pi');
            vals = [allpars.value];
            props = vals(ndx);
            props(end + 1) = 0;
            sumprop = 0;
            for i = 1:this.NumComponents
                if sumprop + props(i) > 1
                    props(i) = 1 - sumprop;
                    if props(i) < 0
                        props(i) = 0;
                    end
                end
                sumprop = sumprop + props(i);
            end
            this.ComponentProportion = props;
            allpars = allpars(~ndx);
            for i = 1:this.NumComponents
                res = allpars([allpars.component] == i);
                this.Components{i}.AllParameters = res;
            end
        end
 
        function res = get.AllParameters(this)
            res = [];
            for i = 1:this.NumComponents
                res1 = this.Components{i}.AllParameters;
                comp = num2cell(zeros(size(res1)) + i);
                [res1.component] = comp{:};
                if this.NumComponents > 1
                    ext = sprintf('%%s_%d,', i);
                    c = {res1.name};
                    parnames = regexp(sprintf(ext, c{1:end}), ',', 'split');
                    [res1.name] = parnames{1:end - 1};
                    if i ~= this.NumComponents
                        res = [res struct('name', sprintf('Pi_%d', i), 'realname', 'Pi', 'isfixed', false, 'component', i, 'value', this.ComponentProportion(i)) res1]; %#ok<AGROW>
                    else
                        res = [res res1]; %#ok<AGROW>
                    end
                else
                    res = res1;
                end
            end
        end
 
        function res = get.ParameterNames(this)
            res1 = this.AllParameters;
            res = {res1(~[res1.isfixed]).name};
        end

        function res = get.BIC(this)
            res = 2 * this.negloglik + this.NumParameters * log(numel(this.InputData));
        end

        function res = get.AIC(this)
            res = 2 * this.negloglik + 2 * this.NumParameters;
        end

        function this = randompars(this, scale)
            %create a distribution with random parameters
            %
            %pd=MixedDistribution('Normal')
            %pd=pd.randompars(scale) - creates a  distribution with a
            %certain scale factor to be used for inf parameter limits (default = 10)
            %pd=makedist_rnd(pd) - copy the distribution pd with random parameters.
            %   (pd should be an distribution object) (scale factor = 10)
            %

            if nargin < 2
                scale = 10;
            end
            pp = rand(1, this.NumComponents);
            this.ComponentProportion = pp / sum(pp);
            for i = 1:this.NumComponents
                this.Components{i} = this.Components{i}.randompars(scale);
            end
        end

        function this = set.ParameterValues(this, values)
            res1 = this.AllParameters;
            values1 = [res1.value];
            if numel(values1) == numel(values)
                values1 = values;
            else
                values1(~[res1.isfixed]) = values;
            end
            values1 = num2cell(values1);
            [res1.value] = values1{:};
            this.AllParameters = res1;
        end

        function res = get.ParameterValues(this)
            res1 = this.AllParameters;
            res = [res1(~[res1.isfixed]).value];
        end

        
        function this1 = set(this, varargin)
            %this.set('parname',value,isfixed)
            %You can use this function to fix or unfix a parameter.
            %Fixed parameters remain unchanged during fitting
            %value=[] keeps the value the same.
            %Add "_1" to the parameter to indicate it only applies to the
            %first component. Without this it applies to all components.
            %
            %Parameter value combinations:
            %   [parameter], value, isfixed - set the value of a specific
            %                  parameter of the distribution and optionnally 
            %                  make it fixed  or not.
            %   [parameter_1], value, isfixed - set the value only of the 
            %                  first component.
            %   'parameters' - set all parameters as a vector
            %   'parameters_1'- set the parameters only of the first component
            %   'mask'       - vector that is true for the free parameters 
            %   'truncate'   - set the truncation range
            %   'truncate_2' - set the truncation range of the second
            %                  component
            %   'name'       - set the kind of distribution. Name can be the
            %                  name/code of the distribution or a object.
            %examples:
            %pd=pd.set('mu_2',4)        - set the value of mu of component 2 to 4
            %pd=pd.set('mu',3, true)    - fix all mu values and set value to 3
            %pd=pd.set('mu_1',[], false)- unfix mu of component 1 and keep value the same
            %pd=pd.set('mu_1',4,'mu_2',2)- set the value of mu of component 1 and 2
            %pd=pd.set('mu',{4,2})      - alternatively you can use a cell
            %                             for all components, [] for skipping a component
            function this1 = SetNumComponents(this1, n)
                if this1.NumComponents > n
                    this1.Components = this1.Components(1:n);
                elseif this1.NumComponents < n
                    for i2 = this1.NumComponents + 1:n
                        this1.Components{i2} = SingleDist('normal');
                    end
                end
            end
            if nargin < 2
                res1 = this.AllParameters;
                fprintf('Usage:\n   set([parameter name], [value], [isfixed])\nExamples:\n')
                for i = 1:this.NumComponents
                    s = this.Components{i};
                    res2 = res1([res1(:).component] == i);
                    for j = 1:numel(res2)
                        fprintf('   set(''%s'', %g, %s)\n', res2(j).name, res2(j).value, mat2str(res2(j).isfixed));
                    end
                    fprintf('   set(''%s'', ''%s'')\n', sprintf('name_%d', i), s.code);
                    fprintf('   set(''%s'', %s)\n', sprintf('mask_%d', i), mat2str(s.ParameterMask));
                    fprintf('   set(''%s'', %s)\n', sprintf('truncate_%d', i), mat2str(s.Truncation));
                    fprintf('   set(''%s'', %s)\n', sprintf('parameters_%d', i), mat2str(s.ParameterValues));
                end
                return
            end
            this1 = this;
            f2 = find(strncmpi('name', varargin, 4));
            if isscalar(f2) && isa(varargin{f2 + 1}, 'gmdistribution')
                if numel(varargin) > f2 + 1 && islogical(varargin{f2 + 2})
                    logarithmic = varargin{f2 + 2};
                    f3 = f2 + 2;
                else
                    logarithmic = false;
                    f3 = f2 + 1;
                end
                if logarithmic
                    distr = 'LogNormal';
                else
                    distr = 'Normal';
                end
                gm = varargin{f2 + 1};
                this1 = SetNumComponents(this1, gm.NumComponents);
                %this1.Components = cell(gm.NumComponents, 1);
                for j = 1:gm.NumComponents
                    this1.Components{j} = this1.Components{j}.set('name', distr, 'parameters', [gm.mu(j), sqrt(gm.Sigma(j))]);
                end
                this1.ComponentProportion = gm.ComponentProportion;
                this1.negloglik = gm.NegativeLogLikelihood;
                varargin(f2:f3) = [];
            elseif isscalar(f2) && ~contains(varargin{f2}, '_')
                dist = varargin{f2(1) + 1};
                if iscell(dist)
                    if ~isvector(dist)
                        %for backward compatibility
                        %now it is not needed anymore to have different
                        %lines
                        dist = dist';
                    end
                    dist = dist(:);
                    fnum = find(cellfun(@isnumeric, dist));
                    if ~isempty(fnum) && fnum(1) == 1
                        h = 1;
                    else
                        h = -1;
                    end
                    %numerics from right to left as we can directly add
                    %them
                    for i = numel(fnum): -1:1
                        prevdist = dist{fnum(i) + h};
                        c = cell(dist{fnum(i)}, 1);
                        c(:) = {prevdist};
                        if h < 0
                            dist = [dist(1:fnum(i) - 2); c; dist(fnum(i) + 1:end)];
                        else
                            dist = [dist(1:fnum(i) - 1); c; dist(fnum(i) + 2:end)];
                        end
                    end
                    this1 = SetNumComponents(this1, numel(dist));
                    for i = 1:numel(dist)
                        this1.Components{i} = this1.Components{i}.set('name', dist{i});
                    end
                    this1.ComponentProportion = ones(1, this1.NumComponents) / this1.NumComponents;
                else
                    this1.Components = {SingleDist(dist)};
                    this1.InputData = this1.Components{1}.InputData;
                end
                varargin(f2:f2 + 1) = [];
            else
                sel = true(size(varargin));
                for i = 1:numel(f2)
                    used = false;
                    nam = varargin{f2(i)};
                    for j = 1:this1.NumComponents
                        pars1 = regexprep(nam, sprintf('_%d', j), '');
                        dist = varargin{f2(i) + 1};
                        if iscell(dist) && numel(dist) == this1.NumComponents
                            dist = dist{j};
                        end
                        if strcmpi(pars1, 'name')
                            used = true;
                            this1.Components{j} = SingleDist(dist);
                        end
                    end
                    if ~used
                        error('MixedDistribution:set:unknown', 'unknown parameter "%s"', varargin{f2(i)})
                    end
                    sel(f2(i):f2(i) + 1) = false;
                end
                varargin = varargin(sel);

            end
            f2 = find(strcmpi('proportions', varargin), 1);
            if any(f2)
                this1.ComponentProportion = varargin{f2 + 1};
                varargin(f2:f2 + 1) = [];
            end

 
            f1 = cellfun(@ischar, varargin);
            if ~isempty(f1) && ~f1(1)
                error('MixedDistribution:set:nopar', 'MixedDistribution/set uses only [''parameter'',value] pairs');
            end
            %             f1(f2+1)=false;
            pars = varargin(f1);
            usedpars = false(size(pars));
            varargin(f1) = regexprep(varargin(f1), '_[0-9]*', '');
            f2 = [find(f1) numel(varargin) + 1];
            parvals = cell(size(pars));
            for i = 1:numel(parvals)
                parvals{i} = varargin(f2(i):f2(i + 1) - 1);
            end
            props = this1.ComponentProportion;
            for i = 1:this1.NumComponents
                allpar = this1.Components{i}.AllParameters;
                validoptions = [{allpar.name}, {'parameters', 'mask', 'truncate'}];
                pars1 = regexprep(pars, sprintf('_%d', i), '');
                ndx = ismember(pars1, validoptions);
                if any(ndx)
                    usedpars = usedpars | ndx;
                    v = [parvals{ndx}];
                    c1 = cellfun(@iscell, v);
                    sizecell = cellfun(@(x)numel(x), v(c1));
                    if any(sizecell ~= this1.NumComponents)
                        p = pars1(ndx);
                        f = find(sizecell ~= this1.NumComponents, 1);
                        error('MixedDistribution:set:sizecell', 'The size of the cell (after "%s") should match the number of components.', p{f})
                    end
                    v(c1) = cellfun(@(x)x(i), v(c1));
                    this1.Components{i} = this1.Components{i}.set(v{:});
                end
                ndx = strcmp(pars1, 'Pi');
                if any(ndx)
                    usedpars = usedpars | ndx;
                    props(i) = parvals{ndx}{2};
                end
            end
            this1.ComponentProportion = props;
            if any(~usedpars)
                res1 = this1.AllParameters;
                if this1.NumComponents == 1
                    validoptions = [{res1(:).name} {'parameters', 'mask', 'truncate', 'name'}];
                else
                    validoptions = [{res1(:).name} {'parameters', 'mask', 'truncate', 'name', 'proportions'}];
                end
                s = sprintf('''%s'',', validoptions{:});
                error('MixedDistribution:set:unpknonwpar', 'Unknown parameter/option, valid parameters are:\n  %s\n', s(1:end - 1));
            end
        end

        function v = get(this, parname)
            all = this.AllParameters;
            ndx = strcmp({all.name}, parname);
            if any(ndx)
                v = [all.value];
                v = v(ndx);
            else
                ndx = strcmp({all.realname}, parname);
                if any(ndx)
                    v = [all.value];
                    v = v(ndx);
                else
                    pars = unique([{all.name}, {all.realname}]);
                    s = sprintf('%s, ', pars{:});
                    error('parameter not found, valid names are: %s', s);
                end
            end
        end
        
        function n = NDifferentComp(this)
            %are there components with <0.005 probability?
            ndx = this.ComponentProportion > 0.005;
            %are there overlapping distributions?
            try
                ran = this.GetRange;
            catch
                ran = [-10000 10000];
            end
            x = linspace(ran(1), ran(2), 1000);
            pdfs = zeros(this.NumComponents, numel(x));
            for i = 1:this.NumComponents
                pdfs(i, :) = this.Components{i}.pdf(x);
            end
            for i = 1:size(pdfs, 1) - 1
                for j = i + 1:size(pdfs, 1)
                    reldiff = sum(abs(pdfs(i, :) - pdfs(j, :)) ./ pdfs(i, :), 2);
                    if reldiff < 0.01
                        ndx(i) = false;
                    end
                end
            end
            n = sum(ndx);
        end
        %
        %'initfun',{'Normal','quantiles'.0.1,'Lognormal','range',[0 0;100 100]}}
        %'initfun','quantiles'
        function ParameterInit = initfun(this, varargin)
            %This function creates function handles that are used for
            %selecting initial conditions for parameters before fitting
            %them to data. 
            %See the option 'initfun' in fit and fit_distr
            if isempty(varargin) || isempty(varargin{1})
                varargin = {'all', 'quantiles'}; %default
            end
            if nargin==2&&iscell(varargin{1})
                varargin=varargin{1};
            end
            if strcmp(varargin{1}, '-init')
                ParInit = varargin{2};
                comps = this.Components;
                truncs = this.Truncation;
                for i = 1:this.NumComponents
                    ndx = strcmp({ParInit(:).distribution}, this.Components{i}.DistributionName);
                    if any(ndx)
                        comps{i}.ParameterValues = ParInit(ndx).function();
                        % cpar = num2cell(ParInit(ndx).function());
                        % comps{i} = makedist(this.name2code(this.Components{i}.DistributionName), cpar{:});
                    end
                end
                this.Components = comps;
                this.ComponentProportion = ones(this.NumComponents, 1) / this.NumComponents;
                if this.IsTruncated
                    this.Truncation = truncs;
                    %this.truncate(this.Truncation(1), this.Truncation(2));
                end
                ParameterInit = this;
                return;
            elseif strcmp(varargin{1}, '-start')
                ParameterInit = varargin{2};
                data = varargin{3};
                args = varargin(4:end);
                for i = 1:length(ParameterInit)
                    if ~isempty(ParameterInit(i).onstart)
                        ParameterInit(i).function = ParameterInit(i).onstart(data, args{:});
                    end
                end
                return;
            end
            charndx = cellfun(@ischar, varargin);
            distndx = false(size(varargin));
            for i = 1:length(charndx)
                if charndx(i)
                    if strcmp(varargin{i}, 'all')
                        distndx(i) = true;
                    else
                        try
                            code = this.name2code(varargin{i});
                            distndx(i) = true;
                            varargin{i} = code;
                        catch
                        end
                    end
                end
            end
            dndx = find(distndx);
            if ~any(dndx)
                varargin = [{'all'}, varargin];
                dndx = 1;
            end
            dndx(end + 1) = numel(varargin) + 1;
            ParameterInit = struct('distribution', {}, 'function', [], 'onstart', []);
            for i = 1:length(dndx) - 1
                parstruc = this.makeinitstruc(varargin{dndx(i):dndx(i + 1) - 1});
                if numel(parstruc) > 1
                    ParameterInit = parstruc;
                else
                    ndx = strcmp({ParameterInit(:).distribution}, varargin{dndx(i)});
                    if ~any(ndx)
                        ParameterInit(end + 1) = parstruc;
                    else
                        ParameterInit(ndx) = parstruc;
                    end
                end
            end
        end

        function this = set.ComponentProportion(this, vals)
            %gets automatically normalized to sum
            this.ComponentProportion = vals;
            if numel(vals) > 1
                this.ComponentProportion(end) = max(0, 1 - sum(vals(1:end - 1)));
            else
                this.ComponentProportion = 1;
            end
            if any(this.ComponentProportion < 0)
                error('MixedDistribution:negproportion', 'Component proportion cannot be negative');
            end
            if any(this.ComponentProportion > 1)
                error('MixedDistribution:negproportion', 'Component proportion cannot be larger than one');
            end
        end

        function pd = MixedDistribution(varargin) %names, proportions, parameters
            % MixedDistribution constructor. You have to give the names of the 
            % distribution (names, proportions, parameters)
            %    m = MixedDistribution(names, proportions, parameters)
            %      names shoud be a cell with distribution names, or names
            %      with number of distributions, or ParametricDistributions
            %      proportions is a vector of proportions ([] is equal
            %      proportions)
            %      parameters is a cell with the parameters per
            %      distribution.
            %  Examples:
            %      m = MixedDistribution({'Normal', 'Normal', 'LogNormal'}, [0.2 0.3 0.5])
            %         creates a mixture of 2 normal and one logNormal (with default
            %         parameters) in proportions 0.2, 0.3 and 0.5
            %      m = MixedDistribution({'Normal', 2; 'LogNormal', 1}, [0.2 0.3 0.5]);
            %         same as above
            %      m = MixedDistribution({'Normal', 2; 'LogNormal', 1}, [0.2 0.3 0.5], {[0 3], [3, 3], [2, 3]})
            %         same as above with parameters specified
            %      m = MixedDistribution({'Normal', 2; 'LogNormal', 1}, {[0 3], [3, 3], [2, 3]})
            %         same as above with parameters specified, now with
            %         equal proportions (left out, can also be [])
            %      m = MixedDistribution({makedist('Normal', 3, 2), makedist('Exponential', 3)}, [0.2 0.8]);
            %         creates a Normal distribution and an exponential
            %      gm = fitgmdist(data,3)
            %      m = MixedDistribution(gm) - you can also convert a
            %         gmdistribution to a MixedDistribution (you can also
            %         used the 'fitgmdist' option in the fit method).
            %
            if verLessThan('matlab', '8.1')
                error('MixedDistribution:version', 'This MATLAB version is not supported (at least R2013a, tested on R2020)');
            end
            makedist('-reset');
            if nargin == 0
                varargin = {'normal'};
            end
            %handle the first 3 default arguments  (name,proportions,parametervalues}
            if ~any(strcmp('name', varargin))
                c = [{'name'} varargin(1)];
                k = 1;
                if nargin > 1 && ~isa(varargin{1}, 'gmdistribution')
                    if iscell(varargin{2}) || ~iscell(varargin{1}) && ~ischar(varargin{2})
                        c = [c {'parameters'}, varargin(2)];
                        k = k + 1;
                    elseif ~ischar(varargin{2}) && iscell(varargin{1})
                        c = [c {'proportions'}, varargin(2)];
                        k = k + 1;
                    end
                    if nargin > 2 && ~ischar(varargin{2})
                        if ~ischar(varargin{3})
                            c = [c {'parameters'}, varargin(3)];
                            k = k + 1;
                        end
                    end
                end
                
                varargin = [c varargin(k + 1:end)];
            end
            %all options of set are available
            pd = pd.set(varargin{:});
        end

        % Implement methods to compute the mean, variance, and standard
        % deviation.
        function m = mean(this)
            %average mean
            if isempty(this.Components)
                m = NaN;
                return;
            end
            m = 0;
            for i = 1:this.NumComponents
                m = m + this.ComponentProportion(i) * this.Components{i}.mean;
            end
        end

        function m = median(this)
            m = this.icdf(0.5);
        end

        function s = std(this)
            s = sqrt(this.var);
        end

        function v = var(this)
            %average var
            if isempty(this.Components)
                v = NaN;
                return;
            end
            v = 0;
            for i = 1:this.NumComponents
                v = v + this.ComponentProportion(i) * this.Components{i}.var;
            end
        end


    end

    methods


        function [res, param1, param2] = sensitivity(this, parnos, param1, param2)
            %sensitivity analysis of the negative loglikelihood with
            %respect to one or two parameters
            %[res,param1,param2] = pd.sensitivity(parnos,param1,param2)
            % - parno's are the indices of the parameters (in
            %   ParameterValues)
            % = param1 are the values of parameter 1
            % = param2 are the values of parameter 2 (max)
            %
            paramvalues = this.ParameterValues;
            if nargin < 4
                param2 = [];
            end
            res = zeros(size(param1));
            pd = this;
            pd.InputData = [];
            for i = 1:numel(param1)
                paramvalues(parnos(1)) = param1(i);
                if ~isempty(param2)
                    paramvalues(parnos(2)) = param2(i);
                end
                pd.ParameterValues = paramvalues;
                res(i) = pd.likefunc(this.InputData.data, this.InputData.cens, this.InputData.freq);
            end
        end

        function [nll, acov] = likefunc(this, x, cens, freq) % likelihood function
            %support freq and cens
            % n = length(x);
            if nargin < 3
                cens = [];
            end
            if nargin < 4
                freq = [];
            end
            if isempty(freq)
                freq = ones(size(x));
            end
            if isempty(cens)
                cens = false(size(x));
            elseif ~islogical(cens) && ~all(cens == 1 | cens == 0)
                %interval censoring
                maxx = cens;
                minx = x;
                cens = maxx > minx;
            else
                maxx = [];
                cens = logical(cens);
            end
 
            if size(x, 2) == 2
                maxx = x(:, 2);
                minx = x(:, 1);
                cens = maxx > minx;
            end

            nll = 0;
            if any(cens) % compute log survivor function for censoring points
                if ~isempty(maxx)
                    %interval censoring
                    if any(cens) % compute log survivor function for censoring points
                        cdfdiff = this.cdf(maxx(cens)) - this.cdf(minx(cens));
                        cdfdiff(cdfdiff < 1E-250) = 1E-250; % log(0)=Inf otherwise
                        nll = nll - sum(freq(cens) .* log(cdfdiff));
                    end
                    %                     if any(~cens) % compute log pdf for observed data
                    %                         nll = nll - sum(freq(~cens) .* log(this.pdf(x(~cens)) + 1E-300));
                    %                     end
               

                    % nll = nll - sum(freq(cens) .* log(this.cdf(maxx(cens)) - this.cdf(x(cens))));
                else
                    nll = 0;
                    % compute log survivor function for censoring points
                    cdf1 = this.cdf(x(cens));
                    cdf1(cdf1 == 1) = 1 - eps;
                    nll = nll - sum(freq(cens) .* log(1 - cdf1));
                end
            end
            if any(~cens) % compute log pdf for observed data
                nll = nll - sum(freq(~cens) .* log(this.pdf(x(~cens)) + 1E-300));
            end

            %                     nll = nll - sum(freq(cens) .* log(1 - this.cdf(x(cens))));
            %                 end
            %             end
            %             if any(~cens) % compute log pdf for observed data
            %                 nll = nll - sum(freq(~cens) .* log(this.pdf(x(~cens))));
            %             end
            acov = nan * eye(numel(this.NumParameters));
        end

        %         function [nll, acov] = likefunc(this, x) % likelihood function
        %             % n = length(x);
        %             nll = -sum(log(this.pdf(x)));
        %             acov = nan * eye(2);
        %         end

        function [y, ys] = hazard(this, x) % (only valid if support=[0 inf] 
            %hazard = pdf/S, and this is also true for the components
            [S, Ss] = this.survival(x);
            [f, fs] = this.pdf(x);
            y = f ./ S; %compound hazard
            ys = fs ./ Ss;
        end

        function [y, ys] = cumhazard(this, x) % (only valid if support=[0 inf] 
            %tricky I am not sure (if needed) how to take proportions for
            %components, now I take just the cumhazards of the components
            if size(x, 1) == 1 && size(x, 2) > 1
                x = x.';
                transp = true;
            else
                transp = false;
            end
            ys = zeros(size(x, 1), this.NumComponents);
            for i = 1:this.NumComponents
                ys(:, i) = this.Components{i}.cumhazard(x);
                %  or   ys(:, i) =  this.ComponentProportion(i).*this.Components{i}.cumhazard(x);
            end
            if this.NumComponents > 1
                %need only to bother with this for more than one components
                y = shared.integrate_ode45(@(x)this.hazard(x), x, []);
            else
                y = ys;
            end
            if transp
                y = y.';
                ys = ys.';
            end
        end

        function [y, ys] = survival(this, x)
            %complementary to cdf, bit tricky with the components, it is
            %p*S, so p*(1-cdf)) (p= proportion)
            if size(x, 1) == 1 && size(x, 2) > 1
                x = x.';
                transp = true;
            else
                transp = false;
            end
            [y, ys] = this.cdf(x);
            y = 1 - y;
            ys = repmat(reshape(this.ComponentProportion, 1, this.NumComponents), numel(x), 1) - ys;
            if transp
                y = y.';
                ys = ys.';
            end
        end


        function [y, ys] = cdf(this, x) % probability density function 
            if size(x, 1) == 1 && size(x, 2) > 1
                x = x.';
                transp = true;
            else
                transp = false;
            end

            ys = zeros(size(x, 1), this.NumComponents);
            for i = 1:this.NumComponents
                ys(:, i) = this.ComponentProportion(i) .* this.Components{i}.cdf(x);
            end
            %compound distribution is just the sum
            y = sum(ys, 2);
            if ~all(y <= 1)
                % y(~(y <= 1));
                error('cdf is larger than one');
            end
            if transp
                y = y.';
                ys = ys.';
            end
        end

        function [y, ys] = pdf(this, x) % probability density function 
            if size(x, 1) == 1 && size(x, 2) > 1
                x = x.';
                transp = true;
            else
                transp = false;
            end

            ys = zeros(size(x, 1), this.NumComponents);
            for i = 1:this.NumComponents
                ys(:, i) = this.ComponentProportion(i) .* this.Components{i}.pdf(x);
            end
            y = sum(ys, 2);
            if transp
                y = y.';
                ys = ys.';
            end
        end

        function y = icdf(this, p) % inverse cdf
            %Cannot construct this from the components?
            if isempty(p)
                y = [];
                return;
            end
            if numel(p) < 10
                y = zeros(size(p));
                for i = 1:numel(p)
                    if p == 1
                        y(i) = inf;
                    else
                        y(i) = fzero(@(x)this.cdf(x) - p(i), 0);
                    end
                end
                return;
            end
            p(p == 1) = 1 - 1e-5;
            %interpolate the cdf with x and y exchanged for speed
            xs = linspace(fzero(@(x)this.cdf(x) - (min(p)-1E-7), 0), fzero(@(x)this.cdf(x) - (max(p)+1E-7), 0), 2000);
            ys = this.cdf(xs);
            [~, ndx] = unique(ys, 'stable');
            y = interp1(ys(ndx), xs(ndx), p);
        end

        function y = random(this, varargin) % random number generator
            %for the components we usually apply Inverse transform sampling
            %(if there is not an easier way available)
            %First draw which component should generate a random
            %number by checking the cumulative component proportions.
            Pc = rand(varargin{:});
            y = ones(numel(Pc), 1);
            cumpcomp = cumsum(this.ComponentProportion);
            ndx = Pc(:) <= cumpcomp(1);
            %draw the sets for each of the components
            %using the random functions of each of the component
            y(ndx) = this.Components{1}.random(sum(ndx), 1);
            for i = 1:this.NumComponents - 1
                ndx = Pc(:) > cumpcomp(i);
                y(ndx) = this.Components{i + 1}.random(sum(ndx), 1);
            end
            y = reshape(y, size(Pc));
        end

        function info = getInfo(this)
            if isempty(this.Components)
                info = NaN;
                return;
            end
            % First get default info from parent class
            %info = this.Components{1}.getInfo;
            %we have to correct the info given by Matlab
            info = shared.basic_info(this.Components{1}.DistributionName);
            %info.support = info2.support;
            %info.pinteger = info2.pinteger;


            % Then override fields as necessary
            if this.NumComponents > 1
                info.name = 'Mixed';
                info.code = 'mixed';
       
                for i = 2:this.NumComponents
                    %info1 = this.Components{i}.getInfo;
                    info1 = shared.basic_info(this.Components{i}.DistributionName);
                    %info.support = info2.support;
                    %info.pinteger = info2.pinteger;
                    if ~(info.iscontinuous == info1.iscontinuous)
                        error('mixed:model', 'Mixed model cannot contain both continuous and discontinous distributions');
                    end
                    if info1.support(1) > info.support(1)
                        info.support(1) = info1.support(1);
                    end
                    if info1.support(2) < info.support(2)
                        info.support(2) = info1.support(2);
                    end
                end
            end
        end

        function res = kstest(this, data1, varargin)
            %Kolmogorov-Smirnov test, by default with the InputData
            if nargin == 1
                [data1, cens1] = shared.unfoldfreq(this.InputData);
                if ~(isempty(cens1) || all(cens1 == 0))
                    if islogical(cens1)
                        %right censored written in generic way
                        cens = data1;
                        cens(cens1) = inf;
                        cens1 = cens;
                    end
                    data1 = [data1, cens1];
                end
            end
            [res.H, res.p, res.KSstat, res.critval] = shared.my_kstest(data1, 'cdf', [data1, this.cdf(data1)]);
            if nargout == 0
                disp(res);
                [sampleCDF, x] = shared.my_ecdf(data1);
                if size(sampeCDF, 2) == 2
                    sampleCDF = sampleCDF(:, 1);
                end
                stairs(x, sampleCDF)
                hold on;
                x = x(2:end);
                nullCDF = this.cdf(x);
                plot(x, nullCDF, 'b-');
                delta1 = sampleCDF(1:end - 1) - nullCDF; % Vertical difference at jumps approaching from the LEFT.
                delta2 = sampleCDF(2:end) - nullCDF; % Vertical difference at jumps approaching from the RIGHT.
                deltaCDF = abs([delta1 ; delta2]);
                [~, ndx] = max(deltaCDF);
                if ~res.H
                    h = '***';
                else
                    h = '';
                end
                text(0.05, 0.95, sprintf('p= %g%s', res.p, h), 'units', 'normalized', 'FontSize', 8, 'tag', 'small-text', 'VerticalAlignment', 'top');
                if ndx > length(delta1)
                    ndx = ndx - length(delta1);
                    plot([x(ndx), x(ndx)], [sampleCDF(ndx + 1), nullCDF(ndx)], 'r*-')
                else
                    plot([x(ndx), x(ndx)], [sampleCDF(ndx), nullCDF(ndx)], 'r*-')
                end
            end
        end

        function s = disp(this)
            fprintf('Mixed distribution with %d components\n', this.NumComponents);
            s = [];
            for i = 1:this.NumComponents
                if isa(this.Components{i}, 'SingleDist')
                    fprintf('\nComponent %d: %s Distribution\nMixing proportion: %g\n', i, this.Components{i}.DisplayName, this.ComponentProportion(i));
                    s = disp(this.Components{i});
                    s = s(3:end);
                    fprintf('%s\n', s{:});
                else
                    fprintf('\nComponent %d: %s Distribution\nMixing proportion: %g\n', i, this.Components{i}.DistributionName, this.ComponentProportion(i));
                    disp(this.Components{i});

                end
          
            end

        end

    end
    methods (Access = protected)
        function [bootdata, bootcens, bootfreq, boottrunc] = draw_bootstrap(this, opt, bootdata)
            %Draw Bootstrap data
            %opt is struct with fields
            %  'method' = method of bootstrapping (parameteric or resampling)
            %  'ndata' = number of data points
            %  'nboot' = number of bootstrap sets
            %  
            %  if method=parametric (including hybrid)
            %        'pd' = mixed distribution for drawing (or 'bootdata' if
            %              only cens needs to be calculated)
            %        'resample_data' = data used for resampling in hybrid method
            %        'totalperiod' = total period for assigning start dates
            %        'censfract' = censoring fraction
            %  if method=resampling
            %        'InputData' = struct with "data", "cens" and "freq".            %   
            %
            if nargin == 1
                opt = this.BootstrapData.bootdata;
            end
            if ~isfield(opt, 'resample_data')
                opt.resample_data = [];
            end
            bootfreq = ones(opt.ndata, opt.nboot);
            boottrunc=repmat({[]},1,opt.nboot);
            if strcmp(opt.method, 'parametric')
                %parametric resampling
                %draw bootstrapped data sets
                % if isfield(opt, 'bootdata')
                %     bootdata = opt.bootdata;
                % else
                if nargin < 3
                    %parametric sampling:
                    if ~isempty(opt.truncdata)
                        if numel(opt.truncdata)==2
                            opt.truncdata=repmat(opt.truncdata(:)',opt.ndata,1);
                        end
                        boottrunc=repmat(opt.truncdata,1,1,opt.nboot);
                        bootdata = zeros(opt.ndata, opt.nboot);
                        [~,ndx1,ndx2]=unique(opt.truncdata,'rows');
                        this1=this;
                        %truncate the distribution (in all unique ways) 
                        %and draw random numbers from the truncated distribution
                        for i=1:numel(ndx1)
                            trunc=opt.truncdata(ndx1(i),:);
                            this1.Truncation=trunc;
                            indx=ndx2==ndx1(i);
                            bootdata(indx,:)=this1.random(sum(indx),opt.nboot);
                        end
                            
                        %repeat sampling till all data are within the
                        %truncation range
%                         
%                         lowertrunc = reshape(boottrunc(:,1,:),opt.ndata,opt.nboot);
%                         highertrunc = reshape(boottrunc(:,2,:),opt.ndata,opt.nboot);
%                         bootdata = this.random(opt.ndata, opt.nboot);
%                         ndx = (bootdata(:) < lowertrunc(:) | bootdata(:) > highertrunc(:));
%                         iter=1;
%                         maxiter=1000;
%                         while ~isempty(ndx)&&iter<maxiter
%                             bootdata(ndx) = this.random(sum(ndx), 1);
%                             ndx = (bootdata(:) < lowertrunc(:) | bootdata(:) > highertrunc(:));
%                             iter=iter+1;
%                         end
%                         if iter==1000
%                             error('bootstrap:truncation','Cannot find enough data in the truncation range after %d iterations,  %.3g%% done',maxiter,100-sum(ndx)/numel(bootdata)*100)
%                         end 
                    else
                        bootdata = this.random(opt.ndata, opt.nboot);
                    end
                    if ~isempty(opt.resample_data)
                        %hybrid method: part of the data resampled and part
                        %parametric
                        nhybrid = numel(opt.resample_data);
                        fract_hybrid = nhybrid / opt.ndata;
                        %draw at random which points will be replaced by
                        %resampled data
                        ndx_resample=rand(size(bootdata)) < fract_hybrid;
                        ndx = find(ndx_resample);
                        resampled=opt.resample_data(randi(nhybrid, numel(ndx), 1));
                        bootdata(ndx) = resampled;
                        if ~iscell(boottrunc)
                            %remove the data truncation of the resampled
                            %data
                            [r,c]=find(ndx_resample);
                            for i=1:numel(r)
                                boottrunc(r(i),:,c(i))=[-inf inf];
                            end
                        end
                    end
                    ndx = isinf(bootdata) | isnan(bootdata);
                    if any(ndx, 'all')
                        warning('Nan/inf values in bootdata drawn from:');
                        disp(this);
                        bootdata = this.random(opt.ndata, 3 * opt.nboot);
                        ndx = any(isinf(bootdata) | isnan(bootdata), 1);
                        bootdata = bootdata(:, ~ndx);
                        bootdata = bootdata(:, 1:opt.nboot);
                    end
                end
                %draw the data that are censored
                %draw the data of formation
                if opt.intervalcens
                    bootfreq = zeros(numel(this.InputData.data), opt.nboot);
                    for i = 1:opt.nboot
                        bootfreq(:, i) = histcounts(bootdata(:, i), [this.InputData.data; this.InputData.cens(end)]');
                    end
                    bootdata = repmat(this.InputData.data, 1, opt.nboot);
                    bootcens = repmat(this.InputData.cens, 1, opt.nboot);
                elseif opt.censfract > 0
                    date_of_formation = -rand(size(bootdata)) * opt.totalperiod;
                    date_of_collapse = date_of_formation + bootdata;
                    %sort descending on date of collapse
                    time_of_observation = quantile(date_of_collapse, 1 - opt.censfract);
                    %lower the censored data
                    age_at_collapse = (time_of_observation - date_of_formation);
                    bootcens = (date_of_collapse >= time_of_observation) & age_at_collapse > 0;
                    %never increase age, always reduce somewhat
                    age_at_collapse(age_at_collapse > bootdata * 0.9) = bootdata(age_at_collapse > bootdata * 0.9) * 0.9;
                    bootdata(bootcens) = age_at_collapse(bootcens);
                else
                    bootcens = false(size(bootdata));
                end
            elseif strcmp(opt.method, 'resampling')
                %simple resampling
                [data, cens, ~, truncdata] = shared.unfoldfreq(this.InputData);

                ndx1 = randi(opt.ndata, opt.ndata, opt.nboot);
                %resample all data
                bootdata = data(ndx1);
                if ~isempty(cens)
                    bootcens = cens(ndx1);
                else
                    bootcens = false(size(bootdata));
                end
                if ~isempty(truncdata)
                    %resample the truncation data in the same way as
                    %bootdata
                    lowertrunc=repmat(truncdata(:,1),1,opt.nboot);
                    highertrunc=repmat(truncdata(:,2),1,opt.nboot);
                    lowertrunc=lowertrunc(ndx1);
                    highertrunc=highertrunc(ndx1);
                    boottrunc=zeros(opt.ndata,opt.nboot,2);
                    boottrunc(:,:,1)=lowertrunc;
                    boottrunc(:,:,2)=highertrunc;
                    boottrunc= permute(boottrunc,[1,3,2]);
                end
                if opt.intervalcens && isempty(boottrunc)
                    bootfreq = zeros(numel(this.InputData.data), opt.nboot);
                    bootdata(isinf(bootdata)) = sign(bootdata(isinf(bootdata))) .* 10000;
                    bootcens(isinf(bootcens)) = sign(bootcens(isinf(bootcens))) .* 10000;
                    avdat = (bootdata + bootcens) / 2;
                    for i = 1:opt.nboot
                        bootfreq(:, i) = histcounts(avdat(:, i), [this.InputData.data; this.InputData.cens(end)]');
                    end
                    bootdata = repmat(this.InputData.data, 1, opt.nboot);
                    bootcens = repmat(this.InputData.cens, 1, opt.nboot);
                end
            else
                error('method not known')
            end
            if ~iscell(boottrunc)
                boottrunc1=boottrunc;
                boottrunc=cell(opt.nboot,1);
                for i=1:opt.nboot
                    boottrunc{i}=boottrunc1(:,:,i);
                end
            end
        end

        function ran = GetRange(this, minpdf, info)
            if nargin < 3
                info = this.getInfo;
            end
            if nargin < 2
                minpdf = 1E-100;
            end

            ran = info.support;
            if isinf(ran(1)) && ~isinf(ran(2))
                x = -logspace(10, -1, 100);
            elseif isinf(ran(2)) && ~isinf(ran(1))
                x1 = logspace(10, -1, 100);
            else
                x = [-logspace(10, -1, 100), 0, logspace(-1, 10, 100)];
                x1 = fliplr(x);
            end
            if isinf(ran(1))
                pdf = this.pdf(x);
                ran(1) = x(find(pdf > minpdf, 1));
            end
            if isinf(ran(2))
                pdf = this.pdf(x1);
                ran(2) = x1(find(pdf > minpdf, 1));
            end
        end

        function Parstruc = makeinitstruc(this, distrib, method, varargin)
            if strcmpi(distrib, 'all')
                distr = this.Distributions;
                udistr = unique(distr);
                for i = length(udistr): -1:1
                    Parstruc(i) = this.makeinitstruc(udistr{i}, method, varargin{:});
                end
                return;
            end
            pd = SingleDist(distrib);
            %pd = makedist(this.name2code(distrib));
            distrib_code = pd.DistributionName;
            %this.name2code(distrib);
            distrib = pd.DistributionName;
            switch method
                case 'function' %first argument function handle, second (optional) argument onstart function
                    func = varargin{1};
                    if nargin > 1
                        onstart = varargin{2};
                    else
                        onstart = [];
                    end
                case 'range'
                    minp = varargin{1};
                    maxp = varargin{2};
                    if ~all(maxp >= minp)
                        error('mixeddistribution:parranges', 'The maximum initial value should be larger or equal than the minimum');
                    end
                    func = @()unifrnd(minp,maxp);
                    onstart = [];
                case 'quantiles'
                    if ~isempty(varargin) && isscalar(varargin{1}) && isnumeric(varargin{1})
                        fracup = varargin{1};
                    else
                        fracup = 0.2;
                    end
                    func = [];
                    onstart = @(data,varargin)init_quantiles(this, distrib_code, fracup, data, varargin{:});
                case 'kmeans'
                    %better but less stable than quantiles
                    if ~isempty(varargin) && isscalar(varargin{1}) && isnumeric(varargin{1})
                        fracup = varargin{1};
                    else
                        fracup = 0.2;
                    end
                    func = [];
                    onstart = @(data,varargin)init_kmeans(this, distrib_code, fracup, data, varargin{:});
                otherwise
                    error('mixeddistribution:unknownmethod', 'Unknown method "%s" fof parinit', method);
            end
            Parstruc = struct('distribution', distrib, 'function', func, 'onstart', onstart);
        end
        % All ToolboxDistributions must implement a getInfo static method
        % so that Statistics and Machine Learning Toolbox functions can get information about
        % the distribution.
    end
end % classdef

function func = init_quantiles(this, distrib, fractplus, data, cens, freq)
    %onstart takes the same parameters as fit
    n = this.NumComponents;
    data = select_data(data, distrib);
    perc = (1:n) / n;
    qs = quantile(data, perc);
    q = ones(size(data));
    for i = 2:length(qs)
        q(data > qs(i - 1)) = i;
    end
    [minp, maxp] = getminmax(data, distrib, q, fractplus, cens, freq);
    func = @()unifrnd(minp,maxp);
end

function func = init_kmeans(this, distrib, fractplus, data, cens, freq)
    %onstart takes the same parameters as fit
    data = select_data(data, distrib);
    n = this.NumComponents;
    try
        q = kmeans(data, n);
    catch
        q = ones(size(data));
        perc = (1:n) / n;
        q1 = quantile(data, perc);
        for i = 2:length(perc)
            q(data <= q1(i)) = i;
        end
    end
    [minp, maxp] = getminmax(data, distrib, q, fractplus, cens, freq);
    func = @()unifrnd(minp,maxp);
end

function data = select_data(data, distrib)
    info = shared.basic_info(distrib);
    %     pd = makedist(info.code);
    %     info = pd.getInfo;
    %     if strcmpi(distrib, 'lognormal') %this seems a MATLAB bug
    %         info.support = [0 inf];
    %     end
    if ~isempty(data) && (min(data) <= info.support(1) || max(data) >= info.support(2))
        ndx = data > info.support(1) & data < info.support(2);
        data = data(ndx);
    end
end

function [minp, maxp] = getminmax(data, distrib, q, fractplus, cens, freq)
    parval = [];
    for i = 1:max(q)
        data1 = data(q == i);
        if ~isempty(cens)
            cens1 = cens(q == i);
        end
        if ~isempty(freq)
            freq1 = freq(q == i);
        end
        if numel(data1) > 10
            pd = SingleDist(distrib);
            pd = pd.fit(data1, 'censoring', cens1, 'frequency', freq1);
            pars = pd.ParameterValues;
            parval = [parval; pars]; %#ok<AGROW>
        end
    end
    if isempty(parval)
        pd = SingleDist(distrib);
        pd = pd.fit(data1, 'censoring', cens1, 'frequency', freq1);
        parval = pd.ParameterValues;
    end
    plim = shared.basic_info(distrib).plim;
    if ~isempty(plim)
        absmin = plim(1, :);
        absmax = plim(2, :);
    else
        absmin = -inf * zeros(size(pars));
        absmax = inf * zeros(size(pars));
    end


    minp = min(parval, [], 1);
    minp = minp .* (1 - sign(minp) .* fractplus);

    maxp = max(parval, [], 1);
    maxp = maxp .* (1 + sign(maxp) .* fractplus);
    maxp(maxp <= minp) = maxp(maxp <= minp) + 0.1;
 
    minp(absmin > minp) = absmin(absmin > minp);
    maxp(absmax < maxp) = absmax(absmax < maxp);
end
