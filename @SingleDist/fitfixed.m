function [this, KS] = fitfixed(this, data, varargin)
    %     SingleDistribution/fitfixed
    %   
    %     You can use this procedure if MLE of some parameters is not
    %     working. For instance xmin in a Pareto distribution cannot be
    %     fitted with MLE as the number of data points changes with xmin.
    %     fitfixed keeps one parameter fixed and uses MLE to fit the
    %     other parameters. The objective function for fitting the fixed
    %     parameter is the Kolmogorov-Smirnov statistic.
    %
    %     Usage:
    %     [pd1,KS]=pd.fitfixed(data,parameter,value...) pd1 is the fitted
    %     object, KS is the Kolmogorov Smirnov statistic
    %     Parameter value pairs:
    %     All parameters for fit can be used (for instance 'censoring',
    %     'frequency'...)
    %     Extra parameters:
    %     fixedpar - the name of the fixed parameter (only one allowed)
    %     parrange - initial value/range/list of the fixed parameter, if
    %                this is:
    %                > a single number --> fminsearch is used with this initial value
    %                > two values --> fmincon is used with this range
    %                > list of values --> minimum among the values is found with
    %                 binary searching.
    %     lowerpar   - true if fixedpar sets the lower boundary of the
    %                 distribution (e.g. xmin of Pareto)
    %     upperpar   - true if fixedpar sets the higher boundary of the
    %                 distribution (e.g. Upper of Uniform)   
    %     opttolfun - TolFun for the optimizing
    %     opttolx   - TolX for the optimizing

    ndx = find(cellfun(@iscell, varargin));
    for i = ndx
        varargin{i} = varargin(i);
    end
    validoptions = {'fixedpar', 'upperpar', 'lowerpar', 'lowerbound', 'upperbound',  ...
        'truncdata', 'parrange', 'opttolfun', 'opttolx', 'cens', 'freq', 'censoring',  ...
        'data', 'datarange', 'fitgmdist', 'frequency', 'replicates', 'initial',  ...
        'options', 'initfun', 'censmethod', 'silent', 'truncate', 'maxweight'};
  
    if nargin == 2 && ischar(data) && strcmp(data, '-validoptions')
        this = validoptions;
        return;
    end

    if ischar(data)
        varargin = [{data}, varargin];
        data = [];
    end
    data = data(:);
    fitopts = reshape(varargin, 2, length(varargin) / 2);
    if ~isempty(fitopts)
        fitopts(1, :) = lower(fitopts(1, :));
    end
    if any(~ismember(fitopts(1, :), validoptions))
        error('SingleDistribution:fit:optionsnotvalid', 'Options invalid.\nValid options are: %s\n', sprintf('''%s'' ', validoptions{:}));
    end
    oldParameterMask = this.ParameterMask;
    ndx = strcmp(fitopts(1, :), 'fixedpar');
    if any(ndx)
        opts.fixedpar = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
        this = this.set(opts.fixedpar, [], true);
        ndx = strcmp(fitopts(1, :), 'options');
        if any(ndx)
            opt = fitopts{2, ndx};
            if isfield(opt, 'start')
                opt.start = opt.start(this.ParameterMask);
            end
            if isfield(opts, 'lowerbound')
                opt.lowerbound = opt.lowerbound(this.ParameterMask);
            end
            if isfield(opts, 'upperbound')
                opt.upperbound = opt.upperbound(this.ParameterMask);
            end
            fitopts{2, ndx} = opt;
        end
    else
        f = find(~this.ParameterMask);
        if numel(f) ~= 1
            error('SingleDist:fixedpar:nopar', 'No fixed parameter selected, use the option ''fixedpar''.')
        else
            opts.fixedpar = this.d.ParameterNames{f};
        end
    end
    if isempty(opts.fixedpar)
        error('SingleDistribution:fitfixed:nopar', 'No fixed parameter selected');
    end

    ndx = strcmp(fitopts(1, :), 'upperpar');
    if any(ndx)
        opts.upperpar = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
    else
        %adapt for minx powerlaw distributions
        opts.upperpar = false;
    end

    ndx = strcmp(fitopts(1, :), 'lowerpar');
    if any(ndx)
        opts.lowerpar = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
    else
        %adapt for minx powerlaw distributions
        opts.lowerpar = false;
    end
 
    ndx = strcmp(fitopts(1, :), 'parrange');
    if any(ndx)
        opts.parrange = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
    else
        %adapt for minx powerlaw distributions
        opts.parrange = [];
    end
    ndx = strcmp(fitopts(1, :), 'lowerbound');
    if any(ndx)
        lb = fitopts{2, ndx};
        if isempty(opts.parrange)
            opts.parrange = [lb(~this.ParameterMask) Inf];
        elseif numel(opts.parrange) == 2
            opts.parrange(1) = lb(~this.ParameterMask);
        else
            opts.parrange = opts.parrange(opts.parrange >= lb(~this.ParameterMask));
        end
        fitopts{2, ndx} = lb(this.ParameterMask);
    end
    ndx = strcmp(fitopts(1, :), 'upperbound');
    if any(ndx)
        ub = fitopts{2, ndx};
        if isempty(opts.parrange)
            opts.parrange = [-Inf ub(~this.ParameterMask)];
        elseif numel(opts.parrange) == 2
            opts.parrange(2) = ub(~this.ParameterMask);
        else
            opts.parrange = opts.parrange(opts.parrange <= ub(~this.ParameterMask));
        end
        fitopts{2, ndx} = ub(this.ParameterMask);
    end
    ndx = strcmp(fitopts(1, :), 'opttolfun');
    if any(ndx)
        opts.opttolfun = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
    else
        opts.opttolfun = 1E-4;
    end
    ndx = strcmp(fitopts(1, :), 'opttolx');
    if any(ndx)
        opts.opttolx = fitopts{2, ndx};
        fitopts = fitopts(:, ~ndx);
    else
        opts.opttolx = 1E-4;
    end
    dataopts = fitopts(:, ismember(fitopts(1, :), {'data', 'cens', 'freq', 'censoring', 'frequency'}));
    %    [sampleCDF, samplex] = shared.my_ecdf(data, dataopts{:});
    this1 = this;
    datafun = @(x,fixedpar)true(size(x));
    if opts.lowerpar || opts.upperpar
        ndx = strncmp(fitopts(1, :), 'cens', 4);
        if any(ndx)
            cens = fitopts{2, ndx};
            cens = cens(:);
            fitopts = fitopts(:, ~ndx);
        else
            cens = [];
        end
        ndx = strncmp(fitopts(1, :), 'freq', 4);
        if any(ndx)
            freq = fitopts{2, ndx};
            freq = freq(:);
            fitopts = fitopts(:, ~ndx);
        else
            freq = [];
        end
        ndx = strcmp(fitopts(1, :), 'truncdata');
        if any(ndx)
            truncdata = fitopts{2, ndx};
            fitopts = fitopts(:, ~ndx);
        else
            truncdata = [];
        end
        if opts.lowerpar
            datafun = @(x,fixedpar)x>=fixedpar;
        else
            datafun = @(x,fixedpar)x<=fixedpar;
        end
    end
    f = @(x)tryfit(this1,opts.fixedpar,datafun,data,x,cens,freq,truncdata,fitopts{:});

    optset = optimset('TolFun', opts.opttolfun, 'TolX', opts.opttolx, 'Display', 'off');
    if isempty(opts.parrange)
        optvalue = fminsearch(f, this.d.(opts.fixedpar), optset);
    elseif numel(opts.parrange) == 2
        if ~exist('fmincon', 'file')
            optvalue = fmincontrained(f, opts.parrange, 10, 5);
        else
            optvalue = fmincon(f, this.d.(opts.fixedpar), [], [], [], [], opts.parrange(1), opts.parrange(2), [], optset);
        end
    else
        optvalue = fmindiscrete(f, sort(opts.parrange), 5);

        %         KS=zeros(size(opts.parrange));
        %         for i=1:numel(opts.parrange)
        %             KS(i) = f(opts.parrange(i));
        %         end
        %         [~,ndx]=min(KS);
        %         optvalue=opts.parrange(ndx);
    end
    % optvalue = fmincontrained(f, opts.parrange, 10, 4);
    [KS, this, L] = f(optvalue);
    KS = -KS;
    this.ParameterMask = oldParameterMask;
    %truncdata, par, nll, cov, x, cens, freq)
    this = this.makeFitted(truncdata, this.ParameterValues, L, [], data, cens, freq);
end

function [x] = fmindiscrete(f, values, n)
    nval = numel(values);
    ftrial = nan(nval, 1);
    maxiters = 100;
    a = 1;
    b = nval;
    for j = 1:maxiters
        trialndx = round(linspace(a, b, n));
        for i = 1:numel(trialndx)
            if isnan(ftrial(trialndx(i)))
                f1 = f(values(trialndx(i)));
                if ~isempty(f1)
                    ftrial(trialndx(i)) = f1;
                end
            end
        end
        [~, c] = min(ftrial(trialndx));
        if b - a == 1
            break
        end
        if c == 1
            a = trialndx(c);
        else
            a = trialndx(c - 1);
        end
        if c == numel(trialndx)
            b = trialndx(c);
        else
            b = trialndx(c + 1);
        end
        %     fprintf('%d %d\n', a, b);
    end
    x = values(trialndx(c));
end

function [x] = fmincontrained(f, range, n, iters)
    ftrial = zeros(n, 1);
    a = range(1);
    b = range(2);
    for j = 1:iters
        trial = linspace(a, b, n);
        for i = 1:numel(trial)
            ftrial(i) = f(trial(i));
        end
        [~, c] = min(ftrial);
        if c == 1
            a = trial(1);
        else
            a = trial(c - 1);
        end
        if c >= numel(trial)
            b = trial(end);
        else
            b = trial(c + 1);
        end
    end
    x = trial(c);
end

function [KS, p, L] = tryfit(this, fixedpar, datafun, data, x, cens, freq, truncdata, varargin)
    this.d.(fixedpar) = x;
    ndx = datafun(data, x);
    data = data(ndx);
    if ~isempty(cens)
        cens = cens(ndx);
    end
    if ~isempty(freq)
        freq = freq(ndx);
    end
    if ~isempty(truncdata)
        truncdata = truncdata(ndx, :);
    end
    %ndx = datafun(samplex, x);
    %ndx = samplex >= x;
    % if any(~ndx)
    % f=find(ndx,1,'first');
    % samplex1 = samplex(f-1:end);
    % sampleCDF1 = sampleCDF(f-1:end);
    % sampleCDF1 = (sampleCDF1 - sampleCDF1(1)) / (1 - sampleCDF1(1));
    % end
    if sum(ndx) > 1
        [sampleCDF, samplex] = shared.my_ecdf(data, 'censoring', cens, 'frequency', freq);
        % 
        % sampleCDF = (sampleCDF - sampleCDF(1)) / (1 - sampleCDF(1));
     
        p = this.fit(data, 'cens', cens, 'freq', freq, 'truncdata', truncdata, varargin{:});
        nullCDF = p.cdf(samplex(2:end));
        delta1 = sampleCDF(1:end - 1) - nullCDF; % Vertical difference at jumps approaching from the LEFT.
        %  delta1 = sampleCDF(1:end - 1) - nullCDF; % Vertical difference at jumps approaching from the LEFT.
        %  delta2 = sampleCDF(2:end) - nullCDF; % Vertical difference at jumps approaching from the RIGHT.
        KS = max(abs(delta1));
        %    fprintf('%g  %g\n', x, KS);
        L = p.negloglik;
    else
        KS = 99999;
        L = NaN;
    end
end

% function [KS, p, L] = tryfitupper(this, fixedpar, data, x, samplex, sampleCDF, cens, freq, varargin)
%     this.d.(fixedpar) = x;
%     ndx = data <= x;
%     data = data(ndx);
%     if ~isempty(cens)
%         cens = cens(ndx);
%     end
%     if ~isempty(freq)
%         freq = freq(ndx);
%     end
%     ndx = samplex <= x;
%     samplex = samplex(ndx);
%     sampleCDF = sampleCDF(ndx);
%     sampleCDF = sampleCDF / sampleCDF(end);
%     p = this.fit(data, 'cens', cens, 'freq', freq, varargin{:});
%     nullCDF = p.cdf(samplex(2:end));
%     delta1 = sampleCDF(1:end - 1) - nullCDF; % Vertical difference at jumps approaching from the LEFT.
%     delta2 = sampleCDF(2:end) - nullCDF; % Vertical difference at jumps approaching from the RIGHT.
%     KS = max(abs([delta2]));
%     L = p.negloglik;
% end
% 
% function [KS, p, L] = tryfit(this, fixedpar, data, x, samplex, sampleCDF, varargin)
%     this.d.(fixedpar) = x;
%     p = this.fit(data, varargin{:});
%     nullCDF = p.cdf(samplex(2:end));
%     delta1 = sampleCDF(1:end - 1) - nullCDF; % Vertical difference at jumps approaching from the LEFT.
%     delta2 = sampleCDF(2:end) - nullCDF; % Vertical difference at jumps approaching from the RIGHT.
%     KS = max(abs([delta1 ]));
%     L = p.negloglik;
% end


