classdef SingleDist
    %SingleDist class. A wrapper around a single Distribution
    %It supports fitting of truncated distributions and fitting of
    %distributions with one or more parameters fixed (or a combination these two)
    %
    %Properties:
    %   d                 The distribution it wraps around (normal number
    %                     of parameters, can be truncated)
    %   Truncation        Truncation range
    %   IsTruncated       True if there is Truncation
    %   ParameterMask     logical vector with true if a parameter is fitted
    %   negloglik         Negloglikelihood
    %   InputData         InputData
    %   NumParameters     Number of parameters
    %   DistributionName  Distribution name (if there are fixed parameters
    %                     the name becomes like "Normal_fixed_mu"
    %   ParameterNames    names of parameter
    %   ParameterDescription descriptions of parameters
    %   ParameterValues   parameter values
    %
    %Methods:
    %   cdf                Combined cumulative density function
    %   disp               Display the components
    %   fit                Fit the distribution to data using the mle method
    %   icdf               Combined inverse cumulative density
    %   likefunc           Likelihood function
    %   mean               Mean of the distribution
    %   median             Median of the distribution
    %   SingleDist         The constructor of the class
    %   pdf                The combined probability density function
    %   random             Draw random numbers from the distribution
    %   std                Standard deviation of the distribution
    %   truncate           Truncate the combined distribution
    %   var                Variance of the distribution
    %
    % 

 
    properties
 
        d = [] %the underlying distribution
    end
 
    
    properties (Constant, Access = private)
        %you can adapt this list of synonyms to be used in the property 
        %"DisplayName". This is used to display a friendlier name than a code
        %like:
        %'Hump_fixed_r(1E-4)' for 'Hump' or 'Normal[0 Inf]' for 'Truncated
        %Normal'
        %
        %
        %the first element of the synolist should be a regexp code that
        %returns empty if there is no match (so end with $)
        % Example:
        %   synolist={'Hump_fixed_r([\(0-9,Ee- \)]*)?$','Hump';...
        %    'Hump$','Hump w.int.'
        synolist = {'Hump_fixed_r([\(0-9,Ee- \)]*)?$', 'Hump';  ... %ignore the value of the fixed parameter
            'Hump$', 'Hump w.int.';  ... %whole word example
            'Normal\[0[ ,][iI]nf\]$', 'Truncated Normal'}; %truncated [0,Inf]
    end


    properties (Dependent = true)

        DistributionName
        NumParameters
        ParameterNames;
        ParameterDescription;
        ParameterValues
        Truncation
        IsTruncated
        DisplayName
    end

    properties
        numericfit = false;
        ParameterMask = []
        negloglik = []
        InputData = [];
    end
    
    properties (Dependent = true)
        FixedValues
        AllParameters
        support
        code
    end

    methods
        [this, KS] = fitfixed(this, data, varargin)
 
        function pd = SingleDist(varargin)
            %pd=SingleDist('Normal',pars,property, value..)
            %properties
            %  'parameters'  vector with parameter values
            %  'mask'        vector false if a parameter is fixed
            %  'truncate'    truncation range
            pd = pd.set(varargin{:});
        end
 
        function s = get.support(this)
            info = shared.basic_info(this);
            s = info.support;
%             if ~isempty(info.range_pars)
%                 %range parameters are parameters that define the support range,
%                 %e.g. xmin xmax of Pareto
%                 if ~isempty(info.range_pars{1})
%                     s1 = this.d.(info.range_pars{1});
%                     if s1 > s(1)
%                         s(1) = s1-1E-10; %subtracted a little bit to make >= work
%                     end
%                 end
%                 if ~isempty(info.range_pars{2})
%                     s2 = this.d.(info.range_pars{2});
%                     if s2 > s(2)
%                         s(2) = s2+1E-10;
%                     end
%                 end
%             end
        end
 

        function s = get.code(this)
            info = shared.basic_info(this);
            s = info.code;
        end
 
        function s = get.DisplayName(this)
            s = this.DistributionName;

            c = regexp(s, this.synolist(:, 1));
            ndx = cellfun(@(x)~isempty(x), c);
            if any(ndx)
                s = this.synolist{ndx, 2};
                return;
            end
            f = find(s == '(');
            if ~isempty(f)
                s = s(1:f - 1);
            end
            s(s == '_') = '-';
        end
        function n = get.NumParameters(this)
            n = sum(this.ParameterMask);
        end
        
 
        function this = set.ParameterMask(this, val)
            if ~islogical(val) && ~all(ismember(val, [0 1]), 'all')
                error('singledist:set:mask', 'Parameter mask is not correct');
            end
            this.ParameterMask = val > 0;
        end
        
        function this1 = set(this, varargin)
            %this.set('parname',value,isfixed)
            %You can use this function to fix or unfix a parameter.
            %Fixed parameters remain unchanged during fitting
            %value=[] keeps the value the same.
            %Parameter value combinations:
            % [parameter], value, isfixed - set the value of a specific
            % parameter of the distribution and optionnally make it fixed
            % or not.
            % 'parameters' - set all parameters as a vector
            % 'mask'       - vector that is true for the free parameters 
            % 'truncate'   - set the truncation range
            % 'name'       - set the kind of distribution. Name can be the
            %                name/code of the distribution or a object.
            %
            %
            %examples:
            %pd=pd.set('mu',2)        - set the value of mu to 2
            %pd=pd.set('mu',3, true)  - fix mu and set value to 3
            %pd=pd.set('mu',[], false,'sigma',2) - unfix mu
            %            validoptions = {'parameters', 'mask', 'truncate', 'name'};
            function this1 = setdistrib(this1, distrib)
                if ~ischar(distrib)
                    if isa(distrib, 'SingleDist')
                        this1 = distrib;
                    else
                        this1.d = distrib;
                        this1.InputData = this1.d.InputData;
                        this1.ParameterMask = true(size(this1.d.ParameterValues));
                    end
                else
                    name_opts = SingleDist.parse_distrib(distrib);
                    this1.d = makedist(name_opts.d_info.code);
                    if isempty(name_opts.mask)
                        this1.ParameterMask = true(1, this1.d.NumParameters);
                    else
                        this1.ParameterMask = name_opts.mask;
                    end
                    if ~isempty(name_opts.fixedpar)
                        val = this1.d.ParameterValues;
                        val(~this1.ParameterMask) = name_opts.fixedpar;
                        this1.ParameterValues = val;
                    end
                    this1.Truncation = name_opts.truncate;
                end
            end
            if nargin < 2 && ~isempty(this.d)
                pars = this.AllParameters;
                fprintf('Usage:\n   set([parameter name], [value], [isfixed])\nExamples (current settings):\n')
                for i = 1:numel(pars)
                    fprintf('   set(''%s'', %g, %s)\n', pars(i).name, pars(i).value, mat2str(pars(i).isfixed));
                end
                fprintf('   set(''%s'', ''%s'')\n', 'name', this.code);
                fprintf('   set(''%s'', %s)\n', 'mask', mat2str(this.ParameterMask));
                fprintf('   set(''%s'', %s)\n', 'truncate', mat2str(this.Truncation));
                fprintf('   set(''%s'', %s)\n', 'parameters', mat2str(this.ParameterValues));
                return
            elseif nargin == 2 && isempty(this.d)
                this1 = setdistrib(this, varargin{1});
                return;
            end
            this1 = this;
            f1 = find(cellfun(@ischar, varargin));
            fname = find(strcmpi(varargin(f1), 'name'), 1);
            if ~isempty(fname)
                %name of the distribution should be handled first
                this1 = setdistrib(this1, varargin{f1(fname) + 1});
                if numel(f1) >= fname + 1 && f1(fname) + 1 == f1(fname + 1)
                    f1(fname + 1) = [];
                end
            elseif isempty(this1.d)
                if isempty(f1)
                    f1 = 1;
                end
                this1 = setdistrib(this1, varargin{f1(1)});
                if numel(varargin) > f1(1) && isnumeric(varargin{f1(1) + 1})
                    varargin{f1(1)} = 'parameters';
                else
                    varargin{f1(1)} = [];
                    f1(1) = [];
                end
            end
            i = 1;
            newparval = this1.ParameterValues;
            oldparval = newparval;
            while i <= numel(f1)
                if i == numel(f1)
                    varargin1 = varargin(f1(i):end);
                else
                    varargin1 = varargin(f1(i):f1(i + 1) - 1);
                end
                if numel(varargin1) == 1
                    error('singledist:set:noval', 'parameter-value combination "%s" is incomplete', varargin1{1});
                end
                parname = varargin1{1};
                if isempty(this1.d)
                    f = [];
                else
                    f = find(strcmp(this1.d.ParameterNames, parname));
                end
                if isempty(f)
                    if any(strcmpi(parname, 'parameters'))
                        if ~isnumeric(varargin1{2}) || ~((numel(varargin1{2}) == this1.NumParameters) || (numel(varargin1{2}) == this1.d.NumParameters))
                            error('singledist:set:par', 'Parameter not correct');
                        end
                        newparval = varargin1{2};
                    elseif any(strcmpi(parname, 'mask'))
                        if (numel(varargin1{2}) ~= this1.d.NumParameters)
                            error('singledist:set:mask', 'Size of parameter mask is not correct');
                        end
                        this1.ParameterMask = varargin1{2};
                    elseif any(strcmpi(parname, 'name'))
                        if i < numel(f1) && (f1(i + 1) == f1(i) + 1)
                            i = i + 1; %is already handled
                        end
                    elseif any(strcmpi(parname, 'truncate'))
                        this1.Truncation = varargin1{2};
                    else
                        validoptions = [this1.d.ParameterNames, {'parameters', 'mask', 'truncate'}];
                        s = sprintf('''%s'',', validoptions{:});
                        error('Parameter/option not found, valid options are:\n%s', s(1:end - 1))
                    end
                else
                    if numel(varargin1) >= 2 && ~isempty(varargin1{2})
                        newparval(f) = varargin1{2};
                        %this1.d.(parname) = varargin1{2};
     
                    end
                    if numel(varargin1) >= 3 && ~isempty(varargin1{3})
                        if ~islogical(varargin1{3}) && ~ismember(varargin1{3}, [0 1])
                            error('singledist:set:thirdarg', 'Third argument of "%s" (isfixed) should be logical (true/false)', parname)
                        end
                        this1.ParameterMask(f) = ~varargin1{3};
                    end
                end
                i = i + 1;
            end
            if ~isequal(newparval, oldparval)
                this1.ParameterValues = newparval;
            end
        end

        function ran = getrange(this, minpdf)
            info = this.getInfo;
            ran = info.support;
            if isinf(ran(1)) && ~isinf(ran(2))
                x = -logspace(5, -1, 100);
            elseif isinf(ran(2)) && ~isinf(ran(1))
                x1 = logspace(5, -1, 100);
            else
                x = [-logspace(5, -1, 100), 0, logspace(-1, 5, 100)];
                x1 = fliplr(x);
            end
            if isinf(ran(1))
                pdf = this.pdf(x);
                ran(1) = x(find(pdf > minpdf, 1) - 1);
            end
            if isinf(ran(2))
                pdf = this.pdf(x1);
                ran(2) = x1(find(pdf > minpdf, 1) - 1);
            end
        end

        function val = get.Truncation(this)
            val = this.d.Truncation;
            if isempty(val)
                val = this.support;
            end
        end

        function this = set.Truncation(this, val)
            supp = this.support;
            if isempty(val)
                val = supp;
            end
            if ~isnumeric(val) || (numel(val) ~= 2)
                error('singledist:set:truncation', 'Truncation should be a 2 element numeric vector');
            end
            if val(2) <= val(1)
                error('singledist:set:truncation', 'Range of truncation not correct');
            end
            this = this.truncate(val(1), val(2));
        end


        function this = set.IsTruncated(this, val)
            if val
                this = this.truncate(0, Inf);
            else
                supp = this.support;
                this = this.truncate(supp(1), supp(2));
            end
        end


        function s = get.DistributionName(this)
            s = this.d.DistributionName;
            if this.IsTruncated
                s = sprintf('%s[%g,%g]', s, this.Truncation(1), this.Truncation(2));
            end
            if any(~this.ParameterMask)
                vals = sprintf('%g,', this.FixedValues);
                s = sprintf('%s_fixed%s(%s)', s, sprintf('_%s', this.d.ParameterNames{~this.ParameterMask}), vals(1:end - 1));
            end
        end
    
        %ParameterName Name of parameter
        %    ParameterName is a two-element cell array containing names
        %    of the parameters of this distribution.
        function nam = get.ParameterNames(this)
            nam = this.d.ParameterNames(this.ParameterMask);
        end
    
        function nam = get.ParameterDescription(this)
            nam = this.d.ParameterDescription(this.ParameterMask);
        end
        %ParameterDescription Description of parameter
        %    ParameterDescription is a two-element cell array containing
        %    descriptions of the parameters of this distribution.
        function res = get.IsTruncated(this)
            res = this.d.IsTruncated;
        end
    
        function nam = get.ParameterValues(this)
            nam = this.d.ParameterValues(this.ParameterMask);
        end
        function nam = get.FixedValues(this)
            nam = this.d.ParameterValues(~this.ParameterMask);
        end
    
        function this = set.AllParameters(this, allpar)
            if isfield(allpar, 'value')
                if ~all(cellfun(@isnumeric, {allpar(:).value}))
                    error('value of parameter should be numeric');
                end
                this.ParameterValues = [allpar(:).value];
            end
            if isfield(allpar, 'isfixed')
                isfixed = [allpar(:).isfixed];
                if any(~ismember(isfixed, [0 1]))
                    error('isfixed should be a logical (0/1) value');
                end
                this.ParameterMask = ~isfixed;
            end
        end
  
        function nam = get.AllParameters(this)
            nam = struct('name', this.d.ParameterNames,  ...
                'realname', this.d.ParameterNames, 'component', cell(size(this.ParameterMask)),  ...
                'value', num2cell(this.d.ParameterValues), 'isfixed', num2cell(~this.ParameterMask));
        end

        function this = set.ParameterValues(this, val)
            %you can both set the non-fixed parameter or all parameter
            if ~isnumeric(val)
                error('singledist:parametervalues', 'The parameters should be numeric')
            end
            if numel(val) == sum(this.ParameterMask)
                %non-fixed
                totval = this.d.ParameterValues;
                totval(this.ParameterMask) = val;
            elseif numel(val) == numel(this.d.ParameterValues)
                %all
                totval = val;
            else
                error('singledist:parametervalues', 'Size of the parameters is not correct')
            end
            if this.IsTruncated
                trunc = this.Truncation;
            else
                trunc = [];
            end
            try
                if ~isprop(this.d, 'InputData') || isempty(this.d.InputData)
                    totval = num2cell(totval);
                    this.d = makedist(this.code, totval{:});
                else
                    this.d = this.d.makeFitted([], totval, this.d.negloglik,  ...
                        this.d.ParameterCovariance, this.d.InputData.data, this.d.InputData.cens, this.d.InputData.freq);
                end
            catch
                if ~iscell(totval)
                    totval = num2cell(totval);
                end
                this.d = makedist(this.code, totval{:});
            end
            if ~isempty(trunc)
                try
                    this = this.truncate(trunc(1), trunc(2));
                catch err
                    if strcmp('stats:probdists:ZeroMassTruncation', err.identifier)
                        warning(err.identifier, '%s', err.message);
                    else
                        retrhow(err);
                    end
                end
            end
        end
    
        function this = set.FixedValues(this, val)
            totval = this.d.ParameterValues;
            totval(~this.ParameterMask) = val;
            this.ParameterValues = totval;
        end
    
    
        function res = cumhazard(this, x)
            if ismethod(this.d, 'cumhazard')
                res = this.d.cumhazard(x);
            else
                res = shared.integrate_ode45(@(x)this.hazard(x), x, []);
            end
        end
    
        function res = hazard(this, x)
            if ismethod(this.d, 'hazard')
                res = this.d.hazard(x);
            elseif isa(this.d, 'prob.ExponentialDistribution')
                res = (zeros(size(x)) + 1 / this.d.mu);
            else
                %Hazard function is  pdf(x)./(1-cdf(x)) only for
                %continuous distributions with support [0 Inf]
                info = shared.basic_info(this);
                if ~info.iscontinuous || (~isequal(info.support, [0 Inf]) && ~(this.IsTruncated && this.Truncation(1) >= 0))
                    error('MixedDistribution:hazard', 'Only continuous distributions with support=[0 Inf] have a hazard function');
                end
                %this is not very accurate if the cdf is close to one
                Survivor = 1 - this.d.cdf(x);

                res = this.d.pdf(x) ./ Survivor;
                res(Survivor < 1E-12) = NaN;
            end
        end

        function res = pdf(this, varargin)
            res = this.d.pdf(varargin{:});
        end

        function res = iqr(this, varargin)
            res = this.d.iqr(varargin{:});
        end

        function res = cdf(this, varargin)
            res = this.d.cdf(varargin{:});
        end
    
        function res = survival(this, varargin)
            res = 1 - this.d.cdf(varargin{:});
        end

        function res = std(this, varargin)
            res = this.d.std(varargin{:});
        end

        function res = var(this, varargin)
            res = this.d.var(varargin{:});
        end

        function res = mean(this, varargin)
            res = this.d.mean(varargin{:});
        end

        function res = icdf(this, varargin)
            res = this.d.icdf(varargin{:});
        end
    
        function res = random(this, varargin)
            res = this.d.random(varargin{:});
        end
    
        function s1 = disp(this)
            nms = this.d.ParameterNames;
            n = length(nms);
            nameWidth = max(cellfun('length', nms));
            s = sprintf('  <a href="matlab:doc(''SingleDist'')">SingleDist</a> containing:\n\n');
            s = sprintf('%s  %s distribution\n', s, this.DisplayName);
            vals = cell(n, 1);
            for j = 1:n
                vals{j} = sprintf('%g', this.d.ParameterValues(j));
            end
            valWidth = max(cellfun('length', vals));
            for j = 1:n
                if this.ParameterMask(j)
                    s = sprintf('%s    %*s = %*s\n', s, nameWidth, nms{j}, valWidth, vals{j});
                else
                    s = sprintf('%s    %*s = %*s  <<fixed>>\n', s, nameWidth, nms{j}, valWidth, vals{j});
                end
            end
            if this.IsTruncated
                s = sprintf('%s   Truncated to the interval %s\n', s, mat2str(this.Truncation));
            end
            if nargout == 0
                disp(s);
            else
                s1 = shared.str2cell(s);
            end
        end
        function this = fitmle(this, x, varargin)
            %force to use mle (for testing)
            [x, intervalcens, truncdata, varargin, this] = this.check_arguments(x, varargin{:});
            info = shared.basic_info(this.DistributionName); %this is needed for adapting plim
            this = shared.fit_mle(this.likefunc_handle(intervalcens, truncdata), @(varargin)this.makeFitted(truncdata,varargin{:}), info.plim, this.ParameterValues, x, varargin{:});

        end
        function this = fit(this, x, varargin)

            try
                [x, intervalcens, truncdata, varargin, this] = this.check_arguments(x, varargin{:});
                info = shared.basic_info(this.DistributionName); %this is needed for adapting plim
                hasbounds = any(strcmpi(varargin(1, :), 'lowerbound')) || any(strcmpi(varargin(1, :), 'upperbound'));
                varargin = varargin(:)';
                if ~any(this.ParameterMask)
                    return;
                end
                if ~this.IsTruncated && all(this.ParameterMask) && isempty(truncdata) && ~intervalcens && ~hasbounds && ~this.numericfit
                    this.d = fitdist(x, info.code, varargin{:}); %normal run can be faster!
                    this = this.makeFitted(truncdata, this.d.ParameterValues, this.d.negloglik, this.d.ParameterCovariance,  ...
                        this.d.InputData.data, this.d.InputData.cens, this.d.InputData.freq);
                else
                    %  info=shared.basic_info(this.DistributionName);
                    %  plim=this.getInfo.plim;
                    if (any(strcmp(info.code, {'pareto', 'taperedpareto'})) && this.ParameterMask(2)) || (strcmp(info.code, 'boundedpareto') && all(this.ParameterMask(2:3)))
                        %we use fitfixed to fit xmin (and xmax)
                        opts = reshape(varargin, 2, length(varargin) / 2);
                        ndx = strcmp(opts(1, :), 'censoring');
                        opts(1, ndx) = {'cens'};
                        ndx = strcmp(opts(1, :), 'frequency');
                        opts(1, ndx) = {'freq'};
                        if ~isempty(truncdata)
                            opts(:, end + 1) = {'truncdata'; truncdata};
                        end
                        d1 = this.d.fit(x, opts{:});
                        this = this.makeFitted(truncdata, d1.ParameterValues(this.ParameterMask), d1.negloglik, d1.ParameterCovariance,  ...
                            d1.InputData.data, d1.InputData.cens, d1.InputData.freq);
                    else
 
                        this = shared.fit_mle(this.likefunc_handle(intervalcens, truncdata), @(varargin)this.makeFitted(truncdata,varargin{:}), info.plim, this.ParameterValues, x, varargin{:});
                    end
                end
            catch err
                if strcmp(err.identifier, 'stats:ProbDistUnivParam:fit:CensoringNotAllowed')
                    %if censoring is not allowed in the distribution we can try to run mle with
                    %general likefunct
                    likefunct = @(varargin)shared.likefunc_mydistrib(@this.pdffunc, @this.cdffunc, varargin{:});
                    % plim=shared.basic_info(this.d.DistributionName).plim;
                    this = shared.fit_mle(likefunct, @(varargin)this.makeFitted(truncdata,varargin{:}), info.plim, this.ParameterValues, x, varargin{:});
                elseif ~any(strcmp(err.identifier, {'stats:ProbDistUnivParam:fit:InsufficientFreq', 'stats:fitdist:AllCensored', 'stats:probdists:NonnegativeParameter', 'stats:probdists:ZeroMassTruncation'}))
                    disp(err.identifier)
                    rethrow(err);
                end
            end
        end

        function this = truncate(this, trunclow, trunchi)
            %info= shared.basic_info(this.d.DistributionName);
            supp = this.support;
            if trunclow <= supp(1) && trunchi >= supp(2)
                try
                    this.d = this.d.makeFitted(this.d.ParameterValues, [], [], [], [], []);
                catch
                    totval = num2cell(this.d.ParameterValues);
                    this.d = makedist(this.d.DistributionName, totval{:});
                end
           
            else
                this.d = this.d.truncate(trunclow, trunchi);
            end

        end
    
        function this = randompars(this, scale, typicalpars)
            %create a distribution with random parameters
            %
            %pd=MixedDistribution('Normal')
            %pd=pd.randompars(scale) - creates a  distribution with a
            %certain scale factor to be used for inf parameter limits (default = 10)
            %pd=makedist_rnd(pd) - copy the distribution pd with random parameters.
            %   (pd should be an distribution object) (scale factor = 10)
            %

            if nargin < 2
                scale = 10;
            end


            info = shared.basic_info(this.DistributionName);

            if nargin >= 3
                info.typicalpars = typicalpars;
            end
            if ~isfield(info, 'typicalpars')
                if isprop(this.d, 'typicalpars')
                    info.typicalpars = this.d.typicalpars;
                else
                    info.typicalpars = ones(1, size(info.plim, 2));
                end
            end
            maxpar = info.plim(2, :);
            ndx = isinf(maxpar);
            maxpar(ndx) = info.typicalpars(ndx) .* scale;
            minpar = info.plim(1, :);
            ndx = isinf(minpar);
            minpar(ndx) = -info.typicalpars(ndx) .* scale;
            pval = rand(size(minpar)) .* (maxpar - minpar) + minpar;

            if isfield(info, 'pinteger') && any(info.pinteger)
                for j = 1:length(info.pinteger)
                    if info.pinteger(j)
                        pval(j) = round(pval(j));
                    end
                end
            end
            %additional conditions
            if any(strcmp(info.code, {'uniform', 'loguniform'}))
                %Lower<Upper
                pval = sort(pval);
            end
            if strcmp(info.code, 'boundedpareto')
                %xmin<xmax
                pval(2:end) = sort(pval(2:end));
            end
            this.ParameterValues = pval(this.ParameterMask); %
        
        end
    
        function info = getInfo(this)
            info = shared.basic_info(this.DistributionName);
            %             try
            %                 info = this.d.getInfo;
            %             catch
            %                 info=shared.basic_info(this.d.DistributionName);
            %                 return;
            %             end
            %             info2 = shared.basic_info(this.d.DistributionName);
            %             info.plim = info2.plim;
            %             info.support = info2.support;
            %             info.pinteger = info2.pinteger;
            %             if isempty(info.pnames)
            %                 info.pnames = this.ParameterNames;
            %             end
            %             info.pnames = info.pnames(this.ParameterMask);
            %             if ~isempty(info.pdescription)
            %                 info.pdescription = info.pdescription(this.ParameterMask);
            %             end
            %             if ~isempty(info.logci)
            %                 info.logci = info.logci(this.ParameterMask);
            %             end
            %             info.prequired = info.prequired(this.ParameterMask);
            %             info.pmask = this.ParameterMask;
            %             info.plim = info.plim(:, this.ParameterMask);
        end
    
    
    end
    methods %(Access = private)
        
        function han = likefunc_handle(this, intervalcens, truncdata)
            if nargin < 3
                truncdata = [];
            end
            %slightly faster to use the handle during fitting
            if ~any(~this.ParameterMask) && ~this.IsTruncated && ~intervalcens && isempty(truncdata)
                h = this.d; %maybe smaller?
                han = @(varargin)h.likefunc(varargin{:});
            elseif ~intervalcens %right censoring
                han = @(varargin)shared.likefunc_mydistrib(@this.pdffunc, @this.cdffunc,truncdata, varargin{:});
            else %interval censoring
                han = @(varargin)shared.likefunc_mydistrib_interval(@this.pdffunc, @this.cdffunc,truncdata, varargin{:});
            end
        end
        function [nll, acov] = likefunc(this, varargin)
            if ~any(~this.ParameterMask) && ~this.IsTruncated && islogical(varargin{3})
                [nll, acov] = this.d.likefunc(varargin{:});
            elseif islogical(varargin{3}) %right censorring
                [nll, acov] = shared.likefunc_mydistrib(@this.pdffunc, @this.cdffunc, varargin{:});
            else %interval censoring
                [nll, acov] = shared.likefunc_mydistrib_interval(@this.pdffunc, @this.cdffunc, varargin{:});
            end
        end
 
        function res = cdffunc(this, x, varargin)
            if any(~this.ParameterMask)
                totalpar = num2cell(this.d.ParameterValues);
                totalpar(this.ParameterMask) = varargin;
                varargin = totalpar;
            end
            if this.IsTruncated
                frac = this.tailprobs(varargin{:});
                res = this.d.cdffunc(x, varargin{:});
                res = (res - frac(1)) ./ (1 - sum(frac));
                res(res < 0) = 0;
                res(res > 1 - 1E-10) = 1 - 1E-10;
            else
                res = this.d.cdffunc(x, varargin{:});
            end
        end
 
        function res = pdffunc(this, x, varargin)
            if any(~this.ParameterMask)
                totalpar = num2cell(this.d.ParameterValues);
                totalpar(this.ParameterMask) = varargin;
                varargin = totalpar;
            end
            if this.IsTruncated
                frac = this.tailprobs(varargin{:});
                res = this.d.pdffunc(x, varargin{:});
                res(x < this.Truncation(1) | x > this.Truncation(2)) = 0;
                res = res ./ (1 - sum(frac));
                res(res <= 0) = 1E-300;
                res(~isfinite(res)) = 1E40;
            else
                res = this.d.pdffunc(x, varargin{:});
            end
        end
        
        function this = makeFitted(this, truncdata, par, nll, cov, x, cens, freq)
            if any(~this.ParameterMask)
                totalpar = this.d.ParameterValues;
                totalpar(this.ParameterMask) = par;
                par = totalpar;
            end
            trunc = this.Truncation;
            istrunc = this.IsTruncated;
            try
                %this is not a required method
                this.d = this.d.makeFitted(par, nll, cov, [], [], []);
            catch
                cpar = num2cell(par);
                this.d = makedist(this.d.DistributionName, cpar{:});
            end
            this.InputData = struct('data', x, 'cens', cens, 'freq', freq, 'truncdata', truncdata);
            this.negloglik = nll;
            try
                if istrunc
                    this = this.truncate(trunc(1), trunc(2));
                end
            catch err
                if ~strcmp('stats:probdists:ZeroMassTruncation', err.identifier)
                    rethrow(err);
                else
                    warning(err.message);
                end
            end
        end
        
        function frac = tailprobs(this, varargin)
            %probabilities of the tails in truncation
            frac = zeros(2, 1);
            if ~isinf(this.Truncation(1))
                frac(1) = this.d.cdffunc(this.Truncation(1), varargin{:});
            end
            if ~isinf(this.Truncation(2))
                frac(2) = (1 - this.d.cdffunc(this.Truncation(2), varargin{:}));
            end
        end
 
        function [data, intervalcens, truncdata, args, this] = check_arguments(this, data, varargin) %this, trunc, cens, freq, options)
            args = reshape(varargin, 2, length(varargin) / 2);
            if isempty(data)
                error('SingleDist:nodata', 'The data for fitting is empty');
            end
            for i = 1:this.d.NumParameters
                if ~isempty(args)
                    ndx = strcmp(this.d.ParameterNames{i}, args(1, :));
                    if any(ndx)
                        this.ParameterValues(i) = args{2, ndx};
                        this.ParameterMask(i) = false;
                        args = args(:, ~ndx);
                    end
                end
            end
            ndx = strncmpi(args(1, :), 'options', 4);
            if ~any(ndx)
                args = [args, {'options'; struct('start', this.ParameterValues)}];
            else
                opts = args{2, ndx};
                if ~isfield(opts, 'start')
                    opts.start = this.ParameterValues;
                    args{2, ndx} = opts;
                end
            end

            %info = MixedDistribution.myinfo(this.d.DistributionName);
            supp = this.support;
            trunc = this.Truncation;
            intervalcens = false;
            cens = [];
            freq = [];
            if size(data, 2) == 2
                cens = data(:, 2);
                data = data(:, 1);
            end

            if min(data) < max(trunc(1), supp(1)) || max(data) > min(supp(2), trunc(2))
                datandx = data >= supp(1) & data <= supp(2);
                if ~isempty(trunc) && any(data < trunc(1) | data > trunc(2))
                    warning('fit_truncated:invaliddata', 'There is some data outside the truncation range of the distribution, removed');
                    datandx = datandx & (data > trunc(1) & data < trunc(2));
                end
                %  data = data(datandx);
                ndx = strncmpi(args(1, :), 'freq', 4);
                if any(ndx)
                    freq = args{2, ndx};
                    freq = freq(:);
                    if ~isempty(freq)
                        args{2, ndx} = freq(datandx);
                    end
                end
                
                ndx = strncmpi(args(1, :), 'cens', 4);
                if any(ndx)
                    cens = args{2, ndx};
                    cens = cens(:);
                    if isempty(cens) || all(cens == 1 | cens == 0)
                        args{2, ndx} = logical(cens);
                    else
                        intervalcens = true;
                    end
                    if ~isempty(cens)
                        args{2, ndx} = cens(datandx);
                    end
                end
                data = data(datandx);
 
            else
                ndx = strncmpi(args(1, :), 'cens', 4);
                if any(ndx)
                    cens = args{2, ndx};
                    if isempty(cens) || all(cens == 1 | cens == 0)
                        args{2, ndx} = logical(cens);
                    else
                        intervalcens = true;
 
                    end
                else
                    args{1, end + 1} = 'cens';
                    args{2, end} = false(size(data));
                end
            end
           
            ndx = strcmpi(args(1, :), 'truncdata');
            if any(ndx)
                truncdata = args{2, ndx};
                args = args(:, ~ndx);
                if numel(truncdata) == 2
                    truncdata = repmat(truncdata(:)', numel(data), 1);
                end
                %replace nan or <supp(1) with supp(1)
                if ~isempty(truncdata)
                    truncdata(isnan(truncdata(:, 1)) | truncdata(:, 1) < supp(1), 1) = supp(1);
                    %replace nan or >supp(2) with supp(2)
                    truncdata(isnan(truncdata(:, 2)) | truncdata(:, 2) > supp(2), 2) = supp(2);
                    if ~any(truncdata(:, 2) < supp(2) | truncdata(:, 1) > supp(1))
                        %none are smaller than the support range
                        truncdata = [];
                    else
                        if intervalcens
                            fndx = (data < truncdata(:, 1) | cens > truncdata(:, 2));
                        else
                            fndx = (data < truncdata(:, 1) | data > truncdata(:, 2));
                        end
                        if any(fndx)
                            warning('fit_truncdata:invaliddata', 'removed %d data points, because they are outside the data truncation range', sum(fndx));
                            data = data(~fndx);
                            truncdata = truncdata(~fndx, :);
                            ndx = strncmpi(args(1, :), 'cens', 4);
                            if any(ndx)
                                cens = args{2, ndx};
                                if ~isempty(cens)
                                    args{2, ndx} = cens(~fndx);
                                end
                            end
                            ndx = strncmpi(args(1, :), 'freq', 4);
                            if any(ndx)
                                freq = args{2, ndx};
                                if ~isempty(freq)
                                    args{2, ndx} = freq(~fndx);
                                end
                            end
                        end
                    end
                end
            else
                truncdata = [];
            end

            if strcmp(this.d.DistributionName, 'Binomial') && (~intervalcens && ~any(cens))
                args = [args, {'ntrials'; this.d.N}];
            end

            if strcmp(this.d.DistributionName, 'Generalized Pareto') && (~intervalcens && ~any(cens))
                args = [args, {'theta'; this.d.theta}];
            end
            ndx = strcmpi(args(1, :), 'truncate');
            if any(ndx)
                trunc=args{2,ndx};
                args = args(:, ~ndx);
            else
                trunc=[];
            end
            if this.IsTruncated
                trunc = this.Truncation;
            elseif isempty(trunc)
                return;
            end
            

            %info = MixedDistribution.myinfo(this.d.DistributionName);
            if isempty(trunc)
                trunc = supp; %info.support;
            end
            ndx = strcmpi(args(1, :), 'start');
            if any(ndx)
                astart = args{2, ndx};
            else
                astart = this.ParameterValues;
            end
            %check tails
            if any(~this.ParameterMask)
                totalpar = this.d.ParameterValues;
                totalpar(this.ParameterMask) = astart;
                pstart = num2cell(totalpar);
            else
                pstart = num2cell(astart);
            end
            if sum(this.tailprobs(pstart{:})) > 0.95
                %try to find initial conditions where the body has a non-zero
                %probability.
                %if the probability of the body is zero, we cannot fit.
                bodyfun = @(varargin)1-sum(this.tailprobs(varargin{:}));
                pd = SingleDist(this.d.DistributionName);
                pstart = pd.fit(data);
                astart = pstart.ParameterValues;
                parvalues = repmat(astart(:)', 1000, 1);
                parvalues = parvalues * 2 .* rand(size(parvalues));
                pbody = zeros(size(parvalues, 1), 1);
                for i = 1:size(parvalues, 1)
                    pstart = num2cell(parvalues(i, :));
                    pbody(i) = bodyfun(pstart{:});
                end
                [~, ndx] = min(abs(pbody - 0.5));
                astart = parvalues(ndx, :);
                ndx = strcmpi(args(1, :), 'start');
                if any(ndx)
                    args{2, ndx} = astart;
                else
                    args = [args, {'start'; astart}];
                end

            end
            this.Truncation=trunc;
        end
    end

    methods (Static)
        function res = parse_distrib(distrib)
            if ~ischar(distrib)
                res = struct('name', [], 'truncate', distrib.Truncation, 'mask', [], 'fixedpar', [], 'd_info', shared.basic_info(distrib));
                res.name = res.d_info.code;
                return
            end
            distrib_parts = regexp(distrib, '(^[A-Za-z0-9_ \-]*(?=(_fixed_)|([\[])|($)))|([\[][^\]]*])|((?<=_fixed_)[_A-Za-z0-9]*)|([\(][0-9\,\.E\- ]*[\)])', 'match');
            fixed_names = {};
            %  distrib_parts = regexp(distrib, '(^[A-Za-z0-9_]*(?=(_fixed_)|([\[])|($)))|([\[][^\]]*])|((?<=_fixed_)[_A-Za-z0-9]*)|([\(][0-9., )*[\)])', 'match');
            res = struct('name', [], 'truncate', [], 'mask', [], 'fixedpar', [], 'd_info', []);
            ndx = shared.strcontains(distrib_parts, '_fixed_');
            if any(ndx)
                %could not repair this
                fixed_names = regexp(distrib_parts{ndx}, '(?<=_)[^_]*', 'match');
                fixed_names = fixed_names(2:end);
                distrib = regexp(distrib, '.*(?=(_fixed))', 'match');
                res.name = distrib{1};
            else
                res.name = distrib_parts{1};
            end
            for i = 2:length(distrib_parts)
                p = distrib_parts{i};
                if shared.strcontains(p, '[')
                    res.truncate = str2num(p); %#ok<ST2NM>
                elseif shared.strcontains(p, '(')
                    p = strrep(strrep(p, '(', '['), ')', ']');
                    pars = str2num(p); %#ok<ST2NM>
                    res.fixedpar = pars;
                else
                    fixed_names = regexp(p, '_', 'split');
                    %info1 = MixedDistribution.myinfo(distrib);

                end
            end

            res.name = regexprep(lower(res.name), {'-', ' '}, {'', ''});
            d = makedist(res.name);
            try
                res.d_info = shared.basic_info(d.DistributionName);
            catch
            end
            if ~isempty(fixed_names)
                if any(~ismember(fixed_names, res.d_info.pnames))
                    error('SingleDist:fixed', 'Did not recognize all fixed parameter names:%s', sprintf(' %s', fixed_names{:}));
                end
                res.mask = ~ismember(res.d_info.pnames, fixed_names);
            end
        end

    end
end
