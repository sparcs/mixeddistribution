function [pd, res] = fitmixdist(data, distrib, varargin)
    %    fitmixdist = fit a mixed distribution to data using the Expectation
    %      Maximalization algorithm.
    % 
    %     Usage: 
    %     [pd,res] = fitmixdist(data, distrib, param, value, param, value...)
    %
    %     data = vector with 1D data
    %
    %     distib= cell list of distributions to fit. Thes names should be
    %         in a cell of strings. For instance: 
    %             {'Normal',Normal','LogNormal'}
    %         alternatively you can define them is rows with the name of the
    %         distribution and the number of the same distributions:
    %         (order is not critical)
    %         for instance (note the semicolons and comma's):
    %            {'Normal',3;'LogNormal',1}
    %
    %     param,value extra options 
    %     'replicates' Number of replicate searches. As the method can easily hit a local
    %                optimum it is recommended to have at least 10 replicates. (default = 10)
    %     'ínitial'  Vector of initial parameter values or a
    %                MixedDistribution object defining the parameters. This will be
    %                the initial conditions of the first replicate run.
    %     'initfun'  Kind of drawing of initial parameters, ('kmeans','range','quantiles')
    %                See the method initfun for details. default 'kmeans'
    %     'truncate' range of trucation of the resulting distribution
    %                []=none
    %     'censoring' vector with true for right censored data (default all false)
    %     'frequency' vector with frequencies of data (default all 1)
    %     'censmethod' Way of handling censored data (for testing)
    %                'ignore','remove','deal', 'deal' (=default) is by far
    %                the best method as the other ways are very biased.
    %     'fitgmdist' logical which is true if fitgmdist is used (for
    %                 Normal distributions only)
    %     'options'  a struct created with statset. The TolFun option is
    %                used only. Additional fields 'start',
    %                'lowerbound','upperbound'
    %
    %
    %     output:
    %     pd         fitted (optimal) MixedDistribution object with all information
    %     res        struct with some information about the runs.
    %               'pars'  parameter and mixture values (vector)
    %               'll'    log-likelihood
    %               'ncomp' Number of efffectively different components.
    %               (the method can converge with some duplicate components
    %               or with some components with almost zero probability)
    %
    % Using fitgmdist for (Log)Normal distributions:
    %     pd = fitmixdist(data, 'fitgmdist', k, param, value, param, value...)
    %         run MATLAB's fitgmdist to create a Gaussian Mixture model with k
    %         components and store the result to a MixedDistribution object.
    %         Additional parameters are used in fitgmdist (see help).
    %         Note that fitgmdist does not support censored data and
    %         frequencies.
    %     pd = fitmixdist(data, 'LogNormal','fitgmdist',true,'k',2)
    %         It can also fit k lognormal distributions (the data are log
    %         transformed then)
    %
    %
    %
    if ischar(distrib)
        distrib = {distrib};
    end
    if any(strcmp(distrib, 'fitgmdist'))
        if ~isempty(varargin) && isnumeric(varargin{1})
            varargin = [{'k'}, varargin];
            varargin = [varargin {'fitgmdist', true}];      
        elseif ~isempty(varargin) && islogical(varargin{1})
            varargin = [{'fitgmdist'}, varargin];
        else
            varargin = [varargin {'fitgmdist', true}];
        end
        distrib = {'Normal'};
    end
    pd = MixedDistribution(distrib);
    [pd, res] = pd.fit(data, varargin{:});
end
