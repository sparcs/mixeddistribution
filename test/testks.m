%pd = MixedDistribution('normal', 'mu', 3, 'sigma', 2);
%data = pd.random(100, 1);
[mindata, maxdata, freq] = shared.makebins(data, 20, false);

%    figure
if true
    shared.my_kstest(pd, data)
    %simple approach from plfit:
    freq1 = ones(size(data));
    x = sort(data);
    cx = (cumsum(freq1) - 1) ./ numel(data);
    cf = pd.cdf(x);
    delta1 = abs(cf - cx);
    [KS, ndx] = max(delta1)
    stairs(x, cx, 'm-')
    plot([x(ndx), x(ndx)], [cx(ndx), cf(ndx)], 'g*-')
    legend({'mixed', 'distrib', 'myopt', 'plfit', 'optplfit'})


    figure
    pd1 = MixedDistribution('normal', 'mu', 2, 'sigma', 2);
    shared.my_kstest(pd1, data)
    %simple approach from plfit:
    freq1 = ones(size(data));
    x = sort(data);
    cx = (cumsum(freq1) - 1) ./ numel(data);
    cf = pd1.cdf(x);
    delta1 = abs(cf - cx);
    [KS, ndx] = max(delta1)
    stairs(x, cx, 'm-')
    plot([x(ndx), x(ndx)], [cx(ndx), cf(ndx)], 'g*-')
    legend({'mixed', 'distrib', 'myopt', 'plfit', 'optplfit'})

    figure
end
shared.my_kstest(pd, mindata, 'censoring', maxdata, 'frequency', freq)
n = sum(freq);
temp = cumsum(freq(end: -1:1));
cx = 1 - temp(end: -1:1) ./ n;
cf = pd.cdf(mindata);
delta2 = abs(cf - cx);
[KS, ndx] = max(delta2)
stairs(mindata, cx, 'm-')
plot([mindata(ndx), mindata(ndx)], [cx(ndx), cf(ndx)], 'g*-')
if ~verLessThan('matlab', '9.1')
    ecdf([mindata, maxdata], 'freq', freq);
end
legend({'mixed', 'distrib', 'myopt', 'plfit', 'optplfit'})
