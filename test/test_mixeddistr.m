%Tests for MixedDistribution
%alldist = fitdist;
addpath('..')
close all

pd = MixedDistribution('normal', 'mu', 1, 'sigma', 2);
data = pd.random(2000, 1);
ndx1 = data(1:1000) < 3 & data(1:1000) > -3;

ndx2 = data(1001:2000) < 5 & data(1001:2000) > -2;
data1 = data([ndx1; ndx2]);
truncdata = [repmat( [-3 3], sum(ndx1), 1); repmat([-2 5], sum(ndx2), 1)];
pdtmp = MixedDistribution('normal');
pdnotrunc = pdtmp.fit(data1)
plotdist(pdnotrunc, 'xlim', [-7 8], 'title', 'No truncation');
hold on
h = plotdist(pd, 'xlim', [-7 8]);
set(h.hmain, 'color', [0 0 1])
legend({'only data>0', 'fitted distribution', 'real distribution'});
figure
pd1 = pdtmp.fit(data1, 'truncdata', truncdata, 'frequency', ones(size(data1)))
plotdist(pd1, 'xlim', [-7 8]);
hold on
h = plotdist(pd, 'xlim', [-7 8]);
set(h.hmain, 'color', [0 0 1])
legend({'only data>0', 'fitted distribution', 'real distribution'});


%censoring
figure
[freq, edges] = histcounts(data, [-5 -1 1 5 8]);
edges(edges < min(data)) = min(data);
freq = freq';
%mindata is the lower edge
mindata = edges(1:end - 1)';
%maxdata is the higher edge;
maxdata = edges(2:end)';
pdnocens = pdtmp.fit((mindata + maxdata) / 2, 'frequency', freq)
plotdist(pdnocens, 'xlim', [-7 8]);
hold on
h = plotdist(pd, 'xlim', [-7 8], 'title', 'Center of bins');
set(h.hmain, 'color', [0 0 1])
ylim([0 0.25])
legend({'centre of bin', 'fitted distribution', 'real distribution'});

figure
pd3 = pdtmp.fit(mindata, 'censoring', maxdata, 'frequency', freq)
plotdist(pd3, 'xlim', [-7 8], 'title', 'Center of bins');
hold on
h = plotdist(pd, 'xlim', [-7 8], 'title', 'Censoring');
set(h.hmain, 'color', [0 0 1])
legend({'', 'binned data', 'fitted distribution', 'real distribution'});
ylim([0 0.25]);
figure
plotdist([], 'data', pd3.InputData, 'title', 'Binned data')
ylim([0 0.25]);


pd = MixedDistribution('pareto', 'alpha', 2.6, 'xmin', 1)
data = pd.random(2000, 1);
ndx1 = data(1:1000) > 3;

ndx2 = data(1001:2000) > 20;
data1 = data([ndx1; ndx2]);
truncdata = [repmat( [3 inf], sum(ndx1), 1); repmat([20 inf], sum(ndx2), 1)];
pdtmp = MixedDistribution('pareto');
figure
subplot(1, 2, 1)
pd1 = pdtmp.fit(data1, 'truncdata', truncdata)
plotdist(pd1, 'scaletodata', true, 'title', sprintf('with truncation, \\alpha=%.3g, real \\alpha=%g', pd1.get('alpha'), pd.get('alpha')));
pd2 = pdtmp.fit(data1)
subplot(1, 2, 2)
plotdist(pd2, 'scaletodata', true, 'title', sprintf('no truncation, \\alpha=%.3g, real \\alpha=%g', pd2.get('alpha'), pd.get('alpha')));


pd=MixedDistribution('normal','mu',4,'sigma',2);
dat=pd.random(100,1);
pd1=pd.fit(dat,'upperbound',[3 1]);
assert(pd1.get('mu')<=3&&pd1.get('sigma')<=1,'upperbound does not work');
pd1=pd.fit(dat,'lowerbound',[5 3]);
assert(pd1.get('mu')>=5&&pd1.get('sigma')>=3,'lowerbound does not work');

pd=MixedDistribution({'normal','normal'},'mu',{4,0},'sigma',{1,3});
dat=pd.random(200,1);
pd1=pd.fit(dat,'lowerbound',[0.1 -inf -inf -inf -inf],'upperbound',[0.1 inf inf inf inf]);
assert(abs(pd1.get('Pi')-0.1)<1E-6,'lowerbound/upperbound does not work for Pi');
pd1=pd.fit(dat,'lowerbound',[0 5 -inf -inf -inf],'upperbound',[1 inf inf inf inf]);
assert(pd1.get('mu_1')>=5,'lowerbound/upperbound does not work for mixed dist');


%different ways to create a SingleDist object:
s=SingleDist('normal','mu',2,true,'sigma',3);
assert(s.d.mu==2&&s.d.sigma==3&&all(s.ParameterMask==[false true]),'SingleDist constructor problem')
s=SingleDist('mu',2,true,'sigma',3,'name','normal');
assert(s.d.mu==2&&s.d.sigma==3&&all(s.ParameterMask==[false true]),'SingleDist constructor problem')
s=SingleDist('normal',[2 3],'mask',[false true]);
assert(s.d.mu==2&&s.d.sigma==3&&all(s.ParameterMask==[false true]),'SingleDist constructor problem')
s=SingleDist('normal_fixed_mu',[2 3]);
assert(s.d.mu==2&&s.d.sigma==3&&all(s.ParameterMask==[false true]),'SingleDist constructor problem')
s=SingleDist('normal_fixed_mu(2)',3);
assert(s.d.mu==2&&s.d.sigma==3&&all(s.ParameterMask==[false true]),'SingleDist constructor problem')
s=SingleDist('normal_fixed_mu(2)[0 inf]',3);
assert(s.d.mu==2&&s.d.sigma==3&&all(s.ParameterMask==[false true])&&isequal(s.Truncation,[0 inf]),'SingleDist constructor problem')
s=SingleDist('name','normal_fixed_mu','parameters',[2 3],'truncate',[0 inf]);
assert(s.d.mu==2&&s.d.sigma==3&&all(s.ParameterMask==[false true])&&isequal(s.Truncation,[0 inf]),'SingleDist constructor problem')
m=MixedDistribution('normal','mu',2,true,'sigma',3);
assert(m.Components{1}.d.mu==2&&m.Components{1}.d.sigma==3&&all(m.Components{1}.ParameterMask==[false true]),'MixedDist constructor problem')
m=MixedDistribution({'normal',3,'lognormal',1},'mu_4',2,true,'sigma',3);
assert(m.Components{1}.d.mu==0&&m.Components{4}.d.mu==2&&m.Components{1}.d.sigma==3&&all(m.Components{4}.ParameterMask==[false true]),'MixedDist constructor problem')
m=MixedDistribution({'normal','normal','normal','lognormal'},'mu_4',2,true,'sigma',3,'truncate_3',[2 inf]);
assert(isequal(m.Components{3}.Truncation,[2 inf])&&m.Components{1}.d.mu==0&&m.Components{4}.d.mu==2&&m.Components{1}.d.sigma==3&&all(m.Components{4}.ParameterMask==[false true]),'MixedDist constructor problem')
m=MixedDistribution({'normal','normal'},[0.4 0.6],{[2 3],[4 5]});
assert(isequal(m.ComponentProportion,[0.4 0.6])&&m.Components{2}.d.mu==4&&m.Components{1}.d.sigma==3,'MixedDist constructor problem');
data=m.random(400,1);
gm=fitgmdist(data,2);
m=MixedDistribution(gm,false);
assert((m.NumComponents==2)&&strcmpi(m.Components{1}.DistributionName,'normal'),'MixedDistr constructor problem')
m = MixedDistribution({'linear','normal'}, 0.002);
data = m.random(1000, 1);


m = MixedDistribution(SingleDist('linear','a', 0.002));
data = m.random(1000, 1);
cens = rand(size(data)) < 0.1;
data(cens) = data(cens) / 2;
m1 = m.fit(data, 'censoring', cens)
cens1 = data;
cens1(cens) = Inf;
m2 = m.fit(data, 'censoring', cens1)
assert(abs(m2.ParameterValues - m1.ParameterValues) < 0.0001, 'Interval censoring seems not correct');

pd=SingleDist('taperedpareto','alpha',3.3,'xmin',3,'lambda',0.2);
dat=pd.random(100,1);
pd1=fitdist(dat,'taperedpareto');
pd2=fitdist(dat,'pareto');
plotdist(pd1)

c = {'r', 'g', 'b'};

% allsurvdist = {'birnbaumsaunders' , 'burr' , 'exponential' , 'flexhazard' , 'gamma' ,  ...
%     'gompertz' , 'gompertzmakeham' , 'hill' , 'hump' , 'inversegaussian' , 'linear' ,  ...
%     'linearexponential' , 'loglogistic' , 'nakagami' , 'rayleigh' , 'rician'  ...
%     , 'saturating' , 'saturatingexponential' , 'siler' , 'weibull' ,'lognormal'};

%alldist = {'exponential' , 'linear', 'linearexponential', 'saturating',  'hump_fixed_r(1E-5)'};
%alldist = {'saturatingexponential', 'hump', 'gompertz', 'hill', 'lognormal'};
alldist = {'birnbaumsaunders' , 'burr', 'gamma', 'nakagami', 'rayleigh'};

k = 0;
figure
set(gcf, 'position', [ 361 152 656 547]);
for i = 1:length(alldist)
    if ~strcmp(alldist{i}, 'kernel')
        s = SingleDist(alldist{i});
        %info = s.getInfo;
        %if isequal(info.support, [0 Inf]) && info.iscontinuous

        distrs = cell(3, 1);
        for j = 1:length(distrs)
            if strcmp(alldist{i}, 'lognormal')
                distrs{j} = s.randompars(10, [0.4 0.05]);
            else
                distrs{j} = s.randompars;
            end
        end
        k = k + 1;
        subplot(5, 2, k)
        for j = 1:length(distrs)
            plotdist(distrs{j}, 'type', 'pdf', 'title', ' ', 'rtext', s.DisplayName);
        end
        xlimits = xlim;
        for j = 1:length(distrs)
            if j == 1
                hold off
            else
                hold on
            end
            if i == 1
                res = plotdist(distrs{j}, 'title', 'Probalility density function', 'type', 'pdf', 'xlim', xlimits);
            else
                res = plotdist(distrs{j}, 'title', '', 'type', 'pdf', 'xlim', xlimits);
            end
            set(res.hmain, 'color', c{j});

        end
        k = k + 1;
        subplot(5, 2, k)
        for j = 1:length(distrs)
            if i == 1
                res = plotdist(distrs{j}, 'title', 'Hazard function', 'type', 'hazard', 'xlim', xlimits);
            else
                res = plotdist(distrs{j}, 'title', '', 'type', 'hazard', 'xlim', xlimits);
            end
            set(res.hmain, 'color', c{j});
        end
        %end
    end
end


%simple test of different ways to use the constructor
m = MixedDistribution({'Normal', 'Normal', 'LogNormal'}, [0.2 0.3 0.5]);
assert(m.NumComponents == 3 && strcmp(m.Components{1}.DistributionName, 'Normal')  ...
    && all(m.ComponentProportion == [0.2, 0.3, 0.5]), 'Constructor problem')
m = MixedDistribution({'Normal', 2; 'LogNormal', 1}, [0.2 0.3 0.5], {[0 3], [3, 3], [2, 3]});
assert(m.NumComponents == 3 && strcmp(m.Components{1}.DistributionName, 'Normal')  ...
    && m.Components{2}.d.mu == 3 && all(m.ComponentProportion == [0.2, 0.3, 0.5]), 'Constructor problem')
m = MixedDistribution({'Normal', 2; 'LogNormal', 1}, [0.2 0.3 0.5], {[0 3], [3, 3], [2, 3]});
assert(m.NumComponents == 3 && strcmp(m.Components{1}.DistributionName, 'Normal')  ...
    && m.Components{2}.d.mu == 3 && all(m.ComponentProportion == [0.2, 0.3, 0.5]), 'Constructor problem')
m = MixedDistribution({'Normal', 2; 'LogNormal', 1}, {[0 3], [3, 3], [2, 3]});
assert(m.NumComponents == 3 && strcmp(m.Components{1}.DistributionName, 'Normal')  ...
    && m.Components{2}.d.mu == 3 && m.ComponentProportion(1) == 1 / 3, 'Constructor problem')
m = MixedDistribution({'Normal[0 Inf]_fixed_mu(3)', 'Normal_fixed_mu(2.2)', 'Normal[0,Inf]'});
assert((m.NumComponents == 3) && (m.Components{2}.d.mu == 2.2) && all([0 Inf; -Inf Inf; 0 Inf] == m.Truncation, 'all'), 'Constructor problem');
m2 = m.test_fit;
m = MixedDistribution({'Normal_fixed_mu(3)', 'Normal_fixed_mu(2.2)', 'Normal'});
m2 = m.test_fit;
s = SingleDist('Normal[0 Inf]');
m = MixedDistribution({s, s.d});
assert(m.NumComponents == 2 && m.Components{1}.IsTruncated && m.Components{2}.IsTruncated);
m = MixedDistribution({makedist('Normal', 3, 2), makedist('Exponential', 3)}, [0.2 0.8]);
assert(m.NumComponents == 2 && strcmp(m.Components{2}.DistributionName, 'Exponential')  ...
    && m.Components{2}.d.mu == 3 && m.ComponentProportion(1) == 0.2, 'Constructor problem')
m = MixedDistribution({SingleDist('Normal_fixed_mu', [7, 2], 'truncate', [0 Inf]), SingleDist('Exponential', 3)}, [0.4 0.6]);
assert(all(m.Components{1}.ParameterMask == [0 1]) && all(m.Components{1}.d.ParameterValues == [7 2]) && all(m.Components{1}.Truncation == [0 Inf]), 'SingleDist problem');
m2 = m.test_fit;
assert((m2.Components{1}.FixedValues == 7) && all(m2.Components{1}.ParameterMask == [0 1]), 'test_fit or fit problem, kind of distr changed');
%end


% example of how to use the distribution to real data
load turchin
%first define your mixed distribution
m = MixedDistribution({'LinearExponential', 'Exponential'});
%then fit to the data, Replicates is the number of random initial
%conditions to start, it is recommended to repeat at least 10 times, as
%a single run will often fail to fit all components
%If the Parallel Computing Toolbox is available the replicates will be
%calculated in parallel.
%
%
%you can help the fitting by providing an initial guess (can either be
%ParameterValues or a MixedDistribution object.
m = m.fit(data, 'Replicates', 20, 'initial', m);
figure
subplot(3, 2, 1)
plotdist(m, 'type', 'pdf', 'aic', true, 'kernel', true, 'numbins', 30, 'title', 'title', 'title2', 'title2', 'rtext', 'rtext');
subplot(3, 2, 2);
plotdist(m, 'type', 'cdf', 'aic', true, 'numbins', 30);
subplot(3, 2, 3);
plotdist(m, 'type', 'survival', 'aic', true, 'numbins', 30);
subplot(3, 2, 4);
plotdist(m, 'type', 'hazard', 'aic', true, 'numbins', 30);
subplot(3, 2, 5);
plotdist(m, 'type', 'cumhazard', 'aic', true, 'numbins', 30);
subplot(3, 2, 6);
plotdist(m, 'type', 'powerlaw', 'aic', true, 'numbins', 30);
%plot the resulting distibutions with the AIC
figure(1)
subplot(3, 1, 1);
plotdist(m, 'xlim', [0 1000], 'AIC', m.AIC, 'numbins', 50)

%now fit a simpler model to check if the mix is needed
m1 = MixedDistribution('LinearExponential');
m1 = m1.fit(data, 'Replicates', 20);
subplot(3, 1, 2);

plotdist(m1, 'xlim', [0 1000], 'AIC', m1.AIC, 'numbins', 50)

%now fit the other simple model to check if the mix is needed
m2 = MixedDistribution('Exponential');
m2 = m2.fit(data, 'Replicates', 20);
subplot(3, 1, 3);
plotdist(m2, 'xlim', [0 1000], 'AIC', m2.AIC, 'numbins', 50)
fprintf('AIC = %g\n', m2.AIC)
fprintf('BIC = %g\n', m2.BIC)
xlabel('Longevity')
ylabel('Density');
%plot the AIC's
figure
plot([1, 2, 3], [m.AIC, m1.AIC, m2.AIC], '-o')
xlabel('model');
ylabel('AIC');


%tests of the method with generated data. In test_fit first data is
%generated and then the distributions is tried to fit back to the generated
%data.
pd = MixedDistribution({'Normal', 'Normal', 'Normal'}, [0.5, 0.2, 0.3], {[3 1], [5 2], [-1 1]});
% test_fit is a test of the method:
% first creates a random data set based on the mixed distribution
% then it is tried to fit pd back from the generated data
pd.test_fit();

%you can write {'Poisson','Poisson','Poisson'} as {'Poisson', 3}
pd = MixedDistribution({'Poisson', 3}, [0.4 0.2 0.4], {1, 5, 10});
%by default the ranges of parameters to choose from are determined using
%kmeans. You can change this behaviour using the initfun option
pd.test_fit('initfun', 'quantiles');

%a single distibution can be entered in a simpler way
pd = MixedDistribution('LogNormal', [5 1]);
pd.test_fit()

pd = MixedDistribution({'LogNormal', 'Normal'}, [0.5, 0.5], {[3, 0.4], [30 5]});
pd.test_fit()

%you write {'LogNormal','LogNormal','Normal','Normal'} as {'LogNormal',2;'Normal',2}
pd = MixedDistribution({'LogNormal', 2; 'Normal', 2}, [0.3, 0.2, 0.2, 0.3], {[3, 0.1], [2, 0.2], [25 1], [40, 10]});
pd.test_fit()

pd = MixedDistribution({'Normal', 5}, [0.2, 0.2, 0.3, 0.2, 0.1], {[3 1], [5 0.7], [-1 1], [8 1], [9 0.1]});
pd.test_fit()

pd = MixedDistribution('Normal');
%truncation is supported for single distributions
pd.Truncation = [0, inf];
pd.test_fit();


pd = MixedDistribution({'normal', 3}, [0.5, 0.2, 0.3], {[3 2], [4 1], [0, 4]});
%truncation is supported also for multiple distributions
pd.Truncation = [0, inf];
pd.test_fit();

%you can also fit all combinations with a number of components
pd = MixedDistribution({'LogNormal', 'Normal'}, [0.5, 0.5], {[3, 0.4], [30 5]});
%other possibility to have normal only:
%pd = MixedDistribution({'Normal',2}, [0.5, 0.5], {[20 3], [30 5]});
data = pd.random(3000, 1);
%k=1:3 means that all 9 combinations LogNormal and 
%Normal distributions of 1 to 3 components are fitted.
res = pd.fit_steps(data, 'k', 1:3, 'replicates', 5);
figure
plot(res.k, res.AIC, 'ro');
xlabel('num components')
ylabel('AIC');
disp('Best fit:')
minAIC = min(res.AIC);
disp(res.pd{res.AIC == minAIC})
fprintf('AIC=%g\n', minAIC);
owndistr = {'gompertz', 'gompertzmakeham' , 'linear', 'linearexponential',  ...
    'saturating', 'saturatingexponential', 'skewnormal'};

disp('gompertz');
m = MixedDistribution({'gompertz'});
m.Components{1}.d.alpha = 0.0001;
m.test_fit();
ch = get(gcf, 'children');
title(ch(2), 'gompertz')

disp('gompertzmakeham');
m = MixedDistribution('gompertzmakeham', [0.001, 0.05, 0.01]);
m.test_fit();
ch = get(gcf, 'children');
title(ch(2), 'gompertzmakeham')
disp('skewnormal');
m = MixedDistribution({'skewnormal', 'normal'}, [0.6 0.4], {[-1 4 -20], [-10, 1]});

%m = MixedDistribution({'skewnormal',3},[0.4,0.3,0.3],{[1,3,10],[-1 2 -4],[10,2,0]});
m.test_fit();
ch = get(gcf, 'children');
title(ch(2), 'skewnormal')
alldist = fitdist;
allnames = cell(size(alldist));
for i = 1:length(alldist)
    if ~strcmpi(alldist{i}, {'kernel', 'binomial'})
        disp(alldist{i});
        m = MixedDistribution({alldist{i}, 1});
        info = shared.basic_info(alldist{i});
        m = m.randompars;
        x = m.random(1000, 1);
        if any(isnan(x))
            error('**nan in generated data')
        else
            if any(x < info.support(1))
                error('generated data < support')
            end
        
            if any(x > info.support(2))
                error('generated data < support')
            end
        end
        figure
        plotdist(m, 'data', x)
    end
    try
        if ~any(strcmpi(alldist{i}, {'binomial', 'kernel', 'stable'}))
            m.test_fit('replicates', 1, 'initial', m);
        end
    catch err
        disp(err.message)
    end
end


% 
%two distributions with different domain of support
m = MixedDistribution({'normal', 'lognormal'}, [0.6 0.4], {[-1 1], [0.1, 0.2]});
m.test_fit('initfun', 'quantiles');
%end
%test the use of censored data and frequencies
x1 = [0.1 0.2 1, 10, 3 2 4 5]';
cens1 = [0 0 0 0 0 0 0 1]';
freq1 = ones(size(cens1));
freq1(1) = 2;
%pd1=fitdist(x,'Exponential','censoring',cens);

distrbs = {'Normal', 'Exponential', 'Linear', 'LinearExponential', 'Saturating', 'SaturatingExponential', 'Gompertz', 'GompertzMakeham'};


%remove random initial conditions to run the asserts
for i = 1:length(distrbs)
    x = x1;
    cens = cens1;
    freq = freq1;
    distrib = distrbs{i};
    m = MixedDistribution(distrib)
    init = m.ParameterValues;
    m = m.fit(x, 'censoring', cens, 'frequency', freq, 'replicates', 1, 'initial', init);
    pd2 = fitdist(x, distrib, 'censoring', cens, 'frequency', freq, 'options', struct('start', init));
    assert(abs(m.negloglik - pd2.negloglik) < 1E-5);
    assert(pd2.negloglik == pd2.likefunc(pd2.ParameterValues, x, cens, freq))
    freq(1) = 1;
    pd_freq = fitdist(x, distrib, 'censoring', cens, 'frequency', freq, 'options', struct('start', init));
    m = m.fit(x, 'censoring', cens, 'frequency', freq, 'replicates', 1, 'initial', init);
    assert(abs(m.negloglik - pd_freq.negloglik) < 1E-5);
    assert(any(pd2.ParameterValues ~= pd_freq.ParameterValues), 'Frequency has no effect');
    freq(1) = 2;
    cens(end) = false;
    pd3 = fitdist(x, distrib, 'censoring', cens, 'frequency', freq, 'options', struct('start', init));
    assert(any(pd2.ParameterValues ~= pd3.ParameterValues), 'Censoring has no effect');
    x = [0.1; x1];
    freq(1) = 1;
    freq = [1; freq];
    cens = [false; cens1];
    pd_freq2 = fitdist(x, distrib, 'censoring', cens, 'frequency', freq, 'options', struct('start', init));
    assert(all(abs(pd2.ParameterValues - pd_freq2.ParameterValues) < 1E-5), 'Frequency does not work correctly')

    fprintf('Censoring in %s seems ok\n', distrib);
end

for i = 1:length(distrbs) - 1
    disp(i)
    m = MixedDistribution(distrbs(i:i + 1));
    m.test_fit('fract_cens', 0.4, 'maxfreq', 2);
    m.test_fit('discretize', 30);
    fprintf('Censoring works %s (cannot evaluate it)\n', m.mixedname(m));
end

m = MixedDistribution('Exponential');
m.Components{1}.d.mu = 0.05;
data = m.random(1000, 1);
m = m.fit(data);
m1 = m.bootstrap;
meanboot = m1.bootCL('mean', 'alpha', 1);
abs(meanboot.CL(1) - m.mean)
assert(abs(meanboot.CL(1) - m.mean) < 1E-2, 'Bootstrapped mean is too different from the original distribution')
cens = rand(size(data)) < 0.1;
m = m.fit(data, 'censoring', cens);
m1 = m.bootstrap;
abs(meanboot.CL(1) - m.mean)
assert(abs(meanboot.CL(1) - m.mean) < 1E-2, 'Bootstrapped mean is too different from the original distribution')

drawnow;
ch = get(0, 'children');
for i = 1:length(ch)
    figure(ch(i));
end

