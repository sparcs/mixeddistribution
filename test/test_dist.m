function [pd, pd1] = test_dist(distname)
    addpath('..')
    if nargin == 0
        distname = makedist;
    end
    if iscell(distname)
        pd = cell(size(distname));
        for i = 1:numel(distname)
            pd{i} = test_dist(distname{i});
        end
        return;
    end
    fprintf('testing %s:\n', distname);
    pd = [];
    if ~any(ismember(distname, {'multinomial', 'piecewiselinear', 'triangular', 'flexhazard', 'siler','loguniform'}))
        pd = SingleDist(distname);
        infos = shared.basic_info;
        assert(any(strcmp({infos.code}, distname)), 'include information in shared.basic_info')
        npar = pd.NumParameters;

        info = pd.d.getInfo;
        if ~any(strcmp(distname, {'epsilonskewnormal'})) %some build-in distributions do not have this
            pd1 = pd.randompars;
            if strcmp(distname, 'uniform')
                %to prevent an error that the lower is higher than the
                %upper
                pd.ParameterValues = pd1.ParameterValues + 1;
            end
            info2 = shared.basic_info(distname);
            for i = 1:numel(info.pnames)
                if (isempty(info2.pinteger) || ~info2.pinteger(i))&&~any(strcmp(distname, {'uniform','boundedpareto'}))
                    r = rand(1);
                    pd2 = pd.set(info.pnames{i}, r, true);
                    assert(any(~pd2.ParameterMask) && any(pd2.d.ParameterValues == r), 'Parameter "%s" cannot set fixed', info.pnames{i});
                    pd.d.(info.pnames{i}) = pd1.ParameterValues(i);
                    assert(pd.d.(info.pnames{i}) == pd1.ParameterValues(i), 'Parameter cannot set as property');
                end
            end
        end
        assert(numel(info.pnames) == npar, 'Info.pnames not correct')
        assert(numel(info.pdescription) == npar, 'Info.pdescription not correct')
        assert(numel(info.prequired) == npar, 'Info.prequired not correct')
        assert(numel(info.logci) == npar, 'Info.logci not correct')
        assert(size(info.plim, 2) == npar, 'Info.plim not correct')
        assert(any(info.plim(1, :) < info.plim(2, :)), 'Info.plim not correct')
 
        info2 = pd.getInfo;
 
        assert(numel(info2.pnames) == npar, 'Info2.pnames not correct')
        assert(size(info2.plim, 2) == npar, 'Info2.plim not correct')
        assert(any(info2.plim(1, :) < info2.plim(2, :)), 'Info2.plim not correct')
        f = fieldnames(info2);
        for k = 1:length(f)
            if strcmp(f{k}, 'code') && ~strcmp(distname, 'uniform')
                pval = info2.plim(1, :);
                ndx = isinf(pval);
                pval(ndx) = sign(pval(ndx)) .* 999;
                pval = num2cell(pval);
                makedist(info2.code, pval{:});
                pval = info2.plim(2, :);
                ndx = isinf(pval);
                pval(ndx) = sign(pval(ndx)) .* 999;
                pval = num2cell(pval);
                makedist(info2.code, pval{:});
            elseif ~strcmp(f{k}, 'plim')
                if isfield(info, f{k}) && ~isequal(info2.(f{k}), info.(f{k}))
                    fprintf('%s %s not correct\n', f{k}, pd.DistributionName);
                end
            end
        end
  
        %allow for 10 iterations with errors because random parameters might give
        %errors in mean

        %         iter = 0;
        %         success = false;
        %         while iter < 10 && ~success
        %             try
        if strcmp(distname, 'lognormal')
            pd = pd.randompars(10, [0.4 0.05]);
        elseif strcmp(distname, 'taperedpareto')
            pd = pd.randompars(10, [1.2 1 0.1]);
        else
            pd = pd.randompars;
        end
        %                 pd.d.alpha=1+rand(1);
        pd
        mean_dist = pd.mean;
        assert(pd.iqr == pd.icdf(0.75) - pd.icdf(0.25), 'Inter quartile range is not correct')
        if info.iscontinuous
            ps = 0:0.01:1;
            ps(1) = 0.0001;
            ps(end) = 1 - 0.0001;
            for p = ps
                %p
                %pd.icdf(p)
                %pd.cdf(pd.icdf(p))-p
                if ~isinf(pd.icdf(p))

                    try
                        assert(abs(pd.cdf(pd.icdf(p)) - p) < 1E-4, 'error in inverse cdf');
                    catch err
                        p
                        Pinv = pd.icdf(p)
                        P1 = pd.cdf(pd.icdf(p))
                        rethrow(err)
                    end
                end
            end

            ran(1) = pd.icdf(0.1);
            ran(2) = pd.icdf(0.9);
            xx = linspace(ran(1), ran(2), 100000);
            %check if the pdf integrates (more or less) to 1
            %   assert(abs(trapz(xx, pd.pdf(xx)) - 0.8) < 1E-3, 'error in pdf or cdf');
            %check if the cdf integrates (more or less) to 1



            assert(pd.icdf(0.5) == pd.d.median, 'median not correct')

            data = pd.random(2000, 1);
            if any(isnan(data))
                error('**nan in generated data')
            else
                if any(data < info.support(1))
                    error('generated data < support')
                end

                if any(data > info.support(2))
                    error('generated data < support')
                end
            end
            mean_data = mean(data);

            fprintf('mean data = %g\nmean distr = %g\n', mean_data, mean_dist)
            var_data = var(data);
            var_dist = pd.var;
            if ~isinf(var_dist)
                fprintf('var data = %g\nvar distr = %g\n', var_data, var_dist)
                if abs(var_data - var_dist) > abs(var_data * 0.1)
                    warning('error in variance: variance of random sample does not match variance of distribution')
                end
            else
                fprintf('The distrubution has no valid variance: %g\n', var_dist)
            end
        else
            data = pd.random(2000, 1);
        end
        fprintf('max data = %g\n',max(data))
        %                 success = true;
        %             catch e
        %                 fprintf('had error on iter %d\n%s\n%s', iter, e.message, e.identifier)
        %                 iter = iter + 1;
        %             end
        %         end

        switch distname
            case 'binomial'
                pd1 = fitdist(data, distname, 'ntrials', pd.d.N)
            case 'halfnormal'
                pd1 = fitdist(data, distname, 'mu', pd.d.mu)
            case 'uniform'
                disp('this distribution is not fittable')
            otherwise
                pd1 = fitdist(data, distname)
        end
        fprintf('distr ''%s'' is ok\n', distname)

    end
end
