addpath('..')

rerun = true;
nboot = 0;
nbins = 20;
ndata = 100;
alpha = 2.5;
logbins = true;
if ~rerun && ~exist('pd1', 'var')
    rerun = true;
end
if rerun
    b = rand(1) * 50;
    perc = rand(1) * 0.5;
    %choose pareto or taperedpareto or 'boundedpareto'
    distkind = 'pareto';
   % distkind = 'boundedpareto';
   % distkind = 'pareto';
    if strcmp(distkind, 'pareto')
        pd = MixedDistribution({'uniform', distkind}, [perc 1 - perc], 'Lower', 0, 'Upper', b, 'alpha', alpha, 'xmin', b);
    elseif strcmp(distkind, 'taperedpareto')
        pd = MixedDistribution({'uniform', distkind}, [perc 1 - perc], 'Lower', 0, 'Upper', b, 'alpha', alpha, 'xmin', b, 'lambda', 0.01);
    elseif strcmp(distkind, 'boundedpareto')
        pd = MixedDistribution({'uniform', distkind}, [perc 1 - perc], 'Lower', 0, 'Upper', b, 'alpha', alpha, 'xmin', b, 'xmax', b+100);
        pd= pd.set('xmax',pd.Components{2}.icdf(0.9+rand(1)*0.05));
    end
    %generate data set using this mixture of two distributions
    %plotdist(pd,'type','pdf')

    data = pd.random(ndata, 1);
    pdtmp = MixedDistribution(distkind);
    pd1 = pdtmp.fit(data, 'silent', true);
    udata = unique(data);
    pd2 = pdtmp.fitfixed(data, 'fixedpar', 'xmin', 'lowerpar', true, 'parrange', udata(udata<median(data)));
end

fprintf('real xmin= %g,   plfit/MixedDistr = %g, fitfixed =%g\n', pd.Components{2}.d.xmin, pd1.Components{1}.d.xmin, pd2.Components{1}.d.xmin);
figure
subplot(3, 2, 1)
plotdist([], 'type', 'powerlaw', 'data', data, 'title', sprintf('alldata, real xmin=%g', pd.Components{2}.d.xmin))
xlim1 = xlim(gca);
subplot(3, 2, 5)
plotdist(pd1, 'title', sprintf('MixedDistr.fit(), xmin=%g', pd1.ParameterValues(2)))
xlim(xlim1)
subplot(3, 2, 3)
if ~strcmp(distkind, 'boundedpareto')
plotdist(pd2, 'title', sprintf('my fit (fixedpar), xmin=%g', pd2.ParameterValues(2)))
xlim(xlim1)
end


%create binned data (20 bins) based on the same data as above
%freq are the counts of 20 bins:
if rerun
    if logbins
        [freq, edges] = histcounts(log10(data), nbins);
        edges = 10.^edges;
    else
        [freq, edges] = histcounts(data, nbins);
        edges(edges < min(data)) = min(data);
    end
    %mindata is the lower edge
    mindata = edges(1:end - 1);
    %maxdata is the higher edge;
    maxdata = edges(2:end);


    pdtmp = MixedDistribution(distkind);
    if strcmp(distkind, 'pareto')
      [F1,x1]=shared.my_ecdf([mindata',maxdata'],'frequency',freq);
      med = x1(find(F1(:,1) >= 0.5, 1) + 1);
      [alpha1, xmin1] = bplfit(freq, edges,'limit',med);
      pd4 = makedist(distkind, 'alpha', alpha1, 'xmin', xmin1);
    elseif strcmp(distkind, 'taperedpareto')
        [alpha1, lambda] = bplcutfit(freq, edges, 'bmin', pd.Components{2}.d.xmin);
        pd4 = makedist(distkind, 'alpha', alpha1, 'xmin', pd.Components{2}.d.xmin, 'lambda', lambda);
    else
        [alpha1, xmin1] = bplfit(freq, edges);
        pd4 = makedist('pareto', 'alpha', alpha1, 'xmin', xmin1);
    end
    pd3 = pdtmp.fit(mindata, 'censoring', maxdata, 'frequency', freq, 'silent', true);
end

subplot(3, 2, 2)
plotdist([], 'type', 'powerlaw', 'title', sprintf('binned data (%d logarithmic bins)', numel(mindata)), 'data', mindata, 'censoring', maxdata, 'frequency', freq);
xlim2 = xlim(gca);

fprintf('real xmin= %g,   bplfit = %g, MixedDistr binned =%g\n', pd.Components{2}.d.xmin, pd4.xmin, pd3.Components{1}.d.xmin);


f = find(mindata >= pd3.ParameterValues(2), 1);
%[mindata(f-1) mindata(f) maxdata(f)]
subplot(3, 2, 4)
%b
plotdist(pd3, 'title', sprintf('my fit, xmin=%g', pd3.ParameterValues(2)));
xlim(xlim2)
% plotdist([], 'type', 'powerlaw', 'data', data)
subplot(3, 2, 6)
if strcmp(distkind, 'pareto')
    plotdist(pd4, 'title', sprintf('bplfit, xmin=%g', pd4.ParameterValues(2)), 'data', mindata, 'censoring', maxdata, 'frequency', freq);
elseif strcmp(distkind, 'taperedpareto')
    plotdist(pd4, 'title', 'bplcutfit, cannot fit xmin', 'data', mindata, 'censoring', maxdata, 'frequency', freq);
end
xlim(xlim2)
% end
figure
subplot(2, 2, 3)
plotdist(pd1, 'title', sprintf('plfit/MixedDistr, xmin=%g', pd1.ParameterValues(2)), 'scaletodata', true)
xlim(xlim1)
subplot(2, 2, 1)
if ~strcmp(distkind, 'boundedpareto')
plotdist(pd2, 'title', sprintf('my fit (fixedpar), xmin=%g', pd2.ParameterValues(2)), 'scaletodata', true)
xlim(xlim1)
end
subplot(2, 2, 4)
if strcmp(distkind, 'pareto')
    plotdist(pd4, 'title', sprintf('bplfit, xmin=%g', pd4.ParameterValues(2)), 'data', mindata, 'censoring', maxdata, 'frequency', freq, 'scaletodata', true);
else
    plotdist(pd4, 'title', 'bplcutfit, cannot fit xmin', 'data', mindata, 'censoring', maxdata, 'frequency', freq, 'scaletodata', true);
end
subplot(2, 2, 2)
%b
if ~strcmp(distkind, 'boundedpareto')
plotdist(pd3, 'title', sprintf('my fit, xmin=%g', pd3.ParameterValues(2)), 'scaletodata', true);
end
xlim(xlim2)
sgtitle(sprintf('Scaled to data, real xmin=%g', pd.Components{2}.d.xmin));
if nboot>0
    if rerun
        %pd1_boot = pd1.bootstrap('resample_data', shared.data_outside_range([pd1.get('xmin'), inf], pd1.InputData), 'nboot', nboot); %ony works after fit has run
        pd1_boot = pd1.bootstrap('method','resampling', 'nboot', nboot,'replicates',1); %ony works after fit has run
        pd2_boot = pd2;
        %pd3_boot = pd3.bootstrap('resample_data', shared.data_outside_range([pd3.get('xmin'), inf], pd3.InputData), 'nboot', nboot);
        pd3_boot = pd3.bootstrap('method','resampling', 'nboot', nboot,'replicates',1); %ony works after fit has run
        pd4_boot = pd4;
    end

    figure
    subplot(2, 2, 3)
    plotdist(pd1_boot, 'title', sprintf('plfit/MixedDistr, xmin=%g', pd1_boot.ParameterValues(2)), 'scaletodata', true)
    xminvals = quantile(pd1_boot.BootstrapData.parvalues(:, 2), [0.05 0.5 0.95]);
    xline(xminvals(1), 'k:', 'linewidth', 0.5)
    xline(xminvals(2), 'k-', 'linewidth', 1)
    xline(xminvals(3), 'k:', 'linewidth', 0.5)
    xlim(xlim1)
    subplot(2, 2, 1)
    plotdist(pd2_boot, 'title', sprintf('my fit (fixedpar), xmin=%g', pd2_boot.ParameterValues(2)), 'scaletodata', true)
    xlim(xlim1)
    subplot(2, 2, 4)
    if strcmp(distkind, 'pareto')
        plotdist(pd4_boot, 'title', sprintf('bplfit, xmin=%g', pd4_boot.ParameterValues(2)), 'data', mindata, 'censoring', maxdata, 'frequency', freq, 'scaletodata', true);
    else
        plotdist(pd4_boot, 'title', 'bplcutfit, cannot fit xmin', 'data', mindata, 'censoring', maxdata, 'frequency', freq, 'scaletodata', true);
    end
    subplot(2, 2, 2);
    plotdist(pd3_boot, 'title', sprintf('my fit, xmin=%g', pd3_boot.ParameterValues(2)), 'scaletodata', true);
    xminvals = quantile(pd3_boot.BootstrapData.parvalues(:, 2), [0.05 0.5 0.95]);
    xline(xminvals(1), 'k:', 'linewidth', 0.5)
    xline(xminvals(2), 'k-', 'linewidth', 1)
    xline(xminvals(3), 'k:', 'linewidth', 0.5)
    xlim(xlim2)
    sgtitle(sprintf('Scaled to data (+bootstrap), real xmin=%g', pd.Components{2}.d.xmin));
end

